var fs = require("fs");
var request = require("request");
var _ = require("lodash");
var url = require("url")
var beautify = require("js-beautify");
var timer;
var child_process = require("child_process");
var formatConfig = {
    "indent_with_tabs": false,
    "max_preserve_newlines": 4,
    "preserve_newlines": true,
    "space_in_paren": false,
    "jslint_happy": false,
    "brace_style": "collapse",
    "keep_array_indentation": false,
    "keep_function_indentation": false,
    "eval_code": false,
    "unescape_strings": false,
    "break_chained_methods": false,
    "e4x": false,
    "wrap_line_length": 0,
    "space_after_anon_function": false
}

function getpage() {
    request({
        url: "https://live.bilibili.com/3",
        headers: {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8",
            "Pragma": "no-cache",
            "Upgrade-Insecure-Requests": 1,
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
        }
    }, (e, r, b) => {
        if (e) {
            errorlog(e);
            clearInterval(timer);
            console.log('statusCode:', r && r.statusCode);
            console.log('body:', b);
        } else {
            try {
                b = b.toString()
                try {
                    var changedate = b.match(/(?<=((Build: )|(Build time: ))).*?(?=\n)/)[0].replace(/:/g, ".");
                } catch (e) {
                    console.log(b);
                    return;
                }
                console.log((new Date()).toLocaleString() + " : " + changedate)
                var dirs = fs.readdirSync(".");
                for (var i = 0; i < dirs.length; i++) {
                    if (dirs[i] == changedate) return;
                }
                fs.writeFileSync("latest/room.html", b);
                fs.mkdirSync(changedate);

                fs.writeFileSync(changedate + "/room.html", b);
                var todownload = JSON.parse(b.match(/"static\/js\/"\+[a-zA-Z0-9_]\+"\."\+(\{([0-9a-zA-Z]+?:"[0-9a-zA-Z]+",?)+\})\[[a-zA-Z0-9_]\]\+"\.js"/)[1].replace(/([0-9a-zA-Z]+):/g, '"$1":'))
                _.each(todownload, (v, k) => {
                    request({
                        url: "https://s1.hdslb.com/bfs/static/blive/blfe-live-room/static/js/" + k + "." + v + ".js",
                        headers: {
                            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                            "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8",
                            "Pragma": "no-cache",
                            "Upgrade-Insecure-Requests": 1,
                            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
                        }
                    }).pipe(fs.createWriteStream(changedate + "/" + k + "." + v + ".js"));
                })
                var jss = b.match(/(?<=\<script src=\/\/)([\w\W]*?)\.js(?=(.*?)(\>\<\/script\>))/g);
                for (i = 0; i < jss.length; i++) {
                    var file = filename(jss[i]);
                    request("https://" + jss[i]).pipe(fs.createWriteStream(changedate + "/" + file));
                }
                setTimeout(function() {
                    pretyandcopy(changedate)
                }, 20000);
            } catch (e) {
                errorlog(e);
                clearInterval(timer);
            }
        }
    })
}

function errorlog(error) {
    console.log("[" + (new Date()).toLocaleString() + "] Happened an Error");
    console.log(error);
}

function filename(link) {
    return url.parse("https://" + link).pathname.match(/(.*)\/(.*)/)[2];
}

function commitAndPush(date) {
    console.log(child_process.execSync("git add .").toString());
    console.log(child_process.execSync("git commit -m \"" + date + "\"").toString());
    console.log(child_process.execSync("git push").toString());
}

function pretyandcopy(date) {
    var dir = date;
    var filelist = fs.readdirSync(dir);
    child_process.execSync("del latest\\* /f /s /q")
    for (var i = 0; i < filelist.length; i++) {
        try {
            switch (filelist[i].match(/.*\.(.*)/)[1]) {
                case "html":
                    fs.writeFileSync("latest/" + filelist[i], beautify.html(fs.readFileSync(dir + "/" + filelist[i], "utf8"), formatConfig))
                    break;
                case "js":
                    fs.writeFileSync("latest/" + filelist[i], beautify.js(fs.readFileSync(dir + "/" + filelist[i], "utf8"), formatConfig))
                    break;
                default:
                    fs.writeFileSync("latest/" + filelist[i], fs.readFileSync(dir + "/" + filelist[i]))
            }
        } catch (e) {
            console.log(e)
        }
		if(global.gc){
			global.gc()
		}
    }
    commitAndPush(dir);
}
getpage();
timer = setInterval(getpage, 5 * 60 * 1000)