(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [3001], {
        3001: function(t, e, n) {
            "use strict";
            n.r(e), n.d(e, {
                default: function() {
                    return h
                }
            });
            var r = n(26849),
                o = n(66570),
                i = n(34074),
                s = n.n(i),
                a = n(30381),
                c = n(70538),
                l = n(95334),
                u = function(t, e, n, r) {
                    var o, i = arguments.length,
                        a = i < 3 ? e : null === r ? r = s()(e, n) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(t, e, n, r);
                    else
                        for (var c = t.length - 1; c >= 0; c--)(o = t[c]) && (a = (i < 3 ? o(a) : i > 3 ? o(e, n, a) : o(e, n)) || a);
                    return i > 3 && a && Object.defineProperty(e, n, a), a
                },
                f = function(t) {
                    function e() {
                        var e;
                        return (e = t.apply(this, arguments) || this).isShowReason = !0, e
                    }
                    return (0, o.Z)(e, t), (0, r.Z)(e, [{
                        key: "blockTiming",
                        get: function() {
                            var t = this.$store.getters.appStatus.blockingTime || 0;
                            if (0 !== t && -2 !== t || (this.isShowReason = !1), -1 === t) return "永久封禁";
                            var e = 13 - t.toString().length;
                            return e > 0 && (t *= Math.pow(10, e)), t ? "至 " + a(t).format("YYYY-MM-DD HH:mm:ss") : "--"
                        }
                    }]), e
                }(c.default),
                p = f = u([l.Component], f),
                h = (0, n(51900).Z)(p, (function() {
                    var t = this,
                        e = t.$createElement,
                        n = t._self._c || e;
                    return n("div", {
                        staticClass: "room-blocked t-center"
                    }, [n("div", {
                        staticClass: "header-img m-auto",
                        attrs: {
                            role: "img"
                        }
                    }), n("div", {
                        staticClass: "supporting-text"
                    }, [n("p", [n("span", [t._v("这个房间已经被封禁")]), t.isShowReason ? n("span", [t._v("（" + t._s(t.blockTiming) + "）！(╯°口°)╯(┴—┴")]) : t._e()])])])
                }), [], !1, null, "c56c8142", null).exports
        }
    }
]);