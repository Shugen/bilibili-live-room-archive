(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [8158], {
        28158: function(t, i, e) {
            "use strict";
            e.r(i), e.d(i, {
                default: function() {
                    return G
                }
            });
            var n, r, s = e(26849),
                o = e(66570),
                a = e(63109),
                l = e.n(a),
                c = e(34074),
                u = e.n(c),
                d = e(93476),
                f = e.n(d),
                g = e(70538),
                h = e(95334),
                p = e(17388),
                v = e(57072),
                m = e(82159),
                b = e(66856),
                k = e(55716),
                y = e(23933),
                I = e(40498),
                C = e(55391),
                x = e(2708),
                w = function(t, i, e, n) {
                    var r, s = arguments.length,
                        o = s < 3 ? i : null === n ? n = u()(i, e) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) o = Reflect.decorate(t, i, e, n);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(r = t[a]) && (o = (s < 3 ? r(o) : s > 3 ? r(i, e, o) : r(i, e)) || o);
                    return s > 3 && o && Object.defineProperty(i, e, o), o
                },
                P = function(t, i, e, n) {
                    return new(e || (e = f()))((function(r, s) {
                        function o(t) {
                            try {
                                l(n.next(t))
                            } catch (t) {
                                s(t)
                            }
                        }

                        function a(t) {
                            try {
                                l(n.throw(t))
                            } catch (t) {
                                s(t)
                            }
                        }

                        function l(t) {
                            var i;
                            t.done ? r(t.value) : (i = t.value, i instanceof e ? i : new e((function(t) {
                                t(i)
                            }))).then(o, a)
                        }
                        l((n = n.apply(t, i || [])).next())
                    }))
                };
            ! function(t) {
                t.gold = "gold", t.silver = "silver"
            }(n || (n = {})),
            function(t) {
                t.gold = "goldSeedStore", t.silver = "silverSeedStore"
            }(r || (r = {}));
            var L = function(t) {
                function i() {
                    var i;
                    return (i = t.apply(this, arguments) || this).giftInfo = null, i.isLoading = !1, i.showMsg = "赠送礼物即可加入粉丝团", i.currency = b.webCurrency, i.userCount = 1, i.isFailed = !1, i.loginStatus = x.loginStatus, i
                }(0, o.Z)(i, t);
                var e = i.prototype;
                return e.getFirstGift = function(t) {
                    var i = (null == t ? void 0 : t.giftList) || [];
                    return null == i ? void 0 : i[0]
                }, e.onClickSend = function() {
                    var t, i, e, s, o;
                    return P(this, void 0, void 0, l().mark((function a() {
                        var c, u, d, f, g, h, p, b, y, x, w, P, L, M;
                        return l().wrap((function(a) {
                            for (;;) switch (a.prev = a.next) {
                                case 0:
                                    if (!this.isLoading) {
                                        a.next = 2;
                                        break
                                    }
                                    return a.abrupt("return");
                                case 2:
                                    if (c = this.$refs.submitBtn.$el, this.currentGiftId) {
                                        a.next = 6;
                                        break
                                    }
                                    return this.linkMsg(c, "未取到礼物id " + k.randomEmoji.sad(), "caution"), a.abrupt("return");
                                case 6:
                                    if (this.isLoading = !0, a.prev = 7, this.isLogin) {
                                        a.next = 12;
                                        break
                                    }
                                    return this.close(), a.next = 12, (0, m.quickLogin)();
                                case 12:
                                    a.next = 20;
                                    break;
                                case 14:
                                    return a.prev = 14, a.t0 = a.catch(7), this.isLoading = !1, (u = a.t0.message) && this.linkMsg(c, u + " " + k.randomEmoji.sad(), "error"), a.abrupt("return");
                                case 20:
                                    if (d = null === (t = this.giftInfo) || void 0 === t ? void 0 : t.giftNum, !(this.userCount > d)) {
                                        a.next = 24;
                                        break
                                    }
                                    return this.isLoading = !1, a.abrupt("return", this.linkMsg(c, "您目前只拥有 " + d + " 个" + (null === (i = this.giftInfo) || void 0 === i ? void 0 : i.label) + "喔 " + k.randomEmoji.sad()));
                                case 24:
                                    return a.next = 26, (0, C.hF)({
                                        giftID: this.currentGiftId,
                                        count: this.userCount,
                                        coinType: this.goldPrice ? n.gold : n.silver,
                                        discountPrice: this.goldPrice ? this.goldPrice : (null === (e = this.giftInfo) || void 0 === e ? void 0 : e.silverPrice) || (null === (s = this.giftInfo) || void 0 === s ? void 0 : s.discountPrice),
                                        bizCode: "Live"
                                    });
                                case 26:
                                    if (f = a.sent, g = f.code, h = f.data, p = f.error, b = f.errorMsg, y = f.errorType, this.isLoading = !1, 200013 !== g) {
                                        a.next = 43;
                                        break
                                    }
                                    if (x = 0, "webFullScreen" !== this.displayMode) {
                                        a.next = 38;
                                        break
                                    }
                                    return a.next = 38, (0, I.j)();
                                case 38:
                                    if (h && (x = (w = h).needCoin - w.leftCoin), !this.isFailed) {
                                        a.next = 41;
                                        break
                                    }
                                    return a.abrupt("return", this.linkMsg(c, "礼物对象为空，无法充值, 请重试 " + k.randomEmoji.sad(), "error"));
                                case 41:
                                    return this.goCharge(this.goldPrice ? r.gold : r.silver, x, this.giftInfo, 1), a.abrupt("return", this.close());
                                case 43:
                                    if (!p) {
                                        a.next = 46;
                                        break
                                    }
                                    return this.linkMsg(c, b, y), a.abrupt("return", this.close());
                                case 46:
                                    P = this.getFirstGift(h), null != (L = null === (o = null == P ? void 0 : P.packageItem) || void 0 === o ? void 0 : o.packageItemRemain) && (this.giftInfo.giftNum = L, v.default.$emit("GiftSenderPanel: updateGiftData", this.giftInfo)), h && 0 === g && (M = h.sendTips) && this.linkMsg(c, M + " " + k.randomEmoji.happy(), "success"), this.close();
                                case 51:
                                case "end":
                                    return a.stop()
                            }
                        }), a, this, [
                            [7, 14]
                        ])
                    })))
                }, e.goCharge = function(t, i, e, n) {
                    var s, o = (null === (s = null == e ? void 0 : e.giftImg) || void 0 === s ? void 0 : s.png) || "",
                        a = (null == e ? void 0 : e.label) || "";
                    this.ShowRechargeTip({
                        type: t,
                        rechargeAmount: i,
                        openFrom: t === r.gold ? "goldGift" : "silverGift",
                        action: {
                            type: "Gift",
                            tipMsg: "赠送该礼物",
                            html: '\n                    <div>\n                        <img src="' + o + '" style="width:40px;height：40px"></img>\n                        <p>' + a + "*" + n + "</p>\n                    </div>\n                    "
                        },
                        contextType: 1,
                        contextId: y.e.getRoomId() + ""
                    })
                }, e.mounted = function() {
                    var t, i, e;
                    this.currentGiftId = (null === (i = null === (t = this.optionData) || void 0 === t ? void 0 : t[0]) || void 0 === i ? void 0 : i.giftId) || null, this.giftInfo = y.e.getGiftById(this.currentGiftId), this.isFailed = null == this.currentGiftId || !this.giftInfo, this.showMsg = this.isFailed ? "未找到匹配的礼物，请重试" : "赠送" + ((null === (e = this.giftInfo) || void 0 === e ? void 0 : e.label) || "礼物") + "即可加入粉丝团"
                }, e.clickCancel = function() {
                    this.close()
                }, (0, s.Z)(i, [{
                    key: "isLogin",
                    get: function() {
                        return this.loginStatus.isLogin
                    }
                }, {
                    key: "goldPrice",
                    get: function() {
                        var t;
                        return null === (t = this.giftInfo) || void 0 === t ? void 0 : t.goldPrice
                    }
                }, {
                    key: "displayMode",
                    get: function() {
                        return this.$store.getters.appStatus.displayMode
                    }
                }]), i
            }(g.default);
            w([(0, h.Prop)({
                type: Function
            })], L.prototype, "close", void 0), w([(0, h.Prop)({
                type: Array
            })], L.prototype, "optionData", void 0);
            var M = L = w([(0, h.Component)({
                    components: {
                        LinkButton: p.default
                    },
                    filters: {
                        formatNumWithUnit: function(t) {
                            return (0, b.formatNumWithUnit)(t)
                        }
                    }
                })], L),
                F = (0, e(51900).Z)(M, (function() {
                    var t = this,
                        i = t.$createElement,
                        e = t._self._c || i;
                    return e("div", {
                        staticClass: "popjoin-fansgroup-wrap p-relative"
                    }, [e("div", {
                        staticClass: "popjoin-fansgroup-content"
                    }, [e("section", {
                        staticClass: "center center-content-part"
                    }, [t.isFailed ? e("img", {
                        staticClass: "fail-img",
                        attrs: {
                            width: 100,
                            height: 100,
                            src: "//i0.hdslb.com/bfs/activity-plat/static/20210621/1d0c5a1b042efb59f46d4ba1286c6727/41aZmZuy9x.png"
                        }
                    }) : e("img", {
                        staticClass: "img",
                        attrs: {
                            src: t.giftInfo && t.giftInfo.giftImg && t.giftInfo.giftImg.png,
                            width: 100,
                            height: 100
                        }
                    }), e("p", {
                        staticClass: "t-center p-content"
                    }, [t._v(t._s(t.showMsg))])]), e("footer", {
                        staticClass: "footer dp-flex btn-box"
                    }, [e("link-button", {
                        ref: "btnCancel",
                        staticClass: "pure-link-cancel",
                        attrs: {
                            type: "ghost",
                            size: "size"
                        },
                        nativeOn: {
                            click: function(i) {
                                return t.clickCancel.apply(null, arguments)
                            }
                        }
                    }, [t._v("取消")]), e("link-button", {
                        ref: "submitBtn",
                        staticClass: "pure-link-send",
                        class: {
                            disable: t.isLoading || t.isFailed
                        },
                        attrs: {
                            size: "size",
                            disabled: t.isLoading || t.isFailed
                        },
                        nativeOn: {
                            click: function(i) {
                                return i.stopPropagation(), t.onClickSend.apply(null, arguments)
                            }
                        }
                    }, [e("span", {
                        staticClass: "bag-info v-middle"
                    }, [t._v("赠送")]), t.goldPrice ? e("i", {
                        staticClass: "currency-icon",
                        style: {
                            backgroundImage: "url(" + t.currency.icon + ")"
                        }
                    }) : t._e(), t.goldPrice ? e("span", {
                        staticClass: "text v-middle"
                    }, [t._v("x" + t._s(t._f("formatNumWithUnit")(t.currency.format(t.goldPrice))))]) : t._e()])], 1)])])
                }), [], !1, null, "8dc949e2", null),
                G = F.exports
        }
    }
]);