(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [619], {
        70619: function(n, e, o) {
            "use strict";
            o.r(e), o.d(e, {
                init: function() {
                    return i
                }
            });
            var t = o(57072);
            var r = o(68913);

            function i(n) {
                return function(e) {
                    e === n && (window.EventBus || Object.defineProperty(window, "EventBus", {
                        get: function() {
                            return t.default
                        }
                    }), Object.defineProperty(window, "Logger", {
                        get: function() {
                            return r.Yd
                        }
                    }), window.showProtocol || Object.defineProperty(window, "showProtocol", {
                        get: function() {
                            return function() {
                                var n = window.server_callback;
                                window.server_callback = function(e) {
                                    console.log("[" + e.cmd + "] ", e), n(e)
                                }
                            }
                        }
                    }), Object.defineProperty(window, "debuggerHelp", {
                        get: function() {
                            console.log("\n        Commands:\n        ===\n         - window.EventBus: 查看 EventBus.\n         - window.Logger: Logger 模块.\n         - window.showProtocol(): 查看广播协议数据.\n      ")
                        }
                    }), console.log('\n      [Info] Debugger is activated. 🐸\n      Type "window.debuggerHelp" to see full commands.\n    '))
                }
            }
        }
    }
]);