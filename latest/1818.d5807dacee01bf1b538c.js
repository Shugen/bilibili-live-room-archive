(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [1818], {
        51818: function(e, t, r) {
            "use strict";
            r.r(t), r.d(t, {
                LplDrawerComponent: function() {
                    return w
                }
            });
            var i = r(66570),
                n = r(34074),
                o = r.n(n),
                a = r(70538),
                l = r(95334),
                c = r(57072),
                s = function(e, t, r, i) {
                    var n, a = arguments.length,
                        l = a < 3 ? t : null === i ? i = o()(t, r) : i;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) l = Reflect.decorate(e, t, r, i);
                    else
                        for (var c = e.length - 1; c >= 0; c--)(n = e[c]) && (l = (a < 3 ? n(l) : a > 3 ? n(t, r, l) : n(t, r)) || l);
                    return a > 3 && l && Object.defineProperty(t, r, l), l
                },
                u = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).isDrawerShow = !1, t.eventTimer = null, t
                    }(0, i.Z)(t, e);
                    var r = t.prototype;
                    return r.hideDrawer = function() {
                        this.isDrawerShow = !1
                    }, r.toggleDrawer = function() {
                        this.isDrawerShow = !this.isDrawerShow
                    }, r.mounted = function() {
                        c.default.$on("lplInteract:closeDrawer", this.hideDrawer.bind(this))
                    }, r.globalClickHandler = function(e) {
                        e["target-lpl-drawer"] || this.hideDrawer()
                    }, r.localClickHandler = function(e) {
                        e["target-lpl-drawer"] = !0
                    }, r.onWatchIsDrawerShow = function(e, t) {
                        var r = this;
                        e !== t && (e ? this.eventTimer = setTimeout((function() {
                            return window.addEventListener("click", r.globalClickHandler)
                        }), 100) : (clearTimeout(this.eventTimer), window.removeEventListener("click", this.globalClickHandler)))
                    }, t
                }(a.default);
            s([(0, l.Watch)("isDrawerShow")], u.prototype, "onWatchIsDrawerShow", null);
            var d = u = s([(0, l.Component)({})], u),
                w = (0, r(51900).Z)(d, (function() {
                    var e = this,
                        t = e.$createElement,
                        r = e._self._c || t;
                    return r("div", {
                        staticClass: "lpl-drawer-ctnr p-relative",
                        on: {
                            click: e.localClickHandler
                        }
                    }, [r("div", {
                        staticClass: "main-icon dp-i-block v-top"
                    }, [e._t("default")], 2), r("div", {
                        staticClass: "toggler dp-i-block border-box v-top p-relative pointer",
                        on: {
                            click: e.toggleDrawer
                        }
                    }, [r("i", {
                        staticClass: "arrow p-absolute ts-dot-4",
                        class: {
                            reverse: e.isDrawerShow
                        }
                    })]), r("transition", {
                        attrs: {
                            "leave-active-class": "a-move-out-bottom",
                            "enter-active-class": "a-move-in-top"
                        }
                    }, [e.isDrawerShow ? r("div", {
                        staticClass: "outer-drawer p-absolute"
                    }, [r("div", {
                        staticClass: "secondary-icon-ctnr"
                    }, [e._t("hiddenContent")], 2)]) : e._e()])], 1)
                }), [], !1, null, "fd126f5a", null).exports
        }
    }
]);