(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [6636], {
        56636: function(e, t, a) {
            "use strict";
            a.r(t), a.d(t, {
                default: function() {
                    return g
                }
            });
            var n = a(66570),
                i = a(34074),
                r = a.n(i),
                s = a(70538),
                l = a(26452),
                c = a(56056),
                o = a(26384),
                u = a(78580),
                d = a.n(u),
                h = a(59340),
                f = a.n(h),
                p = a(92762),
                m = a.n(p),
                v = {
                    components: {
                        LinkInput: o.default
                    },
                    data: function() {
                        return {
                            uid: "link-checkbox-button-" + (Date.now() + 100 * Math.random()),
                            other: ""
                        }
                    },
                    props: {
                        value: {
                            required: !0
                        },
                        currentValue: {
                            required: !0,
                            type: Array,
                            default: function() {
                                return []
                            }
                        },
                        limit: {
                            type: Number,
                            default: -1
                        },
                        noLabel: {
                            type: Boolean,
                            default: !1
                        },
                        name: {
                            type: String,
                            default: ""
                        },
                        id: {
                            type: String
                        },
                        disabled: {
                            type: Boolean,
                            default: !1
                        }
                    },
                    computed: {
                        checked: function() {
                            var e, t, a;
                            return "其他" === this.value ? d()(e = this.currentValue).call(e, this.value) || d()(t = this.currentValue).call(t, this.other) : d()(a = this.currentValue).call(a, this.value)
                        },
                        realDisabled: function() {
                            return this.disabled || -1 !== this.limit && this.currentValue.length === this.limit && !this.checked
                        }
                    },
                    model: {
                        prop: "currentValue",
                        event: "valueChange"
                    },
                    created: function() {
                        this.id && (this.uid = "link-checkbox-button-" + this.id)
                    },
                    watch: {
                        other: function(e, t) {
                            var a;
                            if (this.checked || d()(a = this.currentValue).call(a, t)) {
                                var n = JSON.parse(f()(this.currentValue)) || [],
                                    i = n.indexOf(t || "其他"),
                                    r = e || "其他"; - 1 === i ? n.push(r) : n[i] = r, this.$emit("valueChange", n)
                            }
                        }
                    },
                    methods: {
                        iconClick: function(e) {
                            this.$refs.realInput.click()
                        },
                        valueChange: function(e) {
                            if (!this.realDisabled) {
                                var t = JSON.parse(f()(this.currentValue)) || [],
                                    a = "其他" === e.target.value && this.other || e.target.value;
                                e.target.checked ? t.push(a) : m()(t).call(t, t.indexOf(a), 1), this.$emit("valueChange", t)
                            }
                        }
                    }
                },
                k = a(51900),
                b = (0, k.Z)(v, (function() {
                    var e = this,
                        t = e.$createElement,
                        a = e._self._c || t;
                    return a("div", {
                        staticClass: "link-checkbox-button-ctnr",
                        class: {
                            disabled: e.realDisabled
                        }
                    }, [a("input", {
                        ref: "realInput",
                        staticClass: "checkbox-input",
                        attrs: {
                            type: "checkbox",
                            name: e.name,
                            id: e.uid,
                            disabled: e.realDisabled
                        },
                        domProps: {
                            checked: e.checked,
                            value: e.value
                        },
                        on: {
                            change: e.valueChange
                        }
                    }), a("span", {
                        staticClass: "check-icon svg-icon v-middle",
                        class: e.checked ? "checkbox-selected" : "checkbox-default",
                        on: {
                            click: e.iconClick
                        }
                    }), e.noLabel ? e._e() : a("label", {
                        staticClass: "link-checkbox-button-label",
                        attrs: {
                            for: e.uid
                        }
                    }, [e._t("default"), "其他" === e.value ? a("link-input", {
                        ref: "otherInput",
                        attrs: {
                            placeholder: "请输入"
                        },
                        model: {
                            value: e.other,
                            callback: function(t) {
                                e.other = t
                            },
                            expression: "other"
                        }
                    }) : e._e()], 2)])
                }), [], !1, null, "1927a87f", null).exports,
                x = a(27741),
                _ = function(e, t, a, n) {
                    var i, s = arguments.length,
                        l = s < 3 ? t : null === n ? n = r()(t, a) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) l = Reflect.decorate(e, t, a, n);
                    else
                        for (var c = e.length - 1; c >= 0; c--)(i = e[c]) && (l = (s < 3 ? i(l) : s > 3 ? i(t, a, l) : i(t, a)) || l);
                    return s > 3 && l && Object.defineProperty(t, a, l), l
                },
                y = function(e) {
                    function t() {
                        return e.apply(this, arguments) || this
                    }(0, n.Z)(t, e);
                    var a = t.prototype;
                    return a.mounted = function() {}, a.getLimitText = function(e) {
                        return "checkbox" === e.type ? void 0 !== e.limit && -1 !== e.limit ? "(限选" + e.limit + "项)" : "" : "radio" === e.type ? "(单选)" : ""
                    }, a.checkPrecondition = function(e, t) {
                        var a = this.formData.answers,
                            n = void 0 === a ? [] : a,
                            i = (0, x.E)(e, n);
                        return i || (n[t] = void 0), i
                    }, t
                }(s.default),
                C = y = _([(0, l.ZP)({
                    props: {
                        formData: {
                            type: Object,
                            default: function() {
                                return {}
                            }
                        },
                        questions: {
                            type: Array,
                            default: function() {
                                return []
                            }
                        }
                    },
                    components: {
                        LinkRadioButton: c.Z,
                        LinkInput: o.default,
                        CheckboxButton: b
                    }
                })], y),
                g = (0, k.Z)(C, (function() {
                    var e = this,
                        t = e.$createElement,
                        a = e._self._c || t;
                    return a("div", {
                        staticClass: "feedback-form"
                    }, e._l(e.questions, (function(t, n) {
                        return a("div", {
                            directives: [{
                                name: "show",
                                rawName: "v-show",
                                value: e.checkPrecondition(t, n),
                                expression: "checkPrecondition(item, index)"
                            }],
                            key: n
                        }, [a("p", {
                            staticClass: "tips"
                        }, [e._v(e._s(n + 1) + ". " + e._s(t.title)), a("span", {
                            staticClass: "survey-required"
                        }, [e._v(e._s(t.required ? "*" : "") + " ")]), a("span", [e._v(e._s(e.getLimitText(t)))])]), "checkbox" === t.type ? a("div", {
                            staticClass: "form-item"
                        }, e._l(t.answers, (function(i, r) {
                            return a("checkbox-button", {
                                key: i,
                                attrs: {
                                    limit: t.limit,
                                    value: i
                                },
                                model: {
                                    value: e.formData.answers[n],
                                    callback: function(t) {
                                        e.$set(e.formData.answers, n, t)
                                    },
                                    expression: "formData.answers[index]"
                                }
                            }, [e._v(e._s(String.fromCharCode(65 + r)) + ") " + e._s(i))])
                        })), 1) : e._e(), "radio" === t.type ? a("div", {
                            staticClass: "form-item"
                        }, e._l(t.answers, (function(t, i) {
                            return a("link-radio-button", {
                                key: t,
                                staticClass: "survey-radio",
                                attrs: {
                                    value: t
                                },
                                model: {
                                    value: e.formData.answers[n],
                                    callback: function(t) {
                                        e.$set(e.formData.answers, n, t)
                                    },
                                    expression: "formData.answers[index]"
                                }
                            }, [e._v(e._s(String.fromCharCode(65 + i)) + ") " + e._s(t))])
                        })), 1) : e._e(), "input" === t.type ? a("div", {
                            staticClass: "form-item"
                        }, [a("link-input", {
                            attrs: {
                                maxlength: t.limit
                            },
                            model: {
                                value: e.formData.answers[n],
                                callback: function(t) {
                                    e.$set(e.formData.answers, n, t)
                                },
                                expression: "formData.answers[index]"
                            }
                        })], 1) : e._e()])
                    })), 0)
                }), [], !1, null, "1735dd1e", null).exports
        }
    }
]);