(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [8849], {
        58849: function(t, e, o) {
            "use strict";
            o.r(e), o.d(e, {
                default: function() {
                    return u
                }
            });
            var r = o(26849),
                n = o(66570),
                c = o(34074),
                i = o.n(c),
                a = o(95334),
                l = o(72807),
                s = function(t, e, o, r) {
                    var n, c = arguments.length,
                        a = c < 3 ? e : null === r ? r = i()(e, o) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(t, e, o, r);
                    else
                        for (var l = t.length - 1; l >= 0; l--)(n = t[l]) && (a = (c < 3 ? n(a) : c > 3 ? n(e, o, a) : n(e, o)) || a);
                    return c > 3 && a && Object.defineProperty(e, o, a), a
                },
                p = function(t) {
                    function e() {
                        var e;
                        return (e = t.apply(this, arguments) || this).AnchorLotteryService = l.lY, e
                    }
                    return (0, n.Z)(e, t), e.prototype.handleClickEntry = function() {
                        l.lY.clearCacheData(), this.$emit("click")
                    }, (0, r.Z)(e, [{
                        key: "entryImage",
                        get: function() {
                            return this.picShake ? "https://i0.hdslb.com/bfs/activity-plat/static/20220118/1d0c5a1b042efb59f46d4ba1286c6727/eMc16OUdRu.webp" : this.AnchorLotteryService.lottery.assetIcon || o(71698)
                        }
                    }]), e
                }(a.Vue);
            s([(0, a.Prop)({
                type: String
            })], p.prototype, "text", void 0), s([(0, a.Prop)({
                type: Boolean,
                default: !1
            })], p.prototype, "iframeShow", void 0), s([(0, a.Prop)({
                type: Boolean,
                default: !1
            })], p.prototype, "picShake", void 0);
            var f = p = s([a.Component], p),
                u = (0, o(51900).Z)(f, (function() {
                    var t = this,
                        e = t.$createElement,
                        o = t._self._c || e;
                    return o("div", {
                        staticClass: "anchor-lottery-entry",
                        on: {
                            click: t.handleClickEntry
                        }
                    }, [o("img", {
                        staticClass: "anchor-lot-icon",
                        attrs: {
                            src: t.entryImage
                        }
                    }), o("p", {
                        staticClass: "anchor-lot-text"
                    }, [t._v(t._s(t.text))]), o("div", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: t.iframeShow,
                            expression: "iframeShow"
                        }],
                        staticClass: "lottery-panel z-gift-control-lottery-activity"
                    }, [t._t("default")], 2)])
                }), [], !1, null, "be41c46e", null).exports
        },
        71698: function(t, e, o) {
            "use strict";
            t.exports = o.p + "static/img/lottery.add8cbb..png"
        }
    }
]);