(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [6833], {
        96833: function(n, e, t) {
            "use strict";
            t.r(e), t.d(e, {
                blockUser: function() {
                    return v
                }
            });
            var r = t(63109),
                o = t.n(r),
                u = t(93476),
                a = t.n(u),
                c = t(9669),
                i = t.n(c),
                s = t(3595),
                f = t(12683),
                p = function(n, e, t, r) {
                    return new(t || (t = a()))((function(o, u) {
                        function a(n) {
                            try {
                                i(r.next(n))
                            } catch (n) {
                                u(n)
                            }
                        }

                        function c(n) {
                            try {
                                i(r.throw(n))
                            } catch (n) {
                                u(n)
                            }
                        }

                        function i(n) {
                            var e;
                            n.done ? o(n.value) : (e = n.value, e instanceof t ? e : new t((function(n) {
                                n(e)
                            }))).then(a, c)
                        }
                        i((r = r.apply(n, e || [])).next())
                    }))
                };

            function v(n) {
                return p(this, void 0, void 0, o().mark((function e() {
                    var t;
                    return o().wrap((function(e) {
                        for (;;) switch (e.prev = e.next) {
                            case 0:
                                return e.next = 2, d(n);
                            case 2:
                                return !(t = e.sent).error && t.data && (0, f.Rj)(t.data.uid, t.data.uname), e.abrupt("return", t);
                            case 5:
                            case "end":
                                return e.stop()
                        }
                    }), e)
                })))
            }

            function d(n) {
                return p(this, void 0, void 0, o().mark((function e() {
                    var t, r, u;
                    return o().wrap((function(e) {
                        for (;;) switch (e.prev = e.next) {
                            case 0:
                                return t = s.h.getters.baseInfoRoom.roomID, e.next = 3, i().post("/liveact/shield_user", {
                                    roomid: t,
                                    uid: n,
                                    type: 1
                                });
                            case 3:
                                return r = e.sent, u = r.serverResponse, e.abrupt("return", u);
                            case 6:
                            case "end":
                                return e.stop()
                        }
                    }), e)
                })))
            }
        }
    }
]);