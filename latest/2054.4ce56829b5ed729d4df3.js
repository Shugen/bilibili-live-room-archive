(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [2054], {
        92054: function(t, e, a) {
            "use strict";
            a.r(e), a.d(e, {
                default: function() {
                    return p
                }
            });
            var r = a(26849),
                s = a(66570),
                n = a(34074),
                o = a.n(n),
                c = a(70538),
                i = a(95334),
                l = function(t, e, a, r) {
                    var s, n = arguments.length,
                        c = n < 3 ? e : null === r ? r = o()(e, a) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) c = Reflect.decorate(t, e, a, r);
                    else
                        for (var i = t.length - 1; i >= 0; i--)(s = t[i]) && (c = (n < 3 ? s(c) : n > 3 ? s(e, a, c) : s(e, a)) || c);
                    return n > 3 && c && Object.defineProperty(e, a, c), c
                },
                d = function(t) {
                    function e() {
                        return t.apply(this, arguments) || this
                    }
                    return (0, s.Z)(e, t), e.prototype.close = function() {
                        this.$emit("close")
                    }, (0, r.Z)(e, [{
                        key: "awardDesc",
                        get: function() {
                            return this.serverData.data.award_desc.split("\n").join("<br>")
                        }
                    }, {
                        key: "warnFilter",
                        get: function() {
                            return this.serverData.data.award_warn.replace(/<%.+?%>/g, (function(t) {
                                return '<span style="color: #ef5350;">' + t.match(/[^(<%)(%>)]+/g)[0] + "</span>"
                            }))
                        }
                    }, {
                        key: "bodyBGImageUrl",
                        get: function() {
                            return "anchor_reward" === this.serverData.action ? this.serverData.data.award_url : "./assets/door.png"
                        }
                    }]), e
                }(c.default);
            l([(0, i.Prop)({
                type: Object,
                default: {}
            })], d.prototype, "serverData", void 0);
            var u = d = l([i.Component], d),
                p = (0, a(51900).Z)(u, (function() {
                    var t = this,
                        e = t.$createElement,
                        a = t._self._c || e;
                    return a("div", {
                        attrs: {
                            id: "player-popup-area-top"
                        }
                    }, [a("div", {
                        staticClass: "popup-wrap"
                    }, [a("div", {
                        staticClass: "header"
                    }, [a("h3", {
                        staticClass: "title"
                    }, [t._v(t._s(t.serverData.data.title))]), t.serverData.data.rank_content ? a("p", {
                        staticClass: "desc"
                    }, [a("span", [t._v(t._s(t.serverData.data.rank_content))])]) : t._e()]), a("div", {
                        staticClass: "body",
                        style: {
                            backgroundImage: "url('" + t.bodyBGImageUrl + "')"
                        }
                    }, [a("p", {
                        staticClass: "title",
                        domProps: {
                            innerHTML: t._s(t.awardDesc)
                        }
                    }), a("p", {
                        staticClass: "desc",
                        domProps: {
                            innerHTML: t._s(t.warnFilter)
                        }
                    })]), a("div", {
                        staticClass: "footer"
                    }, [a("button", {
                        staticClass: "btn got",
                        on: {
                            click: t.close
                        }
                    }, [t._v(t._s(t.serverData.data.button_content))]), a("a", {
                        staticClass: "btn read-more",
                        attrs: {
                            href: t.serverData.data.web_jump_url,
                            target: "_blank"
                        }
                    }, [t._v(t._s(t.serverData.data.jump_content))])])])])
                }), [], !1, null, "0da7de80", null).exports
        }
    }
]);