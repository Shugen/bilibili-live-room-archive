(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [6322], {
        46322: function(e, a, t) {
            "use strict";
            t.r(a), t.d(a, {
                default: function() {
                    return m
                }
            });
            var o = t(86902),
                n = t.n(o),
                l = t(2060),
                r = t(57072),
                i = t(36893),
                s = t.n(i),
                d = t(60662),
                u = t(18980),
                c = (0, l.aZ)({
                    props: {
                        config: {
                            type: Object,
                            default: function() {
                                return {}
                            }
                        }
                    },
                    components: {
                        Medal: s()
                    },
                    setup: function(e) {
                        var a = function(e) {
                            var a = (0, l.Fl)((function() {
                                    return e.config.upgrade_medal_info || {}
                                })),
                                t = (0, l.Fl)((function() {
                                    return e.config.origin_medal_info || {}
                                })),
                                o = (0, l.Fl)((function() {
                                    return t.value.level > 0
                                })),
                                n = (0, l.Fl)((function() {
                                    var a = e.config.upgrade_content;
                                    return (a = a.replace(/<%/g, '<span style="color: #D69D4F; font-weight: bold">')).replace(/%>/g, "</span>")
                                }));
                            return {
                                upgradeMedalInfo: a,
                                originMedalInfo: t,
                                gotoOpen: function() {
                                    u.default.report("444.8.guard-guide.0.click", {
                                        page_type: o.value ? 1 : 2
                                    }), d.Z.emit(d.c.closeGuardGuide, null), d.Z.emit(d.c.openGuardStore, {
                                        fromType: 24
                                    })
                                },
                                refactorDesc: n,
                                hasFansMedal: o
                            }
                        }(e);
                        return {
                            upgradeMedalInfo: a.upgradeMedalInfo,
                            originMedalInfo: a.originMedalInfo,
                            gotoOpen: a.gotoOpen,
                            refactorDesc: a.refactorDesc,
                            hasFansMedal: a.hasFansMedal
                        }
                    }
                }),
                g = t(51900),
                p = (0, g.Z)(c, (function() {
                    var e = this,
                        a = e.$createElement,
                        o = e._self._c || a;
                    return o("div", {
                        staticClass: "guard-guide-wrap"
                    }, [o("a", {
                        staticClass: "question",
                        attrs: {
                            href: "https://link.bilibili.com/p/help/index#/great-navigation",
                            target: "_blank"
                        }
                    }), o("div", {
                        staticClass: "title t-center"
                    }, [o("span", [e._v(e._s(e.config.upgrade_title))])]), o("div", {
                        staticClass: "desc"
                    }, [o("span", {
                        domProps: {
                            innerHTML: e._s(e.refactorDesc)
                        }
                    })]), o("div", {
                        staticClass: "fans-medal-wrap"
                    }, [o("img", {
                        staticClass: "bg-img",
                        attrs: {
                            src: t(97564)
                        }
                    }), o("div", {
                        staticClass: "medal-upgrade"
                    }, [e.hasFansMedal ? o("medal", {
                        staticClass: "medal-before",
                        attrs: {
                            level: e.originMedalInfo.level,
                            label: e.originMedalInfo.medal_name,
                            "medal-color-start": e.originMedalInfo.medal_color_start,
                            "medal-color-end": e.originMedalInfo.medal_color_end,
                            "medal-color-border": e.originMedalInfo.medal_color_border,
                            "guard-level": e.originMedalInfo.guard_level
                        }
                    }) : e._e(), e.hasFansMedal ? o("img", {
                        staticClass: "arrow",
                        attrs: {
                            src: t(74941)
                        }
                    }) : e._e(), o("medal", {
                        staticClass: "medal-after",
                        class: {
                            "single-medal": !e.hasFansMedal
                        },
                        attrs: {
                            level: e.upgradeMedalInfo.level,
                            label: e.upgradeMedalInfo.medal_name,
                            "medal-color-start": e.upgradeMedalInfo.medal_color_start,
                            "medal-color-end": e.upgradeMedalInfo.medal_color_end,
                            "medal-color-border": e.upgradeMedalInfo.medal_color_border,
                            "guard-level": e.upgradeMedalInfo.guard_level
                        }
                    })], 1)]), o("div", {
                        staticClass: "guard-btn pointer t-center m-auto",
                        on: {
                            click: e.gotoOpen
                        }
                    }, [o("span", [e._v(e._s(e.config.upgrade_buy_title))])]), o("div", {
                        staticClass: "tips t-center"
                    }, [o("span", [e._v(e._s(e.config.upgrade_buy_desc) + " ")])])])
                }), [], !1, null, "6621a373", null).exports,
                f = t(23933),
                A = (0, l.aZ)({
                    components: {
                        GuardGuide: p
                    },
                    setup: function() {
                        var e = (0, l.iH)(!1),
                            a = (0, l.qj)({
                                origin_medal_info: {
                                    level: 0
                                }
                            }),
                            t = (0, l.Fl)((function() {
                                return a.origin_medal_info.level > 0
                            }));
                        return (0, l.bv)((function() {
                            r.default.$on("GuardGuide:ShowGuardGuide", (function(e) {
                                e.room_id === f.e.getRoomId() && (e && n()(e).forEach((function(t) {
                                    a[t] = e[t]
                                })), d.Z.emit(d.c.openGuardGuide, null))
                            })), d.Z.on(d.c.openGuardGuide, (function() {
                                e.value = !0, u.default.report("444.8.guard-guide.0.show", {
                                    page_type: t.value ? 1 : 2
                                })
                            })), d.Z.on(d.c.closeGuardGuide, (function() {
                                e.value = !1
                            }))
                        })), {
                            isShowGuardGuide: e,
                            closeGuardguide: function() {
                                return d.Z.emit(d.c.closeGuardGuide, null)
                            },
                            guideData: a
                        }
                    }
                }),
                m = (0, g.Z)(A, (function() {
                    var e = this,
                        a = e.$createElement,
                        t = e._self._c || a;
                    return t("div", [t("transition", {
                        attrs: {
                            name: "fade"
                        }
                    }, [e.isShowGuardGuide ? t("div", {
                        staticClass: "guard-guide-box p-fixed w-100 h-100"
                    }, [t("div", {
                        staticClass: "p-absolute w-100 h-100"
                    }), t("div", {
                        staticClass: "cntr p-fixed"
                    }, [t("guard-guide", {
                        attrs: {
                            config: e.guideData
                        }
                    }), t("div", {
                        staticClass: "close icon-font icon-close p-absolute pointer",
                        on: {
                            click: e.closeGuardguide
                        }
                    })], 1)]) : e._e()])], 1)
                }), [], !1, null, "57cf3d4e", null).exports
        },
        74941: function(e) {
            "use strict";
            e.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGkAAAAtCAYAAACtS0VAAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAbRSURBVHgB7ZtNbFRVFMf/9868+aLlI4ApFrGQ0CKYSIBFSYDMFKysSJeGDWBIdGV0ARbQWI1CExNi3EHQ4oYYV124wKrMGCo20RBN/KAltHwU+bDVBk077cx713tm0joDnc6bngPpgt+inc68mUznzLnvd855V91JvbVZB8xiCKBNsH/xtrbfwOBW8vBe+6sOAhh4PU8m2s+CQW9HU6t9pTUQQAFd9fuSZ1AhOnsv8LPJIgsBPJVdNfj1IVbAIwh1Kpg0BFDQjbeSrXXgEPZOKaVGIYABmns74hUHXNfuahtVQbcfQoQiwbVgsCjRNmJgeiCEQWAnGDTsTg0Zo7oghIHajQrR9GPp1mN9rtFjEEAZs+DP84fqwWBZoj2loEYggM3KGptNcTBo2PdNJ5QaggB2yVths6mlkufoyRvZicxPEMIzzsobF16PgoGB2wkhbMAbB5KvLQQH452CEEap5t4T8SV+j58K0vIdx4ZN1huEAFp5Tmiiaj0Y2Gy6qmFEvjh2iYlEEKvo23s/DftSl+w/1g0BlEEMIez3e7wu/ON69dCvUhJBxsiViFGEz0pJhKWOKxFV6dgZKYmwoVrjVyKKgrRp08nMWDbbByEiTuA5Y9qCmCUrE202QDoFIWxGtQwk2yKYJbUvfzHqAmLLMIze/+OJjbFyh+n773j6+fZ+z1XDEMBoxIa6x1eBQU3ifTK9qxDAnpsWRpBuBINn9p6zpqcuQQJlllSHqpvLHaanuzMjmE1AoJ4rEVZFUhBDx9kSAU8um2x2l5OIaYNEEpFVagBCSEgEdQ8ghIREKATEaqdyEqFLPXAzcqdPUiKGkweXg8E4IilJiRhMHmB9ceZNRDpFJeLj+JZSj5YMEkmE/WjEaqdsILKOKxGSJ+0ggju5EqE8yNVOAbW7lETomZ74ROKD21ISQbXTX+fB6kTUJo7RCfsqBKDaKYx0HAxWv3TuopREUO00PzR/2mVYl3vy4wZsGR5BA7ZskKQbsE4wyMom6QYs2R4YSDdgLQ9kU9kgEdeiwwNSDViSiNtfvrkSDNKI9Eg1YC11t5MHWbVT1US0S6oBSxLx++ltRbWTryCRREg2YFVU13MlQrIBa2u5OFciJBuwVu9bCiXCV5AIqp0kJeJut7sODKh2gqBEhDDOmztRA1ZQIqqd6qm5k+8gEZISYTvcT80lidBQ6+eSRNjlc8ukRFQUJJII6QYsGJBESDdgwYAkQroBS78UZsHdb49sU1rNhwhuH02GweCP5JFXaAILEbwUTYbBoK9j+7u2jbUCIpjOijJpEm/CFRkO5t6Cp9kfroac1NgPhH1lkFJGZDiYey2oDRUH6caF49FgLFIHITLpMVYj11rZQg8OS6ELUXBZNRh1tD0PZccPfjHadFWswSH373pbTJYdVPl6A3ZcX/vC8RtgYK0sTnMiCEDjetsK42WlY4tROyeCBHZc37An2V1RJlEWkZVBiMw/Q6xzEWURWRmECGEsBQa5uZC1Mkjh5OdWFQUpau5thhjBvtpdJ1m6GkZmL8TQqUWJD3ldDEe3QgzTSbZIt3wH6dpXrauMlxVZ5tyMN7Z0axsri6iVY41OZJmzrzOyLPFeCgwun25qFlvmbIvJFsdTKu8rSLTMzROUBTeTZi9zkrKgkU2BgbQsQBWP532Jw2NZKMNDkIWiu8o958rnbyyYS7JwN3mgZi7JwpVPd6x4GLJQSNkgLaiNboQYfFnIIvQixODLQtb1XoUY/8tCITMG6bEszMzDlIVCSgaJZKEqHFoNIapi+AUMSBZo7gMhbCOUt7nMyoLxwGrIFuJ6mZKby0qKA8mCp4wDAUgWqrYevQMGJAs094EAJAvL8he1zB4rC0bJyBTJwto95y+WfHi6O/MnZxlZoPkTVxZuJg+tkZIFmj9xZeHyJ00bpGQhN39yZr4idtog6dA81m69ojcRDPZzZUFBs6amhRgEeriy4AEV79YrhTFe13SyUMgDQbLT0vq5JAu0S28uyUJvx/aWRyELhRQFiWQhGnJYV/IU4sScH8CAZIF26UEI21n4DAxIFpQxYp2FjBr/yM9xRUHKy4InJguLG9+5BwbSskBX5IKDsCw8u+e7674Onbwx3PP2cilZoGWOKwuDycPrBWVhhD2GoAvqpToLdI2e43/7zFSQjKtYV5YWEdBsWbABikMMzZYFKC1WE1nxKCsLheTqJGlZqGk6yhqJ57f0y8lCTeIobyROsgBPTBbyuwX9o0kWwuGAWBa5/w59Dwb5zoKOQ4gwxk6DQX4XnieWRQh57agQTbIAIXJjCOYyR7IAIUgW+NNWudZPbgxRwTI3yX8XPSl3aN6aRAAAAABJRU5ErkJggg=="
        },
        97564: function(e, a, t) {
            "use strict";
            e.exports = t.p + "static/img/bg.ec8a8e4..png"
        }
    }
]);