(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [7864], {
        57864: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, {
                default: function() {
                    return f
                }
            });
            var c = n(26849),
                o = n(66570),
                r = n(34074),
                i = n.n(r),
                u = n(95334),
                l = n(18706),
                a = n(82159),
                s = function(e, t, n, c) {
                    var o, r = arguments.length,
                        u = r < 3 ? t : null === c ? c = i()(t, n) : c;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) u = Reflect.decorate(e, t, n, c);
                    else
                        for (var l = e.length - 1; l >= 0; l--)(o = e[l]) && (u = (r < 3 ? o(u) : r > 3 ? o(t, n, u) : o(t, n)) || u);
                    return r > 3 && u && Object.defineProperty(t, n, u), u
                },
                m = function(e) {
                    function t() {
                        return e.apply(this, arguments) || this
                    }
                    return (0, o.Z)(t, e), t.prototype.handleClickEntry = function() {
                        this.linkPopup({
                            button: !1,
                            title: "",
                            width: 408,
                            component: {
                                components: {
                                    EcommerceGuide: function() {
                                        return {
                                            component: n.e(568).then(n.bind(n, 70568)),
                                            loading: l.default
                                        }
                                    }
                                },
                                name: "ecommerce-guide",
                                template: "<EcommerceGuide></EcommerceGuide>"
                            }
                        })
                    }, (0, c.Z)(t, [{
                        key: "entryImage",
                        get: function() {
                            return a.isSupportWebp ? n(48523) : n(28561)
                        }
                    }]), t
                }(u.Vue),
                p = m = s([u.Component], m),
                f = (0, n(51900).Z)(p, (function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("div", {
                        staticClass: "ecommerce-entry",
                        on: {
                            click: e.handleClickEntry
                        }
                    }, [n("img", {
                        staticClass: "ecommerce-lot-icon",
                        attrs: {
                            src: e.entryImage
                        }
                    })])
                }), [], !1, null, "83b623e6", null).exports
        },
        28561: function(e, t, n) {
            "use strict";
            e.exports = n.p + "static/img/ecommerce-shake.a564a66..png"
        },
        48523: function(e, t, n) {
            "use strict";
            e.exports = n.p + "static/img/ecommerce-shake.a0e2baf..webp"
        }
    }
]);