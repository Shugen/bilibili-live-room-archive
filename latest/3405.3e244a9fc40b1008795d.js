(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [3405], {
        33405: function(t, s, e) {
            "use strict";
            e.r(s), e.d(s, {
                default: function() {
                    return a
                }
            });
            var a = (0, e(51900).Z)({}, (function() {
                var t = this,
                    s = t.$createElement;
                t._self._c;
                return t._m(0)
            }), [function() {
                var t = this,
                    s = t.$createElement,
                    e = t._self._c || s;
                return e("div", {
                    staticClass: "url-incorrect t-center"
                }, [e("h1", {
                    staticClass: "title"
                }, [t._v("看起来页面木有正确加载…… (´；ω；`)")]), e("div", {
                    staticClass: "supporting-text"
                }, [e("p", [t._v("但是但是……")]), e("p", [t._v("说起来您可能都不信，没加载成功是因为…… 您输入的"), e("span", {
                    staticClass: "alert-text",
                    staticStyle: {
                        color: "#F44336"
                    }
                }, [t._v("网址有误")]), t._v("~ Σ(ﾟдﾟ;)")]), e("p", [t._v("请确定您输入的网址"), e("span", {
                    staticClass: "alert-text",
                    staticStyle: {
                        color: "#2196F3"
                    }
                }, [t._v("含有房间号")]), t._v("喔 (｡･ω･｡)")]), e("p", [t._v("就是结尾的那一串数字啦 (･∀･)")]), e("p", [t._v("比如"), e("span", {
                    staticClass: "alert-text"
                }, [t._v("这样")]), t._v("的：")]), e("p", [t._v("https://live.bilibili.com"), e("span", {
                    staticClass: "alert-text"
                }, [t._v("/31917")])]), e("p", [t._v("快回去试试，肯定可以正确打开喔 Σ>―(〃°ω°〃)♡→"), e("span", {
                    staticClass: "splashing-cursor"
                }, [t._v("_")])]), e("p", {
                    staticStyle: {
                        color: "#ccc"
                    }
                }, [t._v("自己被自己萌地满地爬 —(°∀°」∠❀)ﾉ")])])])
            }], !1, null, "a221b82c", null).exports
        }
    }
]);