(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [9053], {
        49053: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, {
                default: function() {
                    return s
                }
            });
            var r = n(26849),
                u = n(66570),
                o = n(34074),
                i = n.n(o),
                p = n(70538),
                d = n(26452),
                a = n(3595),
                f = function(e, t, n, r) {
                    var u, o = arguments.length,
                        p = o < 3 ? t : null === r ? r = i()(t, n) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) p = Reflect.decorate(e, t, n, r);
                    else
                        for (var d = e.length - 1; d >= 0; d--)(u = e[d]) && (p = (o < 3 ? u(p) : o > 3 ? u(t, n, p) : u(t, n)) || p);
                    return o > 3 && p && Object.defineProperty(t, n, p), p
                },
                l = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).isNeedCardPopup = !1, t.maxShowAmount = 10, t.pageType = "LIVE", t.popupWrapper = null, t
                    }
                    return (0, u.Z)(t, e), t.prototype.mounted = function() {
                        this.popupWrapper = document.getElementById("link-feed-card-popup")
                    }, (0, r.Z)(t, [{
                        key: "masterUid",
                        get: function() {
                            return a.h.getters.baseInfoAnchor.uid
                        }
                    }, {
                        key: "uid",
                        get: function() {
                            return a.h.getters.baseInfoUser.uid
                        }
                    }, {
                        key: "feedProp",
                        get: function() {
                            var e = this.masterUid;
                            return {
                                loginUid: this.uid,
                                uid: e
                            }
                        }
                    }]), t
                }(p.default),
                c = l = f([(0, d.ZP)({
                    components: {
                        LinkFeed: function() {
                            return n.e(5646).then(n.t.bind(n, 35646, 23)).then((function(e) {
                                return e.default
                            }))
                        }
                    }
                })], l),
                s = (0, n(51900).Z)(c, (function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("div", [n("link-feed", {
                        attrs: {
                            loginUid: e.uid,
                            uid: e.masterUid,
                            isNeedCardPopup: e.isNeedCardPopup,
                            maxShowAmount: e.maxShowAmount,
                            pageType: e.pageType,
                            "popup-dom": e.popupWrapper
                        }
                    })], 1)
                }), [], !1, null, null, null).exports
        }
    }
]);