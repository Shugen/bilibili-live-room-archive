(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [1951], {
        41951: function(t, e, r) {
            "use strict";
            r.r(e), r.d(e, {
                default: function() {
                    return p
                }
            });
            var n = r(26849),
                o = r(66570),
                l = r(34074),
                a = r.n(l),
                i = r(70538),
                c = r(95334),
                s = function(t, e, r, n) {
                    var o, l = arguments.length,
                        i = l < 3 ? e : null === n ? n = a()(e, r) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) i = Reflect.decorate(t, e, r, n);
                    else
                        for (var c = t.length - 1; c >= 0; c--)(o = t[c]) && (i = (l < 3 ? o(i) : l > 3 ? o(e, r, i) : o(e, r)) || i);
                    return l > 3 && i && Object.defineProperty(e, r, i), i
                },
                u = function(t) {
                    function e() {
                        return t.apply(this, arguments) || this
                    }
                    return (0, o.Z)(e, t), e.prototype.mounted = function() {
                        console.warn("小老弟，你在弄撒子咧？")
                    }, (0, n.Z)(e, [{
                        key: "errorMsg",
                        get: function() {
                            return (this.$store.getters.appStatus.errorMsg || "").split("\\n")
                        }
                    }, {
                        key: "legalRoomUrl",
                        get: function() {
                            var t = window.location.href;
                            return t = (t = (t = t.replace(/\/blanc\//g, "/")).replace(/liteVersion=true/g, "")).replace(/\?$/g, "")
                        }
                    }]), e
                }(i.default),
                f = u = s([(0, c.Component)({})], u),
                p = (0, r(51900).Z)(f, (function() {
                    var t = this,
                        e = t.$createElement,
                        r = t._self._c || e;
                    return r("div", {
                        staticClass: "room-is-region-restrict t-center"
                    }, [r("div", {
                        staticClass: "header-img m-auto",
                        attrs: {
                            role: "img"
                        }
                    }), r("div", {
                        staticClass: "supporting-text"
                    }, [t._l(t.errorMsg, (function(e) {
                        return r("p", [t._v(t._s(e))])
                    })), r("p", [t._v("访问被拒绝。")]), t._m(0), r("a", {
                        staticClass: "contact-tip",
                        attrs: {
                            href: t.legalRoomUrl,
                            target: "_top"
                        }
                    }, [t._v("回到哔哩哔哩直播")])], 2)])
                }), [function() {
                    var t = this,
                        e = t.$createElement,
                        r = t._self._c || e;
                    return r("p", [t._v("官方合作请移步 "), r("a", {
                        attrs: {
                            href: "https://www.bilibili.com/html/contact.html",
                            target: "_top"
                        }
                    }, [t._v("联系我们")]), t._v("。")])
                }], !1, null, "ce4107a8", null).exports
        }
    }
]);