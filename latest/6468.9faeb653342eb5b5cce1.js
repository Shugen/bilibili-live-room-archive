(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [6468], {
        56468: function(t, n, e) {
            "use strict";
            e.r(n), e.d(n, {
                default: function() {
                    return g
                }
            });
            var i = e(66570),
                r = e(63109),
                o = e.n(r),
                a = e(34074),
                s = e.n(a),
                u = e(93476),
                c = e.n(u),
                f = e(70538),
                d = e(26452),
                h = e(57072),
                p = e(45354),
                l = e.n(p),
                v = function(t, n, e, i) {
                    var r, o = arguments.length,
                        a = o < 3 ? n : null === i ? i = s()(n, e) : i;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(t, n, e, i);
                    else
                        for (var u = t.length - 1; u >= 0; u--)(r = t[u]) && (a = (o < 3 ? r(a) : o > 3 ? r(n, e, a) : r(n, e)) || a);
                    return o > 3 && a && Object.defineProperty(n, e, a), a
                },
                m = function(t, n, e, i) {
                    return new(e || (e = c()))((function(r, o) {
                        function a(t) {
                            try {
                                u(i.next(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function s(t) {
                            try {
                                u(i.throw(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function u(t) {
                            var n;
                            t.done ? r(t.value) : (n = t.value, n instanceof e ? n : new e((function(t) {
                                t(n)
                            }))).then(a, s)
                        }
                        u((i = i.apply(t, n || [])).next())
                    }))
                },
                w = {
                    1: e(13609),
                    2: e(88835),
                    3: e(47033)
                },
                y = function(t) {
                    function n() {
                        var n;
                        return (n = t.apply(this, arguments) || this).animation = null, n.animationsConfig = w, n.videoItems = {}, n.offMsgIdArr = [], n.type = null, n.isShow = !1, n
                    }(0, i.Z)(n, t);
                    var e = n.prototype;
                    return e.initAnimation = function() {
                        return m(this, void 0, void 0, o().mark((function t() {
                            var n;
                            return o().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        (n = new(l().Player)(this.$refs.guardLotteryAnimation)).loops = 1, this.animation = n, this.animation.onFinished((function() {
                                            h.default.$emit("\bGuardLotteryAnimation:Close")
                                        }));
                                    case 4:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, e.loadAnimation = function(t) {
                        return m(this, void 0, void 0, o().mark((function n() {
                            var e, i, r = this;
                            return o().wrap((function(n) {
                                for (;;) switch (n.prev = n.next) {
                                    case 0:
                                        if (e = (t || this.type).toString(), i = this.videoItems[e]) {
                                            n.next = 5;
                                            break
                                        }
                                        return n.next = 5, new(c())((function(t, n) {
                                            var o = r.animationsConfig[e];
                                            (new(l().Parser)).load(o, (function(n) {
                                                r.videoItems[e] = i = n, t()
                                            }))
                                        }));
                                    case 5:
                                        return n.abrupt("return", i);
                                    case 6:
                                    case "end":
                                        return n.stop()
                                }
                            }), n, this)
                        })))
                    }, e.playAnimation = function() {
                        return m(this, void 0, void 0, o().mark((function t() {
                            var n;
                            return o().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this.loadAnimation();
                                    case 2:
                                        n = t.sent, this.play(n);
                                    case 4:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, e.play = function(t) {
                        this.animation && this.animation.setVideoItem(t), this.animation.startAnimation()
                    }, e.onMsg = function() {
                        var t = this;
                        h.default.$on("\bGuardLotteryAnimation:Show", (function(n) {
                            return m(t, void 0, void 0, o().mark((function t() {
                                var e = this;
                                return o().wrap((function(t) {
                                    for (;;) switch (t.prev = t.next) {
                                        case 0:
                                            this.isShow = !0, this.type = n.type, this.$nextTick((function() {
                                                return m(e, void 0, void 0, o().mark((function t() {
                                                    return o().wrap((function(t) {
                                                        for (;;) switch (t.prev = t.next) {
                                                            case 0:
                                                                return t.next = 2, this.playAnimation();
                                                            case 2:
                                                            case "end":
                                                                return t.stop()
                                                        }
                                                    }), t, this)
                                                })))
                                            }));
                                        case 3:
                                        case "end":
                                            return t.stop()
                                    }
                                }), t, this)
                            })))
                        })), h.default.$on("\bGuardLotteryAnimation:Close", (function() {
                            t.isShow = !1
                        })), h.default.$on("\bGuardLotteryAnimation:Preload", (function(n) {
                            return m(t, void 0, void 0, o().mark((function t() {
                                return o().wrap((function(t) {
                                    for (;;) switch (t.prev = t.next) {
                                        case 0:
                                            return t.next = 2, this.loadAnimation(n.type);
                                        case 2:
                                        case "end":
                                            return t.stop()
                                    }
                                }), t, this)
                            })))
                        }))
                    }, e.created = function() {
                        this.onMsg()
                    }, e.destroyed = function() {
                        this.animation = null
                    }, e.mounted = function() {
                        return m(this, void 0, void 0, o().mark((function t() {
                            return o().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this.initAnimation();
                                    case 2:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, n
                }(f.default),
                x = y = v([(0, d.ZP)({})], y),
                g = (0, e(51900).Z)(x, (function() {
                    var t = this,
                        n = t.$createElement,
                        e = t._self._c || n;
                    return e("div", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: t.isShow,
                            expression: "isShow"
                        }]
                    }, [e("div", {
                        ref: "guardLotteryAnimation",
                        staticClass: "p-absolute",
                        attrs: {
                            id: "guard-lottery-result-animation"
                        }
                    })])
                }), [], !1, null, "10e799e0", null).exports
        },
        13609: function(t, n, e) {
            "use strict";
            t.exports = e.p + "static/img/guard-1.a588004..svga"
        },
        88835: function(t, n, e) {
            "use strict";
            t.exports = e.p + "static/img/guard-2.ec7763d..svga"
        },
        47033: function(t, n, e) {
            "use strict";
            t.exports = e.p + "static/img/guard-3.ad3cf64..svga"
        }
    }
]);