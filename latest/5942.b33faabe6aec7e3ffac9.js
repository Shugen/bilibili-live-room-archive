(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [5942], {
        65942: function(t, e, r) {
            "use strict";
            r.r(e), r.d(e, {
                default: function() {
                    return p
                }
            });
            var n = r(26849),
                o = r(66570),
                s = r(34074),
                i = r.n(s),
                l = r(70538),
                c = r(95334),
                u = function(t, e, r, n) {
                    var o, s = arguments.length,
                        l = s < 3 ? e : null === n ? n = i()(e, r) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) l = Reflect.decorate(t, e, r, n);
                    else
                        for (var c = t.length - 1; c >= 0; c--)(o = t[c]) && (l = (s < 3 ? o(l) : s > 3 ? o(e, r, l) : o(e, r)) || l);
                    return s > 3 && l && Object.defineProperty(e, r, l), l
                },
                a = function(t) {
                    function e() {
                        return t.apply(this, arguments) || this
                    }
                    return (0, o.Z)(e, t), (0, n.Z)(e, [{
                        key: "errorMsg",
                        get: function() {
                            return (this.$store.getters.appStatus.errorMsg || "").split("\\n")
                        }
                    }]), e
                }(l.default),
                f = a = u([(0, c.Component)({})], a),
                p = (0, r(51900).Z)(f, (function() {
                    var t = this,
                        e = t.$createElement,
                        r = t._self._c || e;
                    return r("div", {
                        staticClass: "room-intranet t-center"
                    }, [r("div", {
                        staticClass: "header-img m-auto",
                        attrs: {
                            role: "img"
                        }
                    }), t._l(t.errorMsg, (function(e) {
                        return r("div", {
                            staticClass: "help-text"
                        }, [t._v(t._s(e))])
                    }))], 2)
                }), [], !1, null, "c22e85dc", null).exports
        }
    }
]);