(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [5650], {
        25650: function(t, e, r) {
            "use strict";
            r.r(e), r.d(e, {
                default: function() {
                    return p
                }
            });
            var n = r(26849),
                i = r(66570),
                s = r(34074),
                o = r.n(s),
                l = r(70538),
                a = r(95334),
                c = function(t, e, r, n) {
                    var i, s = arguments.length,
                        l = s < 3 ? e : null === n ? n = o()(e, r) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) l = Reflect.decorate(t, e, r, n);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(i = t[a]) && (l = (s < 3 ? i(l) : s > 3 ? i(e, r, l) : i(e, r)) || l);
                    return s > 3 && l && Object.defineProperty(e, r, l), l
                },
                u = function(t) {
                    function e() {
                        var e;
                        return (e = t.apply(this, arguments) || this).mail = "livehelp@bilibili.com", e
                    }
                    return (0, i.Z)(e, t), (0, n.Z)(e, [{
                        key: "errorMsg",
                        get: function() {
                            return (this.$store.getters.appStatus.errorMsg || "").split("\\n")
                        }
                    }]), e
                }(l.default),
                f = u = c([(0, a.Component)({})], u),
                p = (0, r(51900).Z)(f, (function() {
                    var t = this,
                        e = t.$createElement,
                        r = t._self._c || e;
                    return r("div", {
                        staticClass: "room-is-region-restrict t-center"
                    }, [r("div", {
                        staticClass: "header-img m-auto",
                        attrs: {
                            role: "img"
                        }
                    }), r("div", {
                        staticClass: "supporting-text"
                    }, [t._l(t.errorMsg, (function(e) {
                        return r("p", [t._v(t._s(e))])
                    })), r("p", {
                        staticClass: "contact-tip"
                    }, [t._v("如有疑问，"), r("a", {
                        attrs: {
                            href: "mailto:" + t.mail
                        }
                    }, [t._v("请联系我们")])])], 2)])
                }), [], !1, null, "2126fb4b", null).exports
        }
    }
]);