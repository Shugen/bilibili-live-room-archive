(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [3095], {
        73095: function(r, t, e) {
            "use strict";
            e.r(t), e.d(t, {
                default: function() {
                    return f
                }
            });
            var i = e(26849),
                n = e(66570),
                o = e(34074),
                s = e.n(o),
                a = e(70538),
                b = e(95334),
                c = ["「打死白学家！」<br/><br/>？？", "「どうしてこうなるんだろう…<br/>初めて、好きな人が出来た。<br/>一生ものの友だちができた。嬉しいことが二つ重なって。<br/>その二つの嬉しさが、また、たくさんの嬉しさを連れてきてくれて。<br/>夢のように幸せな時間を手に入れたはずなのに…なのに、<br/>どうして、こうなっちゃうんだろう…」<br/><br/>《白色相簿 2》, 小木曾雪菜.", "「あたしが、先だった……<br/>先だったんだ……キスしたのも、抱き合ったのも。<br/>そいつのこと好きになったのも。」<br/><br/>《白色相簿 2》, 冬马和纱.", "「なんでそんなに慣れてんだよっ！<br/>雪菜と…何回キスしたんだよ！？<br/>どこまであたしを置いてきぼりにすれば気が済むんだよ！？」<br/><br/>《白色相簿 2》, 冬马和纱.", "「ホワイトアルバムなんて知らない。<br/>だって、もう何も歌えない。<br/>届かない恋なんてしない。<br/>だって、もう人を愛せない。」<br/><br/>《白色相簿 2》, 终章", "「少女だったと懐かしく、振り向く日があるのさ」<br/><br/>《想い出がいっぱい》，H2O.", "「今度、悲しみが来ても、友迎えるに微笑うわ、きっと...<br/>約束よ。」<br/><br/>《悲しみよこんにちは》, 斉藤由贵.", "「だけど、時が過ぎ、季節が替わって<br/>ふたり別々の生き方を、選んでいるの。」<br/><br/>《風に消えた言葉》, 元田恵美.", "「やっぱりほら、いつでも気にしてる<br/>自分に嘘ついて、誰も喜ばない。」<br/><br/>《虹色の涙》, 宮崎羽衣.", "「春の光集めたら花咲かせて、夏はつき浮かぶ海で見つめて<br/>秋の風冬の雪も、その吐息で暖めて欲しい<br/>Four Seasons With You Love<br/>もう一度。」<br/><br/>《Four Seasons》, 安室奈美恵.", "「答えは、いつも、私の胸に...」<br/><br/>《冒険でしょでしょ？》, 平野綾.", "「 I'm no ordinary girl, in an ordinary world. 」<br/><br/>《I'm No Ordinary Girl》, Anika Paris.", "「 It's been a long day without you my friend<br/>and I'll tell you all about it when I see you again. 」<br/><br/>《See You Again》, Wiz Khalifa / Charlie Puth.", "「 We are all just prisoners here of our own device. 」<br/><br/>《Hotel California》, The Eagles.", '「不愉快 DEATH！！☆」<br/><br/>《境界线的彼方》, 栗山未来.<br/><span style="color: #ddd">你确定这调调不是中二病……</span>', "「私はあなたがとても幸せだと感じて、<br/>あなたが选ぶことができて私があるいは私を爱しないことを爱するため、<br/>私が选ぶことしかできなくてあなたがあるいは更にあなたを<br/>爱することを爱します。」<br/><br/>《草莓 100%》, 西野司.", "「贫乳はステータスだ、希少価値だ。」<br/><br/>《Shuffle!》, 麻弓百里香.", "「为什么搞得我像坏人一样。」<br/><br/>《Angel Beats》, 立华奏.", "「醒来的时候，不知道自己为什么哭，<br/>时常会有的事情，做过的梦总是回想不起，<br/>只是……<br/>一种有什么消失的丧失感。」<br/><br/>《你的名字》", "「人永远不知道，谁哪次不经意的跟你说了再见之后，就真的不会再见了。」<br/><br/>《千与千寻》", "「人老了的好处，就是可失去的东西越来越少了。」<br/><br/>《千与千寻》", "「你是可以相处的。」<br/><br/>《小林家的龙女仆》, 法夫纳", "「War is delightful to those who have not experienced it.」<br/><br/>Erasmus.", "「Older men declare war, but it is the youth that must fight and die.」<br/><br/>Herbert Hoover.", "「No battle plan survives contact with the enemy.」<br/><br/>Colin Powell.", "「There are only two kinds of people that understand Marines: Marines and the enemy.<br/>Everyone else has a second-hand opinion.」<br/><br/>General William Thornson.", "「Windows 8 是所有 PC 领域从业者及用户的一场灾难.」<br/><br/>Gabe Newell.", "「这世界并不会在意你的自尊。<br/>这世界指望你在自我感觉良好之前先要有所成就。」<br/><br/>Bill Gates.", "「电视里的并不是真实的生活。<br/>实际上，人们得离开咖啡屋去干自己的工作。」<br/><br/>Bill Gates.", "「不再孤单.」<br/><br/>Pixar Animation Studios.", "「金钱是很多事物的外壳，而不是内核。<br/>它能为你换来食物，而不是胃口；<br/>药物，而不是健康；<br/>泛泛之交，而不是肝胆相照的伙伴；<br/>顺服的仆人，而不是忠心的跟随者；<br/>享乐和消遣，而不是平静幸福。」<br/><br/>亨利克·易卜生."],
                l = function(r, t, e, i) {
                    var n, o = arguments.length,
                        a = o < 3 ? t : null === i ? i = s()(t, e) : i;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(r, t, e, i);
                    else
                        for (var b = r.length - 1; b >= 0; b--)(n = r[b]) && (a = (o < 3 ? n(a) : o > 3 ? n(t, e, a) : n(t, e)) || a);
                    return o > 3 && a && Object.defineProperty(t, e, a), a
                },
                p = function(r) {
                    function t() {
                        return r.apply(this, arguments) || this
                    }
                    return (0, n.Z)(t, r), (0, i.Z)(t, [{
                        key: "quotes",
                        get: function() {
                            return c[Math.floor(Math.random() * c.length)]
                        }
                    }, {
                        key: "errorPic",
                        get: function() {
                            var r = Math.ceil(7 * Math.random());
                            return e(26971)("./pic.err-" + (r < 10 ? "0" + r : r) + ".jpg")
                        }
                    }]), t
                }(a.default);
            l([(0, b.Prop)({
                type: String,
                default: "房间初始化接口数据错误."
            })], p.prototype, "subtitle", void 0), l([(0, b.Prop)({
                type: String
            })], p.prototype, "info", void 0);
            var u = p = l([b.Component], p),
                f = (0, e(51900).Z)(u, (function() {
                    var r = this,
                        t = r.$createElement,
                        e = r._self._c || t;
                    return e("div", {
                        staticClass: "loading-error-ctnr w-100 h-100"
                    }, [e("div", {
                        staticClass: "random-img bg-no-repeat bg-center bg-cover b-circle",
                        style: {
                            "background-image": "url(" + r.errorPic + ")"
                        }
                    }, [e("img", {
                        staticClass: "dp-none",
                        staticStyle: {
                            width: "300px",
                            height: "300px"
                        },
                        attrs: {
                            src: r.errorPic,
                            "ref:error-pic": ""
                        }
                    })]), e("div", {
                        staticClass: "text-ctnr t-center"
                    }, [r._m(0), e("p", {
                        staticClass: "supporting-text"
                    }, [r._v("您可以稍作等待后尝试，或点击"), e("a", {
                        staticClass: "bili-link",
                        attrs: {
                            href: "//www.bilibili.com/html/contact.html",
                            target: "_blank"
                        }
                    }, [r._v("这里")]), r._v("帮忙我们反馈问题"), e("br"), r._v("在此对您造成的不便我们表示真诚的歉意... (´；ω；`)"), e("br"), r._v("错误原因：" + r._s(r.subtitle))]), e("p", {
                        staticClass: "info",
                        domProps: {
                            innerHTML: r._s(r.info)
                        }
                    }, [e("span", {
                        staticClass: "cursor"
                    }, [r._v("_")])]), e("p", {
                        staticClass: "quotes ts-dot-8",
                        domProps: {
                            innerHTML: r._s(r.quotes)
                        }
                    })])])
                }), [function() {
                    var r = this,
                        t = r.$createElement,
                        e = r._self._c || t;
                    return e("h2", {
                        staticClass: "title"
                    }, [e("span", [r._v("房间初始化失败惹 (´；ω；`)")])])
                }], !1, null, "6a9387b2", null).exports
        },
        92060: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-01.f6a848b..jpg"
        },
        39964: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-02.eb2b06d..jpg"
        },
        2881: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-03.e24b3a2..jpg"
        },
        46154: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-04.3a2fdd2..jpg"
        },
        22555: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-05.09a74f2..jpg"
        },
        38859: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-06.2f883bd..jpg"
        },
        20349: function(r, t, e) {
            "use strict";
            r.exports = e.p + "static/img/pic.err-07.fba2f77..jpg"
        },
        26971: function(r, t, e) {
            var i = {
                "./pic.err-01.jpg": 92060,
                "./pic.err-02.jpg": 39964,
                "./pic.err-03.jpg": 2881,
                "./pic.err-04.jpg": 46154,
                "./pic.err-05.jpg": 22555,
                "./pic.err-06.jpg": 38859,
                "./pic.err-07.jpg": 20349
            };

            function n(r) {
                var t = o(r);
                return e(t)
            }

            function o(r) {
                if (!e.o(i, r)) {
                    var t = new Error("Cannot find module '" + r + "'");
                    throw t.code = "MODULE_NOT_FOUND", t
                }
                return i[r]
            }
            n.keys = function() {
                return Object.keys(i)
            }, n.resolve = o, r.exports = n, n.id = 26971
        }
    }
]);