/*! For license information please see 457.c8f811f14d882fa5a688.js.LICENSE.txt */
(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [457], {
        83111: function(e, t, o) {
            "use strict";
            o.r(t), o.d(t, {
                startSleepObserver: function() {
                    return Y
                },
                getSleepObserverInst: function() {
                    return X
                },
                setSleepObserverDefaults: function() {
                    return v
                },
                createScoreObserver: function() {
                    return F
                },
                setScoreObserverDefaults: function() {
                    return x
                },
                PlayerPromptFilter: function() {
                    return C
                },
                Config: function() {
                    return w
                },
                setConfig: function() {
                    return D
                },
                setPlayerPromptDefaults: function() {
                    return A
                }
            });
            var r = o(51942),
                i = o.n(r),
                n = o(20116),
                s = o.n(n),
                a = o(59340),
                c = o.n(a),
                u = o(47302),
                l = o.n(u),
                p = o(3649),
                f = o.n(p),
                h = o(2991),
                S = o.n(h),
                d = {
                    DEFAULT_SLEEP_DELAY: 3e5
                };

            function v(e) {
                i()(d, e)
            }
            var m = function(e, t) {
                return m = Object.setPrototypeOf || {
                    __proto__: []
                }
                instanceof Array && function(e, t) {
                    e.__proto__ = t
                } || function(e, t) {
                    for (var o in t) Object.prototype.hasOwnProperty.call(t, o) && (e[o] = t[o])
                }, m(e, t)
            };

            function _(e, t) {
                function o() {
                    this.constructor = e
                }
                m(e, t), e.prototype = null === t ? Object.create(t) : (o.prototype = t.prototype, new o)
            }
            var y = function() {
                return y = i() || function(e) {
                    for (var t, o = 1, r = arguments.length; o < r; o++)
                        for (var i in t = arguments[o]) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
                    return e
                }, y.apply(this, arguments)
            };
            var g = function() {
                    function e(e) {
                        void 0 === e && (e = {}), this.options = {}, this.timer = null, this.isSleeping = !1;
                        this.options = i()({
                            sleepDelay: 6e4,
                            heartbeatMode: !1,
                            onSleep: function() {
                                return null
                            },
                            onWakeup: function() {
                                return null
                            }
                        }, e), this.observe.apply(this)
                    }
                    return e.prototype.triggerSleepCallback = function() {
                        this.options.onSleep && (this.isSleeping = !0, this.options.onSleep(this.options.sleepDelay))
                    }, e.prototype.triggerWakeupCallback = function(e) {
                        void 0 === e && (e = 0), this.options.onWakeup && this.isSleeping && (this.isSleeping = !1, this.options.onWakeup(e))
                    }, e.prototype.clearTimer = function() {
                        this.timer && (this.options.heartbeatMode ? clearInterval(this.timer) : clearTimeout(this.timer))
                    }, e.prototype.sleep = function() {
                        var e = this;
                        this.clearTimer(), this.options.heartbeatMode ? this.timer = setInterval((function() {
                            e.triggerSleepCallback()
                        }), this.options.sleepDelay) : this.timer = setTimeout((function() {
                            e.triggerSleepCallback()
                        }), this.options.sleepDelay)
                    }, e.prototype.wakeup = function(e) {
                        void 0 === e && (e = 0), this.clearTimer(), this.triggerWakeupCallback(e)
                    }, e
                }(),
                T = function(e) {
                    function t(t) {
                        return e.call(this, {
                            sleepDelay: t.sleepDelay || 6e5,
                            heartbeatMode: !0,
                            onSleep: t.onSleep,
                            onWakeup: t.onWakeup
                        }) || this
                    }
                    return _(t, e), t.prototype.observe = function() {
                        var e = this;
                        document.addEventListener("visibilitychange", (function() {
                            var t = "visible" === document.visibilityState || !1 === document.hidden,
                                o = "hidden" === document.visibilityState || !0 === document.hidden;
                            t && e.wakeup(), o && e.sleep()
                        })), "hidden" !== document.visibilityState && !0 !== document.hidden || this.sleep()
                    }, t
                }(g),
                b = function(e) {
                    function t(t) {
                        return e.call(this, {
                            sleepDelay: t.sleepDelay || 6e5,
                            heartbeatMode: !0,
                            onSleep: t.onSleep,
                            onWakeup: t.onWakeup
                        }) || this
                    }
                    return _(t, e), t.prototype.addEventListenersRecursively = function(e, t) {
                        for (var o = 1, r = window; r !== window.top && o < 5;) r.document.addEventListener(e, t), r = r.parent, o++;
                        r.document.addEventListener(e, t)
                    }, t.prototype.wakeupAndSleep = function(e) {
                        void 0 === e && (e = 0), this.wakeup(e), this.sleep()
                    }, t.prototype.observe = function() {
                        var e = this,
                            t = function(e, t) {
                                void 0 === t && (t = 1e3);
                                var o, r, i, n = Date.now(),
                                    s = function() {
                                        e.apply(r, i)
                                    },
                                    a = function() {
                                        var e = Date.now(),
                                            a = t - (e - n);
                                        r = this, i = arguments, clearTimeout(o), a <= 0 ? (s(), n = Date.now()) : o = setTimeout((function() {
                                            s()
                                        }), a)
                                    },
                                    c = function() {
                                        o && (clearTimeout(o), o = void 0), r = void 0, i = void 0
                                    },
                                    u = function() {
                                        o && (clearTimeout(o), o = void 0), s()
                                    };
                                return a.cancel = c, a.exec = u, a
                            }((function() {
                                e.wakeupAndSleep()
                            }), 1e3);
                        this.addEventListenersRecursively("mousemove", t), this.addEventListenersRecursively("mousedown", t), this.addEventListenersRecursively("keyup", t), this.addEventListenersRecursively("scroll", t), document.addEventListener("visibilitychange", t), t()
                    }, t
                }(g);
            var E = {
                status: 0,
                normalSleepTime: 1800,
                fullscreenSleepTime: 3600,
                tabSleepTime: 1800,
                promptTime: 30
            };

            function A(e) {
                i()(E, e)
            }
            var C = function() {
                    function e() {}
                    return e.filterDataFromServer = function(e) {
                        return e && e.status ? {
                            status: e.status || E.status,
                            normalSleepTime: e.normal_sleep_time > 0 ? e.normal_sleep_time : E.normalSleepTime,
                            fullscreenSleepTime: e.fullscreen_sleep_time > 0 ? e.fullscreen_sleep_time : E.fullscreenSleepTime,
                            tabSleepTime: e.tab_sleep_time > 0 ? e.tab_sleep_time : E.tabSleepTime,
                            promptTime: e.prompt_time > 0 ? e.prompt_time : E.promptTime
                        } : y({}, E)
                    }, e
                }(),
                w = {
                    promptOpened: !1,
                    isTab: !1,
                    currentStatus: 0,
                    reporterStatus: 0,
                    activeType: 0,
                    lastSeconds: 0
                };

            function D(e) {
                i()(w, e)
            }
            var O = {
                DEFAULT_USER_INCASES: [
                    [0, 1800],
                    [30, 1200],
                    [120, 900]
                ],
                DEFAULT_TAB_INCASES: [
                    [30, 1800],
                    [120, 1200],
                    [300, 900]
                ],
                DEFAULT_OUTCASES: [
                    [1800, -1800]
                ],
                MAX_TACTIC_NUMS: 2,
                MAX_TACTIC_STORE: 86400,
                MAX_TACTIC_EXPIRE: 604800,
                MAX_TACTIC_LOG_NUMS: 50,
                OBSERVER_USER_ACTIVITY_SCORE: "__OBSERVER_USER_ACTIVITY_SCORE__",
                OBSERVER_USER_ACTIVITY_FULL_SCORE: "__OBSERVER_USER_ACTIVITY_FULL_SCORE__",
                OBSERVER_TAB_CHANGE_SCORE: "__OBSERVER_TAB_CHANGE_SCORE__"
            };

            function x(e) {
                i()(O, e)
            }
            var I = function() {
                    function e(e) {
                        void 0 === e && (e = 1), this.buffers = [], this.overflowCount = 1, this.overflowCount = e
                    }
                    return e.prototype.getLen = function() {
                        return this.get().length
                    }, e.prototype.get = function() {
                        return this.buffers
                    }, e.prototype.set = function(e) {
                        this.buffers.push(e)
                    }, e.prototype.clear = function() {
                        this.buffers = []
                    }, e.prototype.checkOverflow = function() {
                        return this.getLen() >= this.overflowCount
                    }, e
                }(),
                R = function(e) {
                    function t(t, o, r) {
                        void 0 === r && (r = 604800);
                        var i = e.call(this, o) || this;
                        return i.namespace = t, i.expire = r, i
                    }
                    return _(t, e), t.prototype.getLen = function() {
                        return this.get().buffers.length
                    }, t.prototype.get = function() {
                        try {
                            return JSON.parse(window.localStorage.getItem(this.namespace)) || {
                                buffers: [],
                                logs: []
                            }
                        } catch (e) {
                            return console.error(e), {
                                buffers: [],
                                logs: []
                            }
                        }
                    }, t.prototype.set = function(e) {
                        var t, o = this,
                            r = this.get();
                        r.buffers = r.buffers || [], r.buffers = s()(t = r.buffers).call(t, (function(t) {
                            return Number(t) + 1e3 * o.expire >= Number(e)
                        })), r.buffers.push(e), window.localStorage.setItem(this.namespace, c()(r))
                    }, t.prototype.clear = function() {
                        var e = this.get();
                        e.buffers = [], window.localStorage.setItem(this.namespace, c()(e))
                    }, t
                }(I),
                k = function() {
                    function e(e) {
                        void 0 === e && (e = new I), this.cases = [], this.engine = e
                    }
                    return e.prototype.pushCase = function(e, t) {
                        var o;
                        this.cases.push({
                            step: e,
                            score: t
                        }), this.cases = l()(o = this.cases).call(o, (function(e, t) {
                            return e.step - t.step
                        }))
                    }, e.prototype.checkCase = function(e, t, o) {
                        void 0 === e && (e = "in"), "in" === e ? this.checkInCase(t, o) : this.checkOutCase(t, o)
                    }, e.prototype.checkOutCase = function(e, t) {
                        for (var o = this.cases.length - 1; o >= 0;) {
                            if (this.cases[o].step <= e) {
                                this.engine.set(Date.now());
                                var r = this.engine.checkOverflow();
                                r && this.engine.clear(), t(this.cases[o], r);
                                break
                            }
                            o--
                        }
                    }, e.prototype.checkInCase = function(e, t) {
                        for (var o = this.cases.length, r = 0; r <= o - 1;) {
                            if (this.cases[r].step >= e) {
                                this.engine.set(Date.now());
                                var i = this.engine.checkOverflow();
                                i && (this.engine.clear(), t(this.cases[r], i));
                                break
                            }
                            r++
                        }
                    }, e
                }(),
                L = function() {
                    function e(e) {
                        this.tacticAddDisabled = !1, this.tacticSubDisabled = !1, this.options = y({
                            score: 1800,
                            min: 1800,
                            max: 86400,
                            namespace: "__NAMESPACE__",
                            inCases: [
                                [30, 1800],
                                [60, 1200],
                                [150, 900]
                            ],
                            outCases: [
                                [1800, -1800]
                            ],
                            overflowCount: [1, 2],
                            expire: 604800
                        }, e), this.options.score = this.getNewScore(this.options.score, 0), "defaultScore" in this.options || (this.options.defaultScore = this.options.score), this.init()
                    }
                    return e.prototype.init = function() {
                        this.initTactic()
                    }, e.prototype.initTactic = function() {
                        this.initTacticAdd(), this.initTacticSub()
                    }, e.prototype.initTacticAdd = function() {
                        var e = this,
                            t = new I(this.options.overflowCount[0]);
                        this.tacticAdd = new k(t), this.options.inCases.forEach((function(t) {
                            e.tacticAdd.pushCase(t[0], t[1])
                        }))
                    }, e.prototype.initTacticSub = function() {
                        var e = this,
                            t = new R(this.options.namespace, this.options.overflowCount[1], this.options.expire);
                        this.tacticSub = new k(t), this.options.outCases.forEach((function(t) {
                            e.tacticSub.pushCase(e.options.score + t[0], t[1])
                        }))
                    }, e.prototype.getMinScore = function() {
                        return "function" == typeof this.options.min ? this.options.min() : this.options.min
                    }, e.prototype.getMaxScore = function() {
                        return "function" == typeof this.options.max ? this.options.max() : this.options.max
                    }, e.prototype.getNewScore = function(e, t) {
                        return Math.max(Math.min(e + t, this.getMaxScore()), this.getMinScore())
                    }, e.prototype.setScore = function(e, t, o) {
                        void 0 === t && (t = this.options.score);
                        var r = e > 0,
                            i = this.options.score,
                            n = i,
                            s = i;
                        i >= t ? r ? (n = this.getNewScore(t, e), s = t) : this.getNewScore(i, e) <= t && (n = this.getNewScore(i, e)) : r ? this.getNewScore(i, e) >= t && (n = this.getNewScore(i, e)) : (n = this.getNewScore(t, e), s = t), n !== i && (this.options.score = n, o && o({
                            resultScore: n,
                            sourceScore: s
                        }))
                    }, e.prototype.getDefaultScore = function() {
                        return this.options.defaultScore
                    }, e.prototype.getScore = function() {
                        return this.options.score
                    }, e.prototype.observe = function(e, t, o, r) {
                        var i = this;
                        void 0 === r && (r = this.options.score), this.tacticAddDisabled || this.tacticSubDisabled || (this.tacticAddDisabled || "in" !== e || this.tacticAdd.checkCase(e, t, (function(t, n) {
                            if (i.tacticAddDisabled = !0, n) {
                                var s = i.getDefaultScore();
                                i.setScore(t.score, r, (function(r) {
                                    var i = r.resultScore,
                                        n = r.sourceScore;
                                    o && o({
                                        score: s,
                                        sourceScore: n,
                                        addScore: t.score,
                                        resultScore: i,
                                        type: e
                                    })
                                }))
                            }
                        })), this.tacticSubDisabled || "out" !== e || this.tacticSub.checkCase(e, t, (function(t, n) {
                            if (i.tacticSubDisabled = !0, n) {
                                var s = i.getDefaultScore();
                                i.setScore(t.score, r, (function(r) {
                                    var i = r.resultScore,
                                        n = r.sourceScore;
                                    o && o({
                                        score: s,
                                        sourceScore: n,
                                        addScore: t.score,
                                        resultScore: i,
                                        type: e
                                    })
                                }))
                            }
                        })))
                    }, e
                }(),
                M = {};

            function P(e, t, o) {
                void 0 === o && (o = !1);
                try {
                    var r = !o && M[e] || JSON.parse(window.localStorage.getItem(e)) || {
                        buffers: [],
                        logs: []
                    };
                    return r.score = r.score || t, M[e] || (M[e] = r), r.score
                } catch (e) {
                    return t
                }
            }

            function U(e, t, o) {
                var r, i;
                try {
                    r = JSON.parse(window.localStorage.getItem(e)) || {
                        buffers: [],
                        logs: []
                    }
                } catch (e) {
                    console.error(e), r = {
                        buffers: [],
                        logs: []
                    }
                }(r.score = t, void 0 !== o) && (r.logs = r.logs || [], r.logs = f()(i = function() {
                    for (var e = 0, t = 0, o = arguments.length; t < o; t++) e += arguments[t].length;
                    var r = Array(e),
                        i = 0;
                    for (t = 0; t < o; t++)
                        for (var n = arguments[t], s = 0, a = n.length; s < a; s++, i++) r[i] = n[s];
                    return r
                }(r.logs, [y(y({}, o), {
                    ts: Date.now(),
                    resultScore: t
                })])).call(i, -O.MAX_TACTIC_LOG_NUMS));
                window.localStorage.setItem(e, c()(r))
            }

            function F(e, t, o, r) {
                void 0 === t && (t = 30), void 0 === o && (o = !1), void 0 === r && (r = !1);
                var i = r ? O.OBSERVER_TAB_CHANGE_SCORE : o ? O.OBSERVER_USER_ACTIVITY_FULL_SCORE : O.OBSERVER_USER_ACTIVITY_SCORE,
                    n = r ? O.DEFAULT_TAB_INCASES : O.DEFAULT_USER_INCASES,
                    s = new L({
                        namespace: i,
                        score: P(i, e),
                        min: e,
                        max: O.MAX_TACTIC_STORE,
                        inCases: S()(n).call(n, (function(e) {
                            return [e[0] + t, e[1]]
                        })),
                        outCases: O.DEFAULT_OUTCASES,
                        overflowCount: [1, O.MAX_TACTIC_NUMS],
                        expire: O.MAX_TACTIC_EXPIRE
                    });
                return {
                    observe: function(t, o) {
                        var r = P(i, e, !0);
                        s.observe(t, o, (function(e) {
                            U(i, e.resultScore, e)
                        }), r)
                    },
                    getScore: function() {
                        return s.getScore()
                    }
                }
            }
            var N = {
                tabObserverInst: null,
                userObserverInst: null,
                livePlayer: null,
                StoreUtils: null
            };

            function V(e, t) {
                if (!e) return null;
                var o = e.getPlayerInfo();
                return o ? o[t] : null
            }

            function W(e, t, o, r, i, n, s) {
                void 0 === n && (n = 0), void 0 === s && (s = 0);
                var a = N.livePlayer,
                    c = N.StoreUtils,
                    u = C.filterDataFromServer(c.getPlayerThrottle()),
                    l = u.promptTime,
                    p = u.status,
                    f = c.getDisplayMode();
                return {
                    seconds: Math.floor(t / 1e3),
                    total_seconds: Math.floor(o / 1e3),
                    display_mode: f,
                    guid: V(a, "guid"),
                    action_id: r,
                    playing: V(a, "playingStatus") ? 1 : 0,
                    ts: Date.now(),
                    roomid: c.getRoomId(),
                    live_status: V(a, "liveStatus"),
                    parent_area_id: c.getParentAreaId(),
                    area_id: c.getAreaId(),
                    current_status: w.currentStatus,
                    current_seconds: w.currentStatus ? i : 0,
                    prompt_opened: w.promptOpened ? 1 : 0,
                    reporter_status: w.reporterStatus,
                    active_type: w.activeType,
                    last_seconds: w.lastSeconds,
                    status: p,
                    sleep_time: e,
                    prompt_time: l,
                    score: n,
                    next_score: s
                }
            }

            function B(e, t, o, r, i, n, s, a, c, u) {
                void 0 === s && (s = 0), void 0 === a && (a = 0), void 0 === c && (c = 0), void 0 === u && (u = 0);
                var l = N.livePlayer,
                    p = N.StoreUtils,
                    f = C.filterDataFromServer(p.getPlayerThrottle()),
                    h = f.promptTime,
                    S = f.status;
                return {
                    display_mode: p.getDisplayMode(),
                    guid: V(l, "guid"),
                    action_id: r,
                    playing: V(l, "playingStatus") ? 1 : 0,
                    ts: Date.now(),
                    roomid: p.getRoomId(),
                    live_status: V(l, "liveStatus"),
                    parent_area_id: p.getParentAreaId(),
                    area_id: p.getAreaId(),
                    current_status: w.currentStatus,
                    current_offset_seconds: n,
                    last_duration: t,
                    total_last_duration: Math.floor(o / 1e3),
                    active_type: s,
                    last_seconds: i,
                    status: S,
                    sleep_time: e,
                    prompt_time: h,
                    manual: a,
                    score: c,
                    next_score: u
                }
            }

            function Y(e, t, o) {
                N.livePlayer = e, N.StoreUtils = t, N.tabObserverInst = function(e) {
                    var t = N.livePlayer,
                        o = N.StoreUtils,
                        r = C.filterDataFromServer(o.getPlayerThrottle()),
                        i = r.tabSleepTime,
                        n = r.promptTime,
                        s = 0,
                        a = 0,
                        c = 0,
                        u = 0,
                        l = 0,
                        p = 0,
                        f = G(),
                        h = function() {
                            s = 0, p = 0, c = 0, l = 0, f = G()
                        },
                        S = function() {
                            return {
                                sleepTime: i,
                                isFull: !1
                            }
                        },
                        v = F(S().sleepTime, n, S().isFull, !0);
                    return new T({
                        onSleep: function(o) {
                            ("hidden" === document.visibilityState || !0 === document.hidden) && !w.isTab && D({
                                isTab: !0
                            }), p += o, a = Math.floor(o / 1e3);
                            var r = v.getScore();
                            1 === w.currentStatus && w.promptOpened && v && v.observe("out", p / 1e3);
                            var i, n = W(S().sleepTime, o, p, f, a, r, v.getScore());
                            p / 1e3 >= r && !w.promptOpened && (u = Date.now(), i = "pause"), e.onAfterSleep && e.onAfterSleep({
                                livePlayer: t,
                                sourceType: "tab",
                                reportParams: n,
                                thresholdStatus: i
                            })
                        },
                        onWakeup: function() {
                            w.isTab && setTimeout((function() {
                                D({
                                    isTab: !1
                                })
                            }), 1050);
                            var o, r = v.getScore();
                            0 !== u && w.promptOpened && !w.reporterStatus && (c = Math.floor((Date.now() - u) / 1e3), s = 1 === (l = c > n ? 1 : 2) ? c % (d.DEFAULT_SLEEP_DELAY / 1e3) - n : 0, c = Math.max(0, c - n), o = "play", D({
                                reporterStatus: 1,
                                activeType: l,
                                lastSeconds: c
                            }), v && v.observe("in", c + n));
                            var i = B(S().sleepTime, a, p, f, c, s, l, 0, r, v.getScore());
                            e.onAfterWakeup && e.onAfterWakeup({
                                livePlayer: t,
                                sourceType: "tab",
                                reportParams: i,
                                thresholdStatus: o
                            }), h()
                        },
                        sleepDelay: d.DEFAULT_SLEEP_DELAY
                    })
                }(o), N.userObserverInst = function(e) {
                    var t = N.livePlayer,
                        o = N.StoreUtils,
                        r = C.filterDataFromServer(o.getPlayerThrottle()),
                        i = r.normalSleepTime,
                        n = r.fullscreenSleepTime,
                        s = r.promptTime,
                        a = 0,
                        c = 0,
                        u = 0,
                        l = 0,
                        p = 0,
                        f = 0,
                        h = G(),
                        S = o.getDisplayMode(),
                        v = function() {
                            a = 0, f = 0, u = 0, p = 0, h = G()
                        },
                        m = function() {
                            var e = o.getDisplayMode();
                            return {
                                sleepTime: "normal" === e ? i : n,
                                isFull: "normal" !== e
                            }
                        },
                        _ = F(m().sleepTime, s, m().isFull, !1);
                    return new b({
                        onSleep: function(r) {
                            if (f += r, c = Math.floor(r / 1e3), !("hidden" === document.visibilityState || document.hidden)) {
                                S !== o.getDisplayMode() && (S = o.getDisplayMode(), _ = F(m().sleepTime, s, m().isFull, !1));
                                var i = _.getScore();
                                1 === w.currentStatus && w.promptOpened && _ && _.observe("out", f / 1e3);
                                var n, a = W(m().sleepTime, r, f, h, c, i, _.getScore());
                                0, f / 1e3 >= i && !w.promptOpened && (l = Date.now(), n = "pause"), e.onAfterSleep && e.onAfterSleep({
                                    livePlayer: t,
                                    sourceType: "user",
                                    reportParams: a,
                                    thresholdStatus: n
                                })
                            }
                        },
                        onWakeup: function(o) {
                            var r, i = _.getScore();
                            if (!w.isTab) {
                                0 !== l && w.promptOpened && !w.reporterStatus && (u = Math.floor((Date.now() - l) / 1e3), a = 1 === (p = u > s ? 1 : 2) ? u % (d.DEFAULT_SLEEP_DELAY / 1e3) - s : 0, u = Math.max(0, u - s), r = "play", D({
                                    reporterStatus: 1,
                                    activeType: p,
                                    lastSeconds: u
                                }), _ && _.observe("in", u + s));
                                var n = B(m().sleepTime, c, f, h, u, a, p, o, i, _.getScore());
                                0, e.onAfterWakeup && e.onAfterWakeup({
                                    livePlayer: t,
                                    sourceType: "user",
                                    reportParams: n,
                                    thresholdStatus: r
                                }), v()
                            }
                        },
                        sleepDelay: d.DEFAULT_SLEEP_DELAY
                    })
                }(o)
            }

            function X() {
                return N
            }

            function G() {
                return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (function(e) {
                    var t = 16 * Math.random() | 0;
                    return ("x" === e ? t : 3 & t | 8).toString(16)
                }))
            }
        }
    }
]);