(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [7430], {
        96041: function(t) {
            "undefined" != typeof self && self, t.exports = function(t) {
                function i(r) {
                    if (e[r]) return e[r].exports;
                    var n = e[r] = {
                        i: r,
                        l: !1,
                        exports: {}
                    };
                    return t[r].call(n.exports, n, n.exports, i), n.l = !0, n.exports
                }
                var e = {};
                return i.m = t, i.c = e, i.d = function(t, e, r) {
                    i.o(t, e) || Object.defineProperty(t, e, {
                        configurable: !1,
                        enumerable: !0,
                        get: r
                    })
                }, i.n = function(t) {
                    var e = t && t.__esModule ? function() {
                        return t.default
                    } : function() {
                        return t
                    };
                    return i.d(e, "a", e), e
                }, i.o = function(t, i) {
                    return Object.prototype.hasOwnProperty.call(t, i)
                }, i.p = "", i(i.s = 53)
            }([function(t, i) {
                var e = t.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
                "number" == typeof __g && (__g = e)
            }, function(t, i) {
                var e = t.exports = {
                    version: "2.6.12"
                };
                "number" == typeof __e && (__e = e)
            }, function(t, i, e) {
                var r = e(28)("wks"),
                    n = e(19),
                    o = e(0).Symbol,
                    s = "function" == typeof o;
                (t.exports = function(t) {
                    return r[t] || (r[t] = s && o[t] || (s ? o : n)("Symbol." + t))
                }).store = r
            }, function(t, i, e) {
                var r = e(8);
                t.exports = function(t) {
                    if (!r(t)) throw TypeError(t + " is not an object!");
                    return t
                }
            }, function(t, i, e) {
                var r = e(3),
                    n = e(38),
                    o = e(26),
                    s = Object.defineProperty;
                i.f = e(5) ? Object.defineProperty : function(t, i, e) {
                    if (r(t), i = o(i, !0), r(e), n) try {
                        return s(t, i, e)
                    } catch (t) {}
                    if ("get" in e || "set" in e) throw TypeError("Accessors not supported!");
                    return "value" in e && (t[i] = e.value), t
                }
            }, function(t, i, e) {
                t.exports = !e(12)((function() {
                    return 7 != Object.defineProperty({}, "a", {
                        get: function() {
                            return 7
                        }
                    }).a
                }))
            }, function(t, i, e) {
                var r = e(0),
                    n = e(1),
                    o = e(15),
                    s = e(7),
                    a = e(9),
                    h = function(t, i, e) {
                        var _, u, l, c = t & h.F,
                            p = t & h.G,
                            f = t & h.S,
                            $ = t & h.P,
                            d = t & h.B,
                            g = t & h.W,
                            y = p ? n : n[i] || (n[i] = {}),
                            m = y.prototype,
                            v = p ? r : f ? r[i] : (r[i] || {}).prototype;
                        for (_ in p && (e = i), e)(u = !c && v && void 0 !== v[_]) && a(y, _) || (l = u ? v[_] : e[_], y[_] = p && "function" != typeof v[_] ? e[_] : d && u ? o(l, r) : g && v[_] == l ? function(t) {
                            var i = function(i, e, r) {
                                if (this instanceof t) {
                                    switch (arguments.length) {
                                        case 0:
                                            return new t;
                                        case 1:
                                            return new t(i);
                                        case 2:
                                            return new t(i, e)
                                    }
                                    return new t(i, e, r)
                                }
                                return t.apply(this, arguments)
                            };
                            return i.prototype = t.prototype, i
                        }(l) : $ && "function" == typeof l ? o(Function.call, l) : l, $ && ((y.virtual || (y.virtual = {}))[_] = l, t & h.R && m && !m[_] && s(m, _, l)))
                    };
                h.F = 1, h.G = 2, h.S = 4, h.P = 8, h.B = 16, h.W = 32, h.U = 64, h.R = 128, t.exports = h
            }, function(t, i, e) {
                var r = e(4),
                    n = e(17);
                t.exports = e(5) ? function(t, i, e) {
                    return r.f(t, i, n(1, e))
                } : function(t, i, e) {
                    return t[i] = e, t
                }
            }, function(t, i) {
                t.exports = function(t) {
                    return "object" == typeof t ? null !== t : "function" == typeof t
                }
            }, function(t, i) {
                var e = {}.hasOwnProperty;
                t.exports = function(t, i) {
                    return e.call(t, i)
                }
            }, function(t, i, e) {
                var r = e(58),
                    n = e(24);
                t.exports = function(t) {
                    return r(n(t))
                }
            }, function(t, i) {
                t.exports = !0
            }, function(t, i) {
                t.exports = function(t) {
                    try {
                        return !!t()
                    } catch (t) {
                        return !0
                    }
                }
            }, function(t, i) {
                t.exports = {}
            }, function(t, i) {
                var e = {}.toString;
                t.exports = function(t) {
                    return e.call(t).slice(8, -1)
                }
            }, function(t, i, e) {
                var r = e(16);
                t.exports = function(t, i, e) {
                    if (r(t), void 0 === i) return t;
                    switch (e) {
                        case 1:
                            return function(e) {
                                return t.call(i, e)
                            };
                        case 2:
                            return function(e, r) {
                                return t.call(i, e, r)
                            };
                        case 3:
                            return function(e, r, n) {
                                return t.call(i, e, r, n)
                            }
                    }
                    return function() {
                        return t.apply(i, arguments)
                    }
                }
            }, function(t, i) {
                t.exports = function(t) {
                    if ("function" != typeof t) throw TypeError(t + " is not a function!");
                    return t
                }
            }, function(t, i) {
                t.exports = function(t, i) {
                    return {
                        enumerable: !(1 & t),
                        configurable: !(2 & t),
                        writable: !(4 & t),
                        value: i
                    }
                }
            }, function(t, i, e) {
                var r = e(41),
                    n = e(29);
                t.exports = Object.keys || function(t) {
                    return r(t, n)
                }
            }, function(t, i) {
                var e = 0,
                    r = Math.random();
                t.exports = function(t) {
                    return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++e + r).toString(36))
                }
            }, function(t, i, e) {
                var r = e(4).f,
                    n = e(9),
                    o = e(2)("toStringTag");
                t.exports = function(t, i, e) {
                    t && !n(t = e ? t : t.prototype, o) && r(t, o, {
                        configurable: !0,
                        value: i
                    })
                }
            }, function(t, i, e) {
                t.exports = {
                    default: e(54),
                    __esModule: !0
                }
            }, function(t, i, e) {
                "use strict";
                var r = e(55)(!0);
                e(37)(String, "String", (function(t) {
                    this._t = String(t), this._i = 0
                }), (function() {
                    var t, i = this._t,
                        e = this._i;
                    return e >= i.length ? {
                        value: void 0,
                        done: !0
                    } : (t = r(i, e), this._i += t.length, {
                        value: t,
                        done: !1
                    })
                }))
            }, function(t, i) {
                var e = Math.ceil,
                    r = Math.floor;
                t.exports = function(t) {
                    return isNaN(t = +t) ? 0 : (t > 0 ? r : e)(t)
                }
            }, function(t, i) {
                t.exports = function(t) {
                    if (null == t) throw TypeError("Can't call method on  " + t);
                    return t
                }
            }, function(t, i, e) {
                var r = e(8),
                    n = e(0).document,
                    o = r(n) && r(n.createElement);
                t.exports = function(t) {
                    return o ? n.createElement(t) : {}
                }
            }, function(t, i, e) {
                var r = e(8);
                t.exports = function(t, i) {
                    if (!r(t)) return t;
                    var e, n;
                    if (i && "function" == typeof(e = t.toString) && !r(n = e.call(t))) return n;
                    if ("function" == typeof(e = t.valueOf) && !r(n = e.call(t))) return n;
                    if (!i && "function" == typeof(e = t.toString) && !r(n = e.call(t))) return n;
                    throw TypeError("Can't convert object to primitive value")
                }
            }, function(t, i, e) {
                var r = e(28)("keys"),
                    n = e(19);
                t.exports = function(t) {
                    return r[t] || (r[t] = n(t))
                }
            }, function(t, i, e) {
                var r = e(1),
                    n = e(0),
                    o = n["__core-js_shared__"] || (n["__core-js_shared__"] = {});
                (t.exports = function(t, i) {
                    return o[t] || (o[t] = void 0 !== i ? i : {})
                })("versions", []).push({
                    version: r.version,
                    mode: e(11) ? "pure" : "global",
                    copyright: "© 2020 Denis Pushkarev (zloirock.ru)"
                })
            }, function(t, i) {
                t.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")
            }, function(t, i, e) {
                var r = e(24);
                t.exports = function(t) {
                    return Object(r(t))
                }
            }, function(t, i, e) {
                e(62);
                for (var r = e(0), n = e(7), o = e(13), s = e(2)("toStringTag"), a = "CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,TextTrackList,TouchList".split(","), h = 0; h < a.length; h++) {
                    var _ = a[h],
                        u = r[_],
                        l = u && u.prototype;
                    l && !l[s] && n(l, s, _), o[_] = o.Array
                }
            }, function(t, i, e) {
                "use strict";

                function r(t) {
                    var i, e;
                    this.promise = new t((function(t, r) {
                        if (void 0 !== i || void 0 !== e) throw TypeError("Bad Promise constructor");
                        i = t, e = r
                    })), this.resolve = n(i), this.reject = n(e)
                }
                var n = e(16);
                t.exports.f = function(t) {
                    return new r(t)
                }
            }, function(t, i, e) {
                i.f = e(2)
            }, function(t, i, e) {
                var r = e(0),
                    n = e(1),
                    o = e(11),
                    s = e(33),
                    a = e(4).f;
                t.exports = function(t) {
                    var i = n.Symbol || (n.Symbol = o ? {} : r.Symbol || {});
                    "_" == t.charAt(0) || t in i || a(i, t, {
                        value: s.f(t)
                    })
                }
            }, function(t, i) {
                i.f = {}.propertyIsEnumerable
            }, function(t, i) {}, function(t, i, e) {
                "use strict";
                var r = e(11),
                    n = e(6),
                    o = e(39),
                    s = e(7),
                    a = e(13),
                    h = e(56),
                    _ = e(20),
                    u = e(61),
                    l = e(2)("iterator"),
                    c = !([].keys && "next" in [].keys()),
                    p = function() {
                        return this
                    };
                t.exports = function(t, i, e, f, $, d, g) {
                    h(e, i, f);
                    var y, m, v, S = function(t) {
                            if (!c && t in M) return M[t];
                            switch (t) {
                                case "keys":
                                case "values":
                                    return function() {
                                        return new e(this, t)
                                    }
                            }
                            return function() {
                                return new e(this, t)
                            }
                        },
                        T = i + " Iterator",
                        P = "values" == $,
                        L = !1,
                        M = t.prototype,
                        E = M[l] || M["@@iterator"] || $ && M[$],
                        w = E || S($),
                        x = $ ? P ? S("entries") : w : void 0,
                        O = "Array" == i && M.entries || E;
                    if (O && (v = u(O.call(new t))) !== Object.prototype && v.next && (_(v, T, !0), r || "function" == typeof v[l] || s(v, l, p)), P && E && "values" !== E.name && (L = !0, w = function() {
                            return E.call(this)
                        }), r && !g || !c && !L && M[l] || s(M, l, w), a[i] = w, a[T] = p, $)
                        if (y = {
                                values: P ? w : S("values"),
                                keys: d ? w : S("keys"),
                                entries: x
                            }, g)
                            for (m in y) m in M || o(M, m, y[m]);
                        else n(n.P + n.F * (c || L), i, y);
                    return y
                }
            }, function(t, i, e) {
                t.exports = !e(5) && !e(12)((function() {
                    return 7 != Object.defineProperty(e(25)("div"), "a", {
                        get: function() {
                            return 7
                        }
                    }).a
                }))
            }, function(t, i, e) {
                t.exports = e(7)
            }, function(t, i, e) {
                var r = e(3),
                    n = e(57),
                    o = e(29),
                    s = e(27)("IE_PROTO"),
                    a = function() {},
                    h = function() {
                        var t, i = e(25)("iframe"),
                            r = o.length;
                        for (i.style.display = "none", e(43).appendChild(i), i.src = "javascript:", (t = i.contentWindow.document).open(), t.write("<script>document.F=Object<\/script>"), t.close(), h = t.F; r--;) delete h.prototype[o[r]];
                        return h()
                    };
                t.exports = Object.create || function(t, i) {
                    var e;
                    return null !== t ? (a.prototype = r(t), e = new a, a.prototype = null, e[s] = t) : e = h(), void 0 === i ? e : n(e, i)
                }
            }, function(t, i, e) {
                var r = e(9),
                    n = e(10),
                    o = e(59)(!1),
                    s = e(27)("IE_PROTO");
                t.exports = function(t, i) {
                    var e, a = n(t),
                        h = 0,
                        _ = [];
                    for (e in a) e != s && r(a, e) && _.push(e);
                    for (; i.length > h;) r(a, e = i[h++]) && (~o(_, e) || _.push(e));
                    return _
                }
            }, function(t, i, e) {
                var r = e(23),
                    n = Math.min;
                t.exports = function(t) {
                    return t > 0 ? n(r(t), 9007199254740991) : 0
                }
            }, function(t, i, e) {
                var r = e(0).document;
                t.exports = r && r.documentElement
            }, function(t, i, e) {
                var r = e(14),
                    n = e(2)("toStringTag"),
                    o = "Arguments" == r(function() {
                        return arguments
                    }()),
                    s = function(t, i) {
                        try {
                            return t[i]
                        } catch (t) {}
                    };
                t.exports = function(t) {
                    var i, e, a;
                    return void 0 === t ? "Undefined" : null === t ? "Null" : "string" == typeof(e = s(i = Object(t), n)) ? e : o ? r(i) : "Object" == (a = r(i)) && "function" == typeof i.callee ? "Arguments" : a
                }
            }, function(t, i, e) {
                var r = e(44),
                    n = e(2)("iterator"),
                    o = e(13);
                t.exports = e(1).getIteratorMethod = function(t) {
                    if (null != t) return t[n] || t["@@iterator"] || o[r(t)]
                }
            }, function(t, i, e) {
                var r = e(3),
                    n = e(16),
                    o = e(2)("species");
                t.exports = function(t, i) {
                    var e, s = r(t).constructor;
                    return void 0 === s || null == (e = r(s)[o]) ? i : n(e)
                }
            }, function(t, i, e) {
                var r, n, o, s = e(15),
                    a = e(70),
                    h = e(43),
                    _ = e(25),
                    u = e(0),
                    l = u.process,
                    c = u.setImmediate,
                    p = u.clearImmediate,
                    f = u.MessageChannel,
                    $ = u.Dispatch,
                    d = 0,
                    g = {},
                    y = function() {
                        var t = +this;
                        if (g.hasOwnProperty(t)) {
                            var i = g[t];
                            delete g[t], i()
                        }
                    },
                    m = function(t) {
                        y.call(t.data)
                    };
                c && p || (c = function(t) {
                    for (var i = [], e = 1; arguments.length > e;) i.push(arguments[e++]);
                    return g[++d] = function() {
                        a("function" == typeof t ? t : Function(t), i)
                    }, r(d), d
                }, p = function(t) {
                    delete g[t]
                }, "process" == e(14)(l) ? r = function(t) {
                    l.nextTick(s(y, t, 1))
                } : $ && $.now ? r = function(t) {
                    $.now(s(y, t, 1))
                } : f ? (o = (n = new f).port2, n.port1.onmessage = m, r = s(o.postMessage, o, 1)) : u.addEventListener && "function" == typeof postMessage && !u.importScripts ? (r = function(t) {
                    u.postMessage(t + "", "*")
                }, u.addEventListener("message", m, !1)) : r = "onreadystatechange" in _("script") ? function(t) {
                    h.appendChild(_("script")).onreadystatechange = function() {
                        h.removeChild(this), y.call(t)
                    }
                } : function(t) {
                    setTimeout(s(y, t, 1), 0)
                }), t.exports = {
                    set: c,
                    clear: p
                }
            }, function(t, i) {
                t.exports = function(t) {
                    try {
                        return {
                            e: !1,
                            v: t()
                        }
                    } catch (t) {
                        return {
                            e: !0,
                            v: t
                        }
                    }
                }
            }, function(t, i, e) {
                var r = e(3),
                    n = e(8),
                    o = e(32);
                t.exports = function(t, i) {
                    if (r(t), n(i) && i.constructor === t) return i;
                    var e = o.f(t);
                    return (0, e.resolve)(i), e.promise
                }
            }, function(t, i) {
                i.f = Object.getOwnPropertySymbols
            }, function(t, i, e) {
                var r = e(41),
                    n = e(29).concat("length", "prototype");
                i.f = Object.getOwnPropertyNames || function(t) {
                    return r(t, n)
                }
            }, function(t, i, e) {
                t.exports = {
                    default: e(99),
                    __esModule: !0
                }
            }, function(t, i, e) {
                "use strict";

                function r() {}

                function n() {
                    li || (this.fadeInMsec = 1e3, this.fadeOutMsec = 1e3, this.offsetMsec = 1, this._init())
                }

                function o() {}

                function s() {
                    this.name = null, this.time = null
                }

                function a() {
                    this.m = [1, 0, 0, 0, 1, 0, 0, 0, 1]
                }

                function h() {
                    li || (this._$MT = null, this._$5S = null, this._$NP = 0, h._$42++, this._$5S = new Y(this))
                }

                function _(t) {
                    if (!li) {
                        this.clipContextList = new Array, this.glcontext = t.gl, this.dp_webgl = t, this.curFrameNo = 0, this.firstError_clipInNotUpdate = !0, this.colorBuffer = 0, this.isInitGLFBFunc = !1, this.tmpBoundsOnModel = new E, ti.glContext.length > ti.frameBuffers.length && (this.curFrameNo = this.getMaskRenderTexture()), this.tmpModelToViewMatrix = new R, this.tmpMatrix2 = new R, this.tmpMatrixForMask = new R, this.tmpMatrixForDraw = new R, this.CHANNEL_COLORS = new Array;
                        var i = new I;
                        (i = new I).r = 0, i.g = 0, i.b = 0, i.a = 1, this.CHANNEL_COLORS.push(i), (i = new I).r = 1, i.g = 0, i.b = 0, i.a = 0, this.CHANNEL_COLORS.push(i), (i = new I).r = 0, i.g = 1, i.b = 0, i.a = 0, this.CHANNEL_COLORS.push(i), (i = new I).r = 0, i.g = 0, i.b = 1, i.a = 0, this.CHANNEL_COLORS.push(i);
                        for (var e = 0; e < this.CHANNEL_COLORS.length; e++) this.dp_webgl.setChannelFlagAsColor(e, this.CHANNEL_COLORS[e])
                    }
                }

                function u(t, i, e) {
                    this.clipIDList = new Array, this.clipIDList = e, this.clippingMaskDrawIndexList = new Array;
                    for (var r = 0; r < e.length; r++) this.clippingMaskDrawIndexList.push(i.getDrawDataIndex(e[r]));
                    this.clippedDrawContextList = new Array, this.isUsing = !0, this.layoutChannelNo = 0, this.layoutBounds = new E, this.allClippedDrawRect = new E, this.matrixForMask = new Float32Array(16), this.matrixForDraw = new Float32Array(16), this.owner = t
                }

                function l(t, i) {
                    this._$gP = t, this.drawDataIndex = i
                }

                function c() {
                    li || (this.color = null)
                }

                function p() {
                    li || (this.x = null, this.y = null, this.width = null, this.height = null)
                }

                function f(t) {
                    li || et.prototype.constructor.call(this, t)
                }

                function $() {}

                function d(t) {
                    li || et.prototype.constructor.call(this, t)
                }

                function g() {
                    li || (this._$vo = null, this._$F2 = null, this._$ao = 400, this._$1S = 400, g._$42++)
                }

                function y() {
                    li || (this.p1 = new v, this.p2 = new v, this._$Fo = 0, this._$Db = 0, this._$L2 = 0, this._$M2 = 0, this._$ks = 0, this._$9b = 0, this._$iP = 0, this._$iT = 0, this._$lL = new Array, this._$qP = new Array, this.setup(.3, .5, .1))
                }

                function v() {
                    this._$p = 1, this.x = 0, this.y = 0, this.vx = 0, this.vy = 0, this.ax = 0, this.ay = 0, this.fx = 0, this.fy = 0, this._$s0 = 0, this._$70 = 0, this._$7L = 0, this._$HL = 0
                }

                function S(t, i, e) {
                    this._$wL = null, this.scale = null, this._$V0 = null, this._$wL = t, this.scale = i, this._$V0 = e
                }

                function T(t, i, e, r) {
                    S.prototype.constructor.call(this, i, e, r), this._$tL = null, this._$tL = t
                }

                function P(t, i, e) {
                    this._$wL = null, this.scale = null, this._$V0 = null, this._$wL = t, this.scale = i, this._$V0 = e
                }

                function L(t, i, e, r) {
                    P.prototype.constructor.call(this, i, e, r), this._$YP = null, this._$YP = t
                }

                function M() {
                    li || (this._$fL = 0, this._$gL = 0, this._$B0 = 1, this._$z0 = 1, this._$qT = 0, this.reflectX = !1, this.reflectY = !1)
                }

                function E() {
                    li || (this.x = null, this.y = null, this.width = null, this.height = null)
                }

                function w() {}

                function x() {
                    li || (this.x = null, this.y = null)
                }

                function O() {
                    li || (this._$gP = null, this._$dr = null, this._$GS = null, this._$qb = null, this._$Lb = null, this._$mS = null, this.clipID = null, this.clipIDList = new Array)
                }

                function A() {
                    li || (this._$Eb = A._$ps, this._$lT = 1, this._$C0 = 1, this._$tT = 1, this._$WL = 1, this.culling = !1, this.matrix4x4 = new Float32Array(16), this.premultipliedAlpha = !1, this.anisotropy = 0, this.clippingProcess = A.CLIPPING_PROCESS_NONE, this.clipBufPre_clipContextMask = null, this.clipBufPre_clipContextDraw = null, this.CHANNEL_COLORS = new Array)
                }

                function I() {
                    li || (this.a = 1, this.r = 1, this.g = 1, this.b = 1, this.scale = 1, this._$ho = 1, this.blendMode = ti.L2D_COLOR_BLEND_MODE_MULT)
                }

                function b() {
                    li || (this._$kP = null, this._$dr = null, this._$Ai = !0, this._$mS = null)
                }

                function C() {
                    li || (this._$VP = 0, this._$wL = null, this._$GP = null, this._$8o = C._$ds, this._$2r = -1, this._$O2 = 0, this._$ri = 0)
                }

                function D() {
                    li || (this._$Ob = null)
                }

                function R() {
                    this.m = new Float32Array(16), this.identity()
                }

                function F(t) {
                    li || et.prototype.constructor.call(this, t)
                }

                function N() {
                    li || (this._$7 = 1, this._$f = 0, this._$H = 0, this._$g = 1, this._$k = 0, this._$w = 0, this._$hi = STATE_IDENTITY, this._$Z = _$pS)
                }

                function B() {
                    this._$P = new Float32Array(100), this.size = 0
                }

                function G() {
                    this._$4P = null, this._$I0 = null, this._$RP = null
                }

                function k() {}

                function U() {}

                function Y(t) {
                    li || (this._$QT = !0, this._$co = -1, this._$qo = 0, this._$pb = new Array(Y._$is), this._$_2 = new Float32Array(Y._$is), this._$vr = new Float32Array(Y._$is), this._$Rr = new Float32Array(Y._$is), this._$Or = new Float32Array(Y._$is), this._$fs = new Float32Array(Y._$is), this._$Js = new Array(Y._$is), this._$3S = new Array, this._$aS = new Array, this._$Bo = null, this._$F2 = new Array, this._$db = new Array, this._$8b = new Array, this._$Hr = new Array, this._$Ws = null, this._$Vs = null, this._$Er = null, this._$Es = new Int16Array(k._$Qb), this._$ZP = new Float32Array(2 * k._$1r), this._$Ri = t, this._$b0 = Y._$HP++, this.clipManager = null, this.dp_webgl = null)
                }

                function V() {}

                function j() {
                    li || (this._$12 = null, this._$bb = null, this._$_L = null, this._$jo = null, this._$iL = null, this._$0L = null, this._$Br = null, this._$Dr = null, this._$Cb = null, this._$mr = null, this._$_L = pi.STATE_FIRST, this._$Br = 4e3, this._$Dr = 100, this._$Cb = 50, this._$mr = 150, this._$jo = !0, this._$iL = "PARAM_EYE_L_OPEN", this._$0L = "PARAM_EYE_R_OPEN")
                }

                function X() {
                    li || (A.prototype.constructor.call(this), this._$sb = new Int32Array(X._$As), this._$U2 = new Array, this.transform = null, this.gl = null, null == X._$NT && (X._$NT = X._$9r(256), X._$vS = X._$9r(256), X._$no = X._$vb(256)))
                }

                function H() {
                    li || (b.prototype.constructor.call(this), this._$GS = null, this._$Y0 = null)
                }

                function z(t) {
                    at.prototype.constructor.call(this, t), this._$8r = b._$ur, this._$Yr = null, this._$Wr = null
                }

                function W() {
                    li || (O.prototype.constructor.call(this), this._$gP = null, this._$dr = null, this._$GS = null, this._$qb = null, this._$Lb = null, this._$mS = null)
                }

                function q() {
                    li || (this._$NL = null, this._$3S = null, this._$aS = null, q._$42++)
                }

                function Z() {
                    li || (n.prototype.constructor.call(this), this.motions = [], this._$o2 = null, this._$7r = Z._$Co++, this._$D0 = 30, this._$yT = 0, this._$E = !1, this.loopFadeIn = !0, this._$rr = -1, this._$eP = 0)
                }

                function J(t, i) {
                    return String.fromCharCode(t.getUint8(i))
                }

                function Q() {
                    li || (b.prototype.constructor.call(this), this._$o = 0, this._$A = 0, this._$GS = null, this._$Eo = null)
                }

                function K(t) {
                    at.prototype.constructor.call(this, t), this._$8r = b._$ur, this._$Cr = null, this._$hr = null
                }

                function tt() {
                    li || (this.visible = !0, this._$g0 = !1, this._$NL = null, this._$3S = null, this._$aS = null, tt._$42++)
                }

                function it(t) {
                    this._$VS = null, this._$e0 = null, this._$e0 = t
                }

                function et(t) {
                    li || (this.id = t)
                }

                function rt() {}

                function nt() {
                    li || (this._$4S = null)
                }

                function ot(t, i) {
                    this.canvas = t, this.context = i, this.viewport = new Array(0, 0, t.width, t.height), this._$6r = 1, this._$xP = 0, this._$3r = 1, this._$uP = 0, this._$Qo = -1, this.cacheImages = {}
                }

                function st() {
                    li || (this._$TT = null, this._$LT = null, this._$FS = null, this._$wL = null)
                }

                function at(t) {
                    li || (this._$e0 = null, this._$IP = null, this._$JS = !1, this._$AT = !0, this._$e0 = t, this.totalScale = 1, this._$7s = 1, this.totalOpacity = 1)
                }

                function ht(t) {
                    li || (this._$ib = t)
                }

                function _t() {
                    li || (W.prototype.constructor.call(this), this._$LP = -1, this._$d0 = 0, this._$Yo = 0, this._$JP = null, this._$5P = null, this._$BP = null, this._$Eo = null, this._$Qi = null, this._$6s = _t._$ms, this.culling = !0, this.gl_cacheImage = null, this.instanceNo = _t._$42++)
                }

                function ut(t) {
                    St.prototype.constructor.call(this, t), this._$8r = W._$ur, this._$Cr = null, this._$hr = null
                }

                function lt() {
                    li || (this.x = null, this.y = null)
                }

                function ct(t) {
                    li || (h.prototype.constructor.call(this), this.drawParamWebGL = new dt(t), this.drawParamWebGL.setGL(ti.getGL(t)))
                }

                function pt() {
                    li || (this.motions = null, this._$eb = !1, this.motions = new Array)
                }

                function ft() {
                    this._$w0 = null, this._$AT = !0, this._$9L = !1, this._$z2 = -1, this._$bs = -1, this._$Do = -1, this._$sr = null, this._$sr = ft._$Gs++
                }

                function $t(t) {
                    li || et.prototype.constructor.call(this, t)
                }

                function dt(t) {
                    li || (A.prototype.constructor.call(this), this.textures = new Array, this.transform = null, this.gl = null, this.glno = t, this.firstDraw = !0, this.anisotropyExt = null, this.maxAnisotropy = 0, this._$As = 32, this._$Gr = !1, this._$NT = null, this._$vS = null, this._$no = null, this.vertShader = null, this.fragShader = null, this.vertShaderOff = null, this.fragShaderOff = null)
                }

                function gt(t, i, e) {
                    return null == i && (i = t.createBuffer()), t.bindBuffer(t.ARRAY_BUFFER, i), t.bufferData(t.ARRAY_BUFFER, e, t.DYNAMIC_DRAW), i
                }

                function yt(t, i, e) {
                    return null == i && (i = t.createBuffer()), t.bindBuffer(t.ELEMENT_ARRAY_BUFFER, i), t.bufferData(t.ELEMENT_ARRAY_BUFFER, e, t.DYNAMIC_DRAW), i
                }

                function mt(t) {
                    li || (this._$P = new Int8Array(8), this._$R0 = new DataView(this._$P.buffer), this._$3i = new Int8Array(1e3), this._$hL = 0, this._$v0 = 0, this._$S2 = 0, this._$Ko = new Array, this._$T = t, this._$F = 0)
                }

                function vt() {}

                function St(t) {
                    li || (this._$e0 = null, this._$IP = null, this._$Us = null, this._$7s = null, this._$IS = [!1], this._$VS = null, this._$AT = !0, this.baseOpacity = 1, this.clipBufPre_clipContext = null, this._$e0 = t)
                }

                function Tt() {}

                function Pt() {
                    pt.prototype.constructor.call(this), this.currentPriority = null, this.reservePriority = null, this.super = pt.prototype
                }

                function Lt() {
                    this.tr = new Float32Array(16), this.identity()
                }

                function Mt(t, i) {
                    Lt.prototype.constructor.call(this), this.width = t, this.height = i
                }

                function Et() {
                    n.prototype.constructor.call(this), this.paramList = []
                }

                function wt() {
                    this.id = "", this.type = -1, this.value = null
                }

                function xt() {
                    this.physicsList = [], this.startTimeMSec = ei.getUserTimeMSec()
                }

                function Ot() {
                    this.lastTime = 0, this.lastModel = null, this.partsGroups = []
                }

                function At(t) {
                    this.paramIndex = -1, this.partsIndex = -1, this.link = null, this.id = t
                }

                function It() {
                    this.live2DModel = null, this.modelMatrix = null, this.eyeBlink = null, this.physics = null, this.pose = null, this.debugMode = !1, this.initialized = !1, this.updating = !1, this.alpha = 1, this.accAlpha = 0, this.lipSync = !1, this.lipSyncValue = 0, this.accelX = 0, this.accelY = 0, this.accelZ = 0, this.dragX = 0, this.dragY = 0, this.startTimeMSec = null, this.mainMotionManager = new Pt, this.expressionManager = new Pt, this.motions = {}, this.expressions = {}, this.isTexLoaded = !1
                }

                function bt() {
                    this.nextBlinkTime = null, this.stateStartTime = null, this.blinkIntervalMsec = null, this.eyeState = di.STATE_FIRST, this.blinkIntervalMsec = 4e3, this.closingMotionMsec = 100, this.closedMotionMsec = 50, this.openingMotionMsec = 150, this.closeIfZero = !0, this.eyeID_L = "PARAM_EYE_L_OPEN", this.eyeID_R = "PARAM_EYE_R_OPEN"
                }

                function Ct() {
                    this.EPSILON = .01, this.faceTargetX = 0, this.faceTargetY = 0, this.faceX = 0, this.faceY = 0, this.faceVX = 0, this.faceVY = 0, this.lastTimeSec = 0
                }

                function Dt() {
                    Lt.prototype.constructor.call(this), this.screenLeft = null, this.screenRight = null, this.screenTop = null, this.screenBottom = null, this.maxLeft = null, this.maxRight = null, this.maxTop = null, this.maxBottom = null, this.max = Number.MAX_VALUE, this.min = 0
                }

                function Rt() {
                    this.NAME = "name", this.ID = "id", this.MODEL = "model", this.TEXTURES = "textures", this.HIT_AREAS = "hit_areas", this.PHYSICS = "physics", this.POSE = "pose", this.EXPRESSIONS = "expressions", this.MOTION_GROUPS = "motions", this.SOUND = "sound", this.FADE_IN = "fade_in", this.FADE_OUT = "fade_out", this.LAYOUT = "layout", this.INIT_PARAM = "init_param", this.INIT_PARTS_VISIBLE = "init_parts_visible", this.VALUE = "val", this.FILE = "file", this.json = {}
                }

                function Ft(t) {
                    var i = t.appConfig,
                        e = t.matrixStack;
                    this.appConfig = i, this.matrixStack = e, It.prototype.constructor.call(this), this.modelHomeDir = "", this.modelSetting = null, this.tmpMatrix = []
                }

                function Nt(t) {
                    for (var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {
                            premultipliedAlpha: !0
                        }, e = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"], r = 0; r < e.length; r++) try {
                        var n = t.getContext(e[r], i);
                        if (n) return n
                    } catch (t) {}
                    return null
                }

                function Bt(t) {
                    var i = /((https|http)?:\/\/)[^\s]+/,
                        e = t.match(i);
                    return e && (t = e[0]), t
                }

                function Gt(t) {
                    var i = t.canvas,
                        e = t.appConfig;
                    this.canvas = i, this.appConfig = e
                }

                function kt(t) {
                    var i = t.canvas,
                        e = t.gl,
                        r = t.appConfig,
                        n = t.matrixStack;
                    this.gl = e, this.appConfig = r, this.matrixStack = n, this.models = [], this.totalModels = r.MODELS.length;
                    var o = 0;
                    Object.defineProperty(this, "nextModel", {
                        get: function() {
                            return o
                        },
                        set: function(t) {
                            o = t >= this.totalModels ? 0 : t
                        }
                    }), this.reloadFlg = !1, ti.init(), Tt.setPlatformManager(new Gt({
                        canvas: i,
                        appConfig: r
                    }))
                }

                function Ut() {
                    this.matrixStack = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], this.depth = 0, this.currentMatrix = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1], this.tmp = new Array(16)
                }

                function Yt(t) {
                    this.VERSION = "2.1.00.1-blink-15", this.appConfig = t.appConfig || {};
                    var i = null;
                    return "string" == typeof t.canvas ? i = document.querySelector(t.canvas) : "object" === Zt()(t.canvas) && (i = t.canvas), i ? (this._lastScaleRatio = 1, this._canvas = i, this._gl = Nt(this._canvas), this._gl ? (this.platform = window.navigator.platform.toLowerCase(), this._drawMgr = new Ct, this._viewMatrix = new Dt, this._projMatrix = new Lt, this._matrixStack = new Ut, this._deviceToScreen = new Lt, this.live2DMgr = new kt({
                        canvas: i,
                        gl: this._gl,
                        appConfig: this.appConfig,
                        matrixStack: this._matrixStack
                    }), this._modelInLoading = !1, this._inDrawing = !1, this._inDrag = !1, this._lastDoubleFingersTouchDistance = 0, this._isModelShown = !1, this._lastMouseX = 0, this._lastMouseY = 0, this._modelOnLoadFuncs = [], this._initCanvas(), void this._init()) : Si("无法获取 WebGL Context, Live2D 退出.")) : Li('Canvas 节点 "' + t._canvas + '" 不存在，Live2D 退出.')
                }

                function Vt(t) {
                    var i = this._deviceToScreen.transformX(t);
                    return this._viewMatrix.invertTransformX(i)
                }

                function jt(t) {
                    var i = this._deviceToScreen.transformY(t);
                    return this._viewMatrix.invertTransformY(i)
                }

                function Xt(t) {
                    return this._deviceToScreen.transformX(t)
                }

                function Ht(t) {
                    return this._deviceToScreen.transformY(t)
                }
                Object.defineProperty(i, "__esModule", {
                    value: !0
                });
                var zt = e(21),
                    Wt = e.n(zt),
                    qt = e(78),
                    Zt = e.n(qt),
                    Jt = {
                        initTimeMSec: 0,
                        SYSTEM_INFO: null
                    };
                Jt.USER_AGENT = navigator.userAgent, Jt.isIPhone = function() {
                    return Jt.SYSTEM_INFO || Jt.setup(), Jt.SYSTEM_INFO._isIPhone
                }, Jt.isIOS = function() {
                    return Jt.SYSTEM_INFO || Jt.setup(), Jt.SYSTEM_INFO._isIPhone || Jt.SYSTEM_INFO._isIPad
                }, Jt.isAndroid = function() {
                    return Jt.SYSTEM_INFO || Jt.setup(), Jt.SYSTEM_INFO._isAndroid
                }, Jt.getOSVersion = function() {
                    return Jt.SYSTEM_INFO || Jt.setup(), Jt.SYSTEM_INFO.version
                }, Jt.getOS = function() {
                    return Jt.SYSTEM_INFO || Jt.setup(), Jt.SYSTEM_INFO._isIPhone || Jt.SYSTEM_INFO._isIPad ? "iOS" : Jt.SYSTEM_INFO._isAndroid ? "Android" : "_$Q0 OS"
                }, Jt.setup = function() {
                    function t(t, i) {
                        for (var e = t.substring(i).split(/[ _,;\.]/), r = 0, n = 0; n <= 2 && !isNaN(e[n]); n++) {
                            var o = parseInt(e[n]);
                            if (o < 0 || o > 999) {
                                UtDebug.println("err : " + o + " @UtHtml5.setup()"), r = 0;
                                break
                            }
                            r += o * Math.pow(1e3, 2 - n)
                        }
                        return r
                    }
                    var i, e = Jt.USER_AGENT,
                        r = Jt.SYSTEM_INFO = {
                            userAgent: e
                        };
                    if ((i = e.indexOf("iPhone OS ")) >= 0) r.os = "iPhone", r._isIPhone = !0, r.version = t(e, i + "iPhone OS ".length);
                    else if ((i = e.indexOf("iPad")) >= 0) {
                        if ((i = e.indexOf("CPU OS")) < 0) return void UtDebug.println(" err : " + e + " @UtHtml5.setup()");
                        r.os = "iPad", r._isIPad = !0, r.version = t(e, i + "CPU OS ".length)
                    } else(i = e.indexOf("Android")) >= 0 ? (r.os = "Android", r._isAndroid = !0, r.version = t(e, i + "Android ".length)) : (r.os = "-", r.version = -1)
                };
                var Qt = Jt,
                    Kt = {
                        version: "2.1.00_1",
                        versionNumber: 201001e3
                    };
                Kt.debugLog = "development" === {
                    LIVE2D_APP_BUILD_VERSION: "2.1.00.1-blink-15"
                }.NODE_ENV || !1, Kt.L2D_DEFORMER_EXTEND = !0, Kt.L2D_NO_ERROR = 0, Kt._$sP = !0, Kt._$so = !0, Kt._$cb = !1, Kt._$3T = !0, Kt._$Ts = !0, Kt._$fb = !0, Kt._$ts = !0, Kt._$Wb = !1, Kt._$yr = !1, Kt._$Zs = !1, Kt._$i7 = 1e3, Kt._$9s = 1001, Kt._$es = 1100, Kt._$r7 = 2e3, Kt._$07 = 2001, Kt._$b7 = 2002, Kt._$H7 = 4e3, Kt.L2D_COLOR_BLEND_MODE_MULT = 0, Kt.L2D_COLOR_BLEND_MODE_ADD = 1, Kt.L2D_COLOR_BLEND_MODE_INTERPOLATE = 2, Kt._$cT = 0, Kt.clippingMaskBufferSize = 256, Kt.glContext = [], Kt.frameBuffers = [], Kt.fTexture = [], Kt.IGNORE_CLIP = !1, Kt.IGNORE_EXPAND = !1, Kt.EXPAND_W = 2, Kt.USE_ADJUST_TRANSLATION = !0, Kt.USE_CANVAS_TRANSFORM = !0, Kt.USE_CACHED_POLYGON_IMAGE = !1, Kt.DEBUG_DATA = {}, Kt.PROFILE_IOS_SPEED = {
                    PROFILE_NAME: "iOS Speed",
                    USE_ADJUST_TRANSLATION: !0,
                    USE_CACHED_POLYGON_IMAGE: !0,
                    EXPAND_W: 4
                }, Kt.PROFILE_IOS_QUALITY = {
                    PROFILE_NAME: "iOS HiQ",
                    USE_ADJUST_TRANSLATION: !0,
                    USE_CACHED_POLYGON_IMAGE: !1,
                    EXPAND_W: 2
                }, Kt.PROFILE_IOS_DEFAULT = Kt.PROFILE_IOS_QUALITY, Kt.PROFILE_ANDROID = {
                    PROFILE_NAME: "Android",
                    USE_ADJUST_TRANSLATION: !1,
                    USE_CACHED_POLYGON_IMAGE: !1,
                    EXPAND_W: 2
                }, Kt.PROFILE_DESKTOP = {
                    PROFILE_NAME: "Desktop",
                    USE_ADJUST_TRANSLATION: !1,
                    USE_CACHED_POLYGON_IMAGE: !1,
                    EXPAND_W: 2
                }, Kt.initProfile = function() {
                    Qt.isIOS() ? Kt.setupProfile(Kt.PROFILE_IOS_DEFAULT) : Qt.isAndroid() ? Kt.setupProfile(Kt.PROFILE_ANDROID) : Kt.setupProfile(Kt.PROFILE_DESKTOP)
                }, Kt.setupProfile = function(t, i) {
                    if ("number" == typeof t) switch (t) {
                        case 9901:
                            t = Kt.PROFILE_IOS_SPEED;
                            break;
                        case 9902:
                            t = Kt.PROFILE_IOS_QUALITY;
                            break;
                        case 9903:
                            t = Kt.PROFILE_IOS_DEFAULT;
                            break;
                        case 9904:
                            t = Kt.PROFILE_ANDROID;
                            break;
                        case 9905:
                            t = Kt.PROFILE_DESKTOP;
                            break;
                        default:
                            alert("profile _$6 _$Ui : " + t)
                    }
                    for (var e in arguments.length < 2 && (i = !0), i && console.log("profile : " + t.PROFILE_NAME), t) Kt[e] = t[e], i && console.log("  [" + e + "] = " + t[e])
                }, Kt.init = function() {
                    Kt.debugLog && (console.log("Live2D %s", Kt.version), Kt.debugLog = !1, Kt.initProfile())
                }, Kt.getVersionStr = function() {
                    return Kt.version
                }, Kt.getVersionNo = function() {
                    return Kt.versionNumber
                }, Kt._$sT = function(t) {
                    Kt._$cT = t
                }, Kt.getError = function() {
                    var t = Kt._$cT;
                    return Kt._$cT = 0, t
                }, Kt.dispose = function() {
                    Kt.glContext = [], Kt.frameBuffers = [], Kt.fTexture = []
                }, Kt.setGL = function(t) {
                    var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
                    Kt.glContext[i] = t
                }, Kt.getGL = function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
                    return Kt.glContext[t]
                }, Kt.setClippingMaskBufferSize = function(t) {
                    Kt.clippingMaskBufferSize = t
                }, Kt.getClippingMaskBufferSize = function() {
                    return Kt.clippingMaskBufferSize
                }, Kt.deleteBuffer = function(t) {
                    Kt.getGL(t).deleteFramebuffer(Kt.frameBuffers[t].framebuffer), delete Kt.frameBuffers[t], delete Kt.glContext[t]
                };
                var ti = Kt,
                    ii = {
                        initTimeMSec: 0,
                        userTimeMSec: 0,
                        getTimeMSec: function() {
                            return Date.now()
                        },
                        getSystemTimeMSec: function() {
                            return this.getTimeMSec()
                        },
                        getUserTimeMSec: function() {
                            return this.userTimeMSec === this.initTimeMSec ? this.getTimeMSec() : this.userTimeMSec
                        },
                        setUserTimeMSec: function(t) {
                            this.userTimeMSec = t
                        },
                        updateUserTimeMSec: function() {
                            this.userTimeMSec = this.getTimeMSec()
                        },
                        _$jT: function(t, i, e, r, n) {
                            for (var o = 0; o < n; o++) e[r + o] = t[i + o]
                        }
                    },
                    ei = ii;
                r._$2S = Math.PI / 180, r._$bS = Math.PI / 180, r._$wS = 180 / Math.PI, r._$NS = 180 / Math.PI, r.PI_F = Math.PI, r._$kT = [0, .012368, .024734, .037097, .049454, .061803, .074143, .086471, .098786, .111087, .12337, .135634, .147877, .160098, .172295, .184465, .196606, .208718, .220798, .232844, .244854, .256827, .268761, .280654, .292503, .304308, .316066, .327776, .339436, .351044, .362598, .374097, .385538, .396921, .408243, .419502, .430697, .441826, .452888, .463881, .474802, .485651, .496425, .507124, .517745, .528287, .538748, .549126, .559421, .56963, .579752, .589785, .599728, .609579, .619337, .629, .638567, .648036, .657406, .666676, .675843, .684908, .693867, .70272, .711466, .720103, .72863, .737045, .745348, .753536, .76161, .769566, .777405, .785125, .792725, .800204, .807561, .814793, .821901, .828884, .835739, .842467, .849066, .855535, .861873, .868079, .874153, .880093, .885898, .891567, .897101, .902497, .907754, .912873, .917853, .922692, .92739, .931946, .936359, .940629, .944755, .948737, .952574, .956265, .959809, .963207, .966457, .96956, .972514, .97532, .977976, .980482, .982839, .985045, .987101, .989006, .990759, .992361, .993811, .995109, .996254, .997248, .998088, .998776, .999312, .999694, .999924, 1], r._$92 = function(t, i) {
                    var e = Math.atan2(t[1], t[0]),
                        n = Math.atan2(i[1], i[0]);
                    return r._$tS(e, n)
                }, r._$tS = function(t, i) {
                    for (var e = t - i; e < -Math.PI;) e += 2 * Math.PI;
                    for (; e > Math.PI;) e -= 2 * Math.PI;
                    return e
                }, r._$9 = function(t) {
                    return Math.sin(t)
                }, r.fcos = function(t) {
                    return Math.cos(t)
                }, n.prototype._init = function() {}, n._$JT = function(t, i, e) {
                    var r = t / i,
                        n = e / i,
                        o = 1 - (1 - n) * (1 - n),
                        s = 1 - (1 - n) * (1 - n),
                        a = 1 / 3 * (1 - n) * o + (n * (2 / 3) + 1 / 3 * (1 - n)) * (1 - o),
                        h = (n + 2 / 3 * (1 - n)) * s + (n * (1 / 3) + 2 / 3 * (1 - n)) * (1 - s);
                    if (r <= 0) return 0;
                    if (r >= 1) return 1;
                    var _ = r * r;
                    return r * _ * (1 - 3 * h + 3 * a - 0) + (3 * h - 6 * a + 0) * _ + (3 * a - 0) * r + 0
                }, n.prototype.setFadeIn = function(t) {
                    this.fadeInMsec = t
                }, n.prototype.getFadeOut = function() {
                    return this.fadeOutMsec
                }, n.prototype.setFadeOut = function(t) {
                    this.fadeOutMsec = t
                }, n.prototype.getOffsetMSec = function() {
                    return this.offsetMsec
                }, n.prototype.setOffsetMSec = function(t) {
                    this.offsetMsec = t
                }, n.prototype.getDurationMSec = function() {
                    return -1
                }, n.prototype.getLoopDurationMSec = function() {
                    return -1
                }, n.prototype.updateParam = function(t, i) {
                    if (i._$AT && !i._$9L) {
                        var e = ei.getUserTimeMSec();
                        if (i._$z2 < 0) {
                            i._$z2 = e, i._$bs = e;
                            var r = this.getDurationMSec();
                            i._$Do < 0 && (i._$Do = r <= 0 ? -1 : i._$z2 + r)
                        }
                        var n = this.offsetMsec;
                        0 <= (n = n * (0 == this.fadeInMsec ? 1 : o._$r2((e - i._$bs) / this.fadeInMsec)) * (0 == this.fadeOutMsec || i._$Do < 0 ? 1 : o._$r2((i._$Do - e) / this.fadeOutMsec))) && n <= 1 || console.log("### assert!! ### "), this.updateParamExe(t, e, n, i), i._$Do > 0 && i._$Do < e && (i._$9L = !0)
                    }
                }, n.prototype.updateParamExe = function(t, i, e, r) {}, o._$r2 = function(t) {
                    return t < 0 ? 0 : t > 1 ? 1 : .5 - .5 * Math.cos(t * r.PI_F)
                };
                var ri = {
                        _dumps: {},
                        start: function() {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                                i = this._dumps[t];
                            i || ((i = new s).name = t, this._dumps[t] = i), i.time = ei.getSystemTimeMSec()
                        },
                        dump: function() {
                            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
                                i = this._dumps[t];
                            if (i) {
                                var e = ei.getSystemTimeMSec() - i.time;
                                return window.console && console.log("[Live2D Debug] " + t + ": " + e + "ms"), e
                            }
                            return -1
                        },
                        end: function(t) {
                            var i = this._dumps[t];
                            return i ? -1 : ei.getSystemTimeMSec() - i.time
                        },
                        error: function(t) {
                            window.console && (console.error("[Live2D] Dump Exception : " + t), console.error("[Live2D] Stack :: " + t.stack))
                        },
                        println: function(t, i) {
                            window.console && console.error("[Live2D] Error : " + t + "\n", i)
                        },
                        print: function(t, i) {
                            window.console && console.error(t, i)
                        }
                    },
                    ni = ri;
                a.prototype.setContext = function(t) {
                    var i = this.m;
                    t.transform(i[0], i[1], i[3], i[4], i[6], i[7])
                }, a.prototype.toString = function() {
                    for (var t = "LDTransform { ", i = 0; i < 9; i++) t += this.m[i].toFixed(2) + " ,";
                    return t + " }"
                }, a.prototype.identity = function() {
                    var t = this.m;
                    t[0] = t[4] = t[8] = 1, t[1] = t[2] = t[3] = t[5] = t[6] = t[7] = 0
                }, a.prototype._$PS = function(t, i, e) {
                    null == e && (e = [0, 0]);
                    var r = this.m;
                    return e[0] = r[0] * t + r[3] * i + r[6], e[1] = r[1] * t + r[4] * i + r[7], e
                }, a.prototype._$P2 = function(t) {
                    t || (t = new a);
                    var i = this.m,
                        e = i[0],
                        r = i[1],
                        n = i[2],
                        o = i[3],
                        s = i[4],
                        h = i[5],
                        _ = i[6],
                        u = i[7],
                        l = i[8],
                        c = e * s * l + r * h * _ + n * o * u - e * h * u - n * s * _ - r * o * l;
                    if (0 == c) return null;
                    var p = 1 / c;
                    return t.m[0] = p * (s * l - u * h), t.m[1] = p * (u * n - r * l), t.m[2] = p * (r * h - s * n), t.m[3] = p * (_ * h - o * l), t.m[4] = p * (e * l - _ * n), t.m[5] = p * (o * n - e * h), t.m[6] = p * (o * u - _ * s), t.m[7] = p * (_ * r - e * u), t.m[8] = p * (e * s - o * r), t
                }, a.prototype.transform = function(t, i, e) {
                    null == e && (e = [0, 0]);
                    var r = this.m;
                    return e[0] = r[0] * t + r[3] * i + r[6], e[1] = r[1] * t + r[4] * i + r[7], e
                }, a.prototype.translate = function(t, i) {
                    var e = this.m;
                    e[6] = e[0] * t + e[3] * i + e[6], e[7] = e[1] * t + e[4] * i + e[7], e[8] = e[2] * t + e[5] * i + e[8]
                }, a.prototype.scale = function(t, i) {
                    var e = this.m;
                    e[0] *= t, e[1] *= t, e[2] *= t, e[3] *= i, e[4] *= i, e[5] *= i
                }, a.prototype.shear = function(t, i) {
                    var e = this.m,
                        r = e[0] + e[3] * i,
                        n = e[1] + e[4] * i,
                        o = e[2] + e[5] * i;
                    e[3] = e[0] * t + e[3], e[4] = e[1] * t + e[4], e[5] = e[2] * t + e[5], e[0] = r, e[1] = n, e[2] = o
                }, a.prototype.rotate = function(t) {
                    var i = this.m,
                        e = Math.cos(t),
                        r = Math.sin(t),
                        n = i[0] * e + i[3] * r,
                        o = i[1] * e + i[4] * r,
                        s = i[2] * e + i[5] * r;
                    i[3] = -i[0] * r + i[3] * e, i[4] = -i[1] * r + i[4] * e, i[5] = -i[2] * r + i[5] * e, i[0] = n, i[1] = o, i[2] = s
                }, a.prototype.concatenate = function(t) {
                    var i = this.m,
                        e = t.m,
                        r = i[0] * e[0] + i[3] * e[1] + i[6] * e[2],
                        n = i[1] * e[0] + i[4] * e[1] + i[7] * e[2],
                        o = i[2] * e[0] + i[5] * e[1] + i[8] * e[2],
                        s = i[0] * e[3] + i[3] * e[4] + i[6] * e[5],
                        a = i[1] * e[3] + i[4] * e[4] + i[7] * e[5],
                        h = i[2] * e[3] + i[5] * e[4] + i[8] * e[5],
                        _ = i[0] * e[6] + i[3] * e[7] + i[6] * e[8],
                        u = i[1] * e[6] + i[4] * e[7] + i[7] * e[8],
                        l = i[2] * e[6] + i[5] * e[7] + i[8] * e[8];
                    m[0] = r, m[1] = n, m[2] = o, m[3] = s, m[4] = a, m[5] = h, m[6] = _, m[7] = u, m[8] = l
                };
                var oi = a,
                    si = e(91),
                    ai = e.n(si),
                    hi = e(92),
                    _i = e.n(hi),
                    ui = function() {
                        function t() {
                            ai()(this, t)
                        }
                        return _i()(t, null, [{
                            key: "startsWith",
                            value: function(i, e, r) {
                                var n = e + r.length;
                                if (n >= i.byteLength) return !1;
                                for (var o = e; o < n; o++)
                                    if (t.getChar(i, o) !== r.charAt(o - e)) return !1;
                                return !0
                            }
                        }, {
                            key: "getChar",
                            value: function(t, i) {
                                return String.fromCharCode(t.getUint8(i))
                            }
                        }, {
                            key: "createString",
                            value: function(t, i, e) {
                                for (var r = new ArrayBuffer(2 * e), n = new Uint16Array(r), o = 0; o < e; o++) n[o] = t.getUint8(i + o);
                                return String.fromCharCode.apply(null, n)
                            }
                        }, {
                            key: "_$LS",
                            value: function(i, e, r, n) {
                                i instanceof ArrayBuffer && (i = new DataView(i));
                                var o = r,
                                    s = !1,
                                    a = !1,
                                    h = 0,
                                    _ = !1,
                                    u = t.getChar(i, o);
                                for ("-" === u && (s = !0, o++); o < e; o++) {
                                    switch (u = t.getChar(i, o)) {
                                        case "0":
                                            h *= 10;
                                            break;
                                        case "1":
                                            h = 10 * h + 1;
                                            break;
                                        case "2":
                                            h = 10 * h + 2;
                                            break;
                                        case "3":
                                            h = 10 * h + 3;
                                            break;
                                        case "4":
                                            h = 10 * h + 4;
                                            break;
                                        case "5":
                                            h = 10 * h + 5;
                                            break;
                                        case "6":
                                            h = 10 * h + 6;
                                            break;
                                        case "7":
                                            h = 10 * h + 7;
                                            break;
                                        case "8":
                                            h = 10 * h + 8;
                                            break;
                                        case "9":
                                            h = 10 * h + 9;
                                            break;
                                        case ".":
                                            a = !0, o++, _ = !0;
                                            break;
                                        default:
                                            _ = !0
                                    }
                                    if (_) break
                                }
                                if (a)
                                    for (var l = .1, c = !1; o < e; o++) {
                                        switch (u = t.getChar(i, o)) {
                                            case "0":
                                                break;
                                            case "1":
                                                h += 1 * l;
                                                break;
                                            case "2":
                                                h += 2 * l;
                                                break;
                                            case "3":
                                                h += 3 * l;
                                                break;
                                            case "4":
                                                h += 4 * l;
                                                break;
                                            case "5":
                                                h += 5 * l;
                                                break;
                                            case "6":
                                                h += 6 * l;
                                                break;
                                            case "7":
                                                h += 7 * l;
                                                break;
                                            case "8":
                                                h += 8 * l;
                                                break;
                                            case "9":
                                                h += 9 * l;
                                                break;
                                            default:
                                                c = !0
                                        }
                                        if (l *= .1, c) break
                                    }
                                return s && (h = -h), n[0] = o, h
                            }
                        }]), t
                    }(),
                    li = !1;
                h._$0s = 1, h._$4s = 2, h._$42 = 0, h._$62 = function(t, i) {
                    try {
                        if (i instanceof ArrayBuffer && (i = new DataView(i)), !(i instanceof DataView)) throw new ht("_$SS#loadModel(b) / b _$x be DataView or ArrayBuffer");
                        var e, r = new mt(i),
                            n = r._$ST(),
                            o = r._$ST(),
                            s = r._$ST();
                        if (109 != n || 111 != o || 99 != s) throw new ht("_$gi _$C _$li , _$Q0 _$P0.");
                        if (e = r._$ST(), r._$gr(e), e > U._$T7) throw t._$NP |= h._$4s, new ht("_$gi _$C _$li , _$n0 _$_ version _$li ( SDK : " + U._$T7 + " < _$f0 : " + e + " )@_$SS#loadModel()\n");
                        var a = r._$nP();
                        if (e >= U._$s7) {
                            var _ = r._$9T(),
                                u = r._$9T();
                            if (-30584 != _ || -30584 != u) throw t._$NP |= h._$0s, new ht("_$gi _$C _$li , _$0 _$6 _$Ui.")
                        }
                        t._$KS(a);
                        var l = t.getModelContext();
                        l.setDrawParam(t.getDrawParam()), l.init()
                    } catch (t) {
                        ni.error(t)
                    }
                }, h.prototype._$KS = function(t) {
                    this._$MT = t
                }, h.prototype.getModelImpl = function() {
                    return null == this._$MT && (this._$MT = new g, this._$MT._$zP()), this._$MT
                }, h.prototype.getCanvasWidth = function() {
                    return null == this._$MT ? 0 : this._$MT.getCanvasWidth()
                }, h.prototype.getCanvasHeight = function() {
                    return null == this._$MT ? 0 : this._$MT.getCanvasHeight()
                }, h.prototype.getParamFloat = function(t) {
                    return "number" != typeof t && (t = this._$5S.getParamIndex(d.getID(t))), this._$5S.getParamFloat(t)
                }, h.prototype.setParamFloat = function(t, i, e) {
                    "number" != typeof t && (t = this._$5S.getParamIndex(d.getID(t))), arguments.length < 3 && (e = 1), this._$5S.setParamFloat(t, this._$5S.getParamFloat(t) * (1 - e) + i * e)
                }, h.prototype.addToParamFloat = function(t, i, e) {
                    "number" != typeof t && (t = this._$5S.getParamIndex(d.getID(t))), arguments.length < 3 && (e = 1), this._$5S.setParamFloat(t, this._$5S.getParamFloat(t) + i * e)
                }, h.prototype.multParamFloat = function(t, i, e) {
                    "number" != typeof t && (t = this._$5S.getParamIndex(d.getID(t))), arguments.length < 3 && (e = 1), this._$5S.setParamFloat(t, this._$5S.getParamFloat(t) * (1 + (i - 1) * e))
                }, h.prototype.getParamIndex = function(t) {
                    return this._$5S.getParamIndex(d.getID(t))
                }, h.prototype.loadParam = function() {
                    this._$5S.loadParam()
                }, h.prototype.saveParam = function() {
                    this._$5S.saveParam()
                }, h.prototype.init = function() {
                    this._$5S.init()
                }, h.prototype.update = function() {
                    this._$5S.update()
                }, h.prototype._$Rs = function() {
                    return ni.println("_$60 _$PT _$Rs()"), -1
                }, h.prototype._$Ds = function(t) {
                    ni.println("_$60 _$PT _$SS#_$Ds() \n")
                }, h.prototype._$K2 = function() {}, h.prototype.draw = function() {}, h.prototype.getModelContext = function() {
                    return this._$5S
                }, h.prototype.setPartsOpacity = function(t, i) {
                    "number" != typeof t && (t = this._$5S.getPartsDataIndex(f.getID(t))), this._$5S.setPartsOpacity(t, i)
                }, h.prototype.getPartsDataIndex = function(t) {
                    return t instanceof f || (t = f.getID(t)), this._$5S.getPartsDataIndex(t)
                }, h.prototype.getPartsOpacity = function(t) {
                    return "number" != typeof t && (t = this._$5S.getPartsDataIndex(f.getID(t))), t < 0 ? 0 : this._$5S.getPartsOpacity(t)
                }, h.prototype.getDrawParam = function() {}, h.prototype.getDrawDataIndex = function(t) {
                    return this._$5S.getDrawDataIndex(F.getID(t))
                }, h.prototype.getDrawData = function(t) {
                    return this._$5S.getDrawData(t)
                }, h.prototype.getTransformedPoints = function(t) {
                    var i = this._$5S._$C2(t);
                    return i instanceof ut ? i.getTransformedPoints() : null
                }, h.prototype.getIndexArray = function(t) {
                    if (t < 0 || t >= this._$5S._$aS.length) return null;
                    var i = this._$5S._$aS[t];
                    return null != i && i.getType() == W._$wb && i instanceof _t ? i.getIndexArray() : null
                }, _.CHANNEL_COUNT = 4, _.RENDER_TEXTURE_USE_MIPMAP = !1, _.NOT_USED_FRAME = -100, _.prototype._$L7 = function() {
                    if (this.tmpModelToViewMatrix && (this.tmpModelToViewMatrix = null), this.tmpMatrix2 && (this.tmpMatrix2 = null), this.tmpMatrixForMask && (this.tmpMatrixForMask = null), this.tmpMatrixForDraw && (this.tmpMatrixForDraw = null), this.tmpBoundsOnModel && (this.tmpBoundsOnModel = null), this.CHANNEL_COLORS) {
                        for (var t = this.CHANNEL_COLORS.length - 1; t >= 0; --t) this.CHANNEL_COLORS.splice(t, 1);
                        this.CHANNEL_COLORS = []
                    }
                    this.releaseShader()
                }, _.prototype.releaseShader = function() {
                    for (var t = ti.frameBuffers.length, i = 0; i < t; i++) this.gl.deleteFramebuffer(ti.frameBuffers[i].framebuffer);
                    ti.frameBuffers = [], ti.glContext = []
                }, _.prototype.init = function(t, i, e) {
                    for (var r = 0; r < i.length; r++) {
                        var n = i[r].getClipIDList();
                        if (null != n) {
                            var o = this.findSameClip(n);
                            null == o && (o = new u(this, t, n), this.clipContextList.push(o));
                            var s = i[r].getDrawDataID(),
                                a = t.getDrawDataIndex(s);
                            o.addClippedDrawData(s, a), e[r].clipBufPre_clipContext = o
                        }
                    }
                }, _.prototype.getMaskRenderTexture = function() {
                    var t = null;
                    return t = this.dp_webgl.createFramebuffer(), ti.frameBuffers[this.dp_webgl.glno] = t, this.dp_webgl.glno
                }, _.prototype.setupClip = function(t, i) {
                    for (var e = 0, r = 0; r < this.clipContextList.length; r++) {
                        var n = this.clipContextList[r];
                        this.calcClippedDrawTotalBounds(t, n), n.isUsing && e++
                    }
                    if (e > 0) {
                        var o = i.gl.getParameter(i.gl.FRAMEBUFFER_BINDING),
                            s = new Array(4);
                        for (s[0] = 0, s[1] = 0, s[2] = i.gl.canvas.width, s[3] = i.gl.canvas.height, i.gl.viewport(0, 0, ti.clippingMaskBufferSize, ti.clippingMaskBufferSize), this.setupLayoutBounds(e), i.gl.bindFramebuffer(i.gl.FRAMEBUFFER, ti.frameBuffers[this.curFrameNo].framebuffer), i.gl.clearColor(0, 0, 0, 0), i.gl.clear(i.gl.COLOR_BUFFER_BIT), r = 0; r < this.clipContextList.length; r++) {
                            var a = (n = this.clipContextList[r]).allClippedDrawRect,
                                h = (n.layoutChannelNo, n.layoutBounds);
                            this.tmpBoundsOnModel._$jL(a), this.tmpBoundsOnModel.expand(.05 * a.width, .05 * a.height);
                            var _ = h.width / this.tmpBoundsOnModel.width,
                                u = h.height / this.tmpBoundsOnModel.height;
                            this.tmpMatrix2.identity(), this.tmpMatrix2.translate(-1, -1, 0), this.tmpMatrix2.scale(2, 2, 1), this.tmpMatrix2.translate(h.x, h.y, 0), this.tmpMatrix2.scale(_, u, 1), this.tmpMatrix2.translate(-this.tmpBoundsOnModel.x, -this.tmpBoundsOnModel.y, 0), this.tmpMatrixForMask.setMatrix(this.tmpMatrix2.m), this.tmpMatrix2.identity(), this.tmpMatrix2.translate(h.x, h.y, 0), this.tmpMatrix2.scale(_, u, 1), this.tmpMatrix2.translate(-this.tmpBoundsOnModel.x, -this.tmpBoundsOnModel.y, 0), this.tmpMatrixForDraw.setMatrix(this.tmpMatrix2.m);
                            for (var l = this.tmpMatrixForMask.getArray(), c = 0; c < 16; c++) n.matrixForMask[c] = l[c];
                            var p = this.tmpMatrixForDraw.getArray();
                            for (c = 0; c < 16; c++) n.matrixForDraw[c] = p[c];
                            for (var f = n.clippingMaskDrawIndexList.length, $ = 0; $ < f; $++) {
                                var d = n.clippingMaskDrawIndexList[$],
                                    g = t.getDrawData(d),
                                    y = t._$C2(d);
                                i.setClipBufPre_clipContextForMask(n), g.draw(i, t, y)
                            }
                        }
                        i.gl.bindFramebuffer(i.gl.FRAMEBUFFER, o), i.setClipBufPre_clipContextForMask(null), i.gl.viewport(s[0], s[1], s[2], s[3])
                    }
                }, _.prototype.getColorBuffer = function() {
                    return this.colorBuffer
                }, _.prototype.findSameClip = function(t) {
                    for (var i = 0; i < this.clipContextList.length; i++) {
                        var e = this.clipContextList[i],
                            r = e.clipIDList.length;
                        if (r == t.length) {
                            for (var n = 0, o = 0; o < r; o++)
                                for (var s = e.clipIDList[o], a = 0; a < r; a++)
                                    if (t[a] == s) {
                                        n++;
                                        break
                                    }
                            if (n == r) return e
                        }
                    }
                    return null
                }, _.prototype.calcClippedDrawTotalBounds = function(t, i) {
                    for (var e = t._$Ri.getModelImpl().getCanvasWidth(), r = t._$Ri.getModelImpl().getCanvasHeight(), n = e > r ? e : r, o = n, s = n, a = 0, h = 0, _ = i.clippedDrawContextList.length, u = 0; u < _; u++) {
                        var l = i.clippedDrawContextList[u].drawDataIndex,
                            c = t._$C2(l);
                        if (c._$yo()) {
                            for (var p = c.getTransformedPoints(), f = p.length, $ = [], d = [], g = 0, y = k._$i2; y < f; y += k._$No) $[g] = p[y], d[g] = p[y + 1], g++;
                            var m = Math.min.apply(null, $),
                                v = Math.min.apply(null, d),
                                S = Math.max.apply(null, $),
                                T = Math.max.apply(null, d);
                            m < o && (o = m), v < s && (s = v), S > a && (a = S), T > h && (h = T)
                        }
                    }
                    if (o == n) i.allClippedDrawRect.x = 0, i.allClippedDrawRect.y = 0, i.allClippedDrawRect.width = 0, i.allClippedDrawRect.height = 0, i.isUsing = !1;
                    else {
                        var P = a - o,
                            L = h - s;
                        i.allClippedDrawRect.x = o, i.allClippedDrawRect.y = s, i.allClippedDrawRect.width = P, i.allClippedDrawRect.height = L, i.isUsing = !0
                    }
                }, _.prototype.setupLayoutBounds = function(t) {
                    var i = t / _.CHANNEL_COUNT,
                        e = t % _.CHANNEL_COUNT;
                    i = ~~i, e = ~~e;
                    for (var r = 0, n = 0; n < _.CHANNEL_COUNT; n++) {
                        var o = i + (n < e ? 1 : 0);
                        if (0 == o);
                        else if (1 == o)(u = this.clipContextList[r++]).layoutChannelNo = n, u.layoutBounds.x = 0, u.layoutBounds.y = 0, u.layoutBounds.width = 1, u.layoutBounds.height = 1;
                        else if (2 == o)
                            for (var s = 0; s < o; s++) {
                                var a = 0;
                                h = ~~(h = s % 2), (u = this.clipContextList[r++]).layoutChannelNo = n, u.layoutBounds.x = .5 * h, u.layoutBounds.y = 0, u.layoutBounds.width = .5, u.layoutBounds.height = 1
                            } else if (o <= 4)
                                for (s = 0; s < o; s++) h = ~~(h = s % 2), a = ~~(a = s / 2), (u = this.clipContextList[r++]).layoutChannelNo = n, u.layoutBounds.x = .5 * h, u.layoutBounds.y = .5 * a, u.layoutBounds.width = .5, u.layoutBounds.height = .5;
                            else if (o <= 9)
                            for (s = 0; s < o; s++) {
                                var h, u;
                                h = ~~(h = s % 3), a = ~~(a = s / 3), (u = this.clipContextList[r++]).layoutChannelNo = n, u.layoutBounds.x = h / 3, u.layoutBounds.y = a / 3, u.layoutBounds.width = 1 / 3, u.layoutBounds.height = 1 / 3
                            } else ni.println("_$6 _$0P mask count : %d", o)
                    }
                }, u.prototype.addClippedDrawData = function(t, i) {
                    var e = new l(t, i);
                    this.clippedDrawContextList.push(e)
                }, p.prototype._$8P = function() {
                    return .5 * (this.x + this.x + this.width)
                }, p.prototype._$6P = function() {
                    return .5 * (this.y + this.y + this.height)
                }, p.prototype._$EL = function() {
                    return this.x + this.width
                }, p.prototype._$5T = function() {
                    return this.y + this.height
                }, p.prototype._$jL = function(t, i, e, r) {
                    this.x = t, this.y = i, this.width = e, this.height = r
                }, p.prototype._$jL = function(t) {
                    this.x = t.x, this.y = t.y, this.width = t.width, this.height = t.height
                }, f.prototype = new et, f._$tP = new Object, f._$27 = function() {
                    f._$tP.clear()
                }, f.getID = function(t) {
                    var i = f._$tP[t];
                    return null == i && (i = new f(t), f._$tP[t] = i), i
                }, f.prototype._$3s = function() {
                    return new f
                }, d.prototype = new et, d._$tP = new Object, d._$27 = function() {
                    d._$tP.clear()
                }, d.getID = function(t) {
                    var i = d._$tP[t];
                    return null == i && (i = new d(t), d._$tP[t] = i), i
                }, d.prototype._$3s = function() {
                    return new d
                }, g._$42 = 0, g.prototype._$zP = function() {
                    null == this._$vo && (this._$vo = new nt), null == this._$F2 && (this._$F2 = new Array)
                }, g.prototype.getCanvasWidth = function() {
                    return this._$ao
                }, g.prototype.getCanvasHeight = function() {
                    return this._$1S
                }, g.prototype._$F0 = function(t) {
                    this._$vo = t._$nP(), this._$F2 = t._$nP(), this._$ao = t._$6L(), this._$1S = t._$6L()
                }, g.prototype._$6S = function(t) {
                    this._$F2.push(t)
                }, g.prototype._$Xr = function() {
                    return this._$F2
                }, g.prototype._$E2 = function() {
                    return this._$vo
                }, y.prototype.setup = function(t, i, e) {
                    this._$ks = this._$Yb(), this.p2._$xT(), 3 == arguments.length && (this._$Fo = t, this._$L2 = i, this.p1._$p = e, this.p2._$p = e, this.p2.y = t, this.setup())
                }, y.prototype.getPhysicsPoint1 = function() {
                    return this.p1
                }, y.prototype.getPhysicsPoint2 = function() {
                    return this.p2
                }, y.prototype._$qr = function() {
                    return this._$Db
                }, y.prototype._$pr = function(t) {
                    this._$Db = t
                }, y.prototype._$5r = function() {
                    return this._$M2
                }, y.prototype._$Cs = function() {
                    return this._$9b
                }, y.prototype._$Yb = function() {
                    return -180 * Math.atan2(this.p1.x - this.p2.x, -(this.p1.y - this.p2.y)) / Math.PI
                }, y.prototype.addSrcParam = function(t, i, e, r) {
                    var n = new T(t, i, e, r);
                    this._$lL.push(n)
                }, y.prototype.addTargetParam = function(t, i, e, r) {
                    var n = new L(t, i, e, r);
                    this._$qP.push(n)
                }, y.prototype.update = function(t, i) {
                    if (0 == this._$iP) return this._$iP = this._$iT = i, void(this._$Fo = Math.sqrt((this.p1.x - this.p2.x) * (this.p1.x - this.p2.x) + (this.p1.y - this.p2.y) * (this.p1.y - this.p2.y)));
                    var e = (i - this._$iT) / 1e3;
                    if (0 != e) {
                        for (var r = this._$lL.length - 1; r >= 0; --r) this._$lL[r]._$oP(t, this);
                        this._$oo(t, e), this._$M2 = this._$Yb(), this._$9b = (this._$M2 - this._$ks) / e, this._$ks = this._$M2
                    }
                    for (r = this._$qP.length - 1; r >= 0; --r) this._$qP[r]._$YS(t, this);
                    this._$iT = i
                }, y.prototype._$oo = function(t, i) {
                    i < .033 && (i = .033);
                    var e = 1 / i;
                    this.p1.vx = (this.p1.x - this.p1._$s0) * e, this.p1.vy = (this.p1.y - this.p1._$70) * e, this.p1.ax = (this.p1.vx - this.p1._$7L) * e, this.p1.ay = (this.p1.vy - this.p1._$HL) * e, this.p1.fx = this.p1.ax * this.p1._$p, this.p1.fy = this.p1.ay * this.p1._$p, this.p1._$xT();
                    var n, o, s = -Math.atan2(this.p1.y - this.p2.y, this.p1.x - this.p2.x),
                        a = Math.cos(s),
                        h = Math.sin(s),
                        _ = 9.8 * this.p2._$p,
                        u = this._$Db * r._$bS,
                        l = _ * Math.cos(s - u);
                    n = l * h, o = l * a;
                    var c = -this.p1.fx * h * h,
                        p = -this.p1.fy * h * a,
                        f = -this.p2.vx * this._$L2,
                        $ = -this.p2.vy * this._$L2;
                    this.p2.fx = n + c + f, this.p2.fy = o + p + $, this.p2.ax = this.p2.fx / this.p2._$p, this.p2.ay = this.p2.fy / this.p2._$p, this.p2.vx += this.p2.ax * i, this.p2.vy += this.p2.ay * i, this.p2.x += this.p2.vx * i, this.p2.y += this.p2.vy * i;
                    var d = Math.sqrt((this.p1.x - this.p2.x) * (this.p1.x - this.p2.x) + (this.p1.y - this.p2.y) * (this.p1.y - this.p2.y));
                    this.p2.x = this.p1.x + this._$Fo * (this.p2.x - this.p1.x) / d, this.p2.y = this.p1.y + this._$Fo * (this.p2.y - this.p1.y) / d, this.p2.vx = (this.p2.x - this.p2._$s0) * e, this.p2.vy = (this.p2.y - this.p2._$70) * e, this.p2._$xT()
                }, v.prototype._$xT = function() {
                    this._$s0 = this.x, this._$70 = this.y, this._$7L = this.vx, this._$HL = this.vy
                }, S.prototype._$oP = function(t, i) {}, T.prototype = new S, T.prototype._$oP = function(t, i) {
                    var e = this.scale * t.getParamFloat(this._$wL),
                        r = i.getPhysicsPoint1();
                    switch (this._$tL) {
                        default:
                            case y.Src.SRC_TO_X:
                            r.x = r.x + (e - r.x) * this._$V0;
                        break;
                        case y.Src.SRC_TO_Y:
                                r.y = r.y + (e - r.y) * this._$V0;
                            break;
                        case y.Src.SRC_TO_G_ANGLE:
                                var n = i._$qr();n += (e - n) * this._$V0,
                            i._$pr(n)
                    }
                }, P.prototype._$YS = function(t, i) {}, L.prototype = new P, L.prototype._$YS = function(t, i) {
                    switch (this._$YP) {
                        default:
                            case y.Target.TARGET_FROM_ANGLE:
                            t.setParamFloat(this._$wL, this.scale * i._$5r(), this._$V0);
                        break;
                        case y.Target.TARGET_FROM_ANGLE_V:
                                t.setParamFloat(this._$wL, this.scale * i._$Cs(), this._$V0)
                    }
                }, y.Src = function() {}, y.Src.SRC_TO_X = "SRC_TO_X", y.Src.SRC_TO_Y = "SRC_TO_Y", y.Src.SRC_TO_G_ANGLE = "SRC_TO_G_ANGLE", y.Target = function() {}, y.Target.TARGET_FROM_ANGLE = "TARGET_FROM_ANGLE", y.Target.TARGET_FROM_ANGLE_V = "TARGET_FROM_ANGLE_V", M.prototype.init = function(t) {
                    this._$fL = t._$fL, this._$gL = t._$gL, this._$B0 = t._$B0, this._$z0 = t._$z0, this._$qT = t._$qT, this.reflectX = t.reflectX, this.reflectY = t.reflectY
                }, M.prototype._$F0 = function(t) {
                    this._$fL = t._$_T(), this._$gL = t._$_T(), this._$B0 = t._$_T(), this._$z0 = t._$_T(), this._$qT = t._$_T(), t.getFormatVersion() >= U.LIVE2D_FORMAT_VERSION_V2_10_SDK2 && (this.reflectX = t._$po(), this.reflectY = t._$po())
                }, M.prototype._$e = function() {};
                var ci = function() {};
                ci._$ni = function(t, i, e, r, n, o, s, a, h) {
                    var _ = s * o - a * n;
                    if (0 == _) return null;
                    var u, l = ((t - e) * o - (i - r) * n) / _;
                    return u = 0 != n ? (t - e - l * s) / n : (i - r - l * a) / o, isNaN(u) && (u = (t - e - l * s) / n, isNaN(u) && (u = (i - r - l * a) / o), isNaN(u) && (console.log("a is NaN @UtVector#_$ni() "), console.log("v1x : " + n), console.log("v1x != 0 ? " + (0 != n)))), null == h ? new Array(u, l) : (h[0] = u, h[1] = l, h)
                }, E.prototype._$8P = function() {
                    return this.x + .5 * this.width
                }, E.prototype._$6P = function() {
                    return this.y + .5 * this.height
                }, E.prototype._$EL = function() {
                    return this.x + this.width
                }, E.prototype._$5T = function() {
                    return this.y + this.height
                }, E.prototype._$jL = function(t, i, e, r) {
                    this.x = t, this.y = i, this.width = e, this.height = r
                }, E.prototype._$jL = function(t) {
                    this.x = t.x, this.y = t.y, this.width = t.width, this.height = t.height
                }, E.prototype.contains = function(t, i) {
                    return this.x <= this.x && this.y <= this.y && this.x <= this.x + this.width && this.y <= this.y + this.height
                }, E.prototype.expand = function(t, i) {
                    this.x -= t, this.y -= i, this.width += 2 * t, this.height += 2 * i
                }, w._$Z2 = function(t, i, e, r) {
                    var n = i._$Q2(t, e),
                        o = t._$vs(),
                        s = t._$Tr();
                    if (i._$zr(o, s, n), n <= 0) return r[o[0]];
                    if (1 == n) return (a = r[o[0]]) + ((h = r[o[1]]) - a) * (m = s[0]) | 0;
                    if (2 == n) {
                        var a = r[o[0]],
                            h = r[o[1]],
                            _ = r[o[2]],
                            u = r[o[3]];
                        return (T = a + (h - a) * (m = s[0]) | 0) + ((_ + (u - _) * m | 0) - T) * (v = s[1]) | 0
                    }
                    if (3 == n) {
                        var l = r[o[0]],
                            c = r[o[1]],
                            p = r[o[2]],
                            f = r[o[3]],
                            $ = r[o[4]],
                            d = r[o[5]],
                            g = r[o[6]],
                            y = r[o[7]];
                        return (T = (a = l + (c - l) * (m = s[0]) | 0) + ((h = p + (f - p) * m | 0) - a) * (v = s[1]) | 0) + (((_ = $ + (d - $) * m | 0) + ((u = g + (y - g) * m | 0) - _) * v | 0) - T) * (S = s[2]) | 0
                    }
                    if (4 == n) {
                        var m, v, S, T, P = r[o[0]],
                            L = r[o[1]],
                            M = r[o[2]],
                            E = r[o[3]],
                            w = r[o[4]],
                            x = r[o[5]],
                            O = r[o[6]],
                            A = r[o[7]],
                            I = r[o[8]],
                            b = r[o[9]],
                            C = r[o[10]],
                            D = r[o[11]],
                            R = r[o[12]],
                            F = r[o[13]],
                            N = r[o[14]],
                            B = r[o[15]];
                        return (T = (a = (l = P + (L - P) * (m = s[0]) | 0) + ((c = M + (E - M) * m | 0) - l) * (v = s[1]) | 0) + ((h = (p = w + (x - w) * m | 0) + ((f = O + (A - O) * m | 0) - p) * v | 0) - a) * (S = s[2]) | 0) + (((_ = ($ = I + (b - I) * m | 0) + ((d = C + (D - C) * m | 0) - $) * v | 0) + ((u = (g = R + (F - R) * m | 0) + ((y = N + (B - N) * m | 0) - g) * v | 0) - _) * S | 0) - T) * s[3] | 0
                    }
                    for (var G = 1 << n, k = new Float32Array(G), U = 0; U < G; U++) {
                        for (var Y = U, V = 1, j = 0; j < n; j++) V *= Y % 2 == 0 ? 1 - s[j] : s[j], Y /= 2;
                        k[U] = V
                    }
                    for (var X = new Float32Array(G), H = 0; H < G; H++) X[H] = r[o[H]];
                    var z = 0;
                    for (H = 0; H < G; H++) z += k[H] * X[H];
                    return z + .5 | 0
                }, w._$br = function(t, i, e, r) {
                    var n = i._$Q2(t, e),
                        o = t._$vs(),
                        s = t._$Tr();
                    if (i._$zr(o, s, n), n <= 0) return r[o[0]];
                    if (1 == n) return (a = r[o[0]]) + ((h = r[o[1]]) - a) * (l = s[0]);
                    if (2 == n) {
                        var a = r[o[0]],
                            h = r[o[1]],
                            _ = r[o[2]],
                            u = r[o[3]],
                            l = s[0];
                        return (1 - (v = s[1])) * (a + (h - a) * l) + v * (_ + (u - _) * l)
                    }
                    if (3 == n) {
                        var c = r[o[0]],
                            p = r[o[1]],
                            f = r[o[2]],
                            $ = r[o[3]],
                            d = r[o[4]],
                            g = r[o[5]],
                            y = r[o[6]],
                            m = r[o[7]],
                            v = (l = s[0], s[1]);
                        return (1 - (N = s[2])) * ((1 - v) * (c + (p - c) * l) + v * (f + ($ - f) * l)) + N * ((1 - v) * (d + (g - d) * l) + v * (y + (m - y) * l))
                    }
                    if (4 == n) {
                        var S = r[o[0]],
                            T = r[o[1]],
                            P = r[o[2]],
                            L = r[o[3]],
                            M = r[o[4]],
                            E = r[o[5]],
                            w = r[o[6]],
                            x = r[o[7]],
                            O = r[o[8]],
                            A = r[o[9]],
                            I = r[o[10]],
                            b = r[o[11]],
                            C = r[o[12]],
                            D = r[o[13]],
                            R = r[o[14]],
                            F = r[o[15]],
                            N = (l = s[0], v = s[1], s[2]),
                            B = s[3];
                        return (1 - B) * ((1 - N) * ((1 - v) * (S + (T - S) * l) + v * (P + (L - P) * l)) + N * ((1 - v) * (M + (E - M) * l) + v * (w + (x - w) * l))) + B * ((1 - N) * ((1 - v) * (O + (A - O) * l) + v * (I + (b - I) * l)) + N * ((1 - v) * (C + (D - C) * l) + v * (R + (F - R) * l)))
                    }
                    for (var G = 1 << n, k = new Float32Array(G), U = 0; U < G; U++) {
                        for (var Y = U, V = 1, j = 0; j < n; j++) V *= Y % 2 == 0 ? 1 - s[j] : s[j], Y /= 2;
                        k[U] = V
                    }
                    for (var X = new Float32Array(G), H = 0; H < G; H++) X[H] = r[o[H]];
                    var z = 0;
                    for (H = 0; H < G; H++) z += k[H] * X[H];
                    return z
                }, w._$Vr = function(t, i, e, r, n, o, s, a) {
                    var h = i._$Q2(t, e),
                        _ = t._$vs(),
                        u = t._$Tr();
                    i._$zr(_, u, h);
                    var l = 2 * r,
                        c = s;
                    if (h <= 0) {
                        var p = n[_[0]];
                        if (2 == a && 0 == s) ei._$jT(p, 0, o, 0, l);
                        else
                            for (var f = 0; f < l;) o[c] = p[f++], o[c + 1] = p[f++], c += a
                    } else if (1 == h) {
                        p = n[_[0]];
                        var $ = n[_[1]],
                            d = 1 - (m = u[0]);
                        for (f = 0; f < l;) o[c] = p[f] * d + $[f] * m, ++f, o[c + 1] = p[f] * d + $[f] * m, ++f, c += a
                    } else if (2 == h) {
                        p = n[_[0]], $ = n[_[1]];
                        var g = n[_[2]],
                            y = n[_[3]],
                            m = u[0],
                            v = (U = 1 - (b = u[1])) * (d = 1 - m),
                            S = U * m,
                            T = b * d,
                            P = b * m;
                        for (f = 0; f < l;) o[c] = v * p[f] + S * $[f] + T * g[f] + P * y[f], ++f, o[c + 1] = v * p[f] + S * $[f] + T * g[f] + P * y[f], ++f, c += a
                    } else if (3 == h) {
                        var L = n[_[0]],
                            M = n[_[1]],
                            E = n[_[2]],
                            w = n[_[3]],
                            x = n[_[4]],
                            O = n[_[5]],
                            A = n[_[6]],
                            I = n[_[7]],
                            b = (m = u[0], u[1]),
                            C = (Y = 1 - (ot = u[2])) * (U = 1 - b) * (d = 1 - m),
                            D = Y * U * m,
                            R = Y * b * d,
                            F = Y * b * m,
                            N = ot * U * d,
                            B = ot * U * m,
                            G = ot * b * d,
                            k = ot * b * m;
                        for (f = 0; f < l;) o[c] = C * L[f] + D * M[f] + R * E[f] + F * w[f] + N * x[f] + B * O[f] + G * A[f] + k * I[f], ++f, o[c + 1] = C * L[f] + D * M[f] + R * E[f] + F * w[f] + N * x[f] + B * O[f] + G * A[f] + k * I[f], ++f, c += a
                    } else if (4 == h) {
                        var U, Y, V = n[_[0]],
                            j = n[_[1]],
                            X = n[_[2]],
                            H = n[_[3]],
                            z = n[_[4]],
                            W = n[_[5]],
                            q = n[_[6]],
                            Z = n[_[7]],
                            J = n[_[8]],
                            Q = n[_[9]],
                            K = n[_[10]],
                            tt = n[_[11]],
                            it = n[_[12]],
                            et = n[_[13]],
                            rt = n[_[14]],
                            nt = n[_[15]],
                            ot = (m = u[0], b = u[1], u[2]),
                            st = u[3],
                            at = 1 - st,
                            ht = at * (Y = 1 - ot) * (U = 1 - b) * (d = 1 - m),
                            _t = at * Y * U * m,
                            ut = at * Y * b * d,
                            lt = at * Y * b * m,
                            ct = at * ot * U * d,
                            pt = at * ot * U * m,
                            ft = at * ot * b * d,
                            $t = at * ot * b * m,
                            dt = st * Y * U * d,
                            gt = st * Y * U * m,
                            yt = st * Y * b * d,
                            mt = st * Y * b * m,
                            vt = st * ot * U * d,
                            St = st * ot * U * m,
                            Tt = st * ot * b * d,
                            Pt = st * ot * b * m;
                        for (f = 0; f < l;) o[c] = ht * V[f] + _t * j[f] + ut * X[f] + lt * H[f] + ct * z[f] + pt * W[f] + ft * q[f] + $t * Z[f] + dt * J[f] + gt * Q[f] + yt * K[f] + mt * tt[f] + vt * it[f] + St * et[f] + Tt * rt[f] + Pt * nt[f], ++f, o[c + 1] = ht * V[f] + _t * j[f] + ut * X[f] + lt * H[f] + ct * z[f] + pt * W[f] + ft * q[f] + $t * Z[f] + dt * J[f] + gt * Q[f] + yt * K[f] + mt * tt[f] + vt * it[f] + St * et[f] + Tt * rt[f] + Pt * nt[f], ++f, c += a
                    } else {
                        for (var Lt = 1 << h, Mt = new Float32Array(Lt), Et = 0; Et < Lt; Et++) {
                            for (var wt = Et, xt = 1, Ot = 0; Ot < h; Ot++) xt *= wt % 2 == 0 ? 1 - u[Ot] : u[Ot], wt /= 2;
                            Mt[Et] = xt
                        }
                        for (var At = new Float32Array(Lt), It = 0; It < Lt; It++) At[It] = n[_[It]];
                        for (f = 0; f < l;) {
                            var bt = 0,
                                Ct = 0,
                                Dt = f + 1;
                            for (It = 0; It < Lt; It++) bt += Mt[It] * At[It][f], Ct += Mt[It] * At[It][Dt];
                            f += 2, o[c] = bt, o[c + 1] = Ct, c += a
                        }
                    }
                }, x.prototype._$HT = function(t, i) {
                    this.x = t, this.y = i
                }, x.prototype._$HT = function(t) {
                    this.x = t.x, this.y = t.y
                }, O._$ur = -2, O._$ES = 500, O._$wb = 2, O._$8S = 3, O._$52 = O._$ES, O._$R2 = O._$ES, O._$or = function() {
                    return O._$52
                }, O._$Pr = function() {
                    return O._$R2
                }, O.prototype.convertClipIDForV2_11 = function(t) {
                    var i = [];
                    return null == t || 0 == t.length ? null : /,/.test(t) ? i = t.id.split(",") : (i.push(t.id), i)
                }, O.prototype._$F0 = function(t) {
                    this._$gP = t._$nP(), this._$dr = t._$nP(), this._$GS = t._$nP(), this._$qb = t._$6L(), this._$Lb = t._$cS(), this._$mS = t._$Tb(), t.getFormatVersion() >= U._$T7 ? (this.clipID = t._$nP(), this.clipIDList = this.convertClipIDForV2_11(this.clipID)) : this.clipIDList = [], this._$MS(this._$Lb)
                }, O.prototype.getClipIDList = function() {
                    return this.clipIDList
                }, O.prototype.init = function(t) {}, O.prototype._$Nr = function(t, i) {
                    if (i._$IS[0] = !1, i._$Us = w._$Z2(t, this._$GS, i._$IS, this._$Lb), ti._$Zs);
                    else if (i._$IS[0]) return;
                    i._$7s = w._$br(t, this._$GS, i._$IS, this._$mS)
                }, O.prototype._$2b = function(t, i) {}, O.prototype.getDrawDataID = function() {
                    return this._$gP
                }, O.prototype._$j2 = function(t) {
                    this._$gP = t
                }, O.prototype.getOpacity = function(t, i) {
                    return i._$7s
                }, O.prototype._$zS = function(t, i) {
                    return i._$Us
                }, O.prototype._$MS = function(t) {
                    for (var i = t.length - 1; i >= 0; --i) {
                        var e = t[i];
                        e < O._$52 ? O._$52 = e : e > O._$R2 && (O._$R2 = e)
                    }
                }, O.prototype.getTargetBaseDataID = function() {
                    return this._$dr
                }, O.prototype._$gs = function(t) {
                    this._$dr = t
                }, O.prototype._$32 = function() {
                    return null != this._$dr && this._$dr != $t._$2o()
                }, O.prototype.preDraw = function(t, i, e) {}, O.prototype.draw = function(t, i, e) {}, O.prototype.getType = function() {}, O.prototype._$B2 = function(t, i, e) {}, A._$ps = 32, A.CLIPPING_PROCESS_NONE = 0, A.CLIPPING_PROCESS_OVERWRITE_ALPHA = 1, A.CLIPPING_PROCESS_MULTIPLY_ALPHA = 2, A.CLIPPING_PROCESS_DRAW = 3, A.CLIPPING_PROCESS_CLEAR_ALPHA = 4, A.prototype.setChannelFlagAsColor = function(t, i) {
                    this.CHANNEL_COLORS[t] = i
                }, A.prototype.getChannelFlagAsColor = function(t) {
                    return this.CHANNEL_COLORS[t]
                }, A.prototype._$ZT = function() {}, A.prototype._$Uo = function(t, i, e, r, n, o, s) {}, A.prototype._$Rs = function() {
                    return -1
                }, A.prototype._$Ds = function(t) {}, A.prototype.setBaseColor = function(t, i, e, r) {
                    t < 0 ? t = 0 : t > 1 && (t = 1), i < 0 ? i = 0 : i > 1 && (i = 1), e < 0 ? e = 0 : e > 1 && (e = 1), r < 0 ? r = 0 : r > 1 && (r = 1), this._$lT = t, this._$C0 = i, this._$tT = e, this._$WL = r
                }, A.prototype._$WP = function(t) {
                    this.culling = t
                }, A.prototype.setMatrix = function(t) {
                    for (var i = 0; i < 16; i++) this.matrix4x4[i] = t[i]
                }, A.prototype._$IT = function() {
                    return this.matrix4x4
                }, A.prototype.setPremultipliedAlpha = function(t) {
                    this.premultipliedAlpha = t
                }, A.prototype.isPremultipliedAlpha = function() {
                    return this.premultipliedAlpha
                }, A.prototype.setAnisotropy = function(t) {
                    this.anisotropy = t
                }, A.prototype.getAnisotropy = function() {
                    return this.anisotropy
                }, A.prototype.getClippingProcess = function() {
                    return this.clippingProcess
                }, A.prototype.setClippingProcess = function(t) {
                    this.clippingProcess = t
                }, A.prototype.setClipBufPre_clipContextForMask = function(t) {
                    this.clipBufPre_clipContextMask = t
                }, A.prototype.getClipBufPre_clipContextMask = function() {
                    return this.clipBufPre_clipContextMask
                }, A.prototype.setClipBufPre_clipContextForDraw = function(t) {
                    this.clipBufPre_clipContextDraw = t
                }, A.prototype.getClipBufPre_clipContextDraw = function() {
                    return this.clipBufPre_clipContextDraw
                }, b._$ur = -2, b._$c2 = 1, b._$_b = 2, b.prototype._$F0 = function(t) {
                    this._$kP = t._$nP(), this._$dr = t._$nP()
                }, b.prototype.readV2_opacity = function(t) {
                    t.getFormatVersion() >= U.LIVE2D_FORMAT_VERSION_V2_10_SDK2 && (this._$mS = t._$Tb())
                }, b.prototype.init = function(t) {}, b.prototype._$Nr = function(t, i) {}, b.prototype.interpolateOpacity = function(t, i, e, r) {
                    null == this._$mS ? e.setInterpolatedOpacity(1) : e.setInterpolatedOpacity(w._$br(t, i, r, this._$mS))
                }, b.prototype._$2b = function(t, i) {}, b.prototype._$nb = function(t, i, e, r, n, o, s) {}, b.prototype.getType = function() {}, b.prototype._$gs = function(t) {
                    this._$dr = t
                }, b.prototype._$a2 = function(t) {
                    this._$kP = t
                }, b.prototype.getTargetBaseDataID = function() {
                    return this._$dr
                }, b.prototype.getBaseDataID = function() {
                    return this._$kP
                }, b.prototype._$32 = function() {
                    return null != this._$dr && this._$dr != $t._$2o()
                }, C._$ds = -2, C.prototype._$F0 = function(t) {
                    this._$wL = t._$nP(), this._$VP = t._$6L(), this._$GP = t._$nP()
                }, C.prototype.getParamIndex = function(t) {
                    return this._$2r != t && (this._$8o = C._$ds), this._$8o
                }, C.prototype._$Pb = function(t, i) {
                    this._$8o = t, this._$2r = i
                }, C.prototype.getParamID = function() {
                    return this._$wL
                }, C.prototype._$yP = function(t) {
                    this._$wL = t
                }, C.prototype._$N2 = function() {
                    return this._$VP
                }, C.prototype._$d2 = function() {
                    return this._$GP
                }, C.prototype._$t2 = function(t, i) {
                    this._$VP = t, this._$GP = i
                }, C.prototype._$Lr = function() {
                    return this._$O2
                }, C.prototype._$wr = function(t) {
                    this._$O2 = t
                }, C.prototype._$SL = function() {
                    return this._$ri
                }, C.prototype._$AL = function(t) {
                    this._$ri = t
                }, D.prototype._$zP = function() {
                    this._$Ob = new Array
                }, D.prototype._$F0 = function(t) {
                    this._$Ob = t._$nP()
                }, D.prototype._$Ur = function(t) {
                    if (t._$WS()) return !0;
                    for (var i = t._$v2(), e = this._$Ob.length - 1; e >= 0; --e) {
                        var r = this._$Ob[e].getParamIndex(i);
                        if (r == C._$ds && (r = t.getParamIndex(this._$Ob[e].getParamID())), t._$Xb(r)) return !0
                    }
                    return !1
                }, D.prototype._$Q2 = function(t, i) {
                    for (var e, r, n = this._$Ob.length, o = t._$v2(), s = 0, a = 0; a < n; a++) {
                        var h = this._$Ob[a];
                        if ((e = h.getParamIndex(o)) == C._$ds && (e = t.getParamIndex(h.getParamID()), h._$Pb(e, o)), e < 0) throw new Exception("err 23242 : " + h.getParamID());
                        var _ = e < 0 ? 0 : t.getParamFloat(e);
                        r = h._$N2();
                        var u, l, c = h._$d2(),
                            p = -1,
                            f = 0;
                        if (r < 1);
                        else if (1 == r)(u = c[0]) - k._$J < _ && _ < u + k._$J ? (p = 0, f = 0) : (p = 0, i[0] = !0);
                        else if (_ < (u = c[0]) - k._$J) p = 0, i[0] = !0;
                        else if (_ < u + k._$J) p = 0;
                        else {
                            for (var $ = !1, d = 1; d < r; ++d) {
                                if (_ < (l = c[d]) + k._$J) {
                                    l - k._$J < _ ? p = d : (p = d - 1, f = (_ - u) / (l - u), s++), $ = !0;
                                    break
                                }
                                u = l
                            }
                            $ || (p = r - 1, f = 0, i[0] = !0)
                        }
                        h._$wr(p), h._$AL(f)
                    }
                    return s
                }, D.prototype._$zr = function(t, i, e) {
                    var r = 1 << e;
                    r + 1 > k._$Qb && console.log("err 23245\n");
                    for (var n = this._$Ob.length, o = 1, s = 1, a = 0, h = 0; h < r; ++h) t[h] = 0;
                    for (var _ = 0; _ < n; ++_) {
                        var u = this._$Ob[_];
                        if (0 == u._$SL()) {
                            if ((l = u._$Lr() * o) < 0 && ti._$3T) throw new Exception("err 23246");
                            for (h = 0; h < r; ++h) t[h] += l
                        } else {
                            var l = o * u._$Lr(),
                                c = o * (u._$Lr() + 1);
                            for (h = 0; h < r; ++h) t[h] += (h / s | 0) % 2 == 0 ? l : c;
                            i[a++] = u._$SL(), s *= 2
                        }
                        o *= u._$N2()
                    }
                    t[r] = 65535, i[a] = -1
                }, D.prototype._$h2 = function(t, i, e) {
                    for (var r = new Float32Array(i), n = 0; n < i; ++n) r[n] = e[n];
                    var o = new C;
                    o._$yP(t), o._$t2(i, r), this._$Ob.push(o)
                }, D.prototype._$J2 = function(t) {
                    for (var i = t, e = this._$Ob.length, r = 0; r < e; ++r) {
                        var n = this._$Ob[r],
                            o = n._$N2(),
                            s = i % n._$N2(),
                            a = n._$d2()[s];
                        console.log("%s[%d]=%7.2f / ", n.getParamID(), s, a), i /= o
                    }
                    console.log("\n")
                }, D.prototype.getParamCount = function() {
                    return this._$Ob.length
                }, D.prototype._$zs = function() {
                    return this._$Ob
                }, R.prototype.identity = function() {
                    for (var t = 0; t < 16; t++) this.m[t] = t % 5 == 0 ? 1 : 0
                }, R.prototype.getArray = function() {
                    return this.m
                }, R.prototype.getCopyMatrix = function() {
                    return new Float32Array(this.m)
                }, R.prototype.setMatrix = function(t) {
                    if (null != t && 16 == t.length)
                        for (var i = 0; i < 16; i++) this.m[i] = t[i]
                }, R.prototype.mult = function(t, i, e) {
                    return null == i ? null : (this == i ? this.mult_safe(this.m, t.m, i.m, e) : this.mult_fast(this.m, t.m, i.m, e), i)
                }, R.prototype.mult_safe = function(t, i, e, r) {
                    if (t == e) {
                        var n = new Array(16);
                        this.mult_fast(t, i, n, r);
                        for (var o = 15; o >= 0; --o) e[o] = n[o]
                    } else this.mult_fast(t, i, e, r)
                }, R.prototype.mult_fast = function(t, i, e, r) {
                    r ? (e[0] = t[0] * i[0] + t[4] * i[1] + t[8] * i[2], e[4] = t[0] * i[4] + t[4] * i[5] + t[8] * i[6], e[8] = t[0] * i[8] + t[4] * i[9] + t[8] * i[10], e[12] = t[0] * i[12] + t[4] * i[13] + t[8] * i[14] + t[12], e[1] = t[1] * i[0] + t[5] * i[1] + t[9] * i[2], e[5] = t[1] * i[4] + t[5] * i[5] + t[9] * i[6], e[9] = t[1] * i[8] + t[5] * i[9] + t[9] * i[10], e[13] = t[1] * i[12] + t[5] * i[13] + t[9] * i[14] + t[13], e[2] = t[2] * i[0] + t[6] * i[1] + t[10] * i[2], e[6] = t[2] * i[4] + t[6] * i[5] + t[10] * i[6], e[10] = t[2] * i[8] + t[6] * i[9] + t[10] * i[10], e[14] = t[2] * i[12] + t[6] * i[13] + t[10] * i[14] + t[14], e[3] = e[7] = e[11] = 0, e[15] = 1) : (e[0] = t[0] * i[0] + t[4] * i[1] + t[8] * i[2] + t[12] * i[3], e[4] = t[0] * i[4] + t[4] * i[5] + t[8] * i[6] + t[12] * i[7], e[8] = t[0] * i[8] + t[4] * i[9] + t[8] * i[10] + t[12] * i[11], e[12] = t[0] * i[12] + t[4] * i[13] + t[8] * i[14] + t[12] * i[15], e[1] = t[1] * i[0] + t[5] * i[1] + t[9] * i[2] + t[13] * i[3], e[5] = t[1] * i[4] + t[5] * i[5] + t[9] * i[6] + t[13] * i[7], e[9] = t[1] * i[8] + t[5] * i[9] + t[9] * i[10] + t[13] * i[11], e[13] = t[1] * i[12] + t[5] * i[13] + t[9] * i[14] + t[13] * i[15], e[2] = t[2] * i[0] + t[6] * i[1] + t[10] * i[2] + t[14] * i[3], e[6] = t[2] * i[4] + t[6] * i[5] + t[10] * i[6] + t[14] * i[7], e[10] = t[2] * i[8] + t[6] * i[9] + t[10] * i[10] + t[14] * i[11], e[14] = t[2] * i[12] + t[6] * i[13] + t[10] * i[14] + t[14] * i[15], e[3] = t[3] * i[0] + t[7] * i[1] + t[11] * i[2] + t[15] * i[3], e[7] = t[3] * i[4] + t[7] * i[5] + t[11] * i[6] + t[15] * i[7], e[11] = t[3] * i[8] + t[7] * i[9] + t[11] * i[10] + t[15] * i[11], e[15] = t[3] * i[12] + t[7] * i[13] + t[11] * i[14] + t[15] * i[15])
                }, R.prototype.translate = function(t, i, e) {
                    this.m[12] = this.m[0] * t + this.m[4] * i + this.m[8] * e + this.m[12], this.m[13] = this.m[1] * t + this.m[5] * i + this.m[9] * e + this.m[13], this.m[14] = this.m[2] * t + this.m[6] * i + this.m[10] * e + this.m[14], this.m[15] = this.m[3] * t + this.m[7] * i + this.m[11] * e + this.m[15]
                }, R.prototype.scale = function(t, i, e) {
                    this.m[0] *= t, this.m[4] *= i, this.m[8] *= e, this.m[1] *= t, this.m[5] *= i, this.m[9] *= e, this.m[2] *= t, this.m[6] *= i, this.m[10] *= e, this.m[3] *= t, this.m[7] *= i, this.m[11] *= e
                }, R.prototype.rotateX = function(t) {
                    var i = r.fcos(t),
                        e = r._$9(t),
                        n = this.m[4];
                    this.m[4] = n * i + this.m[8] * e, this.m[8] = n * -e + this.m[8] * i, n = this.m[5], this.m[5] = n * i + this.m[9] * e, this.m[9] = n * -e + this.m[9] * i, n = this.m[6], this.m[6] = n * i + this.m[10] * e, this.m[10] = n * -e + this.m[10] * i, n = this.m[7], this.m[7] = n * i + this.m[11] * e, this.m[11] = n * -e + this.m[11] * i
                }, R.prototype.rotateY = function(t) {
                    var i = r.fcos(t),
                        e = r._$9(t),
                        n = this.m[0];
                    this.m[0] = n * i + this.m[8] * -e, this.m[8] = n * e + this.m[8] * i, n = this.m[1], this.m[1] = n * i + this.m[9] * -e, this.m[9] = n * e + this.m[9] * i, n = m[2], this.m[2] = n * i + this.m[10] * -e, this.m[10] = n * e + this.m[10] * i, n = m[3], this.m[3] = n * i + this.m[11] * -e, this.m[11] = n * e + this.m[11] * i
                }, R.prototype.rotateZ = function(t) {
                    var i = r.fcos(t),
                        e = r._$9(t),
                        n = this.m[0];
                    this.m[0] = n * i + this.m[4] * e, this.m[4] = n * -e + this.m[4] * i, n = this.m[1], this.m[1] = n * i + this.m[5] * e, this.m[5] = n * -e + this.m[5] * i, n = this.m[2], this.m[2] = n * i + this.m[6] * e, this.m[6] = n * -e + this.m[6] * i, n = this.m[3], this.m[3] = n * i + this.m[7] * e, this.m[7] = n * -e + this.m[7] * i
                }, F.prototype = new et, F._$tP = new Object, F._$27 = function() {
                    F._$tP.clear()
                }, F.getID = function(t) {
                    var i = F._$tP[t];
                    return null == i && (i = new F(t), F._$tP[t] = i), i
                }, F.prototype._$3s = function() {
                    return new F
                }, N._$kS = -1, N._$pS = 0, N._$hb = 1, N.STATE_IDENTITY = 0, N._$gb = 1, N._$fo = 2, N._$go = 4, N.prototype.transform = function(t, i, e) {
                    var r, n, o, s, a, h, _ = 0,
                        u = 0;
                    switch (this._$hi) {
                        default: return;
                        case N._$go | N._$fo | N._$gb:
                                for (r = this._$7, n = this._$H, o = this._$k, s = this._$f, a = this._$g, h = this._$w; --e >= 0;) {
                                var l = t[_++],
                                    c = t[_++];
                                i[u++] = r * l + n * c + o, i[u++] = s * l + a * c + h
                            }
                            return;
                        case N._$go | N._$fo:
                                for (r = this._$7, n = this._$H, s = this._$f, a = this._$g; --e >= 0;) l = t[_++], c = t[_++], i[u++] = r * l + n * c, i[u++] = s * l + a * c;
                            return;
                        case N._$go | N._$gb:
                                for (n = this._$H, o = this._$k, s = this._$f, h = this._$w; --e >= 0;) l = t[_++], i[u++] = n * t[_++] + o, i[u++] = s * l + h;
                            return;
                        case N._$go:
                                for (n = this._$H, s = this._$f; --e >= 0;) l = t[_++], i[u++] = n * t[_++], i[u++] = s * l;
                            return;
                        case N._$fo | N._$gb:
                                for (r = this._$7, o = this._$k, a = this._$g, h = this._$w; --e >= 0;) i[u++] = r * t[_++] + o, i[u++] = a * t[_++] + h;
                            return;
                        case N._$fo:
                                for (r = this._$7, a = this._$g; --e >= 0;) i[u++] = r * t[_++], i[u++] = a * t[_++];
                            return;
                        case N._$gb:
                                for (o = this._$k, h = this._$w; --e >= 0;) i[u++] = t[_++] + o, i[u++] = t[_++] + h;
                            return;
                        case N.STATE_IDENTITY:
                                return void(t == i && _ == u || ei._$jT(t, _, i, u, 2 * e))
                    }
                }, N.prototype.update = function() {
                    0 == this._$H && 0 == this._$f ? 1 == this._$7 && 1 == this._$g ? 0 == this._$k && 0 == this._$w ? (this._$hi = N.STATE_IDENTITY, this._$Z = N._$pS) : (this._$hi = N._$gb, this._$Z = N._$hb) : 0 == this._$k && 0 == this._$w ? (this._$hi = N._$fo, this._$Z = N._$kS) : (this._$hi = N._$fo | N._$gb, this._$Z = N._$kS) : 0 == this._$7 && 0 == this._$g ? 0 == this._$k && 0 == this._$w ? (this._$hi = N._$go, this._$Z = N._$kS) : (this._$hi = N._$go | N._$gb, this._$Z = N._$kS) : 0 == this._$k && 0 == this._$w ? (this._$hi = N._$go | N._$fo, this._$Z = N._$kS) : (this._$hi = N._$go | N._$fo | N._$gb, this._$Z = N._$kS)
                }, N.prototype._$RT = function(t) {
                    this._$IT(t);
                    var i = t[0],
                        e = t[2],
                        r = t[1],
                        n = t[3],
                        o = Math.sqrt(i * i + r * r),
                        s = i * n - e * r;
                    0 == o ? ti._$so && console.log("affine._$RT() / rt==0") : (t[0] = o, t[1] = s / o, t[2] = (r * n + i * e) / s, t[3] = Math.atan2(r, i))
                }, N.prototype._$ho = function(t, i, e, r) {
                    var n = new Float32Array(6),
                        o = new Float32Array(6);
                    t._$RT(n), i._$RT(o);
                    var s = new Float32Array(6);
                    s[0] = n[0] + (o[0] - n[0]) * e, s[1] = n[1] + (o[1] - n[1]) * e, s[2] = n[2] + (o[2] - n[2]) * e, s[3] = n[3] + (o[3] - n[3]) * e, s[4] = n[4] + (o[4] - n[4]) * e, s[5] = n[5] + (o[5] - n[5]) * e, r._$CT(s)
                }, N.prototype._$CT = function(t) {
                    var i = Math.cos(t[3]),
                        e = Math.sin(t[3]);
                    this._$7 = t[0] * i, this._$f = t[0] * e, this._$H = t[1] * (t[2] * i - e), this._$g = t[1] * (t[2] * e + i), this._$k = t[4], this._$w = t[5], this.update()
                }, N.prototype._$IT = function(t) {
                    t[0] = this._$7, t[1] = this._$f, t[2] = this._$H, t[3] = this._$g, t[4] = this._$k, t[5] = this._$w
                }, B.prototype.clear = function() {
                    this.size = 0
                }, B.prototype.add = function(t) {
                    if (this._$P.length <= this.size) {
                        var i = new Float32Array(2 * this.size);
                        ei._$jT(this._$P, 0, i, 0, this.size), this._$P = i
                    }
                    this._$P[this.size++] = t
                }, B.prototype._$BL = function() {
                    var t = new Float32Array(this.size);
                    return ei._$jT(this._$P, 0, t, 0, this.size), t
                }, G._$Fr = 0, G._$hs = 1, G._$ws = 100, G._$Ns = 101, G._$xs = 102, G._$us = 103, G._$qs = 104, G._$Ys = 105, k._$Ms = 1, k._$Qs = 2, k._$i2 = 0, k._$No = 2, k._$do = k._$Ms, k._$Ls = !0, k._$1r = 5, k._$Qb = 65, k._$J = 1e-4, k._$FT = .001, k._$Ss = 3, U._$o7 = 6, U._$S7 = 7, U._$s7 = 8, U._$77 = 9, U.LIVE2D_FORMAT_VERSION_V2_10_SDK2 = 10, U.LIVE2D_FORMAT_VERSION_V2_11_SDK2_1 = 11, U._$T7 = U.LIVE2D_FORMAT_VERSION_V2_11_SDK2_1, U._$Is = -2004318072, U._$h0 = 0, U._$4L = 23, U._$7P = 33, U._$uT = function(t) {
                    console.log("_$bo :: _$6 _$mo _$E0 : %d\n", t)
                }, U._$9o = function(t) {
                    if (t < 40) return U._$uT(t), null;
                    if (t < 50) return U._$uT(t), null;
                    if (t < 60) return U._$uT(t), null;
                    if (t < 100) switch (t) {
                        case 65:
                            return new Q;
                        case 66:
                            return new D;
                        case 67:
                            return new C;
                        case 68:
                            return new H;
                        case 69:
                            return new M;
                        case 70:
                            return new _t;
                        default:
                            return U._$uT(t), null
                    } else if (t < 150) switch (t) {
                        case 131:
                            return new st;
                        case 133:
                            return new tt;
                        case 136:
                            return new g;
                        case 137:
                            return new nt;
                        case 142:
                            return new q
                    }
                    return U._$uT(t), null
                }, Y._$HP = 0, Y._$_0 = !0, Y._$V2 = -1, Y._$W0 = -1, Y._$jr = !1, Y._$ZS = !0, Y._$tr = -1e6, Y._$lr = 1e6, Y._$is = 32, Y._$e = !1, Y.prototype.getDrawDataIndex = function(t) {
                    for (var i = this._$aS.length - 1; i >= 0; --i)
                        if (null != this._$aS[i] && this._$aS[i].getDrawDataID() == t) return i;
                    return -1
                }, Y.prototype.getDrawData = function(t) {
                    if (t instanceof F) {
                        if (null == this._$Bo) {
                            this._$Bo = new Object;
                            for (var i = this._$aS.length, e = 0; e < i; e++) {
                                var r = this._$aS[e],
                                    n = r.getDrawDataID();
                                null != n && (this._$Bo[n] = r)
                            }
                        }
                        return this._$Bo[id]
                    }
                    return t < this._$aS.length ? this._$aS[t] : null
                }, Y.prototype.release = function() {
                    this._$3S.clear(), this._$aS.clear(), this._$F2.clear(), null != this._$Bo && this._$Bo.clear(), this._$db.clear(), this._$8b.clear(), this._$Hr.clear()
                }, Y.prototype.init = function() {
                    this._$co++, this._$F2.length > 0 && this.release();
                    for (var t = this._$Ri.getModelImpl(), i = t._$Xr(), e = i.length, r = new Array, n = new Array, o = 0; o < e; ++o) {
                        var s = i[o];
                        this._$F2.push(s), this._$Hr.push(s.init(this));
                        for (var a = s.getBaseData(), h = a.length, u = 0; u < h; ++u) r.push(a[u]);
                        for (u = 0; u < h; ++u) {
                            var l = a[u].init(this);
                            l._$l2(o), n.push(l)
                        }
                        var c = s.getDrawData(),
                            p = c.length;
                        for (u = 0; u < p; ++u) {
                            var f = c[u],
                                $ = f.init(this);
                            $._$IP = o, this._$aS.push(f), this._$8b.push($)
                        }
                    }
                    for (var d = r.length, g = $t._$2o();;) {
                        var y = !1;
                        for (o = 0; o < d; ++o) {
                            var m = r[o];
                            if (null != m) {
                                var v = m.getTargetBaseDataID();
                                (null == v || v == g || this.getBaseDataIndex(v) >= 0) && (this._$3S.push(m), this._$db.push(n[o]), r[o] = null, y = !0)
                            }
                        }
                        if (!y) break
                    }
                    var S = t._$E2();
                    if (null != S) {
                        var T = S._$1s();
                        if (null != T) {
                            var P = T.length;
                            for (o = 0; o < P; ++o) {
                                var L = T[o];
                                null != L && this._$02(L.getParamID(), L.getDefaultValue(), L.getMinValue(), L.getMaxValue())
                            }
                        }
                    }
                    this.clipManager = new _(this.dp_webgl), this.clipManager.init(this, this._$aS, this._$8b), this._$QT = !0
                }, Y.prototype.update = function() {
                    Y._$e && ni.start("_$zL");
                    for (var t = this._$_2.length, i = 0; i < t; i++) this._$_2[i] != this._$vr[i] && (this._$Js[i] = Y._$ZS, this._$vr[i] = this._$_2[i]);
                    var e = this._$3S.length,
                        r = this._$aS.length,
                        n = W._$or(),
                        o = W._$Pr() - n + 1;
                    for ((null == this._$Ws || this._$Ws.length < o) && (this._$Ws = new Int16Array(o), this._$Vs = new Int16Array(o)), i = 0; i < o; i++) this._$Ws[i] = Y._$V2, this._$Vs[i] = Y._$V2;
                    for ((null == this._$Er || this._$Er.length < r) && (this._$Er = new Int16Array(r)), i = 0; i < r; i++) this._$Er[i] = Y._$W0;
                    Y._$e && ni.dump("_$zL"), Y._$e && ni.start("_$UL");
                    for (var s = null, a = 0; a < e; ++a) {
                        var h = this._$3S[a],
                            _ = this._$db[a];
                        try {
                            h._$Nr(this, _), h._$2b(this, _)
                        } catch (t) {
                            null == s && (s = t)
                        }
                    }
                    null != s && Y._$_0 && ni.error(s), Y._$e && ni.dump("_$UL"), Y._$e && ni.start("_$DL");
                    for (var u = null, l = 0; l < r; ++l) {
                        var c = this._$aS[l],
                            p = this._$8b[l];
                        try {
                            if (c._$Nr(this, p), p._$u2()) continue;
                            c._$2b(this, p);
                            var f, $ = Math.floor(c._$zS(this, p) - n);
                            try {
                                f = this._$Vs[$]
                            } catch (t) {
                                console.log("_$li :: %s / %s \t\t\t\t@@_$fS\n", t.toString(), c.getDrawDataID().toString()), $ = Math.floor(c._$zS(this, p) - n);
                                continue
                            }
                            f == Y._$V2 ? this._$Ws[$] = l : this._$Er[f] = l, this._$Vs[$] = l
                        } catch (t) {
                            null == u && (u = t, ti._$sT(ti._$H7))
                        }
                    }
                    for (null != u && Y._$_0 && ni.error(u), Y._$e && ni.dump("_$DL"), Y._$e && ni.start("_$eL"), i = this._$Js.length - 1; i >= 0; i--) this._$Js[i] = Y._$jr;
                    return this._$QT = !1, Y._$e && ni.dump("_$eL"), !1
                }, Y.prototype.preDraw = function(t) {
                    null != this.clipManager && (t._$ZT(), this.clipManager.setupClip(this, t))
                }, Y.prototype.draw = function(t) {
                    if (null != this._$Ws) {
                        var i = this._$Ws.length;
                        t._$ZT();
                        for (var e = 0; e < i; ++e) {
                            var r = this._$Ws[e];
                            if (r != Y._$V2)
                                for (;;) {
                                    var n = this._$aS[r],
                                        o = this._$8b[r];
                                    if (o._$yo()) {
                                        var s = o._$IP,
                                            a = this._$Hr[s];
                                        o._$VS = a.getPartsOpacity(), n.draw(t, this, o)
                                    }
                                    var h = this._$Er[r];
                                    if (h <= r || h == Y._$W0) break;
                                    r = h
                                }
                        }
                    } else ni.println("call _$Ri.update() before _$Ri.draw() ")
                }, Y.prototype.getParamIndex = function(t) {
                    for (var i = this._$pb.length - 1; i >= 0; --i)
                        if (this._$pb[i] == t) return i;
                    return this._$02(t, 0, Y._$tr, Y._$lr)
                }, Y.prototype._$BS = function(t) {
                    return this.getBaseDataIndex(t)
                }, Y.prototype.getBaseDataIndex = function(t) {
                    for (var i = this._$3S.length - 1; i >= 0; --i)
                        if (null != this._$3S[i] && this._$3S[i].getBaseDataID() == t) return i;
                    return -1
                }, Y.prototype._$UT = function(t, i) {
                    var e = new Float32Array(i);
                    return ei._$jT(t, 0, e, 0, t.length), e
                }, Y.prototype._$02 = function(t, i, e, r) {
                    if (this._$qo >= this._$pb.length) {
                        var n = this._$pb.length,
                            o = new Array(2 * n);
                        ei._$jT(this._$pb, 0, o, 0, n), this._$pb = o, this._$_2 = this._$UT(this._$_2, 2 * n), this._$vr = this._$UT(this._$vr, 2 * n), this._$Rr = this._$UT(this._$Rr, 2 * n), this._$Or = this._$UT(this._$Or, 2 * n);
                        var s = new Array;
                        ei._$jT(this._$Js, 0, s, 0, n), this._$Js = s
                    }
                    return this._$pb[this._$qo] = t, this._$_2[this._$qo] = i, this._$vr[this._$qo] = i, this._$Rr[this._$qo] = e, this._$Or[this._$qo] = r, this._$Js[this._$qo] = Y._$ZS, this._$qo++
                }, Y.prototype._$Zo = function(t, i) {
                    this._$3S[t] = i
                }, Y.prototype.setParamFloat = function(t, i) {
                    i < this._$Rr[t] && (i = this._$Rr[t]), i > this._$Or[t] && (i = this._$Or[t]), this._$_2[t] = i
                }, Y.prototype.loadParam = function() {
                    var t = this._$_2.length;
                    t > this._$fs.length && (t = this._$fs.length), ei._$jT(this._$fs, 0, this._$_2, 0, t)
                }, Y.prototype.saveParam = function() {
                    var t = this._$_2.length;
                    t > this._$fs.length && (this._$fs = new Float32Array(t)), ei._$jT(this._$_2, 0, this._$fs, 0, t)
                }, Y.prototype._$v2 = function() {
                    return this._$co
                }, Y.prototype._$WS = function() {
                    return this._$QT
                }, Y.prototype._$Xb = function(t) {
                    return this._$Js[t] == Y._$ZS
                }, Y.prototype._$vs = function() {
                    return this._$Es
                }, Y.prototype._$Tr = function() {
                    return this._$ZP
                }, Y.prototype.getBaseData = function(t) {
                    return this._$3S[t]
                }, Y.prototype.getParamFloat = function(t) {
                    return this._$_2[t]
                }, Y.prototype.getParamMax = function(t) {
                    return this._$Or[t]
                }, Y.prototype.getParamMin = function(t) {
                    return this._$Rr[t]
                }, Y.prototype.setPartsOpacity = function(t, i) {
                    this._$Hr[t].setPartsOpacity(i)
                }, Y.prototype.getPartsOpacity = function(t) {
                    return this._$Hr[t].getPartsOpacity()
                }, Y.prototype.getPartsDataIndex = function(t) {
                    for (var i = this._$F2.length - 1; i >= 0; --i)
                        if (null != this._$F2[i] && this._$F2[i]._$p2() == t) return i;
                    return -1
                }, Y.prototype._$q2 = function(t) {
                    return this._$db[t]
                }, Y.prototype._$C2 = function(t) {
                    return this._$8b[t]
                }, Y.prototype._$Bb = function(t) {
                    return this._$Hr[t]
                }, Y.prototype._$5s = function(t, i) {
                    for (var e = this._$Ws.length, r = t, n = 0; n < e; ++n) {
                        var o = this._$Ws[n];
                        if (o != Y._$V2)
                            for (;;) {
                                var s = this._$8b[o];
                                s._$yo() && (s._$GT()._$B2(this, s, r), r += i);
                                var a = this._$Er[o];
                                if (a <= o || a == Y._$W0) break;
                                o = a
                            }
                    }
                }, Y.prototype.setDrawParam = function(t) {
                    this.dp_webgl = t
                }, Y.prototype.getDrawParam = function() {
                    return this.dp_webgl
                }, V._$0T = function(t) {
                    return V._$0T(new _$5(t))
                }, V._$0T = function(t) {
                    if (!t.exists()) throw new _$ls(t._$3b());
                    for (var i, e = t.length(), r = new Int8Array(e), n = new _$Xs(new _$kb(t), 8192), o = 0;
                        (i = n.read(r, o, e - o)) > 0;) o += i;
                    return r
                }, V._$C = function(t) {
                    var i = null,
                        e = null;
                    try {
                        i = t instanceof Array ? t : new _$Xs(t, 8192), e = new _$js;
                        for (var r, n = new Int8Array(1e3);
                            (r = i.read(n)) > 0;) e.write(n, 0, r);
                        return e._$TS()
                    } finally {
                        null != t && t.close(), null != e && (e.flush(), e.close())
                    }
                }, j.prototype._$T2 = function() {
                    return ei.getUserTimeMSec() + Math._$10() * (2 * this._$Br - 1)
                }, j.prototype._$uo = function(t) {
                    this._$Br = t
                }, j.prototype._$QS = function(t, i, e) {
                    this._$Dr = t, this._$Cb = i, this._$mr = e
                }, j.prototype._$7T = function(t) {
                    var i, e = ei.getUserTimeMSec(),
                        r = 0;
                    switch (this._$_L) {
                        case STATE_CLOSING:
                            (r = (e - this._$bb) / this._$Dr) >= 1 && (r = 1, this._$_L = pi.STATE_CLOSED, this._$bb = e), i = 1 - r;
                            break;
                        case STATE_CLOSED:
                            (r = (e - this._$bb) / this._$Cb) >= 1 && (this._$_L = pi.STATE_OPENING, this._$bb = e), i = 0;
                            break;
                        case STATE_OPENING:
                            (r = (e - this._$bb) / this._$mr) >= 1 && (r = 1, this._$_L = pi.STATE_INTERVAL, this._$12 = this._$T2()), i = r;
                            break;
                        case STATE_INTERVAL:
                            this._$12 < e && (this._$_L = pi.STATE_CLOSING, this._$bb = e), i = 1;
                            break;
                        case STATE_FIRST:
                        default:
                            this._$_L = pi.STATE_INTERVAL, this._$12 = this._$T2(), i = 1
                    }
                    this._$jo || (i = -i), t.setParamFloat(this._$iL, i), t.setParamFloat(this._$0L, i)
                };
                var pi = function() {};
                pi.STATE_FIRST = "STATE_FIRST", pi.STATE_INTERVAL = "STATE_INTERVAL", pi.STATE_CLOSING = "STATE_CLOSING", pi.STATE_CLOSED = "STATE_CLOSED", pi.STATE_OPENING = "STATE_OPENING", X.prototype = new A, X._$As = 32, X._$Gr = !1, X._$NT = null, X._$vS = null, X._$no = null, X._$9r = function(t) {
                    return new Float32Array(t)
                }, X._$vb = function(t) {
                    return new Int16Array(t)
                }, X._$cr = function(t, i) {
                    return null == t || t._$yL() < i.length ? ((t = X._$9r(2 * i.length)).put(i), t._$oT(0)) : (t.clear(), t.put(i), t._$oT(0)), t
                }, X._$mb = function(t, i) {
                    return null == t || t._$yL() < i.length ? ((t = X._$vb(2 * i.length)).put(i), t._$oT(0)) : (t.clear(), t.put(i), t._$oT(0)), t
                }, X._$Hs = function() {
                    return X._$Gr
                }, X._$as = function(t) {
                    X._$Gr = t
                }, X.prototype.setGL = function(t) {
                    this.gl = t
                }, X.prototype.setTransform = function(t) {
                    this.transform = t
                }, X.prototype._$ZT = function() {}, X.prototype._$Uo = function(t, i, e, r, n, o, s, a) {
                    if (!(o < .01)) {
                        var h = this._$U2[t],
                            _ = o > .9 ? ti.EXPAND_W : 0;
                        this.gl.drawElements(h, e, r, n, o, _, this.transform, a)
                    }
                }, X.prototype._$Rs = function() {
                    throw new Error("_$Rs")
                }, X.prototype._$Ds = function(t) {
                    throw new Error("_$Ds")
                }, X.prototype._$K2 = function() {
                    for (var t = 0; t < this._$sb.length; t++) 0 != this._$sb[t] && (this.gl._$Sr(1, this._$sb, t), this._$sb[t] = 0)
                }, X.prototype.setTexture = function(t, i) {
                    this._$sb.length < t + 1 && this._$nS(t), this._$sb[t] = i
                }, X.prototype.setTexture = function(t, i) {
                    this._$sb.length < t + 1 && this._$nS(t), this._$U2[t] = i
                }, X.prototype._$nS = function(t) {
                    var i = Math.max(2 * this._$sb.length, t + 1 + 10),
                        e = new Int32Array(i);
                    ei._$jT(this._$sb, 0, e, 0, this._$sb.length), this._$sb = e;
                    var r = new Array;
                    ei._$jT(this._$U2, 0, r, 0, this._$U2.length), this._$U2 = r
                }, H.prototype = new b, H._$Xo = new Float32Array(2), H._$io = new Float32Array(2), H._$0o = new Float32Array(2), H._$Lo = new Float32Array(2), H._$To = new Float32Array(2), H._$Po = new Float32Array(2), H._$gT = new Array, H.prototype._$zP = function() {
                    this._$GS = new D, this._$GS._$zP(), this._$Y0 = new Array
                }, H.prototype.getType = function() {
                    return b._$c2
                }, H.prototype._$F0 = function(t) {
                    b.prototype._$F0.call(this, t), this._$GS = t._$nP(), this._$Y0 = t._$nP(), b.prototype.readV2_opacity.call(this, t)
                }, H.prototype.init = function(t) {
                    var i = new z(this);
                    return i._$Yr = new M, this._$32() && (i._$Wr = new M), i
                }, H.prototype._$Nr = function(t, i) {
                    this != i._$GT() && console.log("### assert!! ### ");
                    var e = i;
                    if (this._$GS._$Ur(t)) {
                        var r = H._$gT;
                        r[0] = !1;
                        var n = this._$GS._$Q2(t, r);
                        i._$Ib(r[0]), this.interpolateOpacity(t, this._$GS, i, r);
                        var o = t._$vs(),
                            s = t._$Tr();
                        if (this._$GS._$zr(o, s, n), n <= 0) {
                            var a = this._$Y0[o[0]];
                            e._$Yr.init(a)
                        } else if (1 == n) {
                            a = this._$Y0[o[0]];
                            var h = this._$Y0[o[1]],
                                _ = s[0];
                            e._$Yr._$fL = a._$fL + (h._$fL - a._$fL) * _, e._$Yr._$gL = a._$gL + (h._$gL - a._$gL) * _, e._$Yr._$B0 = a._$B0 + (h._$B0 - a._$B0) * _, e._$Yr._$z0 = a._$z0 + (h._$z0 - a._$z0) * _, e._$Yr._$qT = a._$qT + (h._$qT - a._$qT) * _
                        } else if (2 == n) {
                            a = this._$Y0[o[0]], h = this._$Y0[o[1]];
                            var u = this._$Y0[o[2]],
                                l = this._$Y0[o[3]],
                                c = (_ = s[0], s[1]),
                                p = a._$fL + (h._$fL - a._$fL) * _,
                                f = u._$fL + (l._$fL - u._$fL) * _;
                            e._$Yr._$fL = p + (f - p) * c, p = a._$gL + (h._$gL - a._$gL) * _, f = u._$gL + (l._$gL - u._$gL) * _, e._$Yr._$gL = p + (f - p) * c, p = a._$B0 + (h._$B0 - a._$B0) * _, f = u._$B0 + (l._$B0 - u._$B0) * _, e._$Yr._$B0 = p + (f - p) * c, p = a._$z0 + (h._$z0 - a._$z0) * _, f = u._$z0 + (l._$z0 - u._$z0) * _, e._$Yr._$z0 = p + (f - p) * c, p = a._$qT + (h._$qT - a._$qT) * _, f = u._$qT + (l._$qT - u._$qT) * _, e._$Yr._$qT = p + (f - p) * c
                        } else if (3 == n) {
                            var $ = this._$Y0[o[0]],
                                d = this._$Y0[o[1]],
                                g = this._$Y0[o[2]],
                                y = this._$Y0[o[3]],
                                m = this._$Y0[o[4]],
                                v = this._$Y0[o[5]],
                                S = this._$Y0[o[6]],
                                T = this._$Y0[o[7]],
                                P = (_ = s[0], c = s[1], s[2]),
                                L = (p = $._$fL + (d._$fL - $._$fL) * _, f = g._$fL + (y._$fL - g._$fL) * _, m._$fL + (v._$fL - m._$fL) * _),
                                M = S._$fL + (T._$fL - S._$fL) * _;
                            e._$Yr._$fL = (1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c), p = $._$gL + (d._$gL - $._$gL) * _, f = g._$gL + (y._$gL - g._$gL) * _, L = m._$gL + (v._$gL - m._$gL) * _, M = S._$gL + (T._$gL - S._$gL) * _, e._$Yr._$gL = (1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c), p = $._$B0 + (d._$B0 - $._$B0) * _, f = g._$B0 + (y._$B0 - g._$B0) * _, L = m._$B0 + (v._$B0 - m._$B0) * _, M = S._$B0 + (T._$B0 - S._$B0) * _, e._$Yr._$B0 = (1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c), p = $._$z0 + (d._$z0 - $._$z0) * _, f = g._$z0 + (y._$z0 - g._$z0) * _, L = m._$z0 + (v._$z0 - m._$z0) * _, M = S._$z0 + (T._$z0 - S._$z0) * _, e._$Yr._$z0 = (1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c), p = $._$qT + (d._$qT - $._$qT) * _, f = g._$qT + (y._$qT - g._$qT) * _, L = m._$qT + (v._$qT - m._$qT) * _, M = S._$qT + (T._$qT - S._$qT) * _, e._$Yr._$qT = (1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c)
                        } else if (4 == n) {
                            var E = this._$Y0[o[0]],
                                w = this._$Y0[o[1]],
                                x = this._$Y0[o[2]],
                                O = this._$Y0[o[3]],
                                A = this._$Y0[o[4]],
                                I = this._$Y0[o[5]],
                                b = this._$Y0[o[6]],
                                C = this._$Y0[o[7]],
                                D = this._$Y0[o[8]],
                                R = this._$Y0[o[9]],
                                F = this._$Y0[o[10]],
                                N = this._$Y0[o[11]],
                                B = this._$Y0[o[12]],
                                G = this._$Y0[o[13]],
                                k = this._$Y0[o[14]],
                                U = this._$Y0[o[15]],
                                Y = (_ = s[0], c = s[1], P = s[2], s[3]),
                                V = (p = E._$fL + (w._$fL - E._$fL) * _, f = x._$fL + (O._$fL - x._$fL) * _, L = A._$fL + (I._$fL - A._$fL) * _, M = b._$fL + (C._$fL - b._$fL) * _, D._$fL + (R._$fL - D._$fL) * _),
                                j = F._$fL + (N._$fL - F._$fL) * _,
                                X = B._$fL + (G._$fL - B._$fL) * _,
                                z = k._$fL + (U._$fL - k._$fL) * _;
                            e._$Yr._$fL = (1 - Y) * ((1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c)) + Y * ((1 - P) * (V + (j - V) * c) + P * (X + (z - X) * c)), p = E._$gL + (w._$gL - E._$gL) * _, f = x._$gL + (O._$gL - x._$gL) * _, L = A._$gL + (I._$gL - A._$gL) * _, M = b._$gL + (C._$gL - b._$gL) * _, V = D._$gL + (R._$gL - D._$gL) * _, j = F._$gL + (N._$gL - F._$gL) * _, X = B._$gL + (G._$gL - B._$gL) * _, z = k._$gL + (U._$gL - k._$gL) * _, e._$Yr._$gL = (1 - Y) * ((1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c)) + Y * ((1 - P) * (V + (j - V) * c) + P * (X + (z - X) * c)), p = E._$B0 + (w._$B0 - E._$B0) * _, f = x._$B0 + (O._$B0 - x._$B0) * _, L = A._$B0 + (I._$B0 - A._$B0) * _, M = b._$B0 + (C._$B0 - b._$B0) * _, V = D._$B0 + (R._$B0 - D._$B0) * _, j = F._$B0 + (N._$B0 - F._$B0) * _, X = B._$B0 + (G._$B0 - B._$B0) * _, z = k._$B0 + (U._$B0 - k._$B0) * _, e._$Yr._$B0 = (1 - Y) * ((1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c)) + Y * ((1 - P) * (V + (j - V) * c) + P * (X + (z - X) * c)), p = E._$z0 + (w._$z0 - E._$z0) * _, f = x._$z0 + (O._$z0 - x._$z0) * _, L = A._$z0 + (I._$z0 - A._$z0) * _, M = b._$z0 + (C._$z0 - b._$z0) * _, V = D._$z0 + (R._$z0 - D._$z0) * _, j = F._$z0 + (N._$z0 - F._$z0) * _, X = B._$z0 + (G._$z0 - B._$z0) * _, z = k._$z0 + (U._$z0 - k._$z0) * _, e._$Yr._$z0 = (1 - Y) * ((1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c)) + Y * ((1 - P) * (V + (j - V) * c) + P * (X + (z - X) * c)), p = E._$qT + (w._$qT - E._$qT) * _, f = x._$qT + (O._$qT - x._$qT) * _, L = A._$qT + (I._$qT - A._$qT) * _, M = b._$qT + (C._$qT - b._$qT) * _, V = D._$qT + (R._$qT - D._$qT) * _, j = F._$qT + (N._$qT - F._$qT) * _, X = B._$qT + (G._$qT - B._$qT) * _, z = k._$qT + (U._$qT - k._$qT) * _, e._$Yr._$qT = (1 - Y) * ((1 - P) * (p + (f - p) * c) + P * (L + (M - L) * c)) + Y * ((1 - P) * (V + (j - V) * c) + P * (X + (z - X) * c))
                        } else {
                            for (var W = 0 | Math.pow(2, n), q = new Float32Array(W), Z = 0; Z < W; Z++) {
                                for (var J = Z, Q = 1, K = 0; K < n; K++) Q *= J % 2 == 0 ? 1 - s[K] : s[K], J /= 2;
                                q[Z] = Q
                            }
                            for (var tt = new Array, it = 0; it < W; it++) tt[it] = this._$Y0[o[it]];
                            var et = 0,
                                rt = 0,
                                nt = 0,
                                ot = 0,
                                st = 0;
                            for (it = 0; it < W; it++) et += q[it] * tt[it]._$fL, rt += q[it] * tt[it]._$gL, nt += q[it] * tt[it]._$B0, ot += q[it] * tt[it]._$z0, st += q[it] * tt[it]._$qT;
                            e._$Yr._$fL = et, e._$Yr._$gL = rt, e._$Yr._$B0 = nt, e._$Yr._$z0 = ot, e._$Yr._$qT = st
                        }
                        a = this._$Y0[o[0]], e._$Yr.reflectX = a.reflectX, e._$Yr.reflectY = a.reflectY
                    }
                }, H.prototype._$2b = function(t, i) {
                    this != i._$GT() && console.log("### assert!! ### ");
                    var e = i;
                    if (e._$hS(!0), this._$32()) {
                        var n = this.getTargetBaseDataID();
                        if (e._$8r == b._$ur && (e._$8r = t.getBaseDataIndex(n)), e._$8r < 0) ti._$so && ni.println("_$L _$0P _$G :: %s", n), e._$hS(!1);
                        else {
                            var o = t.getBaseData(e._$8r);
                            if (null != o) {
                                var s = t._$q2(e._$8r),
                                    a = H._$Xo;
                                a[0] = e._$Yr._$fL, a[1] = e._$Yr._$gL;
                                var h = H._$io;
                                h[0] = 0, h[1] = -.1, s._$GT().getType() == b._$c2 ? h[1] = -10 : h[1] = -.1;
                                var _ = H._$0o;
                                this._$Jr(t, o, s, a, h, _);
                                var u = r._$92(h, _);
                                o._$nb(t, s, a, a, 1, 0, 2), e._$Wr._$fL = a[0], e._$Wr._$gL = a[1], e._$Wr._$B0 = e._$Yr._$B0, e._$Wr._$z0 = e._$Yr._$z0, e._$Wr._$qT = e._$Yr._$qT - u * r._$NS;
                                var l = s.getTotalScale();
                                e.setTotalScale_notForClient(l * e._$Wr._$B0);
                                var c = s.getTotalOpacity();
                                e.setTotalOpacity(c * e.getInterpolatedOpacity()), e._$Wr.reflectX = e._$Yr.reflectX, e._$Wr.reflectY = e._$Yr.reflectY, e._$hS(s._$yo())
                            } else e._$hS(!1)
                        }
                    } else e.setTotalScale_notForClient(e._$Yr._$B0), e.setTotalOpacity(e.getInterpolatedOpacity())
                }, H.prototype._$nb = function(t, i, e, n, o, s, a) {
                    this != i._$GT() && console.log("### assert!! ### ");
                    for (var h, _, u = i, l = null != u._$Wr ? u._$Wr : u._$Yr, c = Math.sin(r._$bS * l._$qT), p = Math.cos(r._$bS * l._$qT), f = u.getTotalScale(), $ = l.reflectX ? -1 : 1, d = l.reflectY ? -1 : 1, g = p * f * $, y = -c * f * d, m = c * f * $, v = p * f * d, S = l._$fL, T = l._$gL, P = o * a, L = s; L < P; L += a) h = e[L], _ = e[L + 1], n[L] = g * h + y * _ + S, n[L + 1] = m * h + v * _ + T
                }, H.prototype._$Jr = function(t, i, e, r, n, o) {
                    i != e._$GT() && console.log("### assert!! ### ");
                    var s = H._$Lo;
                    H._$Lo[0] = r[0], H._$Lo[1] = r[1], i._$nb(t, e, s, s, 1, 0, 2);
                    for (var a = H._$To, h = H._$Po, _ = 1, u = 0; u < 10; u++) {
                        if (h[0] = r[0] + _ * n[0], h[1] = r[1] + _ * n[1], i._$nb(t, e, h, a, 1, 0, 2), a[0] -= s[0], a[1] -= s[1], 0 != a[0] || 0 != a[1]) return o[0] = a[0], void(o[1] = a[1]);
                        if (h[0] = r[0] - _ * n[0], h[1] = r[1] - _ * n[1], i._$nb(t, e, h, a, 1, 0, 2), a[0] -= s[0], a[1] -= s[1], 0 != a[0] || 0 != a[1]) return a[0] = -a[0], a[0] = -a[0], o[0] = a[0], void(o[1] = a[1]);
                        _ *= .1
                    }
                    ti._$so && console.log("_$L0 to transform _$SP\n")
                }, z.prototype = new at, W.prototype = new O, W._$ur = -2, W._$ES = 500, W._$wb = 2, W._$8S = 3, W._$os = 4, W._$52 = W._$ES, W._$R2 = W._$ES, W._$Sb = function(t) {
                    for (var i = t.length - 1; i >= 0; --i) {
                        var e = t[i];
                        e < W._$52 ? W._$52 = e : e > W._$R2 && (W._$R2 = e)
                    }
                }, W._$or = function() {
                    return W._$52
                }, W._$Pr = function() {
                    return W._$R2
                }, W.prototype._$F0 = function(t) {
                    this._$gP = t._$nP(), this._$dr = t._$nP(), this._$GS = t._$nP(), this._$qb = t._$6L(), this._$Lb = t._$cS(), this._$mS = t._$Tb(), t.getFormatVersion() >= U._$T7 ? (this.clipID = t._$nP(), this.clipIDList = this.convertClipIDForV2_11(this.clipID)) : this.clipIDList = null, W._$Sb(this._$Lb)
                }, W.prototype.getClipIDList = function() {
                    return this.clipIDList
                }, W.prototype._$Nr = function(t, i) {
                    if (i._$IS[0] = !1, i._$Us = w._$Z2(t, this._$GS, i._$IS, this._$Lb), ti._$Zs);
                    else if (i._$IS[0]) return;
                    i._$7s = w._$br(t, this._$GS, i._$IS, this._$mS)
                }, W.prototype._$2b = function(t) {}, W.prototype.getDrawDataID = function() {
                    return this._$gP
                }, W.prototype._$j2 = function(t) {
                    this._$gP = t
                }, W.prototype.getOpacity = function(t, i) {
                    return i._$7s
                }, W.prototype._$zS = function(t, i) {
                    return i._$Us
                }, W.prototype.getTargetBaseDataID = function() {
                    return this._$dr
                }, W.prototype._$gs = function(t) {
                    this._$dr = t
                }, W.prototype._$32 = function() {
                    return null != this._$dr && this._$dr != $t._$2o()
                }, W.prototype.getType = function() {}, q._$42 = 0, q.prototype._$1b = function() {
                    return this._$3S
                }, q.prototype.getDrawDataList = function() {
                    return this._$aS
                }, q.prototype._$F0 = function(t) {
                    this._$NL = t._$nP(), this._$aS = t._$nP(), this._$3S = t._$nP()
                }, q.prototype._$kr = function(t) {
                    t._$Zo(this._$3S), t._$xo(this._$aS), this._$3S = null, this._$aS = null
                }, Z.prototype = new n, Z._$cs = "VISIBLE:", Z._$ar = "LAYOUT:", Z.MTN_PREFIX_FADEIN = "FADEIN:", Z.MTN_PREFIX_FADEOUT = "FADEOUT:", Z._$Co = 0, Z._$1T = 1, Z.loadMotion = function(t) {
                    var i = V._$C(t);
                    return Z.loadMotion(i)
                }, Z.loadMotion = function(t) {
                    t instanceof ArrayBuffer && (t = new DataView(t));
                    var i = new Z,
                        e = [0],
                        r = t.byteLength;
                    i._$yT = 0;
                    for (var n = 0; n < r; ++n) {
                        var o = J(t, n),
                            s = o.charCodeAt(0);
                        if ("\n" != o && "\r" != o)
                            if ("#" != o)
                                if ("$" != o) {
                                    if (97 <= s && s <= 122 || 65 <= s && s <= 90 || "_" == o) {
                                        for (var a = n, h = -1; n < r && "\r" != (o = J(t, n)) && "\n" != o; ++n)
                                            if ("=" == o) {
                                                h = n;
                                                break
                                            }
                                        if (h >= 0) {
                                            var _ = new G;
                                            ui.startsWith(t, a, Z._$cs) ? (_._$RP = G._$hs, _._$4P = ui.createString(t, a, h - a)) : ui.startsWith(t, a, Z._$ar) ? (_._$4P = ui.createString(t, a + 7, h - a - 7), ui.startsWith(t, a + 7, "ANCHOR_X") ? _._$RP = G._$xs : ui.startsWith(t, a + 7, "ANCHOR_Y") ? _._$RP = G._$us : ui.startsWith(t, a + 7, "SCALE_X") ? _._$RP = G._$qs : ui.startsWith(t, a + 7, "SCALE_Y") ? _._$RP = G._$Ys : ui.startsWith(t, a + 7, "X") ? _._$RP = G._$ws : ui.startsWith(t, a + 7, "Y") && (_._$RP = G._$Ns)) : (_._$RP = G._$Fr, _._$4P = ui.createString(t, a, h - a)), i.motions.push(_);
                                            var u = 0,
                                                l = [];
                                            for (n = h + 1; n < r && "\r" != (o = J(t, n)) && "\n" != o; ++n)
                                                if ("," != o && " " != o && "\t" != o) {
                                                    var c = ui._$LS(t, r, n, e);
                                                    if (e[0] > 0) {
                                                        l.push(c), u++;
                                                        var p = e[0];
                                                        if (p < n) {
                                                            console.log("_$n0 _$hi . @Live2DMotion loadMotion()\n");
                                                            break
                                                        }
                                                        n = p - 1
                                                    }
                                                }
                                            _._$I0 = new Float32Array(l), u > i._$yT && (i._$yT = u)
                                        }
                                    }
                                } else {
                                    for (a = n, h = -1; n < r && "\r" != (o = J(t, n)) && "\n" != o; ++n)
                                        if ("=" == o) {
                                            h = n;
                                            break
                                        }
                                    var f = !1;
                                    if (h >= 0)
                                        for (h == a + 4 && "f" == J(t, a + 1) && "p" == J(t, a + 2) && "s" == J(t, a + 3) && (f = !0), n = h + 1; n < r && "\r" != (o = J(t, n)) && "\n" != o; ++n) "," != o && " " != o && "\t" != o && (c = ui._$LS(t, r, n, e), e[0] > 0 && f && 5 < c && c < 121 && (i._$D0 = c), n = e[0]);
                                    for (; n < r && "\n" != J(t, n) && "\r" != J(t, n); ++n);
                                }
                        else
                            for (; n < r && "\n" != J(t, n) && "\r" != J(t, n); ++n);
                    }
                    return i._$rr = 1e3 * i._$yT / i._$D0 | 0, i
                }, Z.prototype.getDurationMSec = function() {
                    return this._$E ? -1 : this._$rr
                }, Z.prototype.getLoopDurationMSec = function() {
                    return this._$rr
                }, Z.prototype.dump = function() {
                    for (var t = 0; t < this.motions.length; t++) {
                        var i = this.motions[t];
                        console.log("_$wL[%s] [%d]. ", i._$4P, i._$I0.length);
                        for (var e = 0; e < i._$I0.length && e < 10; e++) console.log("%5.2f ,", i._$I0[e]);
                        console.log("\n")
                    }
                }, Z.prototype.updateParamExe = function(t, i, e, r) {
                    for (var n = (i - r._$z2) * this._$D0 / 1e3, o = 0 | n, s = n - o, a = 0; a < this.motions.length; a++) {
                        var h = this.motions[a],
                            _ = h._$I0.length,
                            u = h._$4P;
                        if (h._$RP == G._$hs) {
                            var l = h._$I0[o >= _ ? _ - 1 : o];
                            t.setParamFloat(u, l)
                        } else if (G._$ws <= h._$RP && h._$RP <= G._$Ys);
                        else {
                            var c = t.getParamIndex(u),
                                p = t.getModelContext(),
                                f = .4 * (p.getParamMax(c) - p.getParamMin(c)),
                                $ = p.getParamFloat(c),
                                d = h._$I0[o >= _ ? _ - 1 : o],
                                g = h._$I0[o + 1 >= _ ? _ - 1 : o + 1],
                                y = $ + ((d < g && g - d > f || d > g && d - g > f ? d : d + (g - d) * s) - $) * e;
                            t.setParamFloat(u, y)
                        }
                    }
                    o >= this._$yT && (this._$E ? (r._$z2 = i, this.loopFadeIn && (r._$bs = i)) : r._$9L = !0), this._$eP = e
                }, Z.prototype._$r0 = function() {
                    return this._$E
                }, Z.prototype._$aL = function(t) {
                    this._$E = t
                }, Z.prototype._$S0 = function() {
                    return this._$D0
                }, Z.prototype._$U0 = function(t) {
                    this._$D0 = t
                }, Z.prototype.isLoopFadeIn = function() {
                    return this.loopFadeIn
                }, Z.prototype.setLoopFadeIn = function(t) {
                    this.loopFadeIn = t
                }, Q._$gT = [], Q.transformPoints_sdk2 = function(t, i, e, r, n, o, s, a) {
                    for (var h = e * n, _ = 0, u = 0, l = 0, c = 0, p = 0, f = 0, $ = !1, d = r; d < h; d += n) {
                        var g = t[d],
                            y = t[d + 1],
                            m = g * s,
                            v = y * a;
                        if (m < 0 || v < 0 || s <= m || a <= v) {
                            var S = s + 1,
                                T = a * S * 2,
                                P = 2 * s,
                                L = 2 * (s + a * S),
                                M = o[0],
                                E = o[1],
                                w = o[T],
                                x = o[P],
                                O = o[P + 1],
                                A = o[L],
                                I = o[L + 1];
                            if (!$) {
                                $ = !0, _ = .25 * (M + x + w + A), u = .25 * (E + O + o[T + 1] + I);
                                var b = A - M,
                                    C = I - E,
                                    D = x - w,
                                    R = O - o[T + 1];
                                _ -= .5 * ((l = .5 * (b + D)) + (p = .5 * (b - D))), u -= .5 * ((c = .5 * (C + R)) + (f = .5 * (C - R)))
                            }
                            var F = void 0,
                                N = void 0,
                                B = void 0,
                                G = void 0,
                                k = void 0,
                                U = void 0,
                                Y = void 0,
                                V = void 0,
                                j = void 0,
                                X = void 0,
                                H = void 0,
                                z = void 0,
                                W = void 0,
                                q = void 0,
                                Z = void 0,
                                J = void 0;
                            if (-2 < g && g < 3 && -2 < y && y < 3)
                                if (g <= 0)
                                    if (y <= 0) U = _ - 2 * l, Y = u - 2 * c, G = _ - 2 * p, k = u - 2 * f, B = _ - 2 * l - 2 * p, N = u - 2 * c - 2 * f, (F = .5 * (g - -2)) + (V = .5 * (y - -2)) <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = M + (U - M) * (1 - F) + (G - M) * (1 - V), i[d + 1] = E + (Y - E) * (1 - F) + (k - E) * (1 - V));
                                    else if (0 < y && y < 1) {
                                (H = 0 | v) === a && (H = a - 1), F = .5 * (g - -2), V = v - H, W = H / a, z = (H + 1) / a;
                                var Q = H * S * 2;
                                G = o[Q], k = o[Q + 1];
                                var K = (H + 1) * S * 2;
                                X = o[K], j = o[K + 1], B = _ - 2 * l + W * p, N = u - 2 * c + W * f, U = _ - 2 * l + z * p, Y = u - 2 * c + z * f, F + V <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V))
                            } else {
                                var tt = 2 * (0 + a * S);
                                G = o[tt], k = o[tt + 1], B = _ - 2 * l + 1 * p, N = u - 2 * c + 1 * f, X = _ + 3 * p, j = u + 3 * f, U = _ - 2 * l + 3 * p, Y = u - 2 * c + 3 * f, (F = .5 * (g - -2)) + (V = .5 * (y - 1)) <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V))
                            } else 0 < g && g < 1 ? y <= 0 ? ((Z = 0 | m) === s && (Z = s - 1), F = m - Z, V = .5 * (y - -2), J = Z / s, q = (Z + 1) / s, U = o[2 * (Z + 0 * S)], Y = o[2 * (Z + 0 * S) + 1], X = o[2 * (Z + 1 + 0 * S)], j = o[2 * (Z + 1 + 0 * S) + 1], B = _ + J * l - 2 * p, N = u + J * c - 2 * f, G = _ + q * l - 2 * p, k = u + q * c - 2 * f, F + V <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V))) : 0 < y && y < 1 ? console.log("_$li calc : %.4f , %.4f\t\t\t\t\t@@BDBoxGrid\n", g, y) : ((Z = 0 | m) === s && (Z = s - 1), F = m - Z, V = .5 * (y - 1), J = Z / s, q = (Z + 1) / s, B = o[2 * (Z + a * S)], N = o[2 * (Z + a * S) + 1], G = o[2 * (Z + 1 + a * S)], k = o[2 * (Z + 1 + a * S) + 1], U = _ + J * l + 3 * p, Y = u + J * c + 3 * f, X = _ + q * l + 3 * p, j = u + q * c + 3 * f, F + V <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V))) : y <= 0 ? (U = x, Y = O, X = _ + 3 * l, j = u + 3 * c, B = _ + 1 * l - 2 * p, N = u + 1 * c - 2 * f, G = _ + 3 * l - 2 * p, k = u + 3 * c - 2 * f, (F = .5 * (g - 1)) + (V = .5 * (y - -2)) <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V))) : 0 < y && y < 1 ? ((H = 0 | v) === a && (H = a - 1), F = .5 * (g - 1), V = v - H, W = H / a, z = (H + 1) / a, B = o[2 * (s + H * S)], N = o[2 * (s + H * S) + 1], U = o[2 * (s + (H + 1) * S)], Y = o[2 * (s + (H + 1) * S) + 1], G = _ + 3 * l + W * p, k = u + 3 * c + W * f, X = _ + 3 * l + z * p, j = u + 3 * c + z * f, F + V <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V))) : (B = A, N = I, G = _ + 3 * l + 1 * p, k = u + 3 * c + 1 * f, U = _ + 1 * l + 3 * p, Y = u + 1 * c + 3 * f, X = _ + 3 * l + 3 * p, j = u + 3 * c + 3 * f, (F = .5 * (g - 1)) + (V = .5 * (y - 1)) <= 1 ? (i[d] = B + (G - B) * F + (U - B) * V, i[d + 1] = N + (k - N) * F + (Y - N) * V) : (i[d] = X + (U - X) * (1 - F) + (G - X) * (1 - V), i[d + 1] = j + (Y - j) * (1 - F) + (k - j) * (1 - V)));
                            else i[d] = _ + g * l + y * p, i[d + 1] = u + g * c + y * f
                        } else {
                            var it = m - (0 | m),
                                et = v - (0 | v),
                                rt = 2 * ((0 | m) + (0 | v) * (s + 1)),
                                nt = rt + 2 * (s + 1);
                            if (it + et < 1) {
                                var ot = 1 - it - et;
                                i[d] = o[rt] * ot + o[rt + 2] * it + o[nt] * et, i[d + 1] = o[rt + 1] * ot + o[rt + 3] * it + o[nt + 1] * et
                            } else {
                                var st = it - 1 + et;
                                i[d] = o[nt + 2] * st + o[nt] * (1 - it) + o[rt + 2] * (1 - et), i[d + 1] = o[nt + 3] * st + o[nt + 1] * (1 - it) + o[rt + 3] * (1 - et)
                            }
                        }
                    }
                }, Q.prototype = new b, Q.prototype._$zP = function() {
                    this._$GS = new D, this._$GS._$zP()
                }, Q.prototype._$F0 = function(t) {
                    b.prototype._$F0.call(this, t), this._$A = t._$6L(), this._$o = t._$6L(), this._$GS = t._$nP(), this._$Eo = t._$nP(), b.prototype.readV2_opacity.call(this, t)
                }, Q.prototype.init = function(t) {
                    var i = new K(this),
                        e = (this._$o + 1) * (this._$A + 1);
                    return null != i._$Cr && (i._$Cr = null), i._$Cr = new Float32Array(2 * e), null != i._$hr && (i._$hr = null), this._$32() ? i._$hr = new Float32Array(2 * e) : i._$hr = null, i
                }, Q.prototype._$Nr = function(t, i) {
                    var e = i;
                    if (this._$GS._$Ur(t)) {
                        var r = this._$VT(),
                            n = Q._$gT;
                        n[0] = !1, w._$Vr(t, this._$GS, n, r, this._$Eo, e._$Cr, 0, 2), i._$Ib(n[0]), this.interpolateOpacity(t, this._$GS, i, n)
                    }
                }, Q.prototype._$2b = function(t, i) {
                    var e = i;
                    if (e._$hS(!0), this._$32()) {
                        var r = this.getTargetBaseDataID();
                        if (e._$8r == b._$ur && (e._$8r = t.getBaseDataIndex(r)), e._$8r < 0) ti._$so && ni.println("_$L _$0P _$G :: %s", r), e._$hS(!1);
                        else {
                            var n = t.getBaseData(e._$8r),
                                o = t._$q2(e._$8r);
                            if (null != n && o._$yo()) {
                                var s = o.getTotalScale();
                                e.setTotalScale_notForClient(s);
                                var a = o.getTotalOpacity();
                                e.setTotalOpacity(a * e.getInterpolatedOpacity()), n._$nb(t, o, e._$Cr, e._$hr, this._$VT(), 0, 2), e._$hS(!0)
                            } else e._$hS(!1)
                        }
                    } else e.setTotalOpacity(e.getInterpolatedOpacity())
                }, Q.prototype._$nb = function(t, i, e, r, n, o, s) {
                    var a = i,
                        h = null != a._$hr ? a._$hr : a._$Cr;
                    Q.transformPoints_sdk2(e, r, n, o, s, h, this._$o, this._$A)
                }, Q.prototype.transformPoints_sdk1 = function(t, i, e, r, n, o, s) {
                    for (var a = i, h = void 0, _ = void 0, u = this._$o, l = this._$A, c = n * s, p = void 0, f = void 0, $ = void 0, d = void 0, g = void 0, y = null != a._$hr ? a._$hr : a._$Cr, m = o; m < c; m += s) ti._$ts ? ((h = e[m]) < 0 ? h = 0 : h > 1 && (h = 1), (_ = e[m + 1]) < 0 ? _ = 0 : _ > 1 && (_ = 1), (p = 0 | (h *= u)) > u - 1 && (p = u - 1), (f = 0 | (_ *= l)) > l - 1 && (f = l - 1), d = h - p, g = _ - f, $ = 2 * (p + f * (u + 1))) : (d = (h = e[m] * u) - (0 | h), g = (_ = e[m + 1] * l) - (0 | _), $ = 2 * ((0 | h) + (0 | _) * (u + 1))), d + g < 1 ? (r[m] = y[$] * (1 - d - g) + y[$ + 2] * d + y[$ + 2 * (u + 1)] * g, r[m + 1] = y[$ + 1] * (1 - d - g) + y[$ + 3] * d + y[$ + 2 * (u + 1) + 1] * g) : (r[m] = y[$ + 2 * (u + 1) + 2] * (d - 1 + g) + y[$ + 2 * (u + 1)] * (1 - d) + y[$ + 2] * (1 - g), r[m + 1] = y[$ + 2 * (u + 1) + 3] * (d - 1 + g) + y[$ + 2 * (u + 1) + 1] * (1 - d) + y[$ + 3] * (1 - g))
                }, Q.prototype._$VT = function() {
                    return (this._$o + 1) * (this._$A + 1)
                }, Q.prototype.getType = function() {
                    return b._$_b
                }, K.prototype = new at, tt._$42 = 0, tt.prototype._$zP = function() {
                    this._$3S = new Array, this._$aS = new Array
                }, tt.prototype._$F0 = function(t) {
                    this._$g0 = t._$8L(), this.visible = t._$8L(), this._$NL = t._$nP(), this._$3S = t._$nP(), this._$aS = t._$nP()
                }, tt.prototype.init = function(t) {
                    var i = new it(this);
                    return i.setPartsOpacity(this.isVisible() ? 1 : 0), i
                }, tt.prototype._$6o = function(t) {
                    if (null == this._$3S) throw new Error("_$3S _$6 _$Wo@_$6o");
                    this._$3S.push(t)
                }, tt.prototype._$3o = function(t) {
                    if (null == this._$aS) throw new Error("_$aS _$6 _$Wo@_$3o");
                    this._$aS.push(t)
                }, tt.prototype._$Zo = function(t) {
                    this._$3S = t
                }, tt.prototype._$xo = function(t) {
                    this._$aS = t
                }, tt.prototype.isVisible = function() {
                    return this.visible
                }, tt.prototype._$uL = function() {
                    return this._$g0
                }, tt.prototype._$KP = function(t) {
                    this.visible = t
                }, tt.prototype._$ET = function(t) {
                    this._$g0 = t
                }, tt.prototype.getBaseData = function() {
                    return this._$3S
                }, tt.prototype.getDrawData = function() {
                    return this._$aS
                }, tt.prototype._$p2 = function() {
                    return this._$NL
                }, tt.prototype._$ob = function(t) {
                    this._$NL = t
                }, tt.prototype.getPartsID = function() {
                    return this._$NL
                }, tt.prototype._$MP = function(t) {
                    this._$NL = t
                }, it.prototype = new $, it.prototype.getPartsOpacity = function() {
                    return this._$VS
                }, it.prototype.setPartsOpacity = function(t) {
                    this._$VS = t
                }, et._$L7 = function() {
                    d._$27(), $t._$27(), F._$27(), f._$27()
                }, et.prototype.toString = function() {
                    return this.id
                }, rt.prototype._$F0 = function(t) {}, nt.prototype._$1s = function() {
                    return this._$4S
                }, nt.prototype._$zP = function() {
                    this._$4S = new Array
                }, nt.prototype._$F0 = function(t) {
                    this._$4S = t._$nP()
                }, nt.prototype._$Ks = function(t) {
                    this._$4S.push(t)
                }, ot.tr = new oi, ot._$50 = new oi, ot._$Ti = new Array(0, 0), ot._$Pi = new Array(0, 0), ot._$B = new Array(0, 0), ot.prototype._$lP = function(t, i, e, r) {
                    this.viewport = new Array(t, i, e, r)
                }, ot.prototype._$bL = function() {
                    this.context.save();
                    var t = this.viewport;
                    null != t && (this.context.beginPath(), this.context._$Li(t[0], t[1], t[2], t[3]), this.context.clip())
                }, ot.prototype._$ei = function() {
                    this.context.restore()
                }, ot.prototype.drawElements = function(t, i, e, r, n, o, s, a) {
                    try {
                        n != this._$Qo && (this._$Qo = n, this.context.globalAlpha = n);
                        for (var h = i.length, _ = t.width, u = t.height, l = this.context, c = this._$xP, p = this._$uP, f = this._$6r, $ = this._$3r, d = ot.tr, g = ot._$Ti, y = ot._$Pi, m = ot._$B, v = 0; v < h; v += 3) {
                            l.save();
                            var S = i[v],
                                T = i[v + 1],
                                P = i[v + 2],
                                L = c + f * e[2 * S],
                                M = p + $ * e[2 * S + 1],
                                E = c + f * e[2 * T],
                                w = p + $ * e[2 * T + 1],
                                x = c + f * e[2 * P],
                                O = p + $ * e[2 * P + 1];
                            s && (s._$PS(L, M, m), L = m[0], M = m[1], s._$PS(E, w, m), E = m[0], w = m[1], s._$PS(x, O, m), x = m[0], O = m[1]);
                            var A = _ * r[2 * S],
                                I = u - u * r[2 * S + 1],
                                b = _ * r[2 * T],
                                C = u - u * r[2 * T + 1],
                                D = _ * r[2 * P],
                                R = u - u * r[2 * P + 1],
                                F = Math.atan2(C - I, b - A),
                                N = Math.atan2(w - M, E - L),
                                B = E - L,
                                G = w - M,
                                k = Math.sqrt(B * B + G * G),
                                U = b - A,
                                Y = C - I,
                                V = k / Math.sqrt(U * U + Y * Y);
                            ci._$ni(D, R, A, I, b - A, C - I, -(C - I), b - A, g), ci._$ni(x, O, L, M, E - L, w - M, -(w - M), E - L, y);
                            var j = (y[0] - g[0]) / g[1],
                                X = Math.min(A, b, D),
                                H = Math.max(A, b, D),
                                z = Math.min(I, C, R),
                                W = Math.max(I, C, R),
                                q = Math.floor(X),
                                Z = Math.floor(z),
                                J = Math.ceil(H),
                                Q = Math.ceil(W);
                            if (d.identity(), d.translate(L, M), d.rotate(N), d.scale(1, y[1] / g[1]), d.shear(j, 0), d.scale(V, V), d.rotate(-F), d.translate(-A, -I), d.setContext(l), o || (o = 1.2), ti.IGNORE_EXPAND && (o = 0), ti.USE_CACHED_POLYGON_IMAGE) {
                                var K = a._$e0;
                                if (K.gl_cacheImage = K.gl_cacheImage || {}, !K.gl_cacheImage[v]) {
                                    var tt = ot.createCanvas(J - q, Q - Z);
                                    ti.DEBUG_DATA.LDGL_CANVAS_MB = ti.DEBUG_DATA.LDGL_CANVAS_MB || 0, ti.DEBUG_DATA.LDGL_CANVAS_MB += (J - q) * (Q - Z) * 4;
                                    var it = tt.getContext("2d");
                                    it.translate(-q, -Z), ot.clip(it, d, o, k, A, I, b, C, D, R, L, M, E, w, x, O), it.drawImage(t, 0, 0), K.gl_cacheImage[v] = {
                                        cacheCanvas: tt,
                                        cacheContext: it
                                    }
                                }
                                l.drawImage(K.gl_cacheImage[v].cacheCanvas, q, Z)
                            } else ti.IGNORE_CLIP || ot.clip(l, d, o, k, A, I, b, C, D, R, L, M, E, w, x, O), ti.USE_ADJUST_TRANSLATION && (X = 0, H = _, z = 0, W = u), l.drawImage(t, X, z, H - X, W - z, X, z, H - X, W - z);
                            l.restore()
                        }
                    } catch (t) {
                        ni.error(t)
                    }
                }, ot.clip = function(t, i, e, r, n, o, s, a, h, _, u, l, c, p, f, $) {
                    e > .02 ? ot.expandClip(t, i, e, r, u, l, c, p, f, $) : ot.clipWithTransform(t, null, n, o, s, a, h, _)
                }, ot.expandClip = function(t, i, e, r, n, o, s, a, h, _) {
                    var u = s - n,
                        l = a - o,
                        c = h - n,
                        p = _ - o,
                        f = u * p - l * c > 0 ? e : -e,
                        $ = -l,
                        d = u,
                        g = h - s,
                        y = _ - a,
                        m = -y,
                        v = g,
                        S = Math.sqrt(g * g + y * y),
                        T = -p,
                        P = c,
                        L = Math.sqrt(c * c + p * p),
                        M = n - f * $ / r,
                        E = o - f * d / r,
                        w = s - f * $ / r,
                        x = a - f * d / r,
                        O = s - f * m / S,
                        A = a - f * v / S,
                        I = h - f * m / S,
                        b = _ - f * v / S,
                        C = n + f * T / L,
                        D = o + f * P / L,
                        R = h + f * T / L,
                        F = _ + f * P / L,
                        N = ot._$50;
                    return null != i._$P2(N) && (ot.clipWithTransform(t, N, M, E, w, x, O, A, I, b, R, F, C, D), !0)
                }, ot.clipWithTransform = function(t, i, e, r, n, o, s, a) {
                    if (arguments.length < 7) ni.println("err : @LDGL.clip()");
                    else if (arguments[1] instanceof oi) {
                        var h = ot._$B,
                            _ = i,
                            u = arguments;
                        if (t.beginPath(), _) {
                            _._$PS(u[2], u[3], h), t.moveTo(h[0], h[1]);
                            for (var l = 4; l < u.length; l += 2) _._$PS(u[l], u[l + 1], h), t.lineTo(h[0], h[1])
                        } else
                            for (t.moveTo(u[2], u[3]), l = 4; l < u.length; l += 2) t.lineTo(u[l], u[l + 1]);
                        t.clip()
                    } else ni.println("err : a[0] is _$6 LDTransform @LDGL.clip()")
                }, ot.createCanvas = function(t, i) {
                    var e = document.createElement("canvas");
                    return e.setAttribute("width", t), e.setAttribute("height", i), e || ni.println("err : " + e), e
                }, ot.dumpValues = function() {
                    for (var t = "", i = 0; i < arguments.length; i++) t += "[" + i + "]= " + arguments[i].toFixed(3) + " , ";
                    console.log(t)
                }, st.prototype._$F0 = function(t) {
                    this._$TT = t._$_T(), this._$LT = t._$_T(), this._$FS = t._$_T(), this._$wL = t._$nP()
                }, st.prototype.getMinValue = function() {
                    return this._$TT
                }, st.prototype.getMaxValue = function() {
                    return this._$LT
                }, st.prototype.getDefaultValue = function() {
                    return this._$FS
                }, st.prototype.getParamID = function() {
                    return this._$wL
                }, at.prototype._$yo = function() {
                    return this._$AT && !this._$JS
                }, at.prototype._$hS = function(t) {
                    this._$AT = t
                }, at.prototype._$GT = function() {
                    return this._$e0
                }, at.prototype._$l2 = function(t) {
                    this._$IP = t
                }, at.prototype.getPartsIndex = function() {
                    return this._$IP
                }, at.prototype._$x2 = function() {
                    return this._$JS
                }, at.prototype._$Ib = function(t) {
                    this._$JS = t
                }, at.prototype.getTotalScale = function() {
                    return this.totalScale
                }, at.prototype.setTotalScale_notForClient = function(t) {
                    this.totalScale = t
                }, at.prototype.getInterpolatedOpacity = function() {
                    return this._$7s
                }, at.prototype.setInterpolatedOpacity = function(t) {
                    this._$7s = t
                }, at.prototype.getTotalOpacity = function(t) {
                    return this.totalOpacity
                }, at.prototype.setTotalOpacity = function(t) {
                    this.totalOpacity = t
                }, ht._$fr = -1, ht.prototype.toString = function() {
                    return this._$ib
                }, _t.prototype = new W, _t._$42 = 0, _t._$Os = 30, _t._$ms = 0, _t._$ns = 1, _t._$_s = 2, _t._$gT = new Array, _t.prototype._$_S = function(t) {
                    this._$LP = t
                }, _t.prototype.getTextureNo = function() {
                    return this._$LP
                }, _t.prototype._$ZL = function() {
                    return this._$Qi
                }, _t.prototype._$H2 = function() {
                    return this._$JP
                }, _t.prototype.getNumPoints = function() {
                    return this._$d0
                }, _t.prototype.getType = function() {
                    return W._$wb
                }, _t.prototype._$B2 = function(t, i, e) {
                    var r = i,
                        n = null != r._$hr ? r._$hr : r._$Cr;
                    switch (k._$do) {
                        default:
                            case k._$Ms:
                            throw new Error("_$L _$ro ");
                        case k._$Qs:
                                for (var o = this._$d0 - 1; o >= 0; --o) n[o * k._$No + 4] = e
                    }
                }, _t.prototype._$zP = function() {
                    this._$GS = new D, this._$GS._$zP()
                }, _t.prototype._$F0 = function(t) {
                    W.prototype._$F0.call(this, t), this._$LP = t._$6L(), this._$d0 = t._$6L(), this._$Yo = t._$6L();
                    var i = t._$nP();
                    this._$BP = new Int16Array(3 * this._$Yo);
                    for (var e = 3 * this._$Yo - 1; e >= 0; --e) this._$BP[e] = i[e];
                    if (this._$Eo = t._$nP(), this._$Qi = t._$nP(), t.getFormatVersion() >= U._$s7) {
                        if (this._$JP = t._$6L(), 0 != this._$JP) {
                            if (0 != (1 & this._$JP)) {
                                var r = t._$6L();
                                null == this._$5P && (this._$5P = new Object), this._$5P._$Hb = parseInt(r)
                            }
                            0 != (this._$JP & _t._$Os) ? this._$6s = (this._$JP & _t._$Os) >> 1 : this._$6s = _t._$ms, 0 != (32 & this._$JP) && (this.culling = !1)
                        }
                    } else this._$JP = 0
                }, _t.prototype.init = function(t) {
                    var i = new ut(this),
                        e = this._$d0 * k._$No,
                        r = this._$32();
                    switch (null != i._$Cr && (i._$Cr = null), i._$Cr = new Float32Array(e), null != i._$hr && (i._$hr = null), i._$hr = r ? new Float32Array(e) : null, k._$do) {
                        default:
                            case k._$Ms:
                            if (k._$Ls)
                            for (var n = this._$d0 - 1; n >= 0; --n) {
                                var o = n << 1;
                                this._$Qi[o + 1] = 1 - this._$Qi[o + 1]
                            }
                        break;
                        case k._$Qs:
                                for (n = this._$d0 - 1; n >= 0; --n) {
                                o = n << 1;
                                var s = n * k._$No,
                                    a = this._$Qi[o],
                                    h = this._$Qi[o + 1];
                                i._$Cr[s] = a, i._$Cr[s + 1] = h, i._$Cr[s + 4] = 0, r && (i._$hr[s] = a, i._$hr[s + 1] = h, i._$hr[s + 4] = 0)
                            }
                    }
                    return i
                }, _t.prototype._$Nr = function(t, i) {
                    var e = i;
                    if (this != e._$GT() && console.log("### assert!! ### "), this._$GS._$Ur(t) && (W.prototype._$Nr.call(this, t, e), !e._$IS[0])) {
                        var r = _t._$gT;
                        r[0] = !1, w._$Vr(t, this._$GS, r, this._$d0, this._$Eo, e._$Cr, k._$i2, k._$No)
                    }
                }, _t.prototype._$2b = function(t, i) {
                    try {
                        this != i._$GT() && console.log("### assert!! ### ");
                        var e = !1;
                        i._$IS[0] && (e = !0);
                        var r = i;
                        if (!e && (W.prototype._$2b.call(this, t), this._$32())) {
                            var n = this.getTargetBaseDataID();
                            if (r._$8r == W._$ur && (r._$8r = t.getBaseDataIndex(n)), r._$8r < 0) ti._$so && ni.println("_$L _$0P _$G :: %s", n);
                            else {
                                var o = t.getBaseData(r._$8r),
                                    s = t._$q2(r._$8r);
                                null == o || s._$x2() ? r._$AT = !1 : (o._$nb(t, s, r._$Cr, r._$hr, this._$d0, k._$i2, k._$No), r._$AT = !0), r.baseOpacity = s.getTotalOpacity()
                            }
                        }
                    } catch (t) {
                        throw t
                    }
                }, _t.prototype.draw = function(t, i, e) {
                    if (this != e._$GT() && console.log("### assert!! ### "), !e._$IS[0]) {
                        var r = e,
                            n = this._$LP;
                        n < 0 && (n = 1);
                        var o = this.getOpacity(i, r) * e._$VS * e.baseOpacity,
                            s = null != r._$hr ? r._$hr : r._$Cr;
                        t.setClipBufPre_clipContextForDraw(e.clipBufPre_clipContext), t._$WP(this.culling), t._$Uo(n, 3 * this._$Yo, this._$BP, s, this._$Qi, o, this._$6s, r)
                    }
                }, _t.prototype.dump = function() {
                    console.log("  _$yi( %d ) , _$d0( %d ) , _$Yo( %d ) \n", this._$LP, this._$d0, this._$Yo), console.log("  _$Oi _$di = { ");
                    for (var t = 0; t < this._$BP.length; t++) console.log("%5d ,", this._$BP[t]);
                    for (console.log("\n  _$5i _$30"), t = 0; t < this._$Eo.length; t++) {
                        console.log("\n    _$30[%d] = ", t);
                        for (var i = this._$Eo[t], e = 0; e < i.length; e++) console.log("%6.2f, ", i[e])
                    }
                    console.log("\n")
                }, _t.prototype._$72 = function(t) {
                    return null == this._$5P ? null : this._$5P[t]
                }, _t.prototype.getIndexArray = function() {
                    return this._$BP
                }, ut.prototype = new St, ut.prototype.getTransformedPoints = function() {
                    return null != this._$hr ? this._$hr : this._$Cr
                }, lt.prototype._$HT = function(t) {
                    this.x = t.x, this.y = t.y
                }, lt.prototype._$HT = function(t, i) {
                    this.x = t, this.y = i
                }, ct.prototype = new h, ct.loadModel = function(t) {
                    var i = new ct;
                    return h._$62(i, t), i
                }, ct.loadModel = function(t, i) {
                    var e = new ct(i || 0);
                    return h._$62(e, t), e
                }, ct._$to = function() {
                    return new ct
                }, ct._$er = function(t) {
                    var i = new _$5("../_$_r/_$t0/_$Ri/_$_P._$d");
                    if (0 == i.exists()) throw new _$ls("_$t0 _$_ _$6 _$Ui :: " + i._$PL());
                    for (var e = ["../_$_r/_$t0/_$Ri/_$_P.512/_$CP._$1", "../_$_r/_$t0/_$Ri/_$_P.512/_$vP._$1", "../_$_r/_$t0/_$Ri/_$_P.512/_$EP._$1", "../_$_r/_$t0/_$Ri/_$_P.512/_$pP._$1"], r = ct.loadModel(i._$3b()), n = 0; n < e.length; n++) {
                        var o = new _$5(e[n]);
                        if (0 == o.exists()) throw new _$ls("_$t0 _$_ _$6 _$Ui :: " + o._$PL());
                        r.setTexture(n, _$nL._$_o(t, o._$3b()))
                    }
                    return r
                }, ct.prototype.setGL = function(t) {
                    ti.setGL(t)
                }, ct.prototype.setTransform = function(t) {
                    this.drawParamWebGL.setTransform(aHR)
                }, ct.prototype.update = function() {
                    this._$5S.update(), this._$5S.preDraw(this.drawParamWebGL)
                }, ct.prototype.draw = function() {
                    this._$5S.draw(this.drawParamWebGL)
                }, ct.prototype._$K2 = function() {
                    this.drawParamWebGL._$K2()
                }, ct.prototype.setTexture = function(t, i) {
                    null == this.drawParamWebGL && ni.println("_$Yi for QT _$ki / _$XS() is _$6 _$ui!!"), this.drawParamWebGL.setTexture(t, i)
                }, ct.prototype.setTexture = function(t, i) {
                    null == this.drawParamWebGL && ni.println("_$Yi for QT _$ki / _$XS() is _$6 _$ui!!"), this.drawParamWebGL.setTexture(t, i)
                }, ct.prototype._$Rs = function() {
                    return this.drawParamWebGL._$Rs()
                }, ct.prototype._$Ds = function(t) {
                    this.drawParamWebGL._$Ds(t)
                }, ct.prototype.getDrawParam = function() {
                    return this.drawParamWebGL
                }, ct.prototype.setMatrix = function(t) {
                    this.drawParamWebGL.setMatrix(t)
                }, ct.prototype.setPremultipliedAlpha = function(t) {
                    this.drawParamWebGL.setPremultipliedAlpha(t)
                }, ct.prototype.isPremultipliedAlpha = function() {
                    return this.drawParamWebGL.isPremultipliedAlpha()
                }, ct.prototype.setAnisotropy = function(t) {
                    this.drawParamWebGL.setAnisotropy(t)
                }, ct.prototype.getAnisotropy = function() {
                    return this.drawParamWebGL.getAnisotropy()
                }, pt.prototype._$tb = function() {
                    return this.motions
                }, pt.prototype.startMotion = function(t, i) {
                    for (var e = null, r = this.motions.length, n = 0; n < r; ++n) null != (e = this.motions[n]) && (e._$qS(e._$w0.getFadeOut()), this._$eb && ni.print("MotionQueueManager[size:%2d]->startMotion() / start _$K _$3 (m%d)\n", r, e._$sr));
                    if (null == t) return -1;
                    (e = new ft)._$w0 = t, this.motions.push(e);
                    var o = e._$sr;
                    return this._$eb && ni.print("MotionQueueManager[size:%2d]->startMotion() / new _$w0 (m%d)\n", r, o), o
                }, pt.prototype.updateParam = function(t) {
                    try {
                        for (var i = !1, e = 0; e < this.motions.length; e++) {
                            var r = this.motions[e];
                            if (null != r) {
                                var n = r._$w0;
                                null != n ? (n.updateParam(t, r), i = !0, r.isFinished() && (this._$eb && ni.print("MotionQueueManager[size:%2d]->updateParam() / _$T0 _$w0 (m%d)\n", this.motions.length - 1, r._$sr), this.motions.splice(e, 1), e--)) : (this.motions = this.motions.splice(e, 1), e--)
                            } else this.motions.splice(e, 1), e--
                        }
                        return i
                    } catch (t) {
                        return ni.println(t), !0
                    }
                }, pt.prototype.isFinished = function(t) {
                    if (arguments.length >= 1) {
                        for (var i = 0; i < this.motions.length; i++)
                            if (null != (e = this.motions[i]) && e._$sr == t && !e.isFinished()) return !1;
                        return !0
                    }
                    for (i = 0; i < this.motions.length; i++) {
                        var e;
                        if (null != (e = this.motions[i]))
                            if (null != e._$w0) {
                                if (!e.isFinished()) return !1
                            } else this.motions.splice(i, 1), i--;
                        else this.motions.splice(i, 1), i--
                    }
                    return !0
                }, pt.prototype.stopAllMotions = function() {
                    for (var t = 0; t < this.motions.length; t++) {
                        var i = this.motions[t];
                        null != i ? (i._$w0, this.motions.splice(t, 1), t--) : (this.motions.splice(t, 1), t--)
                    }
                }, pt.prototype._$Zr = function(t) {
                    this._$eb = t
                }, pt.prototype._$e = function() {
                    console.log("-- _$R --\n");
                    for (var t = 0; t < this.motions.length; t++) {
                        var i = this.motions[t]._$w0;
                        console.log("MotionQueueEnt[%d] :: %s\n", this.motions.length, i.toString())
                    }
                }, ft._$Gs = 0, ft.prototype.isFinished = function() {
                    return this._$9L
                }, ft.prototype._$qS = function(t) {
                    var i = ei.getUserTimeMSec() + t;
                    (this._$Do < 0 || i < this._$Do) && (this._$Do = i)
                }, ft.prototype._$Bs = function() {
                    return this._$sr
                }, $t.prototype = new et, $t._$eT = null, $t._$tP = new Object, $t._$2o = function() {
                    return null == $t._$eT && ($t._$eT = $t.getID("DST_BASE")), $t._$eT
                }, $t._$27 = function() {
                    $t._$tP.clear(), $t._$eT = null
                }, $t.getID = function(t) {
                    var i = $t._$tP[t];
                    return null == i && (i = new $t(t), $t._$tP[t] = i), i
                }, $t.prototype._$3s = function() {
                    return new $t
                }, dt.prototype = new A, dt._$9r = function(t) {
                    return new Float32Array(t)
                }, dt._$vb = function(t) {
                    return new Int16Array(t)
                }, dt._$cr = function(t, i) {
                    return null == t || t._$yL() < i.length ? ((t = dt._$9r(2 * i.length)).put(i), t._$oT(0)) : (t.clear(), t.put(i), t._$oT(0)), t
                }, dt._$mb = function(t, i) {
                    return null == t || t._$yL() < i.length ? ((t = dt._$vb(2 * i.length)).put(i), t._$oT(0)) : (t.clear(), t.put(i), t._$oT(0)), t
                }, dt._$Hs = function() {
                    return this._$Gr
                }, dt._$as = function(t) {
                    this._$Gr = t
                }, dt.prototype.getGL = function() {
                    return this.gl
                }, dt.prototype.setGL = function(t) {
                    this.gl = t
                }, dt.prototype.setTransform = function(t) {
                    this.transform = t
                }, dt.prototype._$ZT = function() {
                    var t = this.gl;
                    this.firstDraw && (this.initShader(), this.firstDraw = !1, this.anisotropyExt = t.getExtension("EXT_texture_filter_anisotropic") || t.getExtension("WEBKIT_EXT_texture_filter_anisotropic") || t.getExtension("MOZ_EXT_texture_filter_anisotropic"), this.anisotropyExt && (this.maxAnisotropy = t.getParameter(this.anisotropyExt.MAX_TEXTURE_MAX_ANISOTROPY_EXT))), t.disable(t.SCISSOR_TEST), t.disable(t.STENCIL_TEST), t.disable(t.DEPTH_TEST), t.frontFace(t.CW), t.enable(t.BLEND), t.colorMask(1, 1, 1, 1), t.bindBuffer(t.ARRAY_BUFFER, null), t.bindBuffer(t.ELEMENT_ARRAY_BUFFER, null)
                }, dt.prototype._$Uo = function(t, i, e, r, n, o, s, a) {
                    if (!(o < .01 && null == this.clipBufPre_clipContextMask)) {
                        var h = (o > .9 && ti.EXPAND_W, this.gl);
                        if (null == this.gl) throw new Error("gl is null");
                        var _, u, l, c, p = 1 * this._$C0 * o,
                            f = 1 * this._$tT * o,
                            $ = 1 * this._$WL * o,
                            d = this._$lT * o;
                        if (null != this.clipBufPre_clipContextMask) {
                            h.frontFace(h.CCW), h.useProgram(this.shaderProgram), this._$vS = gt(h, this._$vS, r), this._$no = yt(h, this._$no, e), h.enableVertexAttribArray(this.a_position_Loc), h.vertexAttribPointer(this.a_position_Loc, 2, h.FLOAT, !1, 0, 0), this._$NT = gt(h, this._$NT, n), h.activeTexture(h.TEXTURE1), h.bindTexture(h.TEXTURE_2D, this.textures[t]), h.uniform1i(this.s_texture0_Loc, 1), h.enableVertexAttribArray(this.a_texCoord_Loc), h.vertexAttribPointer(this.a_texCoord_Loc, 2, h.FLOAT, !1, 0, 0), h.uniformMatrix4fv(this.u_matrix_Loc, !1, this.getClipBufPre_clipContextMask().matrixForMask);
                            var g = this.getClipBufPre_clipContextMask().layoutChannelNo,
                                y = this.getChannelFlagAsColor(g);
                            h.uniform4f(this.u_channelFlag, y.r, y.g, y.b, y.a);
                            var m = this.getClipBufPre_clipContextMask().layoutBounds;
                            h.uniform4f(this.u_baseColor_Loc, 2 * m.x - 1, 2 * m.y - 1, 2 * m._$EL() - 1, 2 * m._$5T() - 1), h.uniform1i(this.u_maskFlag_Loc, !0)
                        } else null != this.getClipBufPre_clipContextDraw() ? (h.useProgram(this.shaderProgramOff), this._$vS = gt(h, this._$vS, r), this._$no = yt(h, this._$no, e), h.enableVertexAttribArray(this.a_position_Loc_Off), h.vertexAttribPointer(this.a_position_Loc_Off, 2, h.FLOAT, !1, 0, 0), this._$NT = gt(h, this._$NT, n), h.activeTexture(h.TEXTURE1), h.bindTexture(h.TEXTURE_2D, this.textures[t]), h.uniform1i(this.s_texture0_Loc_Off, 1), h.enableVertexAttribArray(this.a_texCoord_Loc_Off), h.vertexAttribPointer(this.a_texCoord_Loc_Off, 2, h.FLOAT, !1, 0, 0), h.uniformMatrix4fv(this.u_clipMatrix_Loc_Off, !1, this.getClipBufPre_clipContextDraw().matrixForDraw), h.uniformMatrix4fv(this.u_matrix_Loc_Off, !1, this.matrix4x4), h.activeTexture(h.TEXTURE2), h.bindTexture(h.TEXTURE_2D, ti.fTexture[this.glno]), h.uniform1i(this.s_texture1_Loc_Off, 2), g = this.getClipBufPre_clipContextDraw().layoutChannelNo, y = this.getChannelFlagAsColor(g), h.uniform4f(this.u_channelFlag_Loc_Off, y.r, y.g, y.b, y.a), h.uniform4f(this.u_baseColor_Loc_Off, p, f, $, d)) : (h.useProgram(this.shaderProgram), this._$vS = gt(h, this._$vS, r), this._$no = yt(h, this._$no, e), h.enableVertexAttribArray(this.a_position_Loc), h.vertexAttribPointer(this.a_position_Loc, 2, h.FLOAT, !1, 0, 0), this._$NT = gt(h, this._$NT, n), h.activeTexture(h.TEXTURE1), h.bindTexture(h.TEXTURE_2D, this.textures[t]), h.uniform1i(this.s_texture0_Loc, 1), h.enableVertexAttribArray(this.a_texCoord_Loc), h.vertexAttribPointer(this.a_texCoord_Loc, 2, h.FLOAT, !1, 0, 0), h.uniformMatrix4fv(this.u_matrix_Loc, !1, this.matrix4x4), h.uniform4f(this.u_baseColor_Loc, p, f, $, d), h.uniform1i(this.u_maskFlag_Loc, !1));
                        if (this.culling ? this.gl.enable(h.CULL_FACE) : this.gl.disable(h.CULL_FACE), this.gl.enable(h.BLEND), null != this.clipBufPre_clipContextMask) _ = h.ONE, u = h.ONE_MINUS_SRC_ALPHA, l = h.ONE, c = h.ONE_MINUS_SRC_ALPHA;
                        else switch (s) {
                            case _t._$ms:
                                _ = h.ONE, u = h.ONE_MINUS_SRC_ALPHA, l = h.ONE, c = h.ONE_MINUS_SRC_ALPHA;
                                break;
                            case _t._$ns:
                                _ = h.ONE, u = h.ONE, l = h.ZERO, c = h.ONE;
                                break;
                            case _t._$_s:
                                _ = h.DST_COLOR, u = h.ONE_MINUS_SRC_ALPHA, l = h.ZERO, c = h.ONE
                        }
                        h.blendEquationSeparate(h.FUNC_ADD, h.FUNC_ADD), h.blendFuncSeparate(_, u, l, c), this.anisotropyExt && h.texParameteri(h.TEXTURE_2D, this.anisotropyExt.TEXTURE_MAX_ANISOTROPY_EXT, this.maxAnisotropy);
                        var v = e.length;
                        h.drawElements(h.TRIANGLES, v, h.UNSIGNED_SHORT, 0), h.bindTexture(h.TEXTURE_2D, null)
                    }
                }, dt.prototype._$Rs = function() {
                    throw new Error("_$Rs")
                }, dt.prototype._$Ds = function(t) {
                    throw new Error("_$Ds")
                }, dt.prototype._$K2 = function() {
                    for (var t = 0; t < this.textures.length; t++) 0 != this.textures[t] && (this.gl._$K2(1, this.textures, t), this.textures[t] = null)
                }, dt.prototype.setTexture = function(t, i) {
                    this.textures[t] = i
                }, dt.prototype.initShader = function() {
                    var t = this.gl;
                    this.loadShaders2(), this.a_position_Loc = t.getAttribLocation(this.shaderProgram, "a_position"), this.a_texCoord_Loc = t.getAttribLocation(this.shaderProgram, "a_texCoord"), this.u_matrix_Loc = t.getUniformLocation(this.shaderProgram, "u_mvpMatrix"), this.s_texture0_Loc = t.getUniformLocation(this.shaderProgram, "s_texture0"), this.u_channelFlag = t.getUniformLocation(this.shaderProgram, "u_channelFlag"), this.u_baseColor_Loc = t.getUniformLocation(this.shaderProgram, "u_baseColor"), this.u_maskFlag_Loc = t.getUniformLocation(this.shaderProgram, "u_maskFlag"), this.a_position_Loc_Off = t.getAttribLocation(this.shaderProgramOff, "a_position"), this.a_texCoord_Loc_Off = t.getAttribLocation(this.shaderProgramOff, "a_texCoord"), this.u_matrix_Loc_Off = t.getUniformLocation(this.shaderProgramOff, "u_mvpMatrix"), this.u_clipMatrix_Loc_Off = t.getUniformLocation(this.shaderProgramOff, "u_ClipMatrix"), this.s_texture0_Loc_Off = t.getUniformLocation(this.shaderProgramOff, "s_texture0"), this.s_texture1_Loc_Off = t.getUniformLocation(this.shaderProgramOff, "s_texture1"), this.u_channelFlag_Loc_Off = t.getUniformLocation(this.shaderProgramOff, "u_channelFlag"), this.u_baseColor_Loc_Off = t.getUniformLocation(this.shaderProgramOff, "u_baseColor")
                }, dt.prototype.disposeShader = function() {
                    var t = this.gl;
                    this.shaderProgram && (t.deleteProgram(this.shaderProgram), this.shaderProgram = null), this.shaderProgramOff && (t.deleteProgram(this.shaderProgramOff), this.shaderProgramOff = null)
                }, dt.prototype.compileShader = function(t, i) {
                    var e = this.gl,
                        r = i,
                        n = e.createShader(t);
                    if (null == n) return ni.print("_$L0 to create shader"), null;
                    if (e.shaderSource(n, r), e.compileShader(n), !e.getShaderParameter(n, e.COMPILE_STATUS)) {
                        var o = e.getShaderInfoLog(n);
                        return ni.print("_$L0 to compile shader : " + o), e.deleteShader(n), null
                    }
                    return n
                }, dt.prototype.loadShaders2 = function() {
                    var t = this.gl;
                    if (this.shaderProgram = t.createProgram(), !this.shaderProgram) return !1;
                    if (this.shaderProgramOff = t.createProgram(), !this.shaderProgramOff) return !1;
                    if (this.vertShader = this.compileShader(t.VERTEX_SHADER, "attribute vec4     a_position;attribute vec2     a_texCoord;varying vec2       v_texCoord;varying vec4       v_ClipPos;uniform mat4       u_mvpMatrix;void main(){    gl_Position = u_mvpMatrix * a_position;    v_ClipPos = u_mvpMatrix * a_position;    v_texCoord = a_texCoord;}"), !this.vertShader) return ni.print("Vertex shader compile _$li!"), !1;
                    if (this.vertShaderOff = this.compileShader(t.VERTEX_SHADER, "attribute vec4     a_position;attribute vec2     a_texCoord;varying vec2       v_texCoord;varying vec4       v_ClipPos;uniform mat4       u_mvpMatrix;uniform mat4       u_ClipMatrix;void main(){    gl_Position = u_mvpMatrix * a_position;    v_ClipPos = u_ClipMatrix * a_position;    v_texCoord = a_texCoord ;}"), !this.vertShaderOff) return ni.print("OffVertex shader compile _$li!"), !1;
                    if (this.fragShader = this.compileShader(t.FRAGMENT_SHADER, "precision mediump float;varying vec2       v_texCoord;varying vec4       v_ClipPos;uniform sampler2D  s_texture0;uniform vec4       u_channelFlag;uniform vec4       u_baseColor;uniform bool       u_maskFlag;void main(){    vec4 smpColor;     if(u_maskFlag){        float isInside =             step(u_baseColor.x, v_ClipPos.x/v_ClipPos.w)          * step(u_baseColor.y, v_ClipPos.y/v_ClipPos.w)          * step(v_ClipPos.x/v_ClipPos.w, u_baseColor.z)          * step(v_ClipPos.y/v_ClipPos.w, u_baseColor.w);        smpColor = u_channelFlag * texture2D(s_texture0 , v_texCoord).a * isInside;    }else{        smpColor = texture2D(s_texture0 , v_texCoord) * u_baseColor;    }    gl_FragColor = smpColor;}"), !this.fragShader) return ni.print("Fragment shader compile _$li!"), !1;
                    if (this.fragShaderOff = this.compileShader(t.FRAGMENT_SHADER, "precision mediump float ;varying vec2       v_texCoord;varying vec4       v_ClipPos;uniform sampler2D  s_texture0;uniform sampler2D  s_texture1;uniform vec4       u_channelFlag;uniform vec4       u_baseColor ;void main(){    vec4 col_formask = texture2D(s_texture0, v_texCoord) * u_baseColor;    vec4 clipMask = texture2D(s_texture1, v_ClipPos.xy / v_ClipPos.w) * u_channelFlag;    float maskVal = clipMask.r + clipMask.g + clipMask.b + clipMask.a;    col_formask = col_formask * maskVal;    gl_FragColor = col_formask;}"), !this.fragShaderOff) return ni.print("OffFragment shader compile _$li!"), !1;
                    if (t.attachShader(this.shaderProgram, this.vertShader), t.attachShader(this.shaderProgram, this.fragShader), t.attachShader(this.shaderProgramOff, this.vertShaderOff), t.attachShader(this.shaderProgramOff, this.fragShaderOff), t.linkProgram(this.shaderProgram), t.linkProgram(this.shaderProgramOff), !t.getProgramParameter(this.shaderProgram, t.LINK_STATUS)) {
                        var i = t.getProgramInfoLog(this.shaderProgram);
                        return ni.print("_$L0 to link program: " + i), this.vertShader && (t.deleteShader(this.vertShader), this.vertShader = 0), this.fragShader && (t.deleteShader(this.fragShader), this.fragShader = 0), this.shaderProgram && (t.deleteProgram(this.shaderProgram), this.shaderProgram = 0), this.vertShaderOff && (t.deleteShader(this.vertShaderOff), this.vertShaderOff = 0), this.fragShaderOff && (t.deleteShader(this.fragShaderOff), this.fragShaderOff = 0), this.shaderProgramOff && (t.deleteProgram(this.shaderProgramOff), this.shaderProgramOff = 0), !1
                    }
                    return !0
                }, dt.prototype.createFramebuffer = function() {
                    var t = this.gl,
                        i = ti.clippingMaskBufferSize,
                        e = t.createFramebuffer();
                    t.bindFramebuffer(t.FRAMEBUFFER, e);
                    var r = t.createRenderbuffer();
                    t.bindRenderbuffer(t.RENDERBUFFER, r), t.renderbufferStorage(t.RENDERBUFFER, t.RGBA4, i, i), t.framebufferRenderbuffer(t.FRAMEBUFFER, t.COLOR_ATTACHMENT0, t.RENDERBUFFER, r);
                    var n = t.createTexture();
                    return t.bindTexture(t.TEXTURE_2D, n), t.texImage2D(t.TEXTURE_2D, 0, t.RGBA, i, i, 0, t.RGBA, t.UNSIGNED_BYTE, null), t.texParameteri(t.TEXTURE_2D, t.TEXTURE_MIN_FILTER, t.LINEAR), t.texParameteri(t.TEXTURE_2D, t.TEXTURE_MAG_FILTER, t.LINEAR), t.texParameteri(t.TEXTURE_2D, t.TEXTURE_WRAP_S, t.CLAMP_TO_EDGE), t.texParameteri(t.TEXTURE_2D, t.TEXTURE_WRAP_T, t.CLAMP_TO_EDGE), t.framebufferTexture2D(t.FRAMEBUFFER, t.COLOR_ATTACHMENT0, t.TEXTURE_2D, n, 0), t.bindTexture(t.TEXTURE_2D, null), t.bindRenderbuffer(t.RENDERBUFFER, null), t.bindFramebuffer(t.FRAMEBUFFER, null), ti.fTexture[this.glno] = n, {
                        framebuffer: e,
                        renderbuffer: r,
                        texture: ti.fTexture[this.glno]
                    }
                }, mt.prototype._$fP = function() {
                    var t, i, e, r = this._$ST();
                    if (0 == (128 & r)) return 255 & r;
                    if (0 == (128 & (t = this._$ST()))) return (127 & r) << 7 | 127 & t;
                    if (0 == (128 & (i = this._$ST()))) return (127 & r) << 14 | (127 & t) << 7 | 255 & i;
                    if (0 == (128 & (e = this._$ST()))) return (127 & r) << 21 | (127 & t) << 14 | (127 & i) << 7 | 255 & e;
                    throw new ht("_$L _$0P  _")
                }, mt.prototype.getFormatVersion = function() {
                    return this._$S2
                }, mt.prototype._$gr = function(t) {
                    this._$S2 = t
                }, mt.prototype._$3L = function() {
                    return this._$fP()
                }, mt.prototype._$mP = function() {
                    return this._$zT(), this._$F += 8, this._$T.getFloat64(this._$F - 8)
                }, mt.prototype._$_T = function() {
                    return this._$zT(), this._$F += 4, this._$T.getFloat32(this._$F - 4)
                }, mt.prototype._$6L = function() {
                    return this._$zT(), this._$F += 4, this._$T.getInt32(this._$F - 4)
                }, mt.prototype._$ST = function() {
                    return this._$zT(), this._$T.getInt8(this._$F++)
                }, mt.prototype._$9T = function() {
                    return this._$zT(), this._$F += 2, this._$T.getInt16(this._$F - 2)
                }, mt.prototype._$2T = function() {
                    throw this._$zT(), this._$F += 8, new ht("_$L _$q read long")
                }, mt.prototype._$po = function() {
                    return this._$zT(), 0 != this._$T.getInt8(this._$F++)
                };
                var fi = !0;
                mt.prototype._$bT = function() {
                    this._$zT();
                    var t = this._$3L(),
                        i = null;
                    if (fi) try {
                        var e = new ArrayBuffer(2 * t);
                        i = new Uint16Array(e);
                        for (var r = 0; r < t; ++r) i[r] = this._$T.getUint8(this._$F++);
                        return String.fromCharCode.apply(null, i)
                    } catch (t) {
                        fi = !1
                    }
                    try {
                        var n = new Array;
                        if (null == i)
                            for (r = 0; r < t; ++r) n[r] = this._$T.getUint8(this._$F++);
                        else
                            for (r = 0; r < t; ++r) n[r] = i[r];
                        return String.fromCharCode.apply(null, n)
                    } catch (t) {
                        console.log("read utf8 / _$rT _$L0 !! : " + t)
                    }
                }, mt.prototype._$cS = function() {
                    this._$zT();
                    for (var t = this._$3L(), i = new Int32Array(t), e = 0; e < t; e++) i[e] = this._$T.getInt32(this._$F), this._$F += 4;
                    return i
                }, mt.prototype._$Tb = function() {
                    this._$zT();
                    for (var t = this._$3L(), i = new Float32Array(t), e = 0; e < t; e++) i[e] = this._$T.getFloat32(this._$F), this._$F += 4;
                    return i
                }, mt.prototype._$5b = function() {
                    this._$zT();
                    for (var t = this._$3L(), i = new Float64Array(t), e = 0; e < t; e++) i[e] = this._$T.getFloat64(this._$F), this._$F += 8;
                    return i
                }, mt.prototype._$nP = function() {
                    return this._$Jb(-1)
                }, mt.prototype._$Jb = function(t) {
                    if (this._$zT(), t < 0 && (t = this._$3L()), t == U._$7P) {
                        var i = this._$6L();
                        if (0 <= i && i < this._$Ko.length) return this._$Ko[i];
                        throw new ht("_$sL _$4i @_$m0")
                    }
                    var e = this._$4b(t);
                    return this._$Ko.push(e), e
                }, mt.prototype._$4b = function(t) {
                    if (0 == t) return null;
                    if (50 == t) {
                        var i = this._$bT();
                        return n = F.getID(i)
                    }
                    if (51 == t) return i = this._$bT(), n = $t.getID(i);
                    if (134 == t) return i = this._$bT(), n = f.getID(i);
                    if (60 == t) return i = this._$bT(), n = d.getID(i);
                    if (t >= 48) {
                        var e = U._$9o(t);
                        return null != e ? (e._$F0(this), e) : null
                    }
                    switch (t) {
                        case 1:
                            return this._$bT();
                        case 10:
                            return new c(this._$6L(), !0);
                        case 11:
                            return new E(this._$mP(), this._$mP(), this._$mP(), this._$mP());
                        case 12:
                            return new E(this._$_T(), this._$_T(), this._$_T(), this._$_T());
                        case 13:
                            return new x(this._$mP(), this._$mP());
                        case 14:
                            return new x(this._$_T(), this._$_T());
                        case 15:
                            for (var r = this._$3L(), n = new Array(r), o = 0; o < r; o++) n[o] = this._$nP();
                            return n;
                        case 17:
                            return n = new N(this._$mP(), this._$mP(), this._$mP(), this._$mP(), this._$mP(), this._$mP());
                        case 21:
                            return new p(this._$6L(), this._$6L(), this._$6L(), this._$6L());
                        case 22:
                            return new lt(this._$6L(), this._$6L());
                        case 23:
                            throw new Error("_$L _$ro ");
                        case 16:
                        case 25:
                            return this._$cS();
                        case 26:
                            return this._$5b();
                        case 27:
                            return this._$Tb();
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 18:
                        case 19:
                        case 20:
                        case 24:
                        case 28:
                            throw new ht("_$6 _$q : _$nP() of 2-9 ,18,19,20,24,28 : " + t);
                        default:
                            throw new ht("_$6 _$q : _$nP() NO _$i : " + t)
                    }
                }, mt.prototype._$8L = function() {
                    return 0 == this._$hL ? this._$v0 = this._$ST() : 8 == this._$hL && (this._$v0 = this._$ST(), this._$hL = 0), 1 == (this._$v0 >> 7 - this._$hL++ & 1)
                }, mt.prototype._$zT = function() {
                    0 != this._$hL && (this._$hL = 0)
                }, vt.prototype._$wP = function(t, i, e) {
                    for (var r = 0; r < e; r++) {
                        for (var n = 0; n < i; n++) {
                            var o = 2 * (n + r * i);
                            console.log("(% 7.3f , % 7.3f) , ", t[o], t[o + 1])
                        }
                        console.log("\n")
                    }
                    console.log("\n")
                }, St.prototype._$u2 = function() {
                    return this._$IS[0]
                }, St.prototype._$yo = function() {
                    return this._$AT && !this._$IS[0]
                }, St.prototype._$GT = function() {
                    return this._$e0
                }, ti.init(), Tt.platformManager = null, Tt.getPlatformManager = function() {
                    return Tt.platformManager
                }, Tt.setPlatformManager = function(t) {
                    Tt.platformManager = t
                }, Pt.prototype = new pt, Pt.prototype.getCurrentPriority = function() {
                    return this.currentPriority
                }, Pt.prototype.getReservePriority = function() {
                    return this.reservePriority
                }, Pt.prototype.reserveMotion = function(t) {
                    return !(this.reservePriority >= t || this.currentPriority >= t || (this.reservePriority = t, 0))
                }, Pt.prototype.setReservePriority = function(t) {
                    this.reservePriority = t
                }, Pt.prototype.updateParam = function(t) {
                    var i = pt.prototype.updateParam.call(this, t);
                    return this.isFinished() && (this.currentPriority = 0), i
                }, Pt.prototype.startMotionPrio = function(t, i) {
                    return i == this.reservePriority && (this.reservePriority = 0), this.currentPriority = i, this.startMotion(t, !1)
                }, Lt.mul = function(t, i, e) {
                    var r = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        n = void 0,
                        o = void 0,
                        s = void 0;
                    for (n = 0; n < 4; n++)
                        for (o = 0; o < 4; o++)
                            for (s = 0; s < 4; s++) r[n + 4 * o] += t[n + 4 * s] * i[s + 4 * o];
                    for (n = 0; n < 16; n++) e[n] = r[n]
                }, Lt.prototype.identity = function() {
                    for (var t = 0; t < 16; t++) this.tr[t] = t % 5 == 0 ? 1 : 0
                }, Lt.prototype.getArray = function() {
                    return this.tr
                }, Lt.prototype.getCopyMatrix = function() {
                    return new Float32Array(this.tr)
                }, Lt.prototype.setMatrix = function(t) {
                    if (null != this.tr && this.tr.length == this.tr.length)
                        for (var i = 0; i < 16; i++) this.tr[i] = t[i]
                }, Lt.prototype.getScaleX = function() {
                    return this.tr[0]
                }, Lt.prototype.getScaleY = function() {
                    return this.tr[5]
                }, Lt.prototype.transformX = function(t) {
                    return this.tr[0] * t + this.tr[12]
                }, Lt.prototype.transformY = function(t) {
                    return this.tr[5] * t + this.tr[13]
                }, Lt.prototype.invertTransformX = function(t) {
                    return (t - this.tr[12]) / this.tr[0]
                }, Lt.prototype.invertTransformY = function(t) {
                    return (t - this.tr[13]) / this.tr[5]
                }, Lt.prototype.multTranslate = function(t, i) {
                    var e = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, t, i, 0, 1];
                    Lt.mul(e, this.tr, this.tr)
                }, Lt.prototype.translate = function(t, i) {
                    this.tr[12] = t, this.tr[13] = i
                }, Lt.prototype.translateX = function(t) {
                    this.tr[12] = t
                }, Lt.prototype.translateY = function(t) {
                    this.tr[13] = t
                }, Lt.prototype.multScale = function(t, i) {
                    var e = [t, 0, 0, 0, 0, i, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
                    Lt.mul(e, this.tr, this.tr)
                }, Lt.prototype.scale = function(t, i) {
                    this.tr[0] = t, this.tr[5] = i
                }, Mt.prototype = new Lt, Mt.prototype.setPosition = function(t, i) {
                    this.translate(t, i)
                }, Mt.prototype.setCenterPosition = function(t, i) {
                    var e = this.width * this.getScaleX(),
                        r = this.height * this.getScaleY();
                    this.translate(t - e / 2, i - r / 2)
                }, Mt.prototype.top = function(t) {
                    this.setY(t)
                }, Mt.prototype.bottom = function(t) {
                    var i = this.height * this.getScaleY();
                    this.translateY(t - i)
                }, Mt.prototype.left = function(t) {
                    this.setX(t)
                }, Mt.prototype.right = function(t) {
                    var i = this.width * this.getScaleX();
                    this.translateX(t - i)
                }, Mt.prototype.centerX = function(t) {
                    var i = this.width * this.getScaleX();
                    this.translateX(t - i / 2)
                }, Mt.prototype.centerY = function(t) {
                    var i = this.height * this.getScaleY();
                    this.translateY(t - i / 2)
                }, Mt.prototype.setX = function(t) {
                    this.translateX(t)
                }, Mt.prototype.setY = function(t) {
                    this.translateY(t)
                }, Mt.prototype.setHeight = function(t) {
                    var i = t / this.height,
                        e = -i;
                    this.scale(i, e)
                }, Mt.prototype.setWidth = function(t) {
                    var i = t / this.width,
                        e = -i;
                    this.scale(i, e)
                }, Et.prototype = new n, Et.EXPRESSION_DEFAULT = "DEFAULT", Et.TYPE_SET = 0, Et.TYPE_ADD = 1, Et.TYPE_MULT = 2, Et.loadJson = function(t) {
                    var i = new Et,
                        e = Tt.getPlatformManager().jsonParseFromBytes(t);
                    if (i.setFadeIn(parseInt(e.fade_in) > 0 ? parseInt(e.fade_in) : 1e3), i.setFadeOut(parseInt(e.fade_out) > 0 ? parseInt(e.fade_out) : 1e3), null == e.params) return i;
                    var r = e.params,
                        n = r.length;
                    i.paramList = [];
                    for (var o = 0; o < n; o++) {
                        var s = r[o],
                            a = s.id.toString(),
                            h = parseFloat(s.val),
                            _ = Et.TYPE_ADD,
                            u = null != s.calc ? s.calc.toString() : "add";
                        if ((_ = "add" === u ? Et.TYPE_ADD : "mult" === u ? Et.TYPE_MULT : "set" === u ? Et.TYPE_SET : Et.TYPE_ADD) == Et.TYPE_ADD) h -= l = null == s.def ? 0 : parseFloat(s.def);
                        else if (_ == Et.TYPE_MULT) {
                            var l;
                            0 == (l = null == s.def ? 1 : parseFloat(s.def)) && (l = 1), h /= l
                        }
                        var c = new wt;
                        c.id = a, c.type = _, c.value = h, i.paramList.push(c)
                    }
                    return i
                }, Et.prototype.updateParamExe = function(t, i, e, r) {
                    for (var n = this.paramList.length - 1; n >= 0; --n) {
                        var o = this.paramList[n];
                        o.type == Et.TYPE_ADD ? t.addToParamFloat(o.id, o.value, e) : o.type == Et.TYPE_MULT ? t.multParamFloat(o.id, o.value, e) : o.type == Et.TYPE_SET && t.setParamFloat(o.id, o.value, e)
                    }
                }, xt.load = function(t) {
                    for (var i = new xt, e = Tt.getPlatformManager().jsonParseFromBytes(t).physics_hair, r = e.length, n = 0; n < r; n++) {
                        var o = e[n],
                            s = new y,
                            a = o.setup,
                            h = parseFloat(a.length),
                            _ = parseFloat(a.regist),
                            u = parseFloat(a.mass);
                        s.setup(h, _, u);
                        for (var l = o.src, c = l.length, p = 0; p < c; p++) {
                            var f = l[p],
                                $ = f.id,
                                d = y.Src.SRC_TO_X;
                            "x" === (T = f.ptype) ? d = y.Src.SRC_TO_X: "y" === T ? d = y.Src.SRC_TO_Y : "angle" === T ? d = y.Src.SRC_TO_G_ANGLE : UtDebug.error("live2d", "Invalid parameter:PhysicsHair.Src");
                            var g = parseFloat(f.scale),
                                m = parseFloat(f.weight);
                            s.addSrcParam(d, $, g, m)
                        }
                        var v = o.targets,
                            S = v.length;
                        for (p = 0; p < S; p++) {
                            var T, P = v[p];
                            $ = P.id, d = y.Target.TARGET_FROM_ANGLE, "angle" === (T = P.ptype) ? d = y.Target.TARGET_FROM_ANGLE : "angle_v" === T ? d = y.Target.TARGET_FROM_ANGLE_V : UtDebug.error("live2d", "Invalid parameter:PhysicsHair.Target"), g = parseFloat(P.scale), m = parseFloat(P.weight), s.addTargetParam(d, $, g, m)
                        }
                        i.physicsList.push(s)
                    }
                    return i
                }, xt.prototype.updateParam = function(t) {
                    for (var i = ei.getUserTimeMSec() - this.startTimeMSec, e = 0; e < this.physicsList.length; e++) this.physicsList[e].update(t, i)
                }, Ot.load = function(t) {
                    for (var i = new Ot, e = Tt.getPlatformManager().jsonParseFromBytes(t).parts_visible, r = e.length, n = 0; n < r; n++) {
                        for (var o = e[n].group, s = o.length, a = [], h = 0; h < s; h++) {
                            var _ = o[h],
                                u = new At(_.id);
                            if (a[h] = u, null != _.link) {
                                var l = _.link,
                                    c = l.length;
                                u.link = [];
                                for (var p = 0; p < c; p++) {
                                    var f = new At(l[p]);
                                    u.link.push(f)
                                }
                            }
                        }
                        i.partsGroups.push(a)
                    }
                    return i
                }, Ot.prototype.updateParam = function(t) {
                    if (null != t) {
                        t != this.lastModel && this.initParam(t), this.lastModel = t;
                        var i = ei.getUserTimeMSec(),
                            e = 0 == this.lastTime ? 0 : (i - this.lastTime) / 1e3;
                        this.lastTime = i, e < 0 && (e = 0);
                        for (var r = 0; r < this.partsGroups.length; r++) this.normalizePartsOpacityGroup(t, this.partsGroups[r], e), this.copyOpacityOtherParts(t, this.partsGroups[r])
                    }
                }, Ot.prototype.initParam = function(t) {
                    if (null != t)
                        for (var i = 0; i < this.partsGroups.length; i++)
                            for (var e = this.partsGroups[i], r = 0; r < e.length; r++) {
                                e[r].initIndex(t);
                                var n = e[r].partsIndex,
                                    o = e[r].paramIndex;
                                if (!(n < 0)) {
                                    var s = 0 != t.getParamFloat(o);
                                    if (t.setPartsOpacity(n, s ? 1 : 0), t.setParamFloat(o, s ? 1 : 0), null != e[r].link)
                                        for (var a = 0; a < e[r].link.length; a++) e[r].link[a].initIndex(t)
                                }
                            }
                }, Ot.prototype.normalizePartsOpacityGroup = function(t, i, e) {
                    for (var r = -1, n = 1, o = 0; o < i.length; o++) {
                        var s = i[o].partsIndex,
                            a = i[o].paramIndex;
                        if (!(s < 0) && 0 != t.getParamFloat(a)) {
                            if (r >= 0) break;
                            r = o, n = t.getPartsOpacity(s), (n += e / .5) > 1 && (n = 1)
                        }
                    }
                    for (r < 0 && (r = 0, n = 1), o = 0; o < i.length; o++)
                        if (!((s = i[o].partsIndex) < 0))
                            if (r == o) t.setPartsOpacity(s, n);
                            else {
                                var h, _ = t.getPartsOpacity(s);
                                (1 - (h = n < .5 ? -.5 * n / .5 + 1 : .5 * (1 - n) / .5)) * (1 - n) > .15 && (h = 1 - .15 / (1 - n)), _ > h && (_ = h), t.setPartsOpacity(s, _)
                            }
                }, Ot.prototype.copyOpacityOtherParts = function(t, i) {
                    for (var e = 0; e < i.length; e++) {
                        var r = i[e];
                        if (null != r.link && !(r.partsIndex < 0))
                            for (var n = t.getPartsOpacity(r.partsIndex), o = 0; o < r.link.length; o++) {
                                var s = r.link[o];
                                s.partsIndex < 0 || t.setPartsOpacity(s.partsIndex, n)
                            }
                    }
                }, At.prototype.initIndex = function(t) {
                    this.paramIndex = t.getParamIndex("VISIBLE:" + this.id), this.partsIndex = t.getPartsDataIndex(f.getID(this.id)), t.setParamFloat(this.paramIndex, 1)
                };
                var $i = 0;
                It.prototype.getModelMatrix = function() {
                    return this.modelMatrix
                }, It.prototype.setAlpha = function(t) {
                    t > .999 && (t = 1), t < .001 && (t = 0), this.alpha = t
                }, It.prototype.getAlpha = function() {
                    return this.alpha
                }, It.prototype.isInitialized = function() {
                    return this.initialized
                }, It.prototype.setInitialized = function(t) {
                    this.initialized = t
                }, It.prototype.isUpdating = function() {
                    return this.updating
                }, It.prototype.setUpdating = function(t) {
                    this.updating = t
                }, It.prototype.getLive2DModel = function() {
                    return this.live2DModel
                }, It.prototype.setLipSync = function(t) {
                    this.lipSync = t
                }, It.prototype.setLipSyncValue = function(t) {
                    this.lipSyncValue = t
                }, It.prototype.setAccel = function(t, i, e) {
                    this.accelX = t, this.accelY = i, this.accelZ = e
                }, It.prototype.setDrag = function(t, i) {
                    this.dragX = t, this.dragY = i
                }, It.prototype.getMainMotionManager = function() {
                    return this.mainMotionManager
                }, It.prototype.getExpressionManager = function() {
                    return this.expressionManager
                }, It.prototype.loadModelData = function(t, i) {
                    var e = Tt.getPlatformManager();
                    this.debugMode && e.log("Load model : " + t);
                    var r = this;
                    e.loadLive2DModel(t, (function(t) {
                        r.live2DModel = t, r.live2DModel.saveParam(), 0 == ti.getError() ? (r.modelMatrix = new Mt(r.live2DModel.getCanvasWidth(), r.live2DModel.getCanvasHeight()), r.modelMatrix.setWidth(2), r.modelMatrix.setCenterPosition(0, 0), i(r.live2DModel)) : console.error("Error : Failed to loadModelData().")
                    }))
                }, It.prototype.loadTexture = function(t, i, e) {
                    $i++;
                    var r = Tt.getPlatformManager();
                    this.debugMode && r.log("Load Texture : " + i);
                    var n = this;
                    r.loadTexture(this.live2DModel, t, i, (function() {
                        0 == --$i && (n.isTexLoaded = !0), "function" == typeof e && e()
                    }))
                }, It.prototype.loadMotion = function(t, i, e) {
                    var r = Tt.getPlatformManager();
                    this.debugMode && r.log("Load Motion : " + i);
                    var n = null,
                        o = this;
                    r.loadBytes(i, (function(i) {
                        n = Z.loadMotion(i), null != t && (o.motions[t] = n), e(n)
                    }))
                }, It.prototype.loadExpression = function(t, i, e) {
                    var r = Tt.getPlatformManager();
                    this.debugMode && r.log("Load Expression : " + i);
                    var n = this;
                    r.loadBytes(i, (function(i) {
                        null != t && (n.expressions[t] = Et.loadJson(i)), "function" == typeof e && e()
                    }))
                }, It.prototype.loadPose = function(t, i) {
                    var e = Tt.getPlatformManager();
                    this.debugMode && e.log("Load Pose : " + t);
                    var r = this;
                    try {
                        e.loadBytes(t, (function(t) {
                            r.pose = Ot.load(t), "function" == typeof i && i()
                        }))
                    } catch (t) {
                        console.warn(t)
                    }
                }, It.prototype.loadPhysics = function(t) {
                    var i = Tt.getPlatformManager();
                    this.debugMode && i.log("Load Physics : " + t);
                    var e = this;
                    try {
                        i.loadBytes(t, (function(t) {
                            e.physics = xt.load(t)
                        }))
                    } catch (t) {
                        console.warn(t)
                    }
                }, It.prototype.hitTestSimple = function(t, i, e) {
                    var r = this.live2DModel.getDrawDataIndex(t);
                    if (r < 0) return !1;
                    for (var n = this.live2DModel.getTransformedPoints(r), o = this.live2DModel.getCanvasWidth(), s = 0, a = this.live2DModel.getCanvasHeight(), h = 0, _ = 0; _ < n.length; _ += 2) {
                        var u = n[_],
                            l = n[_ + 1];
                        u < o && (o = u), u > s && (s = u), l < a && (a = l), l > h && (h = l)
                    }
                    var c = this.modelMatrix.invertTransformX(i),
                        p = this.modelMatrix.invertTransformY(e);
                    return o <= c && c <= s && a <= p && p <= h
                }, bt.prototype.calcNextBlink = function() {
                    return ei.getUserTimeMSec() + Math.random() * (2 * this.blinkIntervalMsec - 1)
                }, bt.prototype.setInterval = function(t) {
                    this.blinkIntervalMsec = t
                }, bt.prototype.setEyeMotion = function(t, i, e) {
                    this.closingMotionMsec = t, this.closedMotionMsec = i, this.openingMotionMsec = e
                }, bt.prototype.updateParam = function(t) {
                    var i, e = ei.getUserTimeMSec(),
                        r = 0;
                    switch (this.eyeState) {
                        case di.STATE_CLOSING:
                            (r = (e - this.stateStartTime) / this.closingMotionMsec) >= 1 && (r = 1, this.eyeState = di.STATE_CLOSED, this.stateStartTime = e), i = 1 - r;
                            break;
                        case di.STATE_CLOSED:
                            (r = (e - this.stateStartTime) / this.closedMotionMsec) >= 1 && (this.eyeState = di.STATE_OPENING, this.stateStartTime = e), i = 0;
                            break;
                        case di.STATE_OPENING:
                            (r = (e - this.stateStartTime) / this.openingMotionMsec) >= 1 && (r = 1, this.eyeState = di.STATE_INTERVAL, this.nextBlinkTime = this.calcNextBlink()), i = r;
                            break;
                        case di.STATE_INTERVAL:
                            this.nextBlinkTime < e && (this.eyeState = di.STATE_CLOSING, this.stateStartTime = e), i = 1;
                            break;
                        case di.STATE_FIRST:
                        default:
                            this.eyeState = di.STATE_INTERVAL, this.nextBlinkTime = this.calcNextBlink(), i = 1
                    }
                    this.closeIfZero || (i = -i), t.setParamFloat(this.eyeID_L, i), t.setParamFloat(this.eyeID_R, i)
                };
                var di = function() {};
                di.STATE_FIRST = "STATE_FIRST", di.STATE_INTERVAL = "STATE_INTERVAL", di.STATE_CLOSING = "STATE_CLOSING", di.STATE_CLOSED = "STATE_CLOSED", di.STATE_OPENING = "STATE_OPENING", Ct.FRAME_RATE = 30, Ct.prototype.setPoint = function(t, i) {
                    this.faceTargetX = t, this.faceTargetY = i
                }, Ct.prototype.getX = function() {
                    return this.faceX
                }, Ct.prototype.getY = function() {
                    return this.faceY
                }, Ct.prototype.update = function() {
                    var t = 40 / 7.5 / Ct.FRAME_RATE;
                    if (0 !== this.lastTimeSec) {
                        var i = ei.getUserTimeMSec(),
                            e = (i - this.lastTimeSec) * Ct.FRAME_RATE / 1e3;
                        this.lastTimeSec = i;
                        var r = e * t / (.15 * Ct.FRAME_RATE),
                            n = this.faceTargetX - this.faceX,
                            o = this.faceTargetY - this.faceY;
                        if (!(Math.abs(n) <= this.EPSILON && Math.abs(o) <= this.EPSILON)) {
                            var s = Math.sqrt(n * n + o * o),
                                a = t * o / s,
                                h = t * n / s - this.faceVX,
                                _ = a - this.faceVY,
                                u = Math.sqrt(h * h + _ * _);
                            (u < -r || u > r) && (h *= r / u, _ *= r / u, u = r), this.faceVX += h, this.faceVY += _;
                            var l = .5 * (Math.sqrt(r * r + 16 * r * s - 8 * r * s) - r),
                                c = Math.sqrt(this.faceVX * this.faceVX + this.faceVY * this.faceVY);
                            c > l && (this.faceVX *= l / c, this.faceVY *= l / c), this.faceX += this.faceVX, this.faceY += this.faceVY
                        }
                    } else this.lastTimeSec = ei.getUserTimeMSec()
                }, Dt.prototype = new Lt, Dt.prototype.getMaxScale = function() {
                    return this.max
                }, Dt.prototype.getMinScale = function() {
                    return this.min
                }, Dt.prototype.setMaxScale = function(t) {
                    this.max = t
                }, Dt.prototype.setMinScale = function(t) {
                    this.min = t
                }, Dt.prototype.isMaxScale = function() {
                    return this.getScaleX() === this.max
                }, Dt.prototype.isMinScale = function() {
                    return this.getScaleX() === this.min
                }, Dt.prototype.adjustTranslate = function(t, i) {
                    this.tr[0] * this.maxLeft + (this.tr[12] + t) > this.screenLeft && (t = this.screenLeft - this.tr[0] * this.maxLeft - this.tr[12]), this.tr[0] * this.maxRight + (this.tr[12] + t) < this.screenRight && (t = this.screenRight - this.tr[0] * this.maxRight - this.tr[12]), this.tr[5] * this.maxTop + (this.tr[13] + i) < this.screenTop && (i = this.screenTop - this.tr[5] * this.maxTop - this.tr[13]), this.tr[5] * this.maxBottom + (this.tr[13] + i) > this.screenBottom && (i = this.screenBottom - this.tr[5] * this.maxBottom - this.tr[13]);
                    var e = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, t, i, 0, 1];
                    Lt.mul(e, this.tr, this.tr)
                }, Dt.prototype.adjustScale = function(t, i, e) {
                    var r = e * this.tr[0];
                    r < this.min ? this.tr[0] > 0 && (e = this.min / this.tr[0]) : r > this.max && this.tr[0] > 0 && (e = this.max / this.tr[0]);
                    var n = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, t, i, 0, 1],
                        o = [e, 0, 0, 0, 0, e, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1],
                        s = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -t, -i, 0, 1];
                    Lt.mul(s, this.tr, this.tr), Lt.mul(o, this.tr, this.tr), Lt.mul(n, this.tr, this.tr)
                }, Dt.prototype.setScreenRect = function(t, i, e, r) {
                    this.screenLeft = t, this.screenRight = i, this.screenTop = r, this.screenBottom = e
                }, Dt.prototype.setMaxScreenRect = function(t, i, e, r) {
                    this.maxLeft = t, this.maxRight = i, this.maxTop = r, this.maxBottom = e
                }, Dt.prototype.getScreenLeft = function() {
                    return this.screenLeft
                }, Dt.prototype.getScreenRight = function() {
                    return this.screenRight
                }, Dt.prototype.getScreenBottom = function() {
                    return this.screenBottom
                }, Dt.prototype.getScreenTop = function() {
                    return this.screenTop
                }, Dt.prototype.getMaxLeft = function() {
                    return this.maxLeft
                }, Dt.prototype.getMaxRight = function() {
                    return this.maxRight
                }, Dt.prototype.getMaxBottom = function() {
                    return this.maxBottom
                }, Dt.prototype.getMaxTop = function() {
                    return this.maxTop
                };
                var gi = e(96),
                    yi = e.n(gi),
                    mi = e(52),
                    vi = e.n(mi);
                Rt.prototype.loadModelSetting = function(t, i) {
                    var e = this;
                    Tt.getPlatformManager().loadBytes(t, (function(t) {
                        var r = String.fromCharCode.apply(null, new Uint8Array(t));
                        e.json = JSON.parse(r), i()
                    }))
                }, Rt.prototype.getTextureFile = function(t) {
                    return null == this.json[this.TEXTURES] || null == this.json[this.TEXTURES][t] ? null : this.json[this.TEXTURES][t]
                }, Rt.prototype.getModelFile = function() {
                    return this.json[this.MODEL]
                }, Rt.prototype.getTextureNum = function() {
                    return null == this.json[this.TEXTURES] ? 0 : this.json[this.TEXTURES].length
                }, Rt.prototype.getHitAreaNum = function() {
                    return null == this.json[this.HIT_AREAS] ? 0 : this.json[this.HIT_AREAS].length
                }, Rt.prototype.getHitAreaID = function(t) {
                    return null == this.json[this.HIT_AREAS] || null == this.json[this.HIT_AREAS][t] ? null : this.json[this.HIT_AREAS][t][this.ID]
                }, Rt.prototype.getHitAreaName = function(t) {
                    return null == this.json[this.HIT_AREAS] || null == this.json[this.HIT_AREAS][t] ? null : this.json[this.HIT_AREAS][t][this.NAME]
                }, Rt.prototype.getPhysicsFile = function() {
                    return this.json[this.PHYSICS]
                }, Rt.prototype.getPoseFile = function() {
                    return this.json[this.POSE]
                }, Rt.prototype.getExpressionNum = function() {
                    return null == this.json[this.EXPRESSIONS] ? 0 : this.json[this.EXPRESSIONS].length
                }, Rt.prototype.getExpressionFile = function(t) {
                    return null == this.json[this.EXPRESSIONS] ? null : this.json[this.EXPRESSIONS][t][this.FILE]
                }, Rt.prototype.getExpressionName = function(t) {
                    return null == this.json[this.EXPRESSIONS] ? null : this.json[this.EXPRESSIONS][t][this.NAME]
                }, Rt.prototype.getLayout = function() {
                    return this.json[this.LAYOUT]
                }, Rt.prototype.getInitParamNum = function() {
                    return null == this.json[this.INIT_PARAM] ? 0 : this.json[this.INIT_PARAM].length
                }, Rt.prototype.getMotionNum = function(t) {
                    return null == this.json[this.MOTION_GROUPS] || null == this.json[this.MOTION_GROUPS][t] ? 0 : this.json[this.MOTION_GROUPS][t].length
                }, Rt.prototype.getMotionFile = function(t, i) {
                    return null == this.json[this.MOTION_GROUPS] || null == this.json[this.MOTION_GROUPS][t] || null == this.json[this.MOTION_GROUPS][t][i] ? null : this.json[this.MOTION_GROUPS][t][i][this.FILE]
                }, Rt.prototype.getMotionSound = function(t, i) {
                    return null == this.json[this.MOTION_GROUPS] || null == this.json[this.MOTION_GROUPS][t] || null == this.json[this.MOTION_GROUPS][t][i] || null == this.json[this.MOTION_GROUPS][t][i][this.SOUND] ? null : this.json[this.MOTION_GROUPS][t][i][this.SOUND]
                }, Rt.prototype.getMotionFadeIn = function(t, i) {
                    return null == this.json[this.MOTION_GROUPS] || null == this.json[this.MOTION_GROUPS][t] || null == this.json[this.MOTION_GROUPS][t][i] || null == this.json[this.MOTION_GROUPS][t][i][this.FADE_IN] ? 1e3 : this.json[this.MOTION_GROUPS][t][i][this.FADE_IN]
                }, Rt.prototype.getMotionFadeOut = function(t, i) {
                    return null == this.json[this.MOTION_GROUPS] || null == this.json[this.MOTION_GROUPS][t] || null == this.json[this.MOTION_GROUPS][t][i] || null == this.json[this.MOTION_GROUPS][t][i][this.FADE_OUT] ? 1e3 : this.json[this.MOTION_GROUPS][t][i][this.FADE_OUT]
                }, Rt.prototype.getInitParamID = function(t) {
                    return null == this.json[this.INIT_PARAM] || null == this.json[this.INIT_PARAM][t] ? null : this.json[this.INIT_PARAM][t][this.ID]
                }, Rt.prototype.getInitParamValue = function(t) {
                    return null == this.json[this.INIT_PARAM] || null == this.json[this.INIT_PARAM][t] ? NaN : this.json[this.INIT_PARAM][t][this.VALUE]
                }, Rt.prototype.getInitPartsVisibleNum = function() {
                    return null == this.json[this.INIT_PARTS_VISIBLE] ? 0 : this.json[this.INIT_PARTS_VISIBLE].length
                }, Rt.prototype.getInitPartsVisibleID = function(t) {
                    return null == this.json[this.INIT_PARTS_VISIBLE] || null == this.json[this.INIT_PARTS_VISIBLE][t] ? null : this.json[this.INIT_PARTS_VISIBLE][t][this.ID]
                }, Rt.prototype.getInitPartsVisibleValue = function(t) {
                    return null == this.json[this.INIT_PARTS_VISIBLE] || null == this.json[this.INIT_PARTS_VISIBLE][t] ? NaN : this.json[this.INIT_PARTS_VISIBLE][t][this.VALUE]
                }, Rt.prototype.getExternalMotions = function() {
                    return console.log(this.json), this.json.externalMotions || {}
                };
                var Si = function(t) {
                    return window.console && console.error("[Live2D Error] " + t), !1
                };
                Ft.prototype = new It, Ft.prototype.load = function(t, i, e) {
                    var r = this;
                    return new Wt.a((function(t, n) {
                        r.setUpdating(!0), r.setInitialized(!1), r.modelHomeDir = i.substring(0, i.lastIndexOf("/") + 1), r.modelSetting = new Rt, r.modelSetting.loadModelSetting(i, (function() {
                            var i = r.modelHomeDir + r.modelSetting.getModelFile();
                            r.loadModelData(i, (function(i) {
                                for (var n = 0; n < r.modelSetting.getTextureNum(); n++) {
                                    var o = r.modelHomeDir + r.modelSetting.getTextureFile(n);
                                    r.loadTexture(n, o, (function() {
                                        if (r.isTexLoaded) {
                                            if (r.modelSetting.getExpressionNum() > 0) {
                                                r.expressions = {};
                                                for (var i = 0; i < r.modelSetting.getExpressionNum(); i++) {
                                                    var n = r.modelSetting.getExpressionName(i),
                                                        o = r.modelHomeDir + r.modelSetting.getExpressionFile(i);
                                                    r.loadExpression(n, o)
                                                }
                                            } else r.expressionManager = null, r.expressions = {};
                                            if (null == r.eyeBlink && (r.eyeBlink = new bt), null != r.modelSetting.getPhysicsFile() ? r.loadPhysics(r.modelHomeDir + r.modelSetting.getPhysicsFile()) : r.physics = null, null != r.modelSetting.getPoseFile() ? r.loadPose(r.modelHomeDir + r.modelSetting.getPoseFile(), (function() {
                                                    return r.pose.updateParam(r.live2DModel)
                                                })) : r.pose = null, null != r.modelSetting.getLayout()) {
                                                var s = r.modelSetting.getLayout();
                                                null != s.width && r.modelMatrix.setWidth(s.width), null != s.height && r.modelMatrix.setHeight(s.height), null != s.x && r.modelMatrix.setX(s.x), null != s.y && r.modelMatrix.setY(s.y), null != s.center_x && r.modelMatrix.centerX(s.center_x), null != s.center_y && r.modelMatrix.centerY(s.center_y), null != s.top && r.modelMatrix.top(s.top), null != s.bottom && r.modelMatrix.bottom(s.bottom), null != s.left && r.modelMatrix.left(s.left), null != s.right && r.modelMatrix.right(s.right)
                                            }
                                            for (i = 0; i < r.modelSetting.getInitParamNum(); i++) r.live2DModel.setParamFloat(r.modelSetting.getInitParamID(i), r.modelSetting.getInitParamValue(i));
                                            for (i = 0; i < r.modelSetting.getInitPartsVisibleNum(); i++) r.live2DModel.setPartsOpacity(r.modelSetting.getInitPartsVisibleID(i), r.modelSetting.getInitPartsVisibleValue(i));
                                            r.live2DModel.saveParam(), r.preloadMotionGroup(r.appConfig.MOTION_GROUP_IDLE), r.mainMotionManager.stopAllMotions(), r.setUpdating(!1), r.setInitialized(!0), "function" == typeof e && e(), t(r.modelSetting.json)
                                        }
                                    }))
                                }
                            }))
                        }))
                    }))
                }, Ft.prototype.release = function(t) {
                    var i = Tt.getPlatformManager();
                    t.deleteTexture(i.texture)
                }, Ft.prototype.preloadMotionGroup = function(t) {
                    for (var i = this, e = 0; e < this.modelSetting.getMotionNum(t); e++) {
                        var r = this.modelSetting.getMotionFile(t, e);
                        this.loadMotion(r, this.modelHomeDir + r, (function(r) {
                            r.setFadeIn(i.modelSetting.getMotionFadeIn(t, e)), r.setFadeOut(i.modelSetting.getMotionFadeOut(t, e))
                        }))
                    }
                }, Ft.prototype.update = function() {
                    if (null == this.live2DModel) return Si("Failed to update, in AppModel.");
                    var t = (ei.getUserTimeMSec() - this.startTimeMSec) / 1e3 * 2 * Math.PI;
                    this.mainMotionManager.isFinished() && this.startMotionGroup(this.appConfig.MOTION_GROUP_IDLE, this.appConfig.PRIORITY_IDLE), this.live2DModel.loadParam(), this.mainMotionManager.updateParam(this.live2DModel) || null != this.eyeBlink && this.eyeBlink.updateParam(this.live2DModel), this.live2DModel.saveParam(), null == this.expressionManager || null == this.expressions || this.expressionManager.isFinished() || this.expressionManager.updateParam(this.live2DModel), this.live2DModel.addToParamFloat("PARAM_ANGLE_X", 30 * this.dragX, 1), this.live2DModel.addToParamFloat("PARAM_ANGLE_Y", 30 * this.dragY, 1), this.live2DModel.addToParamFloat("PARAM_ANGLE_Z", this.dragX * this.dragY * -30, 1), this.live2DModel.addToParamFloat("PARAM_BODY_ANGLE_X", 10 * this.dragX, 1), this.live2DModel.addToParamFloat("PARAM_EYE_BALL_X", this.dragX, 1), this.live2DModel.addToParamFloat("PARAM_EYE_BALL_Y", this.dragY, 1), this.live2DModel.addToParamFloat("PARAM_ANGLE_X", Number(15 * Math.sin(t / 6.5345)), .5), this.live2DModel.addToParamFloat("PARAM_ANGLE_Y", Number(8 * Math.sin(t / 3.5345)), .5), this.live2DModel.addToParamFloat("PARAM_ANGLE_Z", Number(10 * Math.sin(t / 5.5345)), .5), this.live2DModel.addToParamFloat("PARAM_BODY_ANGLE_X", Number(4 * Math.sin(t / 15.5345)), .5), this.live2DModel.setParamFloat("PARAM_BREATH", Number(.5 + .5 * Math.sin(t / 3.2345)), 1), null != this.physics && this.physics.updateParam(this.live2DModel), null == this.lipSync && this.live2DModel.setParamFloat("PARAM_MOUTH_OPEN_Y", this.lipSyncValue), null != this.pose && this.pose.updateParam(this.live2DModel), this.live2DModel.update()
                }, Ft.prototype.setRandomExpression = function() {
                    var t = vi()(this.expressions);
                    if (t.length) {
                        var i = Math.floor(Math.random() * t.length);
                        this.setExpression(t[i])
                    }
                }, Ft.prototype.startMotionGroup = function(t, i) {
                    var e = this.modelSetting.getMotionNum(t),
                        r = parseInt(Math.random() * e);
                    this.startMotion(t, r, i)
                }, Ft.prototype.startMotion = function(t, i, e) {
                    var r = this,
                        n = this.modelSetting.getMotionFile(t, i);
                    if (n) {
                        if (e == this.appConfig.PRIORITY_FORCE) this.mainMotionManager.setReservePriority(e);
                        else if (!this.mainMotionManager.reserveMotion(e)) return void(this.appConfig.DEBUG_LOG && console.log("Motion is running."));
                        var o;
                        null == this.motions[t] ? this.loadMotion(null, this.modelHomeDir + n, (function(n) {
                            o = n, r.setFadeInFadeOut(t, i, e, o)
                        })) : (o = this.motions[t], this.setFadeInFadeOut(t, i, e, o))
                    } else this.appConfig.DEBUG_LOG && console.error("Failed to motion.")
                }, Ft.prototype.setFadeInFadeOut = function(t, i, e, r) {
                    var n = this.modelSetting.getMotionFile(t, i);
                    if (r.setFadeIn(this.modelSetting.getMotionFadeIn(t, i)), r.setFadeOut(this.modelSetting.getMotionFadeOut(t, i)), this.appConfig.DEBUG_LOG && console.log("Start motion : " + n), null == this.modelSetting.getMotionSound(t, i)) this.mainMotionManager.startMotionPrio(r, e);
                    else {
                        var o = this.modelSetting.getMotionSound(t, i),
                            s = document.createElement("audio");
                        s.src = this.modelHomeDir + o, this.appConfig.DEBUG_LOG && console.log("Start sound : " + o), s.play(), this.mainMotionManager.startMotionPrio(r, e)
                    }
                }, Ft.prototype.setExpression = function(t) {
                    var i = this.expressions[t];
                    this.appConfig.DEBUG_LOG && console.log("Expression : " + t), this.expressionManager.startMotion(i, !1)
                }, Ft.prototype.draw = function(t) {
                    this.matrixStack.push(), this.matrixStack.multMatrix(this.modelMatrix.getArray()), this.tmpMatrix = this.matrixStack.getMatrix(), this.live2DModel.setMatrix(this.tmpMatrix), this.live2DModel.draw(), this.matrixStack.pop()
                }, Ft.prototype.hitTest = function(t, i, e) {
                    for (var r = this.modelSetting.getHitAreaNum(), n = 0; n < r; n++)
                        if (t == this.modelSetting.getHitAreaName(n)) {
                            var o = this.modelSetting.getHitAreaID(n);
                            return this.hitTestSimple(o, i, e)
                        }
                    return !1
                };
                var Ti = {};
                Gt.prototype.loadBytes = function(t, i) {
                    if (t = Bt(t), !this.appConfig.NO_CACHE && Ti[t]) return i(Ti[t]);
                    var e = new XMLHttpRequest;
                    e.open("GET", t, !0), e.responseType = "arraybuffer", this.appConfig.ALLOW_CORS && ("function" == typeof this.appConfig.CORS_RULES && this.appConfig.CORS_RULES(t) || void 0 === this.appConfig.CORS_RULES) && (e.withCredentials = !0), e.onload = function() {
                        if (200 === e.status) {
                            var r = vi()(Ti);
                            r.length > 100 && r.forEach((function(t) {
                                delete Ti[t]
                            })), Ti[t] = e.response, i(e.response)
                        } else console.error("Failed to load (" + e.status + ") : " + t)
                    }, e.send(null)
                }, Gt.prototype.loadString = function(t) {
                    this.loadBytes(t, (function(t) {
                        return t
                    }))
                }, Gt.prototype.loadLive2DModel = function(t, i) {
                    var e = null;
                    this.loadBytes(t, (function(t) {
                        e = ct.loadModel(t), i(e)
                    }))
                };
                var Pi = {};
                Gt.prototype.loadTexture = function(t, i, e, r) {
                    function n() {
                        var n = Nt(o.canvas, {
                                premultipliedAlpha: !0
                            }),
                            s = n.createTexture();
                        if (!s) return console.error("Failed to generate gl texture name."), -1;
                        0 == t.isPremultipliedAlpha() && n.pixelStorei(n.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1), n.pixelStorei(n.UNPACK_FLIP_Y_WEBGL, 1), n.activeTexture(n.TEXTURE0), n.bindTexture(n.TEXTURE_2D, s), n.texImage2D(n.TEXTURE_2D, 0, n.RGBA, n.RGBA, n.UNSIGNED_BYTE, a), n.texParameteri(n.TEXTURE_2D, n.TEXTURE_MAG_FILTER, n.LINEAR), n.texParameteri(n.TEXTURE_2D, n.TEXTURE_MIN_FILTER, n.LINEAR_MIPMAP_NEAREST), n.generateMipmap(n.TEXTURE_2D), t.setTexture(i, s), s = null, o.appConfig.NO_CACHE || Pi[e] || (Pi[e] = a), "function" == typeof r && r()
                    }
                    var o = this,
                        s = Bt(e),
                        a = null;
                    if (this.appConfig.NO_CACHE || !Pi[e]) a = new Image, this.appConfig.ALLOW_CORS && (a.crossOrigin = "anonymous"), a.src = s, a.onload = n, a.onerror = function() {
                        console.error("Failed to load image : " + e)
                    };
                    else {
                        a = Pi[e];
                        var h = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame;
                        h ? h(n) : setTimeout(n, 1)
                    }
                }, Gt.prototype.jsonParseFromBytes = function(t) {
                    var i, e = new Uint8Array(t, 0, 3);
                    return i = 239 == e[0] && 187 == e[1] && 191 == e[2] ? String.fromCharCode.apply(null, new Uint8Array(t, 3)) : String.fromCharCode.apply(null, new Uint8Array(t)), JSON.parse(i)
                }, Gt.prototype.log = function(t) {
                    console.log(t)
                }, kt.prototype.createModel = function() {
                    var t = new Ft({
                        appConfig: this.appConfig,
                        matrixStack: this.matrixStack
                    });
                    return this.models.push(t), t
                }, kt.prototype.changeModel = function() {
                    var t = this,
                        i = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                    return new Wt.a((function(e, r) {
                        if (i === t.nextModel - 1) return e();
                        t.reloadFlg = !0, t.releaseAllModels(), t.createModel(), void 0 !== t.appConfig.MODELS && t.appConfig.MODELS.length && ("number" == typeof i ? t.models[0].load(t.gl, t.appConfig.MODELS[i]).then((function(r) {
                            t.nextModel = i + 1, t.reloadFlg = !1, e(r)
                        })) : t.models[0].load(t.gl, t.appConfig.MODELS[t.nextModel]).then((function(i) {
                            t.nextModel++, t.reloadFlg = !1, e(i)
                        })))
                    }))
                }, kt.prototype.loadModelByURL = function(t) {
                    var i = this;
                    return new Wt.a((function(e, r) {
                        i.releaseAllModels(), i.createModel(), i.models[0].load(i.gl, t).then((function(t) {
                            e(t)
                        }))
                    }))
                }, kt.prototype.getModel = function(t) {
                    return t >= this.models.length ? null : this.models[t]
                }, kt.prototype.releaseModel = function(t, i) {
                    this.models.length <= t || (this.models[t].release(i), delete this.models[t], this.models.splice(t, 1))
                }, kt.prototype.releaseAllModels = function(t) {
                    var i = this;
                    t = t || this.gl, this.models.forEach((function(e, r) {
                        i.releaseModel(r, t)
                    }))
                }, kt.prototype.getModelCount = function() {
                    return this.models.length
                }, kt.prototype.setDrag = function(t, i) {
                    for (var e = 0; e < this.models.length; e++) this.models[e].setDrag(t, i)
                }, kt.prototype.maxScaleEvent = function() {
                    this.appConfig.DEBUG_LOG && console.log("Max scale event.");
                    for (var t = 0; t < this.models.length; t++) this.models[t].startMotionGroup(this.appConfig.MOTION_GROUP_PINCH_IN, this.appConfig.PRIORITY_NORMAL)
                }, kt.prototype.minScaleEvent = function() {
                    this.appConfig.DEBUG_LOG && console.log("Min scale event.");
                    for (var t = 0; t < this.models.length; t++) this.models[t].startMotionGroup(this.appConfig.MOTION_GROUP_PINCH_OUT, this.appConfig.PRIORITY_NORMAL)
                }, kt.prototype.tapEvent = function(t, i) {
                    this.appConfig.DEBUG_LOG && console.log("tapEvent view x:" + t + " y:" + i);
                    var e = 0,
                        r = !0,
                        n = !1,
                        o = void 0;
                    try {
                        for (var s, a = yi()(this.models); !(r = (s = a.next()).done); r = !0) {
                            var h = s.value;
                            if (this.appConfig.NO_TAP_AREA_DETECTION) return this.setRandomExpression(h), void this.startMotionGroup(this.appConfig.MOTION_GROUP_TAP_BODY, h, this.appConfig.MOTION_GROUP_TAP_BODY);
                            if (h.hitTest(this.appConfig.HIT_AREA_HEAD, t, i)) return this.appConfig.DEBUG_LOG && console.log("Tap face."), void this.setRandomExpression(h);
                            h.hitTest(this.appConfig.HIT_AREA_BODY, t, i) && (this.appConfig.DEBUG_LOG && console.log("Tap body. models[" + e + "]"), this.startMotionGroup(this.appConfig.MOTION_GROUP_TAP_BODY, h, this.appConfig.MOTION_GROUP_TAP_BODY)), e++
                        }
                    } catch (t) {
                        n = !0, o = t
                    } finally {
                        try {
                            !r && a.return && a.return()
                        } finally {
                            if (n) throw o
                        }
                    }
                    return !0
                }, kt.prototype.startMotionGroup = function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.appConfig.MOTION_GROUP_TAP_BODY,
                        i = arguments[1],
                        e = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : this.appConfig.PRIORITY_NORMAL;
                    if (i) try {
                        i.startMotionGroup(t, e)
                    } catch (t) {
                        this.appConfig.DEBUG_LOG && Si("目标模型非法，无法执行 startMotionGroup: ", t)
                    } else this.models.forEach((function(i) {
                        i.startMotionGroup(t, e)
                    }))
                }, kt.prototype.setRandomExpression = function(t) {
                    if (t) try {
                        t.setRandomExpression()
                    } catch (t) {
                        this.appConfig.DEBUG_LOG && Si("目标模型非法，无法执行 setRandomExpression: " + t)
                    } else this.models.forEach((function(t) {
                        t.setRandomExpression()
                    }))
                };
                var Li = function(t) {
                    return window.console && console.warn("[Live2D Warn] " + t), !1
                };
                Ut.prototype.reset = function() {
                    this.depth = 0
                }, Ut.prototype.loadIdentity = function() {
                    for (var t = 0; t < 16; t++) this.currentMatrix[t] = t % 5 == 0 ? 1 : 0
                }, Ut.prototype.push = function() {
                    var t = (this.depth, 16 * (this.depth + 1));
                    this.length < t + 16 && (this.length = t + 16);
                    for (var i = 0; i < 16; i++) this.matrixStack[t + i] = this.currentMatrix[i];
                    this.depth++
                }, Ut.prototype.pop = function() {
                    --this.depth < 0 && (console.error("[MatrixStack] Invalid matrix stack."), this.depth = 0);
                    for (var t = 16 * this.depth, i = 0; i < 16; i++) this.currentMatrix[i] = this.matrixStack[t + i]
                }, Ut.prototype.getMatrix = function() {
                    return this.currentMatrix
                }, Ut.prototype.multMatrix = function(t) {
                    var i = void 0,
                        e = void 0,
                        r = void 0;
                    for (i = 0; i < 16; i++) this.tmp[i] = 0;
                    for (i = 0; i < 4; i++)
                        for (e = 0; e < 4; e++)
                            for (r = 0; r < 4; r++) this.tmp[i + 4 * e] += this.currentMatrix[i + 4 * r] * t[r + 4 * e];
                    for (i = 0; i < 16; i++) this.currentMatrix[i] = this.tmp[i]
                }, e.d(i, "Live2DApp", (function() {
                    return Yt
                })), Yt.prototype._noSupporting = function() {
                    return console.error("[Live2D Error] 浏览器不支持 Live2D，即将退出."), !1
                }, Yt.prototype._initCanvas = function() {
                    var t = this;
                    this._canvas.addEventListener && (["click", "mousedown", "mousemove", "mouseup", "mouseout"].forEach((function(i) {
                        return t._canvas.addEventListener(i, (function(i) {
                            return t._mouseEvent(i)
                        }), !1)
                    })), this.appConfig.NO_SCALING || ["mousewheel"].forEach((function(i) {
                        return t._canvas.addEventListener(i, (function(i) {
                            return t._mouseEvent(i)
                        }), !1)
                    })), ["touchstart", "touchmove", "touchend"].forEach((function(i) {
                        return t._canvas.addEventListener(i, (function(i) {
                            return t._touchEvent(i)
                        }), !1)
                    })), this.appConfig.FOLLOW_CURSOR && ["mousemove"].forEach((function(i) {
                        return t._canvas.addEventListener(i, (function(i) {
                            return t._followPointer(i)
                        }), !1)
                    })))
                }, Yt.prototype._mouseEvent = function(t) {
                    switch (t.preventDefault(), t.type) {
                        case "mousewheel":
                            if (t.clientX < 0 || this._canvas.clientWidth < t.clientX || t.clientY < 0 || this._canvas.clientHeight < t.clientY) return;
                            this._scaling(t.wheelDelta > 0 ? 1.1 : .9);
                            break;
                        case "mousedown":
                            if ("button" in t && 0 !== t.button) return;
                            this.turnHead(t);
                            break;
                        case "mousemove":
                            this._followPointer(t);
                            break;
                        case "mouseup":
                            if ("button" in t && 0 !== t.button) return;
                            this.lookFront();
                            break;
                        case "mouseout":
                            this.lookFront()
                    }
                }, Yt.prototype._touchEvent = function(t) {
                    t.preventDefault();
                    var i = t.touches[0];
                    switch (t.type) {
                        case "touchstart":
                            1 === t.touches.length && this.turnHead(i);
                            break;
                        case "touchmove":
                            if (this._followPointer(i), 2 === t.touches.length) {
                                var e = t.touches[0],
                                    r = t.touches[1],
                                    n = Math.pow(e.pageX - r.pageX, 2) + Math.pow(e.pageY - r.pageY, 2);
                                this._scaling(n - this._lastDoubleFingersTouchDistance > 0 ? 1.025 : .975), this._lastDoubleFingersTouchDistance = n
                            }
                            break;
                        case "touchend":
                            this.lookFront()
                    }
                }, Yt.prototype._init = function() {
                    var t = this._canvas.width,
                        i = this._canvas.height,
                        e = i / t,
                        r = this.appConfig.VIEW_LOGICAL_LEFT,
                        n = this.appConfig.VIEW_LOGICAL_RIGHT,
                        o = -e,
                        s = e;
                    this._viewMatrix.setScreenRect(r, n, o, s), this._viewMatrix.setMaxScreenRect(this.appConfig.VIEW_LOGICAL_MAX_LEFT, this.appConfig.VIEW_LOGICAL_MAX_RIGHT, this.appConfig.VIEW_LOGICAL_MAX_BOTTOM, this.appConfig.VIEW_LOGICAL_MAX_TOP), this._viewMatrix.setMaxScale(this.appConfig.VIEW_MAX_SCALE), this._viewMatrix.setMinScale(this.appConfig.VIEW_MIN_SCALE), this._projMatrix.multScale(1, t / i), this._deviceToScreen.multTranslate(-t / 2, -i / 2), this._deviceToScreen.multScale(2 / t, -2 / t), ti.setGL(this._gl), this._gl.clearColor(0, 0, 0, 0), this.changeModel(0), this.start()
                }, Yt.prototype._scaling = function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1,
                        i = this._viewMatrix.isMaxScale(),
                        e = this._viewMatrix.isMinScale();
                    this._viewMatrix.adjustScale(0, 0, t), i || this._viewMatrix.isMaxScale() && this.live2DMgr.maxScaleEvent(), e || this._viewMatrix.isMinScale() && this.live2DMgr.minScaleEvent()
                }, Yt.prototype.setScale = function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1,
                        i = this._lastScaleRatio * t;
                    this._scaling(i), this._lastScaleRatio = 1 / t
                }, Yt.prototype.turnHead = function(t) {
                    this._inDrag = !0;
                    var i = t.target.getBoundingClientRect(),
                        e = Xt.call(this, t.clientX - i.left),
                        r = Ht.call(this, t.clientY - i.top),
                        n = Vt.call(this, t.clientX - i.left),
                        o = jt.call(this, t.clientY - i.top);
                    this.appConfig.DEBUG_MOUSE_LOG && console.info("[Live2D] onMouseDown device( x:" + t.clientX + " y:" + t.clientY + " ) view( x:" + n + " y:" + o + ")"), this._lastMouseX = e, this._lastMouseY = r, this._drawMgr.setPoint(n, o), this.live2DMgr.tapEvent(n, o)
                }, Yt.prototype.startRandomMotion = function() {
                    this.live2DMgr.startMotionGroup()
                }, Yt.prototype._followPointer = function(t) {
                    var i = t.target.getBoundingClientRect(),
                        e = Xt.call(this, t.clientX - i.left),
                        r = Ht.call(this, t.clientY - i.top),
                        n = Vt.call(this, t.clientX - i.left),
                        o = jt.call(this, t.clientY - i.top);
                    this.appConfig.DEBUG_MOUSE_LOG && l2dLog("onMouseMove device( x:" + t.clientX + " y:" + t.clientY + " ) view( x:" + n + " y:" + o + ")"), (this._inDrag || this.appConfig.FOLLOW_CURSOR) && (this._lastMouseX = e, this._lastMouseY = r, this._drawMgr.setPoint(n, o))
                }, Yt.prototype.lookFront = function(t) {
                    this._inDrag && (this._inDrag = !1), this._drawMgr.setPoint(0, 0)
                }, Yt.prototype._draw = function() {
                    this._matrixStack.reset(), this._matrixStack.loadIdentity(), this._drawMgr.update(), this.live2DMgr.setDrag(this._drawMgr.getX(), this._drawMgr.getY()), this._gl.clear(this._gl.COLOR_BUFFER_BIT), this._matrixStack.multMatrix(this._projMatrix.getArray()), this._matrixStack.multMatrix(this._viewMatrix.getArray()), this._matrixStack.push();
                    for (var t = 0; t < this.live2DMgr.getModelCount(); t++) {
                        var i = this.live2DMgr.getModel(t);
                        if (!i) return;
                        i.initialized && !i.updating && (i.update(), i.draw(this._gl))
                    }
                    this._matrixStack.pop()
                }, Yt.prototype.start = function() {
                    this._inDrawing || (this._inDrawing = !0, function t(i) {
                        if (i._inDrawing) {
                            i._draw();
                            var e = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
                            if (!e) return i._noSupporting();
                            e(t.bind(null, i), i._canvas)
                        }
                    }(this))
                }, Yt.prototype.stop = function() {
                    this._inDrawing = !1
                }, Yt.prototype.changeModel = function(t) {
                    var i = this;
                    return new Wt.a((function(e, r) {
                        i._isModelShown = !1, i._modelInLoading = !0, i.live2DMgr.changeModel(t).then((function(t) {
                            i._modelInLoading = !1, i._isModelShown = !0, i._modelOnLoadFuncs.forEach((function(i) {
                                return i.call(null, t)
                            })), e(t)
                        }))
                    }))
                }, Yt.prototype.loadModelByURL = function(t) {
                    var i = this;
                    return new Wt.a((function(e, r) {
                        i.live2DMgr.loadModelByURL(t).then((function(t) {
                            i._modelOnLoadFuncs.forEach((function(i) {
                                return i(t)
                            })), e(t)
                        }))
                    }))
                }, Yt.prototype.onload = function(t) {
                    "function" == typeof t && this._modelOnLoadFuncs.indexOf(t) < 0 && this._modelOnLoadFuncs.push(t)
                }, Yt.prototype.startMotionGroup = function(t) {
                    t && this.live2DMgr.startMotionGroup(t)
                }
            }, function(t, i, e) {
                e(36), e(22), e(31), e(65), e(76), e(77), t.exports = e(1).Promise
            }, function(t, i, e) {
                var r = e(23),
                    n = e(24);
                t.exports = function(t) {
                    return function(i, e) {
                        var o, s, a = String(n(i)),
                            h = r(e),
                            _ = a.length;
                        return h < 0 || h >= _ ? t ? "" : void 0 : (o = a.charCodeAt(h)) < 55296 || o > 56319 || h + 1 === _ || (s = a.charCodeAt(h + 1)) < 56320 || s > 57343 ? t ? a.charAt(h) : o : t ? a.slice(h, h + 2) : s - 56320 + (o - 55296 << 10) + 65536
                    }
                }
            }, function(t, i, e) {
                "use strict";
                var r = e(40),
                    n = e(17),
                    o = e(20),
                    s = {};
                e(7)(s, e(2)("iterator"), (function() {
                    return this
                })), t.exports = function(t, i, e) {
                    t.prototype = r(s, {
                        next: n(1, e)
                    }), o(t, i + " Iterator")
                }
            }, function(t, i, e) {
                var r = e(4),
                    n = e(3),
                    o = e(18);
                t.exports = e(5) ? Object.defineProperties : function(t, i) {
                    n(t);
                    for (var e, s = o(i), a = s.length, h = 0; a > h;) r.f(t, e = s[h++], i[e]);
                    return t
                }
            }, function(t, i, e) {
                var r = e(14);
                t.exports = Object("z").propertyIsEnumerable(0) ? Object : function(t) {
                    return "String" == r(t) ? t.split("") : Object(t)
                }
            }, function(t, i, e) {
                var r = e(10),
                    n = e(42),
                    o = e(60);
                t.exports = function(t) {
                    return function(i, e, s) {
                        var a, h = r(i),
                            _ = n(h.length),
                            u = o(s, _);
                        if (t && e != e) {
                            for (; _ > u;)
                                if ((a = h[u++]) != a) return !0
                        } else
                            for (; _ > u; u++)
                                if ((t || u in h) && h[u] === e) return t || u || 0;
                        return !t && -1
                    }
                }
            }, function(t, i, e) {
                var r = e(23),
                    n = Math.max,
                    o = Math.min;
                t.exports = function(t, i) {
                    return (t = r(t)) < 0 ? n(t + i, 0) : o(t, i)
                }
            }, function(t, i, e) {
                var r = e(9),
                    n = e(30),
                    o = e(27)("IE_PROTO"),
                    s = Object.prototype;
                t.exports = Object.getPrototypeOf || function(t) {
                    return t = n(t), r(t, o) ? t[o] : "function" == typeof t.constructor && t instanceof t.constructor ? t.constructor.prototype : t instanceof Object ? s : null
                }
            }, function(t, i, e) {
                "use strict";
                var r = e(63),
                    n = e(64),
                    o = e(13),
                    s = e(10);
                t.exports = e(37)(Array, "Array", (function(t, i) {
                    this._t = s(t), this._i = 0, this._k = i
                }), (function() {
                    var t = this._t,
                        i = this._k,
                        e = this._i++;
                    return !t || e >= t.length ? (this._t = void 0, n(1)) : n(0, "keys" == i ? e : "values" == i ? t[e] : [e, t[e]])
                }), "values"), o.Arguments = o.Array, r("keys"), r("values"), r("entries")
            }, function(t, i) {
                t.exports = function() {}
            }, function(t, i) {
                t.exports = function(t, i) {
                    return {
                        value: i,
                        done: !!t
                    }
                }
            }, function(t, i, e) {
                "use strict";
                var r, n, o, s, a = e(11),
                    h = e(0),
                    _ = e(15),
                    u = e(44),
                    l = e(6),
                    c = e(8),
                    p = e(16),
                    f = e(66),
                    $ = e(67),
                    d = e(46),
                    g = e(47).set,
                    y = e(71)(),
                    m = e(32),
                    v = e(48),
                    S = e(72),
                    T = e(49),
                    P = h.TypeError,
                    L = h.process,
                    M = L && L.versions,
                    E = M && M.v8 || "",
                    w = h.Promise,
                    x = "process" == u(L),
                    O = function() {},
                    A = n = m.f,
                    I = !! function() {
                        try {
                            var t = w.resolve(1),
                                i = (t.constructor = {})[e(2)("species")] = function(t) {
                                    t(O, O)
                                };
                            return (x || "function" == typeof PromiseRejectionEvent) && t.then(O) instanceof i && 0 !== E.indexOf("6.6") && -1 === S.indexOf("Chrome/66")
                        } catch (t) {}
                    }(),
                    b = function(t) {
                        var i;
                        return !(!c(t) || "function" != typeof(i = t.then)) && i
                    },
                    C = function(t, i) {
                        if (!t._n) {
                            t._n = !0;
                            var e = t._c;
                            y((function() {
                                for (var r = t._v, n = 1 == t._s, o = 0; e.length > o;) ! function(i) {
                                    var e, o, s, a = n ? i.ok : i.fail,
                                        h = i.resolve,
                                        _ = i.reject,
                                        u = i.domain;
                                    try {
                                        a ? (n || (2 == t._h && F(t), t._h = 1), !0 === a ? e = r : (u && u.enter(), e = a(r), u && (u.exit(), s = !0)), e === i.promise ? _(P("Promise-chain cycle")) : (o = b(e)) ? o.call(e, h, _) : h(e)) : _(r)
                                    } catch (t) {
                                        u && !s && u.exit(), _(t)
                                    }
                                }(e[o++]);
                                t._c = [], t._n = !1, i && !t._h && D(t)
                            }))
                        }
                    },
                    D = function(t) {
                        g.call(h, (function() {
                            var i, e, r, n = t._v,
                                o = R(t);
                            if (o && (i = v((function() {
                                    x ? L.emit("unhandledRejection", n, t) : (e = h.onunhandledrejection) ? e({
                                        promise: t,
                                        reason: n
                                    }) : (r = h.console) && r.error && r.error("Unhandled promise rejection", n)
                                })), t._h = x || R(t) ? 2 : 1), t._a = void 0, o && i.e) throw i.v
                        }))
                    },
                    R = function(t) {
                        return 1 !== t._h && 0 === (t._a || t._c).length
                    },
                    F = function(t) {
                        g.call(h, (function() {
                            var i;
                            x ? L.emit("rejectionHandled", t) : (i = h.onrejectionhandled) && i({
                                promise: t,
                                reason: t._v
                            })
                        }))
                    },
                    N = function(t) {
                        var i = this;
                        i._d || (i._d = !0, (i = i._w || i)._v = t, i._s = 2, i._a || (i._a = i._c.slice()), C(i, !0))
                    },
                    B = function(t) {
                        var i, e = this;
                        if (!e._d) {
                            e._d = !0, e = e._w || e;
                            try {
                                if (e === t) throw P("Promise can't be resolved itself");
                                (i = b(t)) ? y((function() {
                                    var r = {
                                        _w: e,
                                        _d: !1
                                    };
                                    try {
                                        i.call(t, _(B, r, 1), _(N, r, 1))
                                    } catch (t) {
                                        N.call(r, t)
                                    }
                                })): (e._v = t, e._s = 1, C(e, !1))
                            } catch (t) {
                                N.call({
                                    _w: e,
                                    _d: !1
                                }, t)
                            }
                        }
                    };
                I || (w = function(t) {
                    f(this, w, "Promise", "_h"), p(t), r.call(this);
                    try {
                        t(_(B, this, 1), _(N, this, 1))
                    } catch (t) {
                        N.call(this, t)
                    }
                }, (r = function(t) {
                    this._c = [], this._a = void 0, this._s = 0, this._d = !1, this._v = void 0, this._h = 0, this._n = !1
                }).prototype = e(73)(w.prototype, {
                    then: function(t, i) {
                        var e = A(d(this, w));
                        return e.ok = "function" != typeof t || t, e.fail = "function" == typeof i && i, e.domain = x ? L.domain : void 0, this._c.push(e), this._a && this._a.push(e), this._s && C(this, !1), e.promise
                    },
                    catch: function(t) {
                        return this.then(void 0, t)
                    }
                }), o = function() {
                    var t = new r;
                    this.promise = t, this.resolve = _(B, t, 1), this.reject = _(N, t, 1)
                }, m.f = A = function(t) {
                    return t === w || t === s ? new o(t) : n(t)
                }), l(l.G + l.W + l.F * !I, {
                    Promise: w
                }), e(20)(w, "Promise"), e(74)("Promise"), s = e(1).Promise, l(l.S + l.F * !I, "Promise", {
                    reject: function(t) {
                        var i = A(this);
                        return (0, i.reject)(t), i.promise
                    }
                }), l(l.S + l.F * (a || !I), "Promise", {
                    resolve: function(t) {
                        return T(a && this === s ? w : this, t)
                    }
                }), l(l.S + l.F * !(I && e(75)((function(t) {
                    w.all(t).catch(O)
                }))), "Promise", {
                    all: function(t) {
                        var i = this,
                            e = A(i),
                            r = e.resolve,
                            n = e.reject,
                            o = v((function() {
                                var e = [],
                                    o = 0,
                                    s = 1;
                                $(t, !1, (function(t) {
                                    var a = o++,
                                        h = !1;
                                    e.push(void 0), s++, i.resolve(t).then((function(t) {
                                        h || (h = !0, e[a] = t, --s || r(e))
                                    }), n)
                                })), --s || r(e)
                            }));
                        return o.e && n(o.v), e.promise
                    },
                    race: function(t) {
                        var i = this,
                            e = A(i),
                            r = e.reject,
                            n = v((function() {
                                $(t, !1, (function(t) {
                                    i.resolve(t).then(e.resolve, r)
                                }))
                            }));
                        return n.e && r(n.v), e.promise
                    }
                })
            }, function(t, i) {
                t.exports = function(t, i, e, r) {
                    if (!(t instanceof i) || void 0 !== r && r in t) throw TypeError(e + ": incorrect invocation!");
                    return t
                }
            }, function(t, i, e) {
                var r = e(15),
                    n = e(68),
                    o = e(69),
                    s = e(3),
                    a = e(42),
                    h = e(45),
                    _ = {},
                    u = {};
                i = t.exports = function(t, i, e, l, c) {
                    var p, f, $, d, g = c ? function() {
                            return t
                        } : h(t),
                        y = r(e, l, i ? 2 : 1),
                        m = 0;
                    if ("function" != typeof g) throw TypeError(t + " is not iterable!");
                    if (o(g)) {
                        for (p = a(t.length); p > m; m++)
                            if ((d = i ? y(s(f = t[m])[0], f[1]) : y(t[m])) === _ || d === u) return d
                    } else
                        for ($ = g.call(t); !(f = $.next()).done;)
                            if ((d = n($, y, f.value, i)) === _ || d === u) return d
                }, i.BREAK = _, i.RETURN = u
            }, function(t, i, e) {
                var r = e(3);
                t.exports = function(t, i, e, n) {
                    try {
                        return n ? i(r(e)[0], e[1]) : i(e)
                    } catch (i) {
                        var o = t.return;
                        throw void 0 !== o && r(o.call(t)), i
                    }
                }
            }, function(t, i, e) {
                var r = e(13),
                    n = e(2)("iterator"),
                    o = Array.prototype;
                t.exports = function(t) {
                    return void 0 !== t && (r.Array === t || o[n] === t)
                }
            }, function(t, i) {
                t.exports = function(t, i, e) {
                    var r = void 0 === e;
                    switch (i.length) {
                        case 0:
                            return r ? t() : t.call(e);
                        case 1:
                            return r ? t(i[0]) : t.call(e, i[0]);
                        case 2:
                            return r ? t(i[0], i[1]) : t.call(e, i[0], i[1]);
                        case 3:
                            return r ? t(i[0], i[1], i[2]) : t.call(e, i[0], i[1], i[2]);
                        case 4:
                            return r ? t(i[0], i[1], i[2], i[3]) : t.call(e, i[0], i[1], i[2], i[3])
                    }
                    return t.apply(e, i)
                }
            }, function(t, i, e) {
                var r = e(0),
                    n = e(47).set,
                    o = r.MutationObserver || r.WebKitMutationObserver,
                    s = r.process,
                    a = r.Promise,
                    h = "process" == e(14)(s);
                t.exports = function() {
                    var t, i, e, _ = function() {
                        var r, n;
                        for (h && (r = s.domain) && r.exit(); t;) {
                            n = t.fn, t = t.next;
                            try {
                                n()
                            } catch (r) {
                                throw t ? e() : i = void 0, r
                            }
                        }
                        i = void 0, r && r.enter()
                    };
                    if (h) e = function() {
                        s.nextTick(_)
                    };
                    else if (!o || r.navigator && r.navigator.standalone)
                        if (a && a.resolve) {
                            var u = a.resolve(void 0);
                            e = function() {
                                u.then(_)
                            }
                        } else e = function() {
                            n.call(r, _)
                        };
                    else {
                        var l = !0,
                            c = document.createTextNode("");
                        new o(_).observe(c, {
                            characterData: !0
                        }), e = function() {
                            c.data = l = !l
                        }
                    }
                    return function(r) {
                        var n = {
                            fn: r,
                            next: void 0
                        };
                        i && (i.next = n), t || (t = n, e()), i = n
                    }
                }
            }, function(t, i, e) {
                var r = e(0).navigator;
                t.exports = r && r.userAgent || ""
            }, function(t, i, e) {
                var r = e(7);
                t.exports = function(t, i, e) {
                    for (var n in i) e && t[n] ? t[n] = i[n] : r(t, n, i[n]);
                    return t
                }
            }, function(t, i, e) {
                "use strict";
                var r = e(0),
                    n = e(1),
                    o = e(4),
                    s = e(5),
                    a = e(2)("species");
                t.exports = function(t) {
                    var i = "function" == typeof n[t] ? n[t] : r[t];
                    s && i && !i[a] && o.f(i, a, {
                        configurable: !0,
                        get: function() {
                            return this
                        }
                    })
                }
            }, function(t, i, e) {
                var r = e(2)("iterator"),
                    n = !1;
                try {
                    var o = [7][r]();
                    o.return = function() {
                        n = !0
                    }, Array.from(o, (function() {
                        throw 2
                    }))
                } catch (t) {}
                t.exports = function(t, i) {
                    if (!i && !n) return !1;
                    var e = !1;
                    try {
                        var o = [7],
                            s = o[r]();
                        s.next = function() {
                            return {
                                done: e = !0
                            }
                        }, o[r] = function() {
                            return s
                        }, t(o)
                    } catch (t) {}
                    return e
                }
            }, function(t, i, e) {
                "use strict";
                var r = e(6),
                    n = e(1),
                    o = e(0),
                    s = e(46),
                    a = e(49);
                r(r.P + r.R, "Promise", {
                    finally: function(t) {
                        var i = s(this, n.Promise || o.Promise),
                            e = "function" == typeof t;
                        return this.then(e ? function(e) {
                            return a(i, t()).then((function() {
                                return e
                            }))
                        } : t, e ? function(e) {
                            return a(i, t()).then((function() {
                                throw e
                            }))
                        } : t)
                    }
                })
            }, function(t, i, e) {
                "use strict";
                var r = e(6),
                    n = e(32),
                    o = e(48);
                r(r.S, "Promise", {
                    try: function(t) {
                        var i = n.f(this),
                            e = o(t);
                        return (e.e ? i.reject : i.resolve)(e.v), i.promise
                    }
                })
            }, function(t, i, e) {
                "use strict";

                function r(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    }
                }
                i.__esModule = !0;
                var n = r(e(79)),
                    o = r(e(81)),
                    s = "function" == typeof o.default && "symbol" == typeof n.default ? function(t) {
                        return typeof t
                    } : function(t) {
                        return t && "function" == typeof o.default && t.constructor === o.default && t !== o.default.prototype ? "symbol" : typeof t
                    };
                i.default = "function" == typeof o.default && "symbol" === s(n.default) ? function(t) {
                    return void 0 === t ? "undefined" : s(t)
                } : function(t) {
                    return t && "function" == typeof o.default && t.constructor === o.default && t !== o.default.prototype ? "symbol" : void 0 === t ? "undefined" : s(t)
                }
            }, function(t, i, e) {
                t.exports = {
                    default: e(80),
                    __esModule: !0
                }
            }, function(t, i, e) {
                e(22), e(31), t.exports = e(33).f("iterator")
            }, function(t, i, e) {
                t.exports = {
                    default: e(82),
                    __esModule: !0
                }
            }, function(t, i, e) {
                e(83), e(36), e(89), e(90), t.exports = e(1).Symbol
            }, function(t, i, e) {
                "use strict";
                var r = e(0),
                    n = e(9),
                    o = e(5),
                    s = e(6),
                    a = e(39),
                    h = e(84).KEY,
                    _ = e(12),
                    u = e(28),
                    l = e(20),
                    c = e(19),
                    p = e(2),
                    f = e(33),
                    $ = e(34),
                    d = e(85),
                    g = e(86),
                    y = e(3),
                    m = e(8),
                    v = e(30),
                    S = e(10),
                    T = e(26),
                    P = e(17),
                    L = e(40),
                    M = e(87),
                    E = e(88),
                    w = e(50),
                    x = e(4),
                    O = e(18),
                    A = E.f,
                    I = x.f,
                    b = M.f,
                    C = r.Symbol,
                    D = r.JSON,
                    R = D && D.stringify,
                    F = p("_hidden"),
                    N = p("toPrimitive"),
                    B = {}.propertyIsEnumerable,
                    G = u("symbol-registry"),
                    k = u("symbols"),
                    U = u("op-symbols"),
                    Y = Object.prototype,
                    V = "function" == typeof C && !!w.f,
                    j = r.QObject,
                    X = !j || !j.prototype || !j.prototype.findChild,
                    H = o && _((function() {
                        return 7 != L(I({}, "a", {
                            get: function() {
                                return I(this, "a", {
                                    value: 7
                                }).a
                            }
                        })).a
                    })) ? function(t, i, e) {
                        var r = A(Y, i);
                        r && delete Y[i], I(t, i, e), r && t !== Y && I(Y, i, r)
                    } : I,
                    z = function(t) {
                        var i = k[t] = L(C.prototype);
                        return i._k = t, i
                    },
                    W = V && "symbol" == typeof C.iterator ? function(t) {
                        return "symbol" == typeof t
                    } : function(t) {
                        return t instanceof C
                    },
                    q = function(t, i, e) {
                        return t === Y && q(U, i, e), y(t), i = T(i, !0), y(e), n(k, i) ? (e.enumerable ? (n(t, F) && t[F][i] && (t[F][i] = !1), e = L(e, {
                            enumerable: P(0, !1)
                        })) : (n(t, F) || I(t, F, P(1, {})), t[F][i] = !0), H(t, i, e)) : I(t, i, e)
                    },
                    Z = function(t, i) {
                        y(t);
                        for (var e, r = d(i = S(i)), n = 0, o = r.length; o > n;) q(t, e = r[n++], i[e]);
                        return t
                    },
                    J = function(t, i) {
                        return void 0 === i ? L(t) : Z(L(t), i)
                    },
                    Q = function(t) {
                        var i = B.call(this, t = T(t, !0));
                        return !(this === Y && n(k, t) && !n(U, t)) && (!(i || !n(this, t) || !n(k, t) || n(this, F) && this[F][t]) || i)
                    },
                    K = function(t, i) {
                        if (t = S(t), i = T(i, !0), t !== Y || !n(k, i) || n(U, i)) {
                            var e = A(t, i);
                            return !e || !n(k, i) || n(t, F) && t[F][i] || (e.enumerable = !0), e
                        }
                    },
                    tt = function(t) {
                        for (var i, e = b(S(t)), r = [], o = 0; e.length > o;) n(k, i = e[o++]) || i == F || i == h || r.push(i);
                        return r
                    },
                    it = function(t) {
                        for (var i, e = t === Y, r = b(e ? U : S(t)), o = [], s = 0; r.length > s;) !n(k, i = r[s++]) || e && !n(Y, i) || o.push(k[i]);
                        return o
                    };
                V || (C = function() {
                    if (this instanceof C) throw TypeError("Symbol is not a constructor!");
                    var t = c(arguments.length > 0 ? arguments[0] : void 0),
                        i = function(e) {
                            this === Y && i.call(U, e), n(this, F) && n(this[F], t) && (this[F][t] = !1), H(this, t, P(1, e))
                        };
                    return o && X && H(Y, t, {
                        configurable: !0,
                        set: i
                    }), z(t)
                }, a(C.prototype, "toString", (function() {
                    return this._k
                })), E.f = K, x.f = q, e(51).f = M.f = tt, e(35).f = Q, w.f = it, o && !e(11) && a(Y, "propertyIsEnumerable", Q, !0), f.f = function(t) {
                    return z(p(t))
                }), s(s.G + s.W + s.F * !V, {
                    Symbol: C
                });
                for (var et = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), rt = 0; et.length > rt;) p(et[rt++]);
                for (var nt = O(p.store), ot = 0; nt.length > ot;) $(nt[ot++]);
                s(s.S + s.F * !V, "Symbol", {
                    for: function(t) {
                        return n(G, t += "") ? G[t] : G[t] = C(t)
                    },
                    keyFor: function(t) {
                        if (!W(t)) throw TypeError(t + " is not a symbol!");
                        for (var i in G)
                            if (G[i] === t) return i
                    },
                    useSetter: function() {
                        X = !0
                    },
                    useSimple: function() {
                        X = !1
                    }
                }), s(s.S + s.F * !V, "Object", {
                    create: J,
                    defineProperty: q,
                    defineProperties: Z,
                    getOwnPropertyDescriptor: K,
                    getOwnPropertyNames: tt,
                    getOwnPropertySymbols: it
                });
                var st = _((function() {
                    w.f(1)
                }));
                s(s.S + s.F * st, "Object", {
                    getOwnPropertySymbols: function(t) {
                        return w.f(v(t))
                    }
                }), D && s(s.S + s.F * (!V || _((function() {
                    var t = C();
                    return "[null]" != R([t]) || "{}" != R({
                        a: t
                    }) || "{}" != R(Object(t))
                }))), "JSON", {
                    stringify: function(t) {
                        for (var i, e, r = [t], n = 1; arguments.length > n;) r.push(arguments[n++]);
                        if (e = i = r[1], (m(i) || void 0 !== t) && !W(t)) return g(i) || (i = function(t, i) {
                            if ("function" == typeof e && (i = e.call(this, t, i)), !W(i)) return i
                        }), r[1] = i, R.apply(D, r)
                    }
                }), C.prototype[N] || e(7)(C.prototype, N, C.prototype.valueOf), l(C, "Symbol"), l(Math, "Math", !0), l(r.JSON, "JSON", !0)
            }, function(t, i, e) {
                var r = e(19)("meta"),
                    n = e(8),
                    o = e(9),
                    s = e(4).f,
                    a = 0,
                    h = Object.isExtensible || function() {
                        return !0
                    },
                    _ = !e(12)((function() {
                        return h(Object.preventExtensions({}))
                    })),
                    u = function(t) {
                        s(t, r, {
                            value: {
                                i: "O" + ++a,
                                w: {}
                            }
                        })
                    },
                    l = function(t, i) {
                        if (!n(t)) return "symbol" == typeof t ? t : ("string" == typeof t ? "S" : "P") + t;
                        if (!o(t, r)) {
                            if (!h(t)) return "F";
                            if (!i) return "E";
                            u(t)
                        }
                        return t[r].i
                    },
                    c = function(t, i) {
                        if (!o(t, r)) {
                            if (!h(t)) return !0;
                            if (!i) return !1;
                            u(t)
                        }
                        return t[r].w
                    },
                    p = function(t) {
                        return _ && f.NEED && h(t) && !o(t, r) && u(t), t
                    },
                    f = t.exports = {
                        KEY: r,
                        NEED: !1,
                        fastKey: l,
                        getWeak: c,
                        onFreeze: p
                    }
            }, function(t, i, e) {
                var r = e(18),
                    n = e(50),
                    o = e(35);
                t.exports = function(t) {
                    var i = r(t),
                        e = n.f;
                    if (e)
                        for (var s, a = e(t), h = o.f, _ = 0; a.length > _;) h.call(t, s = a[_++]) && i.push(s);
                    return i
                }
            }, function(t, i, e) {
                var r = e(14);
                t.exports = Array.isArray || function(t) {
                    return "Array" == r(t)
                }
            }, function(t, i, e) {
                var r = e(10),
                    n = e(51).f,
                    o = {}.toString,
                    s = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [],
                    a = function(t) {
                        try {
                            return n(t)
                        } catch (t) {
                            return s.slice()
                        }
                    };
                t.exports.f = function(t) {
                    return s && "[object Window]" == o.call(t) ? a(t) : n(r(t))
                }
            }, function(t, i, e) {
                var r = e(35),
                    n = e(17),
                    o = e(10),
                    s = e(26),
                    a = e(9),
                    h = e(38),
                    _ = Object.getOwnPropertyDescriptor;
                i.f = e(5) ? _ : function(t, i) {
                    if (t = o(t), i = s(i, !0), h) try {
                        return _(t, i)
                    } catch (t) {}
                    if (a(t, i)) return n(!r.f.call(t, i), t[i])
                }
            }, function(t, i, e) {
                e(34)("asyncIterator")
            }, function(t, i, e) {
                e(34)("observable")
            }, function(t, i, e) {
                "use strict";
                i.__esModule = !0, i.default = function(t, i) {
                    if (!(t instanceof i)) throw new TypeError("Cannot call a class as a function")
                }
            }, function(t, i, e) {
                "use strict";
                i.__esModule = !0;
                var r = function(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    }
                }(e(93));
                i.default = function() {
                    function t(t, i) {
                        for (var e = 0; e < i.length; e++) {
                            var n = i[e];
                            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), (0, r.default)(t, n.key, n)
                        }
                    }
                    return function(i, e, r) {
                        return e && t(i.prototype, e), r && t(i, r), i
                    }
                }()
            }, function(t, i, e) {
                t.exports = {
                    default: e(94),
                    __esModule: !0
                }
            }, function(t, i, e) {
                e(95);
                var r = e(1).Object;
                t.exports = function(t, i, e) {
                    return r.defineProperty(t, i, e)
                }
            }, function(t, i, e) {
                var r = e(6);
                r(r.S + r.F * !e(5), "Object", {
                    defineProperty: e(4).f
                })
            }, function(t, i, e) {
                t.exports = {
                    default: e(97),
                    __esModule: !0
                }
            }, function(t, i, e) {
                e(31), e(22), t.exports = e(98)
            }, function(t, i, e) {
                var r = e(3),
                    n = e(45);
                t.exports = e(1).getIterator = function(t) {
                    var i = n(t);
                    if ("function" != typeof i) throw TypeError(t + " is not iterable!");
                    return r(i.call(t))
                }
            }, function(t, i, e) {
                e(100), t.exports = e(1).Object.keys
            }, function(t, i, e) {
                var r = e(30),
                    n = e(18);
                e(101)("keys", (function() {
                    return function(t) {
                        return n(r(t))
                    }
                }))
            }, function(t, i, e) {
                var r = e(6),
                    n = e(1),
                    o = e(12);
                t.exports = function(t, i) {
                    var e = (n.Object || {})[t] || Object[t],
                        s = {};
                    s[t] = i(e), r(r.S + r.F * o((function() {
                        e(1)
                    })), "Object", s)
                }
            }])
        },
        7430: function(t, i, e) {
            "use strict";
            e.r(i), e.d(i, {
                LiveHaruna: function() {
                    return W
                }
            });
            var r = e(26849),
                n = e(66570),
                o = e(63109),
                s = e.n(o),
                a = e(34074),
                h = e.n(a),
                _ = e(93476),
                u = e.n(_),
                l = e(70538),
                c = e(95334),
                p = e(55716),
                f = e(2708),
                $ = e(96041),
                d = e(57072);

            function g(t) {
                return {
                    DEBUG_LOG: !1,
                    DEBUG_MOUSE_LOG: !1,
                    DEBUG_DRAW_HIT_AREA: !1,
                    DEBUG_DRAW_ALPHA_MODEL: !1,
                    NO_CACHE: !1,
                    ALLOW_CORS: !0,
                    CORS_RULES: function(t) {
                        return /bilibili\.com/.test(t)
                    },
                    NO_TAP_AREA_DETECTION: !0,
                    FOLLOW_CURSOR: !0,
                    NO_SCALING: !0,
                    VIEW_MAX_SCALE: 2,
                    VIEW_MIN_SCALE: .8,
                    VIEW_LOGICAL_LEFT: -1,
                    VIEW_LOGICAL_RIGHT: 1,
                    VIEW_LOGICAL_MAX_LEFT: -2,
                    VIEW_LOGICAL_MAX_RIGHT: 2,
                    VIEW_LOGICAL_MAX_BOTTOM: -2,
                    VIEW_LOGICAL_MAX_TOP: 2,
                    PRIORITY_NONE: 0,
                    PRIORITY_IDLE: 1,
                    PRIORITY_NORMAL: 2,
                    PRIORITY_FORCE: 3,
                    MODELS: ["//api.live.bilibili.com/live/getRoomKanBanModel?roomid=" + t],
                    MOTION_GROUP_IDLE: "idle",
                    MOTION_GROUP_TAP_BODY: "tap_body",
                    MOTION_GROUP_FLICK_HEAD: "flick_head",
                    MOTION_GROUP_PINCH_IN: "pinch_in",
                    MOTION_GROUP_PINCH_OUT: "pinch_out",
                    MOTION_GROUP_SHAKE: "shake",
                    HIT_AREA_HEAD: "head",
                    HIT_AREA_BODY: "body"
                }
            }
            var y = [.8 * window.innerWidth - window.innerWidth / 2, 0],
                m = e(58898),
                v = function() {
                    function t() {}
                    return t.init = function(t) {
                        var i = t.$store.getters.baseInfoRoom.roomID,
                            e = t.$refs.harunaCanvas;
                        if ((0, f.getGLContext)(e)) {
                            var r = new $.Live2DApp({
                                canvas: e,
                                appConfig: g(i)
                            });
                            r.onload((function(i) {
                                var e = i.label;
                                t.modelLabel = e
                            })), t.harunaApp = r
                        } else t.isSupported = !1
                    }, t.initEvents = function(t) {
                        d.default.$on("Haruna: StartMotionGroup", (function(i) {
                            t.harunaApp.startMotionGroup(i)
                        })), d.default.$on("Haruna: Speak", (function(t) {
                            m.C.shutup(), m.C.modules.speaking.speak(t)
                        })), d.default.$on("Haruna: Shutup", (function() {
                            m.C.shutup()
                        }))
                    }, t
                }();
            v.startPerformanceDoctor = function() {
                var t = 64,
                    i = !1,
                    e = 0,
                    r = null,
                    n = window.requestAnimationFrame || window.webkitRequestAnimationFrame;
                return function(t) {
                    r || (r = t), n && window.performance && !i && (i = !0, n(o))
                };

                function o() {
                    if (i) {
                        var s = performance.now();
                        e || (e = s), s - e > 62.5 && r && --t <= 0 && (r.goEcoMode(), t = 64, i = !1), e = s, n(o)
                    }
                }
            }();
            var S = e(2991),
                T = e.n(S),
                P = function() {
                    function t() {}
                    return t.getLastestPosition = function() {
                        var t;
                        if (!window.localStorage) return y;
                        var i = localStorage.getItem("LiveHaruna:Position");
                        return i ? T()(t = i.split(",")).call(t, (function(t) {
                            return parseInt(t)
                        })) : y
                    }, t.setLatestPositionToLocalStorage = function(t, i) {
                        void 0 === t && (t = 0), void 0 === i && (i = 0), window.localStorage && localStorage.setItem("LiveHaruna:Position", t + "," + i)
                    }, t.getIsMinimizedToLocalStorage = function() {
                        return !!window.localStorage && Boolean(localStorage.getItem("LiveHaruna:isMinimized"))
                    }, t.setIsMinimizedToLocalStorage = function(t) {
                        window.localStorage && localStorage.setItem("LiveHaruna:isMinimized", t ? "true" : "")
                    }, t
                }(),
                L = function(t, i, e, r) {
                    var n, o = arguments.length,
                        s = o < 3 ? i : null === r ? r = h()(i, e) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, i, e, r);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(n = t[a]) && (s = (o < 3 ? n(s) : o > 3 ? n(i, e, s) : n(i, e)) || s);
                    return o > 3 && s && Object.defineProperty(i, e, s), s
                },
                M = function(t) {
                    function i() {
                        var i;
                        return (i = t.apply(this, arguments) || this).label = "22", i
                    }
                    return (0, n.Z)(i, t), i
                }(l.default);
            L([(0, c.Prop)({
                type: String,
                default: "22"
            })], M.prototype, "label", void 0);
            var E = M = L([c.Component], M),
                w = e(51900),
                x = (0, w.Z)(E, (function() {
                    var t = this,
                        i = t.$createElement;
                    return (t._self._c || i)("div", {
                        staticClass: "avatar-btn pointer",
                        class: "model-" + t.label,
                        attrs: {
                            role: "button"
                        }
                    })
                }), [], !1, null, "cee07930", null).exports,
                O = function(t, i, e, r) {
                    var n, o = arguments.length,
                        s = o < 3 ? i : null === r ? r = h()(i, e) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, i, e, r);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(n = t[a]) && (s = (o < 3 ? n(s) : o > 3 ? n(i, e, s) : n(i, e)) || s);
                    return o > 3 && s && Object.defineProperty(i, e, s), s
                },
                A = function(t) {
                    function i() {
                        return t.apply(this, arguments) || this
                    }
                    return (0, n.Z)(i, t), i
                }(l.default);
            O([(0, c.Prop)({
                type: Boolean,
                default: !1
            })], A.prototype, "show", void 0);
            var I = A = O([c.Component], A),
                b = (0, w.Z)(I, (function() {
                    var t = this,
                        i = t.$createElement,
                        e = t._self._c || i;
                    return e("transition", {
                        attrs: {
                            "leave-active-class": "a-scale-out"
                        }
                    }, [e("div", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: t.show,
                            expression: "show"
                        }],
                        staticClass: "base-bubble a-move-in-left"
                    }, [e("div", {
                        staticClass: "content-ctnr p-relative"
                    }, [t._t("default")], 2)])])
                }), [], !1, null, "c0d9dea8", null).exports,
                C = e(46633),
                D = function(t, i, e, r) {
                    var n, o = arguments.length,
                        s = o < 3 ? i : null === r ? r = h()(i, e) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, i, e, r);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(n = t[a]) && (s = (o < 3 ? n(s) : o > 3 ? n(i, e, s) : n(i, e)) || s);
                    return o > 3 && s && Object.defineProperty(i, e, s), s
                },
                R = function(t) {
                    function i() {
                        var i;
                        return (i = t.apply(this, arguments) || this).ControlSwitchService = C.m, i
                    }
                    return (0, n.Z)(i, t), (0, r.Z)(i, [{
                        key: "isShowHaruna",
                        get: function() {
                            return C.m.switchInfo.isShowHaruna
                        }
                    }, {
                        key: "content",
                        get: function() {
                            return m.C.modules.speaking.content
                        }
                    }, {
                        key: "show",
                        get: function() {
                            return m.C.modules.speaking.showing && this.isShowHaruna
                        }
                    }, {
                        key: "bubbleWidth",
                        get: function() {
                            return m.C.modules.speaking.bubbleWidth
                        }
                    }]), i
                }(l.default),
                F = R = D([(0, c.Component)({
                    components: {
                        BaseBubble: b
                    }
                })], R),
                N = (0, w.Z)(F, (function() {
                    var t = this,
                        i = t.$createElement,
                        e = t._self._c || i;
                    return e("base-bubble", {
                        staticClass: "speaking-bubble t-nowrap",
                        style: {
                            width: !!t.bubbleWidth && t.bubbleWidth + "px"
                        },
                        attrs: {
                            show: t.show
                        }
                    }, [e("div", {
                        staticClass: "content a-move-in-left",
                        domProps: {
                            textContent: t._s(t.content)
                        }
                    })])
                }), [], !1, null, "73b342bb", null).exports,
                B = function(t, i, e, r) {
                    var n, o = arguments.length,
                        s = o < 3 ? i : null === r ? r = h()(i, e) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, i, e, r);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(n = t[a]) && (s = (o < 3 ? n(s) : o > 3 ? n(i, e, s) : n(i, e)) || s);
                    return o > 3 && s && Object.defineProperty(i, e, s), s
                },
                G = function(t) {
                    function i() {
                        return t.apply(this, arguments) || this
                    }
                    return (0, n.Z)(i, t), i
                }(l.default),
                k = G = B([(0, c.Component)({
                    components: {
                        SpeakingBubble: N
                    }
                })], G),
                U = (0, w.Z)(k, (function() {
                    var t = this.$createElement,
                        i = this._self._c || t;
                    return i("div", {
                        staticClass: "speaking-bubbles"
                    }, [i("speaking-bubble", {
                        staticClass: "bubble-item"
                    })], 1)
                }), [], !1, null, "c870c03e", null).exports,
                Y = function(t, i, e, r) {
                    var n, o = arguments.length,
                        s = o < 3 ? i : null === r ? r = h()(i, e) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, i, e, r);
                    else
                        for (var a = t.length - 1; a >= 0; a--)(n = t[a]) && (s = (o < 3 ? n(s) : o > 3 ? n(i, e, s) : n(i, e)) || s);
                    return o > 3 && s && Object.defineProperty(i, e, s), s
                },
                V = function(t, i, e, r) {
                    return new(e || (e = u()))((function(n, o) {
                        function s(t) {
                            try {
                                h(r.next(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function a(t) {
                            try {
                                h(r.throw(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function h(t) {
                            var i;
                            t.done ? n(t.value) : (i = t.value, i instanceof e ? i : new e((function(t) {
                                t(i)
                            }))).then(s, a)
                        }
                        h((r = r.apply(t, i || [])).next())
                    }))
                },
                j = function(t) {
                    function i() {
                        var i;
                        return (i = t.apply(this, arguments) || this).isLiteVersion = (0, f.getLiteVersionStatus)(), i.inited = !1, i.isSupported = !0, i.isMsBrowsers = f.isMsBrowsers, i.ControlSwitchService = C.m, i.x = 0, i.y = 0, i.inMoving = !1, i.zoom = 1, i.modelLabel = "22", i.minimized = (0, f.getLiteVersionStatus)() || !i.isShowHaruna, i.harunaApp = null, i.clickingTimer = null, i.clickingCount = 0, i
                    }(0, n.Z)(i, t);
                    var e = i.prototype;
                    return e.onWatchMinimized = function(t) {
                        t ? this.harunaApp.stop() : (m.C.modules.speaking.speak(p.randomEmoji.happy()), this.harunaApp.start())
                    }, e.onShowHarunaStatusChange = function(t) {
                        t || (this.minimized = !0)
                    }, e.toggleDisplayMode = function(t) {
                        this.minimized = "boolean" == typeof t ? t : !this.minimized, P.setIsMinimizedToLocalStorage(this.minimized)
                    }, e.saveHarunaPosition = function() {
                        this.minimized || P.setLatestPositionToLocalStorage(this.x, this.y)
                    }, e.loadHarunaPosition = function() {
                        var t = P.getLastestPosition(),
                            i = t[0],
                            e = t[1];
                        this.x = i, this.y = e
                    }, e.registerMouseEvents = function() {
                        var t = this,
                            i = [this.$el, this.$refs.harunaCanvas],
                            e = 0,
                            r = 0;
                        window.addEventListener("mousedown", (function(n) {
                            var o = n.target;
                            i.indexOf(o) < 0 || t.minimized || (t.inMoving = !0, t.zoom = +window.getComputedStyle(document.documentElement).zoom || 1, e = n.clientX, r = n.clientY)
                        })), window.addEventListener("mousemove", (function(i) {
                            if (t.inMoving) {
                                var n = i.clientX - e,
                                    o = i.clientY - r;
                                t.x += n / t.zoom, t.y += o / t.zoom, e = i.clientX, r = i.clientY
                            }
                        })), window.addEventListener("mouseup", (function(i) {
                            t.inMoving && (t.saveHarunaPosition(), t.inMoving = !1)
                        }))
                    }, e.watchResizing = function() {
                        var t = this,
                            i = this.$el,
                            e = null;

                        function r() {
                            if (!t.minimized) {
                                t.inMoving = !0;
                                var e = window.innerWidth,
                                    r = window.innerHeight,
                                    n = i.getBoundingClientRect(),
                                    o = {
                                        left: t.x,
                                        top: t.y
                                    },
                                    s = e / 2 + 570,
                                    a = n.width + n.left - e - 20;
                                a > 0 && (t.x = o.left - a, t.y = o.top), n.left < 0 && (t.x = -1 * s + 20, t.y = o.top);
                                var h = n.height + n.top - r + 20;
                                h > 0 && (t.x = o.left, t.y = o.top - h), n.top < 0 && (t.x = o.left, t.y = n.height + 20), t.inMoving = !1
                            }
                        }
                        window.addEventListener("resize", (function() {
                            clearTimeout(e), r(), e = setTimeout(r, 100)
                        }))
                    }, e.harunaIsGone = function() {
                        m.C.shutup(), m.C.modules.speaking.speak("呜~~ 这个浏览器不能让看板娘自由活动呢~~ 尝试下 Chrome 吧 _(:3」∠)_")
                    }, e.swtichDisplayMode = function() {
                        this.isSupported ? this.toggleDisplayMode() : this.harunaIsGone()
                    }, e.onWatchisSupported = function(t) {
                        t || this.toggleDisplayMode(!0)
                    }, e.harunaClicking = function() {
                        var t = this,
                            i = 31917 === window.BilibiliLive.ROOMID;
                        clearTimeout(this.clickingTimer), this.clickingCount++, 5 === this.clickingCount && (m.C.shutup(), m.C.modules.speaking.speak(p.randomEmoji.helpless())), 10 === this.clickingCount && (m.C.shutup(), m.C.modules.speaking.speak(i ? X(["由于您的到来，今天的天气好转了 " + p.randomEmoji.happy(), "Apply for professor", "人呐就是都不兹道，自己就不可以预料 " + p.randomEmoji.helpless(), "天堂的下面是你们的天堂 " + p.randomEmoji.happy()]) : p.randomEmoji.happy())), 20 === this.clickingCount && (m.C.shutup(), m.C.modules.speaking.speak(X(["噫", "哧溜~", "绅士是对我的夸奖 " + p.randomEmoji.helpless()]))), 30 === this.clickingCount && (m.C.shutup(), m.C.modules.speaking.speak(i ? "I'm angry!! " + p.randomEmoji.angry() : p.randomEmoji.shock())), this.clickingTimer = setTimeout((function() {
                            t.clickingCount = 0
                        }), 1e3)
                    }, e.goEcoMode = function() {
                        var t;
                        this.minimized || (this.toggleDisplayMode(!0), m.C.modules.speaking.speak(X("22" === (t = this.modelLabel) ? ["当前页面速度小于 17 帧惹~~ " + t + " 就不打扰了哼唧 " + p.randomEmoji.helpless(), "唔~ 好像页面开始卡惹，先找个洞钻起来惹 " + p.randomEmoji.sad(), "害怕 |･_･`)", "当……当页面流畅后请一定将 " + t + " 娶出来啊 ⁄(⁄ ⁄•⁄ω⁄•⁄ ⁄)⁄"] : ["不卡的时候我再出来看看 (・_・)", "进入节能模式 (・_・)", "害怕 |・_・)"])))
                    }, e.init = function() {
                        return V(this, void 0, void 0, s().mark((function t() {
                            return s().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if ((0, f.getLiteVersionStatus)()) {
                                            t.next = 9;
                                            break
                                        }
                                        return this.minimized = P.getIsMinimizedToLocalStorage(), this.loadHarunaPosition(), this.registerMouseEvents(), this.watchResizing(), t.next = 7, v.init(this);
                                    case 7:
                                        v.initEvents(this), v.startPerformanceDoctor(this);
                                    case 9:
                                        this.inited = !0;
                                    case 10:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, e.mounted = function() {
                        return V(this, void 0, void 0, s().mark((function t() {
                            return s().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this.init();
                                    case 2:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, (0, r.Z)(i, [{
                        key: "isShowHaruna",
                        get: function() {
                            return this.ControlSwitchService.switchInfo.isShowHaruna
                        }
                    }]), i
                }(l.default);

            function X(t) {
                return t[Math.floor(Math.random() * t.length)]
            }
            Y([(0, c.Watch)("minimized")], j.prototype, "onWatchMinimized", null), Y([(0, c.Watch)("isShowHaruna")], j.prototype, "onShowHarunaStatusChange", null), Y([(0, c.Watch)("isSupported")], j.prototype, "onWatchisSupported", null);
            var H = j = Y([(0, c.Component)({
                    components: {
                        AvatarBtn: x,
                        SpeakingBubbles: U
                    }
                })], j),
                z = (0, w.Z)(H, (function() {
                    var t = this,
                        i = t.$createElement,
                        e = t._self._c || i;
                    return e("div", {
                        staticClass: "live-haruna-ctnr z-live-haruna",
                        class: {
                            docked: t.minimized, floating: !t.minimized, "ts-dot-4": !t.inMoving && !t.isMsBrowsers && t.inited, "lite-version-mode": t.isLiteVersion
                        },
                        style: {
                            transform: t.minimized ? "none" : "translate(" + t.x + "px, " + t.y + "px)"
                        }
                    }, [e("speaking-bubbles"), t.isSupported && !t.isLiteVersion ? e("div", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: !t.minimized && t.isShowHaruna,
                            expression: "!minimized && isShowHaruna"
                        }],
                        staticClass: "haruna-ctnr none-select",
                        attrs: {
                            "data-bl-report-view": JSON.stringify({
                                id: "liveroom_kanban_show"
                            })
                        },
                        on: {
                            click: t.harunaClicking
                        }
                    }, [e("canvas", {
                        ref: "harunaCanvas",
                        staticClass: "haruna-canvas",
                        attrs: {
                            width: "250",
                            height: "250"
                        }
                    }), e("button", {
                        staticClass: "minimize-btn pointer bg-contain",
                        on: {
                            click: t.swtichDisplayMode
                        }
                    })]) : t._e(), t.isLiteVersion ? t._e() : e("avatar-btn", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: t.minimized && t.isShowHaruna,
                            expression: "minimized && isShowHaruna"
                        }],
                        staticClass: "a-scale-in-ease",
                        attrs: {
                            label: t.modelLabel
                        },
                        nativeOn: {
                            click: function(i) {
                                return t.swtichDisplayMode.apply(null, arguments)
                            }
                        }
                    })], 1)
                }), [], !1, null, "ce01f0ca", null),
                W = z.exports
        }
    }
]);