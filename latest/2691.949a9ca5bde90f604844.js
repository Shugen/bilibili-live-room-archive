(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [2691], {
        62691: function(t, e, n) {
            "use strict";
            n.r(e), n.d(e, {
                default: function() {
                    return A
                }
            });
            var r = n(26849),
                a = n(66570),
                o = n(34074),
                i = n.n(o),
                s = n(70538),
                c = n(95334),
                u = n(63109),
                l = n.n(u),
                p = n(93476),
                d = n.n(p),
                f = n(55716),
                v = n(82159),
                m = n(68913),
                h = n(55826),
                C = n(2708),
                y = n(17388),
                k = n(26384),
                b = n(95290),
                g = n(88825),
                _ = n(58932),
                I = function(t, e, n, r) {
                    var a, o = arguments.length,
                        s = o < 3 ? e : null === r ? r = i()(e, n) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, e, n, r);
                    else
                        for (var c = t.length - 1; c >= 0; c--)(a = t[c]) && (s = (o < 3 ? a(s) : o > 3 ? a(e, n, s) : a(e, n)) || s);
                    return o > 3 && s && Object.defineProperty(e, n, s), s
                },
                w = function(t, e, n, r) {
                    return new(n || (n = d()))((function(a, o) {
                        function i(t) {
                            try {
                                c(r.next(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function s(t) {
                            try {
                                c(r.throw(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function c(t) {
                            var e;
                            t.done ? a(t.value) : (e = t.value, e instanceof n ? e : new n((function(t) {
                                t(e)
                            }))).then(i, s)
                        }
                        c((r = r.apply(t, e || [])).next())
                    }))
                },
                D = function(t) {
                    function e() {
                        return t.apply(this, arguments) || this
                    }(0, a.Z)(e, t);
                    var n = e.prototype;
                    return n.closePopup = function() {
                        return w(this, void 0, void 0, l().mark((function t() {
                            return l().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this.popup.close();
                                    case 2:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, n.onSubmit = function() {
                        return w(this, void 0, void 0, l().mark((function t() {
                            var e, n, r, a, o, i, s = this;
                            return l().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return e = this.$refs.confirmBtn.$el, t.next = 3, g.v.remove(this.id);
                                    case 3:
                                        if (t.t0 = t.sent, t.t0) {
                                            t.next = 6;
                                            break
                                        }
                                        t.t0 = {
                                            data: null,
                                            errorMsg: ""
                                        };
                                    case 6:
                                        n = t.t0, r = n.data, a = void 0 === r ? null : r, o = n.errorMsg, i = void 0 === o ? "" : o, a && 1 === a.status ? (this.linkMsg(e, "删除成功~ " + f.randomEmoji.happy(), "success"), (0, _.cM)(_.hi.NETWORK, {
                                            eventId: "live.super_chat.HostSubmitRemoveChat",
                                            chatId: this.id
                                        })) : this.linkMsg(e, "删除失败！" + i + " " + f.randomEmoji.sad(), "error"), setTimeout((function() {
                                            return w(s, void 0, void 0, l().mark((function t() {
                                                return l().wrap((function(t) {
                                                    for (;;) switch (t.prev = t.next) {
                                                        case 0:
                                                            return t.next = 2, this.closePopup();
                                                        case 2:
                                                            return t.abrupt("return", void t.sent);
                                                        case 3:
                                                        case "end":
                                                            return t.stop()
                                                    }
                                                }), t, this)
                                            })))
                                        }), 300);
                                    case 13:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, n.setCancelBtnType = function() {
                        var t = this;
                        (0, C.nextJob)((function() {
                            t.$refs.cancelBtn.$el.type = "button"
                        }))
                    }, n.mounted = function() {
                        return w(this, void 0, void 0, l().mark((function t() {
                            return l().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        this.setCancelBtnType();
                                    case 1:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this)
                        })))
                    }, e
                }(s.default);
            I([(0, c.Inject)("popup")], D.prototype, "popup", void 0), I([(0, c.Prop)({
                type: Number,
                default: 0
            })], D.prototype, "id", void 0);
            var x = D = I([(0, c.Component)({
                    components: {
                        LinkBtn: y.default,
                        LinkInput: k.default,
                        LinkSelector: b.Z
                    }
                })], D),
                M = n(51900),
                P = (0, M.Z)(x, (function() {
                    var t = this,
                        e = t.$createElement,
                        n = t._self._c || e;
                    return n("div", {
                        staticClass: "confirm-popup-panel"
                    }, [n("div", {
                        staticClass: "content"
                    }, [t._v("残忍删除这位粉丝的留言，删除后无法恢复")]), n("div", {
                        staticClass: "button-ctnr t-center"
                    }, [n("link-btn", {
                        ref: "confirmBtn",
                        attrs: {
                            type: "primary"
                        },
                        nativeOn: {
                            click: function(e) {
                                return t.onSubmit.apply(null, arguments)
                            }
                        }
                    }, [t._v("确认")]), n("link-btn", {
                        ref: "cancelBtn",
                        staticClass: "cancel-btn",
                        attrs: {
                            type: "ghost"
                        },
                        nativeOn: {
                            click: function(e) {
                                return t.closePopup.apply(null, arguments)
                            }
                        }
                    }, [t._v("取消")])], 1)])
                }), [], !1, null, "272ff4c0", null),
                R = P.exports;
            var S = function(t, e, n, r) {
                    var a, o = arguments.length,
                        s = o < 3 ? e : null === r ? r = i()(e, n) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, e, n, r);
                    else
                        for (var c = t.length - 1; c >= 0; c--)(a = t[c]) && (s = (o < 3 ? a(s) : o > 3 ? a(e, n, s) : a(e, n)) || s);
                    return o > 3 && s && Object.defineProperty(e, n, s), s
                },
                E = function(t, e, n, r) {
                    return new(n || (n = d()))((function(a, o) {
                        function i(t) {
                            try {
                                c(r.next(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function s(t) {
                            try {
                                c(r.throw(t))
                            } catch (t) {
                                o(t)
                            }
                        }

                        function c(t) {
                            var e;
                            t.done ? a(t.value) : (e = t.value, e instanceof n ? e : new n((function(t) {
                                t(e)
                            }))).then(i, s)
                        }
                        c((r = r.apply(t, e || [])).next())
                    }))
                },
                $ = (0, m.$S)("components/aside-area/danmaku-menu"),
                j = function(t) {
                    function e() {
                        return t.apply(this, arguments) || this
                    }(0, a.Z)(e, t);
                    var o = e.prototype;
                    return o.preventClose = function(t) {
                        t.stopPropagation()
                    }, o.closeMenu = function() {
                        this.$emit("closeMoreInfo")
                    }, o.removeMessage = function() {
                        var t;
                        t = {
                            id: this.id
                        }, (0, h.f)({
                            width: 376,
                            title: "移除超级留言",
                            component: {
                                name: "confirm-popup",
                                data: function() {
                                    return t
                                },
                                components: {
                                    ConfirmPopupPanel: R
                                },
                                template: '<confirm-popup-panel :id="id"></confirm-popup-panel>'
                            },
                            button: !1
                        }), (0, _.cM)(_.hi.NETWORK, {
                            eventId: "live.super_chat.HostOpenRemoveChatPanel",
                            userName: this.username,
                            content: this.content
                        })
                    }, o.report = function() {
                        return E(this, void 0, void 0, l().mark((function t() {
                            var e;
                            return l().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if (O()) {
                                            t.next = 2;
                                            break
                                        }
                                        return t.abrupt("return");
                                    case 2:
                                        return t.prev = 2, t.next = 5, n.e(3885).then(n.bind(n, 3885));
                                    case 5:
                                        e = t.sent, (0, e.openReportPanel)({
                                            title: "举报醒目留言",
                                            url: "/av/v1/SuperChat/report",
                                            reportUrl: "/av/v1/SuperChat/forMsgReason",
                                            uid: this.uid,
                                            username: this.username,
                                            content: this.content,
                                            ts: this.ts,
                                            token: this.token,
                                            id: this.id,
                                            sign: "",
                                            type: "superChat"
                                        }), (0, _.cM)(_.hi.NETWORK, {
                                            eventId: "live.super_chat.UserOpenReportChatPanel",
                                            userName: this.username,
                                            content: this.content
                                        }), this.closeMenu(), t.next = 15;
                                        break;
                                    case 12:
                                        t.prev = 12, t.t0 = t.catch(2), $("modules/danmaku-report 加载失败", t.t0);
                                    case 15:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this, [
                                [2, 12]
                            ])
                        })))
                    }, (0, r.Z)(e, [{
                        key: "isAnchor",
                        get: function() {
                            return this.$store.getters.baseInfoUser.isAnchor
                        }
                    }]), e
                }(s.default);

            function O() {
                try {
                    return (0, v.quickLogin)(), !0
                } catch (t) {
                    return !1
                }
            }
            S([(0, c.Prop)({
                type: String,
                default: "神秘用户 " + f.randomEmoji.happy()
            })], j.prototype, "username", void 0), S([(0, c.Prop)({
                type: Number,
                default: 0
            })], j.prototype, "uid", void 0), S([(0, c.Prop)({
                type: Number,
                default: 0
            })], j.prototype, "id", void 0), S([(0, c.Prop)({
                type: String,
                default: ""
            })], j.prototype, "content", void 0), S([(0, c.Prop)({
                type: String,
                default: ""
            })], j.prototype, "token", void 0), S([(0, c.Prop)({
                type: Number,
                default: ""
            })], j.prototype, "ts", void 0);
            var B = j = S([c.Component], j),
                N = (0, M.Z)(B, (function() {
                    var t = this,
                        e = t.$createElement,
                        n = t._self._c || e;
                    return n("div", {
                        staticClass: "danmaku-menu p-fixed ts-dot-4 a-move-in-top p-relative z-danmaku-menu",
                        on: {
                            click: t.preventClose
                        }
                    }, [n("div", {
                        staticClass: "username t-nowrap t-over-hidden"
                    }, [n("span", {
                        domProps: {
                            textContent: t._s(t.username)
                        }
                    })]), n("div", {
                        staticClass: "none-select"
                    }, [n("div", {
                        staticClass: "report-this-guy"
                    }, [n("a", {
                        staticClass: "clickable bili-link pointer",
                        on: {
                            click: t.report
                        }
                    }, [n("span", [t._v("举报醒目留言")])])]), t.isAnchor ? n("div", {
                        staticClass: "block-this-guy"
                    }, [n("a", {
                        staticClass: "clickable bili-link pointer",
                        on: {
                            click: t.removeMessage
                        }
                    }, [n("span", [t._v("移除醒目留言")])])]) : t._e()])])
                }), [], !1, null, "7c7a41c2", null).exports,
                Z = n(66856),
                T = function(t, e, n, r) {
                    var a, o = arguments.length,
                        s = o < 3 ? e : null === r ? r = i()(e, n) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(t, e, n, r);
                    else
                        for (var c = t.length - 1; c >= 0; c--)(a = t[c]) && (s = (o < 3 ? a(s) : o > 3 ? a(e, n, s) : a(e, n)) || s);
                    return o > 3 && s && Object.defineProperty(e, n, s), s
                },
                L = function(t) {
                    function e() {
                        var e;
                        return (e = t.apply(this, arguments) || this).showInfo = !1, e.currency = Z.webCurrency, e
                    }(0, a.Z)(e, t);
                    var n = e.prototype;
                    return n.showMoreInfo = function() {
                        this.showInfo = !0
                    }, n.closeMoreInfo = function() {
                        this.showInfo = !1, window.removeEventListener("click", this.closeMoreInfo, !1)
                    }, n.mounted = function() {
                        window.addEventListener("click", this.closeMoreInfo, !1)
                    }, (0, r.Z)(e, [{
                        key: "isSelf",
                        get: function() {
                            return +(this.currentCardData || {
                                uid: 0
                            }).uid === this.$store.getters.baseInfoUser.uid
                        }
                    }]), e
                }(s.default);
            T([(0, c.Prop)()], L.prototype, "currentCardData", void 0);
            var F = L = T([(0, c.Component)({
                    components: {
                        MoreInfoDialog: N
                    },
                    filters: {
                        timeFormat: function(t) {
                            var e = Math.floor(t / 3600),
                                n = Math.floor((t - 3600 * e) / 60),
                                r = Math.floor(t - 3600 * e - 60 * n);
                            return (e < 10 ? "0" + e : e) + ":" + (n < 10 ? "0" + n : n) + ":" + (r < 10 ? "0" + r : r)
                        }
                    }
                })], L),
                U = (0, M.Z)(F, (function() {
                    var t = this,
                        e = t.$createElement,
                        n = t._self._c || e;
                    return n("div", {
                        staticClass: "card-detail",
                        on: {
                            click: function(t) {
                                return t.stopPropagation(),
                                    function() {}.apply(null, arguments)
                            }
                        }
                    }, [n("div", {
                        staticClass: "card-item-middle-top",
                        style: {
                            border: "1px solid " + t.currentCardData.backgroundBottomColor,
                            backgroundImage: "url(" + t.currentCardData.backgroundImage + ")",
                            backgroundColor: t.currentCardData.backgroundColor
                        }
                    }, [n("div", {
                        staticClass: "card-item-middle-top-left"
                    }, [t.currentCardData.userInfo.face ? n("div", {
                        staticClass: "icon-face",
                        style: {
                            backgroundImage: "url(" + t.currentCardData.userInfo.face + ")"
                        }
                    }) : t._e(), t.currentCardData.userInfo.faceFrame ? n("div", {
                        staticClass: "icon-face-frame",
                        style: {
                            backgroundImage: "url(" + t.currentCardData.userInfo.faceFrame + ")"
                        }
                    }) : t._e()]), n("div", {
                        staticClass: "card-item-middle-top-right"
                    }, [n("div", {
                        staticClass: "name",
                        class: {
                            isVip: t.currentCardData.userInfo.isVip || t.currentCardData.userInfo.isSvip
                        }
                    }, [t._v(t._s(t.currentCardData.userInfo.uname))]), n("div", {
                        staticClass: "content-bottom"
                    }, [n("div", {
                        staticClass: "price"
                    }, [t._v("￥" + t._s(t.currentCardData.price)), n("span", {
                        staticClass: "exp"
                    }, [t._v("（" + t._s(t.currency.format(t.currentCardData.price * t.currentCardData.rate)) + t._s(t.currency.name) + "）")])]), t.isSelf ? n("div", {
                        staticClass: "time"
                    }, [t._v(t._s(t._f("timeFormat")(t.currentCardData.time)))]) : t._e()])])]), n("div", {
                        staticClass: "card-item-middle-bottom",
                        style: {
                            backgroundColor: t.currentCardData.backgroundBottomColor
                        }
                    }, [n("div", {
                        staticClass: "input-contain"
                    }, [n("span", {
                        staticClass: "text",
                        domProps: {
                            textContent: t._s(t.currentCardData.message)
                        }
                    })]), 1 === t.currentCardData.transMark ? n("div", {
                        staticClass: "input-trans-contain"
                    }, [n("span", {
                        staticClass: "text",
                        domProps: {
                            textContent: t._s(t.currentCardData.messageTrans)
                        }
                    })]) : t._e(), t.currentCardData.backgroundIcon ? n("div", {
                        staticClass: "bottom-background",
                        style: {
                            backgroundImage: "url(" + t.currentCardData.backgroundIcon + ")"
                        }
                    }) : t._e()]), t.isSelf ? t._e() : n("div", {
                        staticClass: "more",
                        on: {
                            click: t.showMoreInfo
                        }
                    }), n("transition", {
                        attrs: {
                            "leave-active-class": "a-scale-out"
                        }
                    }, [t.showInfo ? n("more-info-dialog", {
                        attrs: {
                            username: t.currentCardData.userInfo.uname,
                            uid: t.currentCardData.uid,
                            id: t.currentCardData.id,
                            content: t.currentCardData.message,
                            ts: t.currentCardData.ts,
                            token: t.currentCardData.token
                        },
                        on: {
                            closeMoreInfo: t.closeMoreInfo
                        }
                    }) : t._e()], 1)], 1)
                }), [], !1, null, "512f2328", null),
                A = U.exports
        }
    }
]);