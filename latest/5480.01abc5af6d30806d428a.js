(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [5480], {
        15480: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, {
                default: function() {
                    return k
                }
            });
            var r = n(66570),
                o = n(63109),
                i = n.n(o),
                a = n(34074),
                s = n.n(a),
                l = n(93476),
                c = n.n(l),
                u = n(70538),
                f = n(95334),
                p = n(27071),
                h = n.n(p),
                d = n(2762),
                v = n(55716),
                m = n(23933),
                g = {
                    1: "总督",
                    2: "提督",
                    3: "舰长",
                    0: ""
                },
                y = 16;

            function w(e, t, n, r) {
                var o = ["恭喜 ", "" + t, 0 === r ? " 成为" : " 续任"],
                    i = ["" + e, " 的" + g[n]],
                    a = function(e, t, n, r) {
                        var o = (0, d.o_)(0, 0, !1),
                            i = (o.canvas, o.ctx);
                        i.font = "normal bold 16px PingFangSC-Regular";
                        var a = i.measureText(e).width,
                            s = i.measureText(t).width,
                            l = i.measureText(n).width,
                            c = i.measureText(r).width,
                            u = !1,
                            f = !1;
                        if (l > 160) {
                            a -= l - i.measureText(n.substring(0, 9) + "...").width, u = !0
                        }
                        if (c > 160) {
                            s -= c - i.measureText(r.substring(0, 9) + "...").width, f = !0
                        }
                        return {
                            firstLine: a,
                            secondLine: s,
                            clientLength: l,
                            authorLength: c,
                            clientTrim: u,
                            authorTrim: f
                        }
                    }("恭喜 " + t + " 成为", e + " 的" + g[n], t, e);
                return a.clientTrim && (o[1] = t.substring(0, 9) + "..."), a.authorTrim && (i[0] = e.substring(0, 9) + "..."),
                    function(e, t, n) {
                        var r = e.firstLine,
                            o = e.secondLine,
                            i = 248,
                            a = 47,
                            s = (i - r) / 2,
                            l = (i - o) / 2,
                            c = y,
                            u = (0, d.o_)(i, a, !1),
                            f = u.canvas,
                            p = u.ctx,
                            h = 0,
                            v = ["rgb(204, 204, 204)", "rgb(230, 188, 75)"];
                        p.font = "normal bold 16px PingFangSC-Regular";
                        for (var m = s, g = 0; g < t.length; g++) p.fillStyle = v[h], p.fillText(t[g], m, c), m += p.measureText(t[g]).width, h = ++h > 1 ? 0 : h;
                        for (var w = l, x = 0; x < n.length; x++) p.fillStyle = v[h], p.fillText(n[x], w, 2 * c + 5), w += p.measureText(n[x]).width, h = ++h > 1 ? 0 : h;
                        var b = p.getImageData(0, 0, i, a);
                        return (0, d.bX)(f), b
                    }(a, o, i)
            }
            var x = function(e, t, n, r) {
                    var o, i = arguments.length,
                        a = i < 3 ? t : null === r ? r = s()(t, n) : r;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, r);
                    else
                        for (var l = e.length - 1; l >= 0; l--)(o = e[l]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, n, a) : o(t, n)) || a);
                    return i > 3 && a && Object.defineProperty(t, n, a), a
                },
                b = function(e, t, n, r) {
                    return new(n || (n = c()))((function(o, i) {
                        function a(e) {
                            try {
                                l(r.next(e))
                            } catch (e) {
                                i(e)
                            }
                        }

                        function s(e) {
                            try {
                                l(r.throw(e))
                            } catch (e) {
                                i(e)
                            }
                        }

                        function l(e) {
                            var t;
                            e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n((function(e) {
                                e(t)
                            }))).then(a, s)
                        }
                        l((r = r.apply(e, t || [])).next())
                    }))
                },
                T = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).curTarget = null, t.re = null, t
                    }(0, r.Z)(t, e);
                    var n = t.prototype;
                    return n.generatorPlayId = function(e) {
                        var t = Date.now();
                        try {
                            return h()("" + e + t + m.e.getUid + m.e.getRoomId + "4")
                        } catch (e) {
                            return "" + t
                        }
                    }, n.playAnimation = function(e, t) {
                        return b(this, void 0, void 0, i().mark((function n() {
                            var r, o, a;
                            return i().wrap((function(n) {
                                for (;;) switch (n.prev = n.next) {
                                    case 0:
                                        if (r = null, o = m.e.getAnchorUserName(), !(null == e ? void 0 : e.json)) {
                                            n.next = 6;
                                            break
                                        }
                                        return n.next = 5, w(o, t.username, t.level, null == t ? void 0 : t.payType);
                                    case 5:
                                        r = n.sent;
                                    case 6:
                                        if (!e) {
                                            n.next = 18;
                                            break
                                        }
                                        return n.prev = 7, n.next = 10, this.playVideo(e, r);
                                    case 10:
                                        (a = n.sent) ? console.log("success"): (console.log("play mp4 res error", a), this.onClickClose()), n.next = 18;
                                        break;
                                    case 14:
                                        n.prev = 14, n.t0 = n.catch(7), console.log("play mp4 error", n.t0), this.onClickClose();
                                    case 18:
                                    case "end":
                                        return n.stop()
                                }
                            }), n, this, [
                                [7, 14]
                            ])
                        })))
                    }, n.showToast = function(e, t) {
                        var n = this.$refs.nailAnimToast;
                        return n && e && this.linkMsg(n, e, t)
                    }, n.playVideo = function(e, t) {
                        return b(this, void 0, void 0, i().mark((function n() {
                            var r, o, a = this;
                            return i().wrap((function(n) {
                                for (;;) switch (n.prev = n.next) {
                                    case 0:
                                        return n.prev = 0, console.log(e, "play video assets"), n.next = 4, (0, d.Bm)(e.json);
                                    case 4:
                                        r = n.sent, n.next = 12;
                                        break;
                                    case 7:
                                        return n.prev = 7, n.t0 = n.catch(0), this.showToast("动画资源解析错误 " + v.randomEmoji.sad() + " 尝试刷新或重新登录一下吧～ ", "info"), console.log("play video assets error", e, n.t0), n.abrupt("return", c().resolve(!1));
                                    case 12:
                                        return o = function(n) {
                                            try {
                                                (0, d.yn)({
                                                    container: a.$refs.mp4Container,
                                                    src: e.url,
                                                    config: r,
                                                    width: r.info.w / 2 || 360,
                                                    height: r.info.h / 2 || 640,
                                                    "live.guard.purchase.animation.own": t,
                                                    onLoadError: function(t) {
                                                        console.error("[mp4 error]: " + e.url), console.error("[mp4 error]: " + t), a.showToast("动画加载错误 " + v.randomEmoji.sad() + " 尝试刷新或重新登录一下吧～ ", "info"), null, n(!1), a.onClickClose()
                                                    },
                                                    onDestroy: function(e) {
                                                        e && e.isEnded && (null, n(!0), a.onClickClose())
                                                    }
                                                })
                                            } catch (e) {
                                                n(!1), a.onClickClose()
                                            }
                                        }, n.abrupt("return", new(c())(o));
                                    case 14:
                                    case "end":
                                        return n.stop()
                                }
                            }), n, this, [
                                [0, 7]
                            ])
                        })))
                    }, n.loadAnimationJson = function(e) {
                        var t;
                        return b(this, void 0, void 0, i().mark((function n() {
                            var r, o, a, s;
                            return i().wrap((function(n) {
                                for (;;) switch (n.prev = n.next) {
                                    case 0:
                                        r = m.e.getEffectItemById(null == e ? void 0 : e.effectId), o = {
                                            json: r && r.webMP4Json,
                                            url: r && r.webMP4
                                        }, a = m.e.getUserName(), s = m.e.getGuardLevel(), this.playAnimation(o, {
                                            username: a,
                                            level: s,
                                            payType: null !== (t = null == e ? void 0 : e.payType) && void 0 !== t ? t : 0
                                        });
                                    case 5:
                                    case "end":
                                        return n.stop()
                                }
                            }), n, this)
                        })))
                    }, n.mounted = function() {
                        console.log(this.optionData, "optionData"), this.loadAnimationJson(this.optionData)
                    }, t
                }(u.default);
            x([(0, f.Prop)({
                type: Object
            })], T.prototype, "optionData", void 0), x([(0, f.Prop)({
                type: Function
            })], T.prototype, "onClickClose", void 0);
            var C = T = x([f.Component], T),
                k = (0, n(51900).Z)(C, (function() {
                    var e = this.$createElement,
                        t = this._self._c || e;
                    return t("div", {
                        ref: "nailAnimToast"
                    }, [t("div", {
                        ref: "mp4Container",
                        staticClass: "p-absolute z-gift-screen-animation",
                        attrs: {
                            id: "gift-animation-cntr2"
                        }
                    })])
                }), [], !1, null, "144ac852", null).exports
        }
    }
]);