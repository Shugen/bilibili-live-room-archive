(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [1482], {
        91482: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, {
                loadCutOffMsg: function() {
                    return C
                }
            });
            var o = n(63109),
                c = n.n(o),
                r = n(93476),
                a = n.n(r),
                i = n(9669),
                s = n.n(i),
                u = n(55826),
                l = n(68913),
                f = n(3595),
                p = n(2708),
                d = n(66570),
                h = n(34074),
                m = n.n(h),
                v = n(95334),
                k = function(e, t, n, o) {
                    var c, r = arguments.length,
                        a = r < 3 ? t : null === o ? o = m()(t, n) : o;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, o);
                    else
                        for (var i = e.length - 1; i >= 0; i--)(c = e[i]) && (a = (r < 3 ? c(a) : r > 3 ? c(t, n, a) : c(t, n)) || a);
                    return r > 3 && a && Object.defineProperty(t, n, a), a
                },
                _ = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).checked = !1, t
                    }
                    return (0, d.Z)(t, e), t.prototype.onChecking = function() {
                        this.$emit("on-check", this.checked)
                    }, t
                }(v.Vue);
            k([(0, v.Prop)({
                type: String,
                default: "--"
            })], _.prototype, "reason", void 0);
            var b = _ = k([v.Component], _),
                g = (0, n(51900).Z)(b, (function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("div", {
                        staticClass: "cut-off-msg"
                    }, [n("form", {
                        attrs: {
                            name: "cut-off-msg-form"
                        }
                    }, [n("p", [n("span", [e._v("您的直播间因"), n("span", {
                        staticClass: "in-red"
                    }, [e._v("“" + e._s(e.reason) + "”")]), e._v("，已被管理员")]), n("span", {
                        staticClass: "in-red"
                    }, [e._v("“切断”")]), n("span", [e._v("，请更改直播内容。")])]), e._m(0), e._m(1), n("p", [n("label", [n("input", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: e.checked,
                            expression: "checked"
                        }],
                        staticClass: "v-middle",
                        staticStyle: {
                            "margin-right": "5px"
                        },
                        attrs: {
                            type: "checkbox"
                        },
                        domProps: {
                            checked: Array.isArray(e.checked) ? e._i(e.checked, null) > -1 : e.checked
                        },
                        on: {
                            change: [function(t) {
                                var n = e.checked,
                                    o = t.target,
                                    c = !!o.checked;
                                if (Array.isArray(n)) {
                                    var r = e._i(n, null);
                                    o.checked ? r < 0 && (e.checked = n.concat([null])) : r > -1 && (e.checked = n.slice(0, r).concat(n.slice(r + 1)))
                                } else e.checked = c
                            }, e.onChecking]
                        }
                    }), n("span", {
                        staticClass: "v-middle"
                    }, [e._v("我知道了")])])])])])
                }), [function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("p", [n("span", [e._v("如有疑问，请查看bilibili主播直播规范。")])])
                }, function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("p", [n("a", {
                        staticClass: "in-cyan",
                        attrs: {
                            href: "//link.bilibili.com/p/eden/news#/newsdetail?id=135",
                            target: "_blank"
                        }
                    }, [e._v("查看规范")])])
                }], !1, null, "e508c416", null).exports,
                y = function(e, t, n, o) {
                    return new(n || (n = a()))((function(c, r) {
                        function a(e) {
                            try {
                                s(o.next(e))
                            } catch (e) {
                                r(e)
                            }
                        }

                        function i(e) {
                            try {
                                s(o.throw(e))
                            } catch (e) {
                                r(e)
                            }
                        }

                        function s(e) {
                            var t;
                            e.done ? c(e.value) : (t = e.value, t instanceof n ? t : new n((function(e) {
                                e(t)
                            }))).then(a, i)
                        }
                        s((o = o.apply(e, t || [])).next())
                    }))
                };

            function C() {
                return y(this, void 0, void 0, c().mark((function e() {
                    var t, n, o, r;
                    return c().wrap((function(e) {
                        for (;;) switch (e.prev = e.next) {
                            case 0:
                                return t = f.h.getters.baseInfoRoom.roomID || window.BilibiliLive.ROOMID, e.prev = 1, e.next = 4, s().post("/room/v1/RoomEx/getCutReason", {
                                    room_id: t
                                });
                            case 4:
                                if (n = e.sent, o = n.data, r = o.data && o.data.cutMsg, 0 === o.code && r && !/直播结束|运营无理由切断/.test(r)) {
                                    e.next = 9;
                                    break
                                }
                                return e.abrupt("return");
                            case 9:
                                x(r), e.next = 16;
                                break;
                            case 12:
                                e.prev = 12, e.t0 = e.catch(1), (0, l.$S)("modules.admin/cut-off-msg")("切断消息获取失败", e.t0);
                            case 16:
                            case "end":
                                return e.stop()
                        }
                    }), e, null, [
                        [1, 12]
                    ])
                })))
            }

            function x(e) {
                var t = !1;
                (0, u.f)({
                    title: "友情提示 > <",
                    component: {
                        name: "cut-off-msg-popup",
                        data: function() {
                            return {
                                reason: e
                            }
                        },
                        methods: {
                            onCheck: function(e) {
                                t = e
                            }
                        },
                        components: {
                            InfoPopup: g
                        },
                        template: '<info-popup :reason="reason" @on-check="onCheck"></info-popup>'
                    },
                    button: {
                        confirm: "确定",
                        cancel: !1
                    }
                }).onConfirm((function(e) {
                    t && function() {
                        y(this, void 0, void 0, c().mark((function e() {
                            var t;
                            return c().wrap((function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        return t = f.h.getters.baseInfoRoom.roomID, e.prev = 1, e.next = 4, s().post("/room/v1/RoomEx/delCutReason", {
                                            room_id: t
                                        });
                                    case 4:
                                        e.next = 9;
                                        break;
                                    case 6:
                                        e.prev = 6, e.t0 = e.catch(1), console.error("[Error] 切断消息已读请求发送失败: ", e.t0);
                                    case 9:
                                    case "end":
                                        return e.stop()
                                }
                            }), e, null, [
                                [1, 6]
                            ])
                        })))
                    }(), e.close().catch(p.noop)
                }))
            }
        }
    }
]);