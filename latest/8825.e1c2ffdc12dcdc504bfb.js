(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [8825], {
        48825: function(t, n, e) {
            "use strict";
            e.d(n, {
                S: function() {
                    return x
                },
                T: function() {
                    return b
                }
            });
            var r = e(63109),
                a = e.n(r),
                c = e(93476),
                u = e.n(c),
                o = e(9669),
                i = e.n(o),
                s = e(73645),
                f = function(t, n, e, r) {
                    return new(e || (e = u()))((function(a, c) {
                        function u(t) {
                            try {
                                i(r.next(t))
                            } catch (t) {
                                c(t)
                            }
                        }

                        function o(t) {
                            try {
                                i(r.throw(t))
                            } catch (t) {
                                c(t)
                            }
                        }

                        function i(t) {
                            var n;
                            t.done ? a(t.value) : (n = t.value, n instanceof e ? n : new e((function(t) {
                                t(n)
                            }))).then(u, o)
                        }
                        i((r = r.apply(t, n || [])).next())
                    }))
                };

            function v(t, n) {
                return void 0 === n && (n = !1), f(this, void 0, void 0, a().mark((function e() {
                    var r, c;
                    return a().wrap((function(e) {
                        for (;;) switch (e.prev = e.next) {
                            case 0:
                                if (!n) {
                                    e.next = 2;
                                    break
                                }
                                return e.abrupt("return", {
                                    code: 0,
                                    data: s.y9.getInfoFromParam("lol_info"),
                                    msg: null
                                });
                            case 2:
                                return r = "/activity/v1/lol/getLolActivity?room_id=" + t, e.next = 5, i().get(r);
                            case 5:
                                return c = e.sent, e.abrupt("return", c.data);
                            case 7:
                            case "end":
                                return e.stop()
                        }
                    }), e)
                })))
            }
            var p = e(3595),
                h = e(68913),
                l = function(t, n, e, r) {
                    return new(e || (e = u()))((function(a, c) {
                        function u(t) {
                            try {
                                i(r.next(t))
                            } catch (t) {
                                c(t)
                            }
                        }

                        function o(t) {
                            try {
                                i(r.throw(t))
                            } catch (t) {
                                c(t)
                            }
                        }

                        function i(t) {
                            var n;
                            t.done ? a(t.value) : (n = t.value, n instanceof e ? n : new e((function(t) {
                                t(n)
                            }))).then(u, o)
                        }
                        i((r = r.apply(t, n || [])).next())
                    }))
                },
                d = (0, h.$S)("lpl-interact/service/async");

            function x() {
                return l(this, void 0, void 0, a().mark((function t() {
                    var n, r;
                    return a().wrap((function(t) {
                        for (;;) switch (t.prev = t.next) {
                            case 0:
                                return t.next = 2, b(!0);
                            case 2:
                                if (t.sent) {
                                    t.next = 5;
                                    break
                                }
                                return t.abrupt("return");
                            case 5:
                                return t.next = 7, e.e(7695).then(e.bind(e, 87695));
                            case 7:
                                return n = t.sent, r = n.refreshMatchInfo, t.next = 11, r();
                            case 11:
                            case "end":
                                return t.stop()
                        }
                    }), t)
                })))
            }

            function b(t) {
                return l(this, void 0, void 0, a().mark((function n() {
                    var e, r, c, u, o;
                    return a().wrap((function(n) {
                        for (;;) switch (n.prev = n.next) {
                            case 0:
                                return e = p.h.getters.baseInfoRoom.roomID, n.next = 3, v(e, t);
                            case 3:
                                if (0 === (r = n.sent).code && r.data) {
                                    n.next = 7;
                                    break
                                }
                                return d(r), n.abrupt("return");
                            case 7:
                                if (c = r.data, u = c.lol_activity) {
                                    n.next = 11;
                                    break
                                }
                                return n.abrupt("return");
                            case 11:
                                return o = !!u.status, n.next = 14, p.h.dispatch("setLPLEntranceLogo", {
                                    support: u.vote_cover || "",
                                    seckill: u.guess_cover || ""
                                });
                            case 14:
                                return n.next = 16, p.h.dispatch("setLPLInteractFlag", o);
                            case 16:
                                return n.abrupt("return", o);
                            case 17:
                            case "end":
                                return n.stop()
                        }
                    }), n)
                })))
            }
        }
    }
]);