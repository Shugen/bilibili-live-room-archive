(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [6260], {
        76260: function(e, t, a) {
            "use strict";
            a.r(t), a.d(t, {
                default: function() {
                    return d
                }
            });
            var r = a(2060),
                n = a(45651),
                s = a(26384),
                i = a(3649),
                c = a.n(i),
                o = function(e, t) {
                    void 0 === t && (t = 20);
                    var a = (0, r.iH)(e),
                        n = (0, r.Fl)((function() {
                            return Math.ceil(a.value.length / t)
                        })),
                        s = (0, r.iH)(1),
                        i = (0, r.Fl)((function() {
                            return l(s.value)
                        })),
                        o = function(e) {
                            return s.value = e
                        },
                        l = function(e) {
                            var r, n = (e - 1) * t - 1,
                                s = e * t;
                            return c()(r = a.value).call(r, n <= 0 ? 0 : n, s)
                        };
                    return {
                        pageTotal: n,
                        currentPage: s,
                        currentPageData: i,
                        setCurrentPage: o,
                        updateData: function(e) {
                            a.value = e, o(1)
                        },
                        getData: l
                    }
                },
                l = a(18706),
                u = a(58209),
                p = a(55716),
                v = (0, r.aZ)({
                    components: {
                        LinkInput: s.default,
                        ProgressTv: l.default
                    },
                    setup: function() {
                        var e = (0, r.f3)("popup"),
                            t = (0, r.iH)([]),
                            a = (0, n.ac)(),
                            s = a.recentChooseList,
                            i = a.parentAreaList,
                            c = a.curSubAreaList,
                            l = a.curSelectParentAreaIdx,
                            v = a.selectedParentArea,
                            d = a.searchCurrentSubArea,
                            g = a.submitModify,
                            h = (0, r.Fl)((function() {
                                return t.value.length > 0 ? t.value : c.value
                            })),
                            f = o(h.value, 20),
                            m = f.pageTotal,
                            C = f.currentPage,
                            b = f.setCurrentPage,
                            P = f.currentPageData,
                            k = f.updateData,
                            w = f.getData,
                            _ = (0, r.Fl)((function() {
                                for (var e = [], t = 0; t < m.value; t++) e.push(w(t + 1));
                                return e
                            })),
                            x = (0, r.iH)(""),
                            A = null;
                        return (0, r.YP)(h, k), (0, r.YP)(l, (function() {
                            A && clearTimeout(A), x.value = "", t.value = []
                        })), {
                            recentChooseList: s,
                            parentAreaList: i,
                            curSubAreaList: c,
                            curSelectParentAreaIdx: l,
                            selectedParentArea: v,
                            pages: _,
                            pageTotal: m,
                            currentPage: C,
                            setCurrentPage: b,
                            currentPageData: P,
                            keywords: x,
                            search: function() {
                                clearTimeout(A), A = setTimeout((function() {
                                    return t.value = d(x.value)
                                }), 100)
                            },
                            submit: function(t, a) {
                                g(t).then((function() {
                                    (0, u.Z)(a.target, "修改成功" + p.randomEmoji.happy(), "success"), e.close()
                                })).catch((function(e) {
                                    var t = e.errorMsg;
                                    (0, u.Z)(a.target, t, "error")
                                }))
                            }
                        }
                    }
                }),
                d = (0, a(51900).Z)(v, (function() {
                    var e = this,
                        t = e.$createElement,
                        a = e._self._c || t;
                    return a("div", {
                        staticClass: "edit-live-area"
                    }, [e.recentChooseList.length ? a("div", {
                        staticClass: "recent-selected-area-ctnr p-relative a-move-in-left"
                    }, [a("span", {
                        staticClass: "v-middle dp-i-block recent-label v-top"
                    }, [e._v("最近选择：")]), a("div", {
                        staticClass: "dp-i-block recent-items-ctnr v-top"
                    }, e._l(e.recentChooseList, (function(t) {
                        return a("span", {
                            staticClass: "recent-selected-item dp-i-block p-relative pointer",
                            on: {
                                click: function(a) {
                                    return e.submit(t, a)
                                }
                            }
                        }, [e._v(e._s(t.parentName + " · " + t.name) + " "), t.activityFlag ? a("span", {
                            staticClass: "svg-icon hot1 hot dp-block p-absolute"
                        }) : e._e()])
                    })), 0)]) : e._e(), a("div", {
                        staticClass: "area-category-selector"
                    }, e._l(e.parentAreaList, (function(t, r) {
                        return a("div", {
                            staticClass: "area-category-item dp-i-block p-relative t-center pointer",
                            class: {
                                active: e.curSelectParentAreaIdx === r
                            },
                            style: {
                                padding: "0 " + (e.parentAreaList.length < 9 ? 10 : 6) + "px"
                            },
                            on: {
                                click: function(t) {
                                    return e.selectedParentArea(r)
                                }
                            }
                        }, [e._v(e._s(t.name)), 0 !== r ? a("div", {
                            staticClass: "category-divider p-absolute"
                        }) : e._e()])
                    })), 0), a("div", {
                        staticClass: "p-relative"
                    }, [a("link-input", {
                        staticClass: "search-bar dp-block border-box",
                        attrs: {
                            placeholder: "输入拼音首字母或全称，快速搜索",
                            width: 408
                        },
                        on: {
                            input: e.search
                        },
                        model: {
                            value: e.keywords,
                            callback: function(t) {
                                e.keywords = t
                            },
                            expression: "keywords"
                        }
                    }), a("i", {
                        staticClass: "icon-font icon-search p-absolute"
                    })], 1), a("div", {
                        staticClass: "area-tags-ctnr p-relative over-hidden"
                    }, [a("p", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: 0 === e.pages.length,
                            expression: "pages.length === 0"
                        }],
                        staticClass: "p-absolute empty-ctnr"
                    }, [e._v("这里空空如也")]), a("progress-tv", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: 0 === e.parentAreaList.length,
                            expression: "parentAreaList.length === 0"
                        }],
                        staticClass: "progress p-relative"
                    }), e.pages.length > 0 ? a("div", {
                        staticClass: "tags-scroll-ctnr ts-dot-4 border-box",
                        style: {
                            width: 420 * e.pages.length + "px",
                            transform: "translateX(-" + 420 * (e.currentPage - 1) + "px)"
                        }
                    }, e._l(e.pages, (function(t) {
                        return a("div", {
                            staticClass: "tags-inner-ctnr dp-i-block v-top border-box"
                        }, e._l(t, (function(t) {
                            return a("div", {
                                staticClass: "area-tag-item dp-i-block border-box none-select pointer",
                                domProps: {
                                    textContent: e._s(t.name)
                                },
                                on: {
                                    click: function(a) {
                                        return e.submit(t, a)
                                    }
                                }
                            })
                        })), 0)
                    })), 0) : e._e()], 1), a("div", {
                        staticClass: "t-right page-num v-middle no-select"
                    }, [a("i", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: e.currentPage > 1,
                            expression: "currentPage > 1"
                        }],
                        staticClass: "icon-font icon-arrow-left v-middle pointer",
                        on: {
                            click: function(t) {
                                return e.setCurrentPage(e.currentPage - 1)
                            }
                        }
                    }), a("span", {
                        staticClass: "v-middle"
                    }, [e._v(e._s(e.currentPage) + "/" + e._s(e.pageTotal))]), a("i", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: e.currentPage < e.pageTotal,
                            expression: "currentPage < pageTotal"
                        }],
                        staticClass: "icon-font icon-arrow-right v-middle pointer",
                        on: {
                            click: function(t) {
                                return e.setCurrentPage(e.currentPage + 1)
                            }
                        }
                    })])])
                }), [], !1, null, "4a3d62a4", null).exports
        }
    }
]);