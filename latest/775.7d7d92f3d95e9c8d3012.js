(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [775], {
        80775: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, {
                LPLSeckillPanelComponent: function() {
                    return g
                }
            });
            var i = n(26849),
                s = n(66570),
                o = n(63109),
                l = n.n(o),
                a = n(34074),
                c = n.n(a),
                r = n(93476),
                p = n.n(r),
                d = n(70538),
                u = n(95334),
                f = n(63990),
                h = n(55716),
                v = function(e, t, n, i) {
                    var s, o = arguments.length,
                        l = o < 3 ? t : null === i ? i = c()(t, n) : i;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) l = Reflect.decorate(e, t, n, i);
                    else
                        for (var a = e.length - 1; a >= 0; a--)(s = e[a]) && (l = (o < 3 ? s(l) : o > 3 ? s(t, n, l) : s(t, n)) || l);
                    return o > 3 && l && Object.defineProperty(t, n, l), l
                },
                m = function(e, t, n, i) {
                    return new(n || (n = p()))((function(s, o) {
                        function l(e) {
                            try {
                                c(i.next(e))
                            } catch (e) {
                                o(e)
                            }
                        }

                        function a(e) {
                            try {
                                c(i.throw(e))
                            } catch (e) {
                                o(e)
                            }
                        }

                        function c(e) {
                            var t;
                            e.done ? s(e.value) : (t = e.value, t instanceof n ? t : new n((function(e) {
                                e(t)
                            }))).then(l, a)
                        }
                        c((i = i.apply(e, t || [])).next())
                    }))
                },
                k = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).selectedTeam = 0, t.isPanelLocked = !1, t
                    }(0, s.Z)(t, e);
                    var o = t.prototype;
                    return o.hidePanel = function() {
                        this.isPanelShow = !1
                    }, o.selectTeam = function(e, t) {
                        if (this.isPanelLocked) return this.linkMsg(t.target, "操作进行中，请稍候", "info");
                        this.selectedTeam = this.selectedTeam === e ? 0 : e
                    }, o.resetInput = function() {
                        this.selectedTeam = 0
                    }, o.doSeckill = function(e) {
                        return m(this, void 0, void 0, l().mark((function t() {
                            var i, s, o, a = this;
                            return l().wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if (!this.isPanelLocked) {
                                            t.next = 2;
                                            break
                                        }
                                        return t.abrupt("return", this.linkMsg(this.$refs.tipPos || e.target, "骚年你的手速太快啦", "info"));
                                    case 2:
                                        if (this.selectedTeam) {
                                            t.next = 4;
                                            break
                                        }
                                        return t.abrupt("return", this.linkMsg(this.$refs.tipPos || e.target, "请选择你要支持的队伍", "info"));
                                    case 4:
                                        return this.isPanelLocked = !0, t.prev = 5, t.next = 8, n.e(7695).then(n.bind(n, 87695));
                                    case 8:
                                        return i = t.sent, s = i.doSeckill, t.next = 12, s(this.selectedTeam);
                                    case 12:
                                        0 === (o = t.sent).code ? this.linkPopup({
                                            title: "助力成功",
                                            html: '<p style="margin-bottom:32px;line-height:20px;font-size:13px;color:#222" class="t-center">' + o.msg + "</p>",
                                            width: 344,
                                            button: {
                                                confirm: "我知道了",
                                                cancel: !1
                                            }
                                        }).onConfirm((function(e) {
                                            return m(a, void 0, void 0, l().mark((function t() {
                                                return l().wrap((function(t) {
                                                    for (;;) switch (t.prev = t.next) {
                                                        case 0:
                                                            return t.next = 2, e.close();
                                                        case 2:
                                                            this.isPanelLocked = !1;
                                                        case 3:
                                                        case "end":
                                                            return t.stop()
                                                    }
                                                }), t, this)
                                            })))
                                        })).onCancel((function(e) {
                                            return m(a, void 0, void 0, l().mark((function t() {
                                                return l().wrap((function(t) {
                                                    for (;;) switch (t.prev = t.next) {
                                                        case 0:
                                                            return t.next = 2, e.close();
                                                        case 2:
                                                            this.isPanelLocked = !1;
                                                        case 3:
                                                        case "end":
                                                            return t.stop()
                                                    }
                                                }), t, this)
                                            })))
                                        })) : (o.hideTips || this.linkMsg(this.$refs.tipPos || e.target, o.msg || o.message || o.errorMsg || "出错了，请稍候再试", o.errorType || "info"), this.isPanelLocked = !1), t.next = 20;
                                        break;
                                    case 16:
                                        t.prev = 16, t.t0 = t.catch(5), this.isPanelLocked = !1, this.linkMsg(this.$refs.tipPos || e.target, t.t0.message || "出错了，请稍候再试", "error");
                                    case 20:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, this, [
                                [5, 16]
                            ])
                        })))
                    }, (0, i.Z)(t, [{
                        key: "isPanelShow",
                        get: function() {
                            return !!this.$store.getters.LPL.panelStatus.seckill
                        },
                        set: function(e) {
                            !e && this.isPanelLocked || this.$store.dispatch("setLPLPanelStatus", {
                                name: "seckill",
                                status: !!e
                            }).catch((function() {
                                return null
                            }))
                        }
                    }, {
                        key: "matchInfo",
                        get: function() {
                            return this.$store.getters.LPL.matchInfo
                        }
                    }, {
                        key: "seckillInfo",
                        get: function() {
                            return this.$store.getters.LPL.seckillInfo
                        }
                    }, {
                        key: "seckillPrice",
                        get: function() {
                            return this.seckillInfo.price ? h.numberFormat.autoParse(this.seckillInfo.price) : "0"
                        }
                    }, {
                        key: "isInProcess",
                        get: function() {
                            return !!this.seckillInfo && 1 === this.seckillInfo.status
                        }
                    }, {
                        key: "countdownDisplay",
                        get: function() {
                            return this.$store.getters.LPL.countdown.displaySeconds
                        }
                    }]), t
                }(d.default),
                P = k = v([(0, u.Component)({
                    components: {
                        PanelWrapper: f.k
                    }
                })], k),
                g = (0, n(51900).Z)(P, (function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("panel-wrapper", {
                        attrs: {
                            "is-panel-show": e.isPanelShow,
                            "close-lock": e.isPanelLocked
                        },
                        on: {
                            "update:isPanelShow": function(t) {
                                e.isPanelShow = t
                            },
                            "update:is-panel-show": function(t) {
                                e.isPanelShow = t
                            }
                        }
                    }, [e.matchInfo.id && e.seckillInfo.id ? n("div", {
                        staticClass: "panel-ctnr m-auto t-center"
                    }, [n("div", {
                        staticClass: "project-logo dp-i-block v-middle bg-cover bg-no-repeat bg-center",
                        style: {
                            "background-image": e.seckillInfo.logoUrl ? "url(" + e.seckillInfo.logoUrl + ")" : ""
                        }
                    }), n("div", {
                        staticClass: "project-info dp-i-block v-middle t-left"
                    }, [n("p", {
                        staticClass: "project-line-1"
                    }, [n("span", {
                        staticClass: "project-title dp-i-block",
                        domProps: {
                            textContent: e._s("高光时刻 · " + e.seckillInfo.name)
                        }
                    }), n("span", {
                        staticClass: "project-time-left dp-i-block",
                        domProps: {
                            textContent: e._s(e.isInProcess ? "剩余 " + e.countdownDisplay + " s" : " ")
                        }
                    })]), n("p", {
                        staticClass: "project-price"
                    }, [n("span", {
                        staticClass: "price-desc dp-i-block v-middle"
                    }, [e._v("秒杀价格")]), n("span", {
                        staticClass: "price-text dp-i-block v-middle"
                    }, [e._v(e._s(e.seckillPrice) + " 金瓜子")])])]), n("div", {
                        staticClass: "team-select dp-i-block v-middle"
                    }, e._l(e.matchInfo.teams, (function(t) {
                        return n("div", {
                            staticClass: "team-item dp-i-block v-middle p-relative border-box pointer ts-dot-2",
                            class: {
                                active: !!e.selectedTeam && e.selectedTeam === t.id
                            },
                            on: {
                                click: function(n) {
                                    t.id && e.selectTeam(t.id, n)
                                }
                            }
                        }, [n("div", {
                            staticClass: "btn-item-vertical-wrapper p-absolute w-100"
                        }, [n("div", {
                            staticClass: "team-logo dp-i-block v-middle bg-contain bg-no-repeat bg-center",
                            style: {
                                "background-image": t.logoUrl ? "url(" + t.logoUrl + ")" : ""
                            }
                        }), n("div", {
                            staticClass: "team-name dp-i-block v-middle",
                            domProps: {
                                textContent: e._s(t.name)
                            }
                        })])])
                    })), 0), n("div", {
                        ref: "tipPos",
                        staticClass: "pay-button dp-i-block v-middle pointer p-relative",
                        class: {
                            disabled: !e.isInProcess || 0 !== e.seckillInfo.choice[e.selectedTeam]
                        },
                        on: {
                            click: function(t) {
                                e.isInProcess && e.doSeckill(t)
                            }
                        }
                    }, [n("div", {
                        staticClass: "btn-item-vertical-wrapper p-absolute w-100"
                    }, [e.isInProcess ? [n("div", {
                        staticClass: "pay-logo dp-i-block v-middle bg-contain bg-center bg-no-repeat"
                    }), n("div", {
                        staticClass: "pay-text dp-i-block v-middle"
                    }, [e._v("高光时刻秒杀")])] : n("div", {
                        staticClass: "pay-countdown-text t-center",
                        domProps: {
                            textContent: e._s(e.countdownDisplay + "s 后开启")
                        }
                    })], 2)])]) : n("div", {
                        staticClass: "empty-ctnr"
                    }, [n("p", {
                        staticClass: "empty-text t-center"
                    }, [e._v("当前没有正在秒杀的高光时刻")]), n("p", {
                        staticClass: "empty-detail t-left"
                    }, [e._v("关于高光时刻：在一血、一塔、大小龙、多杀等情况下发生下，将选取助力的粉丝在全平台直播窗内展示其昵称；目前高光时刻通过参与过选手投票的用户中随机抽取一名作为展示。")])])])
                }), [], !1, null, "e8f4458e", null).exports
        },
        63990: function(e, t, n) {
            "use strict";
            n.d(t, {
                k: function() {
                    return u
                }
            });
            var i = n(66570),
                s = n(34074),
                o = n.n(s),
                l = n(70538),
                a = n(95334),
                c = n(57072),
                r = function(e, t, n, i) {
                    var s, l = arguments.length,
                        a = l < 3 ? t : null === i ? i = o()(t, n) : i;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, n, i);
                    else
                        for (var c = e.length - 1; c >= 0; c--)(s = e[c]) && (a = (l < 3 ? s(a) : l > 3 ? s(t, n, a) : s(t, n)) || a);
                    return l > 3 && a && Object.defineProperty(t, n, a), a
                },
                p = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).instanceId = "lpl-panel-" + Date.now() * Math.random(), t.eventTimer = null, t.isPanelShow = !1, t.closeLock = !1, t
                    }(0, i.Z)(t, e);
                    var n = t.prototype;
                    return n.hidePanel = function() {
                        this.$emit("update:isPanelShow", !1), c.default.$emit("panelUnderPlayerUpdate:isPanelShow", !1)
                    }, n.globalClickHandler = function(e) {
                        e["target-lpl-panel"] === this.instanceId || this.closeLock || this.hidePanel()
                    }, n.localClickHandler = function(e) {
                        e["target-lpl-panel"] = this.instanceId
                    }, n.onWatchIsPanelShow = function(e, t) {
                        var n = this;
                        e !== t && (e ? this.eventTimer = setTimeout((function() {
                            return window.addEventListener("click", n.globalClickHandler)
                        }), 100) : (clearTimeout(this.eventTimer), window.removeEventListener("click", this.globalClickHandler)))
                    }, t
                }(l.default);
            r([(0, a.Prop)({
                type: Boolean,
                default: !1
            })], p.prototype, "isPanelShow", void 0), r([(0, a.Prop)({
                type: Boolean,
                default: !1
            })], p.prototype, "closeLock", void 0), r([(0, a.Watch)("isPanelShow")], p.prototype, "onWatchIsPanelShow", null);
            var d = p = r([(0, a.Component)({})], p),
                u = (0, n(51900).Z)(d, (function() {
                    var e = this,
                        t = e.$createElement,
                        n = e._self._c || t;
                    return n("div", {
                        staticClass: "panel-wrapper p-absolute over-hidden",
                        on: {
                            click: e.localClickHandler
                        }
                    }, [n("transition", {
                        attrs: {
                            "leave-active-class": "a-move-out-bottom",
                            "enter-active-class": "a-move-in-top"
                        }
                    }, [e.isPanelShow ? n("div", {
                        staticClass: "popup-ctnr"
                    }, [n("i", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: e.isPanelShow,
                            expression: "isPanelShow"
                        }],
                        staticClass: "icon-font icon-close p-absolute ts-dot-4 pointer",
                        on: {
                            click: e.hidePanel
                        }
                    }), e._t("default")], 2) : e._e()])], 1)
                }), [], !1, null, "fccbc03a", null).exports
        }
    }
]);