(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [1602], {
        21602: function(e, t, r) {
            "use strict";
            r.r(t), r.d(t, {
                default: function() {
                    return P
                }
            });
            var n = r(26849),
                o = r(66570),
                i = r(63109),
                a = r.n(i),
                s = r(34074),
                c = r.n(s),
                u = r(93476),
                l = r.n(u),
                h = r(2991),
                f = r.n(h),
                m = r(78580),
                d = r.n(m),
                y = r(86902),
                v = r.n(y),
                g = r(50630),
                w = r(95334),
                p = r(72807),
                I = r(88066),
                S = r(23933),
                A = r(3595),
                L = r(82159),
                b = r(27681),
                k = r(2708),
                T = r(18980),
                F = r(75225),
                G = function(e, t, r, n) {
                    var o, i = arguments.length,
                        a = i < 3 ? t : null === n ? n = c()(t, r) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, r, n);
                    else
                        for (var s = e.length - 1; s >= 0; s--)(o = e[s]) && (a = (i < 3 ? o(a) : i > 3 ? o(t, r, a) : o(t, r)) || a);
                    return i > 3 && a && Object.defineProperty(t, r, a), a
                },
                _ = function(e, t, r, n) {
                    return new(r || (r = l()))((function(o, i) {
                        function a(e) {
                            try {
                                c(n.next(e))
                            } catch (e) {
                                i(e)
                            }
                        }

                        function s(e) {
                            try {
                                c(n.throw(e))
                            } catch (e) {
                                i(e)
                            }
                        }

                        function c(e) {
                            var t;
                            e.done ? o(e.value) : (t = e.value, t instanceof r ? t : new r((function(e) {
                                e(t)
                            }))).then(a, s)
                        }
                        c((n = n.apply(e, t || [])).next())
                    }))
                },
                x = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).AnchorLotteryService = p.lY, t.isShowGuestIframe = !1, t.guestFrame = null, t.iframeTimer = null, t.isPicShake = !1, t.joinSucceed = !1, t
                    }(0, o.Z)(t, e);
                    var r = t.prototype;
                    return r.formatTime = function(e) {
                        return e >= 10 ? "" + e : "0" + e
                    }, r.showGuestPanel = function() {
                        var e = this;
                        this.isShowGuestIframe || (this.isShowGuestIframe = !0, document.body.style.overflow = "hidden", this.$nextTick((function() {
                            e.genFrameGear()
                        })))
                    }, r.genFrameGear = function() {
                        return _(this, void 0, void 0, a().mark((function e() {
                            var t, r = this;
                            return a().wrap((function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        return e.next = 2, this.checkIframe();
                                    case 2:
                                        this.guestFrame && this.guestFrame.dispose && this.guestFrame.dispose(), this.setCacheData(), document.querySelector("#anchor-guest-box-id") && document.body.removeChild(document.querySelector("#anchor-guest-box-id")), (t = document.createElement("div")).setAttribute("id", "anchor-guest-box-id"), t.style.cssText = "z-index: 10000; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%)", document.body.appendChild(t), this.guestFrame = g.o.createChildFrame(this.frameUrl, {
                                            parentElement: document.querySelector("#anchor-guest-box-id"),
                                            attrs: {
                                                width: "500px",
                                                height: "420px",
                                                frameBorder: "0"
                                            },
                                            initialEvents: {
                                                removeAnchorLottery: function() {
                                                    document.body.removeChild(document.querySelector("#anchor-guest-box-id")), r.clearGuestFrame(), r.AnchorLotteryService.init()
                                                },
                                                closeGuestPanel: function() {
                                                    document.body.removeChild(document.querySelector("#anchor-guest-box-id")), r.clearGuestFrame()
                                                },
                                                anchorLotToLogin: function() {
                                                    (0, L.quickLogin)()
                                                },
                                                anchorLotRefreshPrice: function(e) {
                                                    (0, b.Y7)({
                                                        silverSeed: e.silver,
                                                        goldSeed: e.gold
                                                    })
                                                },
                                                anchorLotFollow: function() {
                                                    if (!A.h.state.baseInfoAnchor.isAttention) {
                                                        var e = A.h.getters.baseInfoAnchor.fansCount + 1;
                                                        A.h.dispatch("baseInfoAnchor", {
                                                            isAttention: !0,
                                                            fansCount: e
                                                        })
                                                    }
                                                }
                                            }
                                        }), this.guestFrame.eventPort.emit("IFRAME:ANCHOR_LOTTERY_USERINFO", {
                                            userLevel: A.h.getters.baseInfoUser.level,
                                            guardLevel: A.h.getters.baseInfoUser.privilege.currentTopGuardLevel || 0,
                                            isFollowing: !!A.h.getters.baseInfoAnchor.isAttention
                                        }), this.guestFrame.eventPort.on("lottery:joinSucceed", (function() {
                                            r.joinSucceed = !0
                                        }));
                                    case 12:
                                    case "end":
                                        return e.stop()
                                }
                            }), e, this)
                        })))
                    }, r.setCacheData = function() {
                        p.lY.setCacheData("WALLET", {
                            gold: b.gU.goldSeed,
                            silver: b.gU.silverSeed
                        });
                        var e = A.h.getters["gift/giftPresets"],
                            t = [];
                        Array.isArray(e) && (t = f()(e).call(e, (function(e) {
                            return {
                                gift_id: e.id,
                                discount_price: e.discountPrice
                            }
                        }))), p.lY.setCacheData("GIFT_DISCOUNT_INFO", t)
                    }, r.checkIframe = function() {
                        var e = this;
                        return this.iframeTimer && clearInterval(this.iframeTimer), new(l())((function(t) {
                            e.$refs.iframeGuestLottery ? t() : e.iframeTimer = setInterval((function() {
                                e.$refs.iframeGuestLottery && (t(), clearInterval(e.iframeTimer))
                            }), 1e3)
                        }))
                    }, r.clearGuestFrame = function() {
                        this.isShowGuestIframe = !1, document.body.style.overflow = ""
                    }, r.eventListeners = function() {
                        var e = this;
                        I.Z.on(I.c.anchorLotteryStart, (function(t) {
                            e.clearGuestFrame(), t.startNow || e.playAnimation(), t.startNow && 0 === t.isDisableAutoShowPanel && e.showGuestPanel()
                        })), I.Z.on(I.c.anchorLotteryEnd, (function() {
                            e.isPicShake = !1, e.guestFrame && e.guestFrame.eventPort.emit("IFRAME:ANCHOR_LOTTERY_LOADING")
                        })), I.Z.on(I.c.anchorLotteryGuestAward, (function(t) {
                            var r;
                            if (e.playAnimation(), e.guestFrame && e.isShowGuestIframe) e.guestFrame.eventPort.emit("IFRAME:ANCHOR_LOTTERY_AWARD", t);
                            else {
                                var n = f()(r = t.award_users).call(r, (function(e) {
                                    return e.uid
                                }));
                                (1 !== t.award_dont_popup || d()(n).call(n, S.e.getUid())) && e.joinSucceed && (e.showGuestPanel(), e.joinSucceed = !1)
                            }
                        }))
                    }, r.playAnimation = function() {
                        var e = this;
                        this.isPicShake = !0, setTimeout((function() {
                            e.isPicShake = !1
                        }), 1e3)
                    }, r.report = function(e, t) {
                        T.default.report(e, t, (0, F.D)())
                    }, r.mounted = function() {
                        this.eventListeners(), this.AnchorLotteryService.init()
                    }, (0, n.Z)(t, [{
                        key: "lotteryInfo",
                        get: function() {
                            return this.AnchorLotteryService.lottery || {}
                        }
                    }, {
                        key: "lotteryId",
                        get: function() {
                            return this.lotteryInfo.lotteryId
                        }
                    }, {
                        key: "goawayTime",
                        get: function() {
                            return this.lotteryInfo.goawayTime
                        }
                    }, {
                        key: "frameUrl",
                        get: function() {
                            var e, t, r = {
                                    roomId: S.e.getRoomId(),
                                    uid: S.e.getUid(),
                                    anchorId: S.e.getAnchorUid(),
                                    from: "web",
                                    liteVersion: (0, k.getLiteVersionStatus)() ? "1" : "0"
                                },
                                n = f()(e = v()(r)).call(e, (function(e) {
                                    return e + "=" + r[e]
                                })).join("&");
                            return this.lotteryInfo.webUrl ? (t = this.lotteryInfo.webUrl, "https:" !== window.location.protocol && (t = t.replace("https://", window.location.protocol + "//")), t + "?" + n) : "//live.bilibili.com/p/html/live-lottery/anchor-join.html?" + n
                        }
                    }, {
                        key: "guestText",
                        get: function() {
                            switch (this.lotteryInfo.lotStatus) {
                                case 0:
                                    return this.time;
                                case 1:
                                case 2:
                                    return "已开奖";
                                default:
                                    return ""
                            }
                        }
                    }, {
                        key: "time",
                        get: function() {
                            var e = this.lotteryInfo.time;
                            return e > 0 ? this.formatTime(Math.floor(e / 60)) + ":" + this.formatTime(e % 60) : ""
                        }
                    }, {
                        key: "showAnchorLotteryEntry",
                        get: function() {
                            var e = this.lotteryInfo,
                                t = e.lotStatus,
                                r = e.goawayTime;
                            return this.lotteryId && (0 === t || (1 === t || 2 === t) && r > 0)
                        }
                    }]), t
                }(w.Vue),
                C = x = G([(0, w.Component)({
                    components: {
                        AnchorLotEntry: function() {
                            return r.e(8849).then(r.bind(r, 58849)).then((function(e) {
                                return e.default
                            }))
                        }
                    }
                })], x),
                P = (0, r(51900).Z)(C, (function() {
                    var e = this,
                        t = e.$createElement,
                        r = e._self._c || t;
                    return e.showAnchorLotteryEntry ? r("anchor-lot-entry", {
                        directives: [{
                            name: "click-report",
                            rawName: "v-click-report",
                            value: {
                                eventName: "_anchordraw_icon_click"
                            },
                            expression: "{eventName: '_anchordraw_icon_click'}"
                        }],
                        attrs: {
                            text: e.guestText,
                            iframeShow: e.isShowGuestIframe,
                            picShake: e.isPicShake
                        },
                        on: {
                            click: e.showGuestPanel
                        }
                    }, [r("div", {
                        directives: [{
                            name: "show",
                            rawName: "v-show",
                            value: e.isShowGuestIframe,
                            expression: "isShowGuestIframe"
                        }],
                        ref: "iframeGuestLottery",
                        staticClass: "anchor-lottery-iframe"
                    })]) : e._e()
                }), [], !1, null, null, null).exports
        }
    }
]);