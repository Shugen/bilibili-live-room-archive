(self.webpackChunklive_room = self.webpackChunklive_room || []).push([
    [2338], {
        52338: function(e, t, a) {
            "use strict";
            a.r(t), a.d(t, {
                default: function() {
                    return y
                }
            });
            var n = a(66570),
                r = a(63109),
                o = a.n(r),
                i = a(34074),
                s = a.n(i),
                c = a(93476),
                u = a.n(c),
                f = a(70538),
                l = a(26452),
                p = a(56056),
                d = a(26384),
                m = a(90801),
                v = function(e, t, a, n) {
                    var r, o = arguments.length,
                        i = o < 3 ? t : null === n ? n = s()(t, a) : n;
                    if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) i = Reflect.decorate(e, t, a, n);
                    else
                        for (var c = e.length - 1; c >= 0; c--)(r = e[c]) && (i = (o < 3 ? r(i) : o > 3 ? r(t, a, i) : r(t, a)) || i);
                    return o > 3 && i && Object.defineProperty(t, a, i), i
                },
                k = function(e, t, a, n) {
                    return new(a || (a = u()))((function(r, o) {
                        function i(e) {
                            try {
                                c(n.next(e))
                            } catch (e) {
                                o(e)
                            }
                        }

                        function s(e) {
                            try {
                                c(n.throw(e))
                            } catch (e) {
                                o(e)
                            }
                        }

                        function c(e) {
                            var t;
                            e.done ? r(e.value) : (t = e.value, t instanceof a ? t : new a((function(e) {
                                e(t)
                            }))).then(i, s)
                        }
                        c((n = n.apply(e, t || [])).next())
                    }))
                },
                h = function(e) {
                    function t() {
                        var t;
                        return (t = e.apply(this, arguments) || this).feedback = [], t
                    }(0, n.Z)(t, e);
                    var a = t.prototype;
                    return a.getConfig = function() {
                        return k(this, void 0, void 0, o().mark((function e() {
                            var t;
                            return o().wrap((function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        return e.next = 2, m.Z.get("live_room_feedback_pc");
                                    case 2:
                                        (t = e.sent) ? (this.feedback = JSON.parse(t).feedback.split(","), this.feedback.push("天选时刻/红包抽奖")) : this.feedback = [];
                                    case 4:
                                    case "end":
                                        return e.stop()
                                }
                            }), e, this)
                        })))
                    }, a.mounted = function() {
                        return k(this, void 0, void 0, o().mark((function e() {
                            return o().wrap((function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        return e.next = 2, this.getConfig();
                                    case 2:
                                    case "end":
                                        return e.stop()
                                }
                            }), e, this)
                        })))
                    }, t
                }(f.default),
                b = h = v([(0, l.ZP)({
                    props: {
                        formData: {
                            type: Object,
                            default: {}
                        }
                    },
                    components: {
                        LinkRadioButton: p.Z,
                        LinkInput: d.default
                    }
                })], h),
                y = (0, a(51900).Z)(b, (function() {
                    var e = this,
                        t = e.$createElement,
                        a = e._self._c || t;
                    return a("div", {
                        staticClass: "feedback-form"
                    }, [a("p", {
                        staticClass: "tips"
                    }, [e._v("你在当前直播间遇到的问题是？(必选)")]), a("div", {
                        staticClass: "form-item"
                    }, [a("span", {
                        staticClass: "label required"
                    }, [e._v("问题类型：")]), e._l(e.feedback, (function(t) {
                        return a("link-radio-button", {
                            key: t,
                            attrs: {
                                value: t
                            },
                            model: {
                                value: e.formData.type,
                                callback: function(t) {
                                    e.$set(e.formData, "type", t)
                                },
                                expression: "formData.type"
                            }
                        }, [e._v(e._s(t))])
                    }))], 2), a("div", {
                        staticClass: "form-item-remark"
                    }, [a("p", {
                        staticClass: "label"
                    }, [e._v("补充说明：")]), a("textarea", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: e.formData.remark,
                            expression: "formData.remark"
                        }],
                        staticClass: "remark",
                        attrs: {
                            placeholder: "请详细描述你遇到的观看体验问题"
                        },
                        domProps: {
                            value: e.formData.remark
                        },
                        on: {
                            input: function(t) {
                                t.target.composing || e.$set(e.formData, "remark", t.target.value)
                            }
                        }
                    })])])
                }), [], !1, null, "d2a20a62", null).exports
        }
    }
]);